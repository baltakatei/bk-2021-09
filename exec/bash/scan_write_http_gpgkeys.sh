#!/usr/bin/env bash
# Desc: Scans HTTP URL targets for gpg fingerprints and Writes gpg pubkeys to minimal armored files
# Ref/Attrib: [1] Get gpg key fingerprint via machine-readable format ( https://git.gnupg.org/cgi-bin/gitweb.cgi?p=gnupg.git;a=blob;f=doc/DETAILS;h=7933863ab80890ff68da91f676de04b03d90dd69;hb=1c841c8389fb9640762822395b988e0d1584c9ae ).

yell() { echo "$0: $*" >&2; } # print script path and all args to stderr
die() { yell "$*"; exit 111; } # same as yell() but non-zero exit status
try() { "$@" || die "cannot $*"; } # runs args as command, reports args if command fails
list_fileurls_pgpfp() {
    # Desc: Scans HTTP URLs listed in file for pgp fingerprints
    # Input: arg1: input FILE
    # Output: stdout: newline-delimited list of fingerprints

    local file_in pattern;
    local input filter1result filter2result filter3result;
    local buffer candidates;
    yell "DEBUG:start list_fileurls_pgpfp()"
    # Check if arg1 is file
    if [[ ! -f $1 ]]; then die "ERROR:Not a file:$1"; else
	file_in="$1"; fi;

    # Process urls in $file_in
    while read -r url; do
	## Check if $url is URL
	pattern="^(https?)+://.*$"
	if [[ ! "$url" =~ $pattern ]]; then
	    yell "ERROR:Probably not an HTTP URL:$1"; continue; else
	    url_in="$url";
	    yell "DEBUG:url_in:$url_in";
	fi;
	input="$(wget -qO - "$url_in")";
	#yell "DEBUG:input:$input";

	## Filter $input
	# filter 0
	# filter 0: replace tabs and newlines spaces; squeeze spaces
	filter0result="$(echo "$input" | expand -t 4 | tr '\n' ' ' | tr -s ' ')";
	#yell "DEBUG:filter0result:$filter0result;"

	# filter 1
	# filter1:find things that look like fingerprints framed between one non-hexadecimal char on either end
	# Note: designed to look for things like: |3457 A265 922A 1F38 39DB  0264 A0A2 95AB DC34 69C9>
	#                                         xA0A2 95AB DC34 69C9>
	#                                         |A0A295ABDC3469C9>
	filter1result="$(echo "$filter0result" | grep -Eo "([^0-9A-F]{1})([0-9A-F \\]){16,53}([^0-9A-F]{1})" )";
	#yell "DEBUG:filter1result:$filter1result";

	# filter 2
	# filter2:strip off char on either end, remove spaces and backslashes
	filter2result="$(echo "$filter1result" | sed 's/^.//' | sed 's/.$//' | tr -d " \\\\")";
	#yell "DEBUG:filter2result:$filter2result";

	# filter 3
	# filter3:remove strings that aren't exactly 16 or 40 characters long
	filter3result="$(echo "$filter2result" | grep -E "(^.{16}$)|(^.{40}$)" )";
	#yell "DEBUG:filter3result:$filter8result";

	## Append results to $buffer
	candidates="$filter3result";
	unset input filter1result filter2result filter3result;
	yell "DEBUG:candidates:$candidates";
	buffer="$(echo -e "$buffer"\\n"$candidates")";
    done < "$file_in";

    # Sort and deduplicate buffer
    echo "$buffer" | sort | uniq;
    yell "DEBUG:buffer:$buffer";
    yell "DEBUG:end list_fileurls_pgpfp()";
    yell "";
}; # scan URL target for PGP fingerprints
list_tm_pgpfp() {
    # Desc: Scans .tm files in specified dir for pgp fingerprints
    # Input: arg1: target dir
    # Output: stdout: newline-delimited list of fingerprints

    local dir_in buffer candidates buffer;
    local input filter1result filter2result filter3result;
    yell "DEBUG:start list_tm_pgpfp()"
    if [[ ! -d $1 ]]; then die "ERROR:Not a directory:$1"; else
	dir_in="$1"; fi;
    while read -r file; do
	yell "DEBUG:.tm file:$file";
	input="$(cat "$file")";

	# filter 0
	# filter 0: replace tabs and newlines spaces; squeeze spaces
	filter0result="$(echo "$input" | expand -t 4 | tr '\n' ' ' | tr -s ' ')";
	#yell "DEBUG:filter0result:$filter0result;"
	
	# filter 1
	# filter1:find things that look like fingerprints framed between one non-hexadecimal char on either end
	# Note: designed to look for things like: |3457 A265 922A 1F38 39DB  0264 A0A2 95AB DC34 69C9>
	#                                         xA0A2 95AB DC34 69C9>
	#                                         |A0A295ABDC3469C9>
	filter1result="$(echo "$filter0result" | grep -Eo "([^0-9A-F]{1})([0-9A-F \\]){16,53}([^0-9A-F]{1})" )";
	#yell "DEBUG:filter1result:$filter1result";
	
	# filter 2
	# filter2:strip off char on either end, remove spaces and backslashes
	filter2result="$(echo "$filter1result" | sed 's/^.//' | sed 's/.$//' | tr -d " \\\\")";
	#yell "DEBUG:filter2result:$filter2result";

	# filter 3
	# filter3:remove strings that aren't exactly 16 or 40 characters long
	filter3result="$(echo "$filter2result" | grep -E "(^.{16}$)|(^.{40}$)" )";
	#yell "DEBUG:filter3result:$filter8result";
	
	candidates="$filter3result";
	unset input filter1result filter2result filter3result;
	yell "DEBUG:candidates:$candidates";
	buffer="$(echo -e "$buffer"\\n"$candidates")";
    done < <(find "$dir_in" -type f -iname "*.tm");
    echo "$buffer" | sort | uniq;
    yell "DEBUG:buffer:$buffer";
    yell "DEBUG:end list_tm_pgpfp()";
    yell "";
}; # scan .tm files for PGP fingerprints
list_pubkey_fingerprints() {
    # Desc: Returns new-line separated list of available full public key fingerprints
    # Input: none
    # Output: stdout: newline-separated list of full pubkeys
    # Depends: gpg 2.2.29, yell()

    local dump field1
    local -a kl # key listings
    local -a fp # fingerprint array
    local i # key listings index
    local n # loop counter

    # Get machine-readable gpg key list dump
    yell "DEBUG:Creating colon-separated key listings dump...";
    dump="$(gpg --with-colons --list-keys)";

    # Break up dump using `pub:` as delimiter
    i=0; # array index
    n=0; # loop counter
    yell "DEBUG:Splitting key listings dump into array...";
    yell "DEBUG:lines in dump:$(echo "$dump" | wc -l)"
    while read -r line; do
        #yell "DEBUG:loop n:$((n++))";
        ## decide what to do based on first field
        field1="$(echo "$line" | cut -d':' -f1)";
        if [[ $field1 == "pub" ]]; then
            ### start new entry to array
            ((i++));
            kl[$i]="$line";
        else
            ### continue to populate entry in array
            kl[$i]="$(echo -e "${kl[$i]}\n$line")";
        fi;
        unset field1;
    done < <(echo "$dump");

    # Pull first 'fpr:' line from each key listing
    #   note: this should be the primary pubkey
    #   note: sometimes a 'rvk:' line is present, necessitating array processing
    yell "DEBUG:reading key listings array..."
    for listing in "${kl[@]}"; do
        ## Process listing line-by-line to build fp (primary fingerprint array)
        while read -r line; do            
            ### Check first field
            field1="$(echo "$line" | cut -d':' -f1)";
            if [[ $field1 == "fpr" ]]; then
                #### Save primary key fingerprint
                fp+=("$(echo "$line" | cut -d':' -f10)"); # See [1]
                break; # skip rest of listing
            else
                : # do nothing;
            fi;
        done < <(echo "$listing");
    done;

    # Output primary fingerprint array to stdout
    for fingerprint in "${fp[@]}"; do
        echo "$fingerprint";
    done;
    
}; # list gpg pubkey primary key fingerprints
list_subkey_fingerprints() {
    # Desc: Returns new-line separated list of available full sub key fingerprints
    # Input: none
    # Output: stdout: newline-separated list of full subkeys
    # Depends: gpg 2.2.29

    local dump field1
    local -a kl # key listings
    local -a fp # fingerprint array
    local i # key listings index
    local n # loop counter
    
    # Get machine-readable gpg key list dump
    yell "DEBUG:Creating colon-separated key listings dump...";
    dump="$(gpg --with-colons --list-keys)";

    # Break up dump using `sub:` as delimiter
    i=0; # array index
    n=0; # loop counter
    yell "DEBUG:Splitting subkey listings dump into array...";
    yell "DEBUG:lines in dump:$(echo "$dump" | wc -l)"
    while read -r line; do
        #yell "DEBUG:loop n:$((n++))";
        ## decide what to do based on first field
        field1="$(echo "$line" | cut -d':' -f1)";
        if [[ $field1 == "sub" ]] || [[ $field1 == "pub" ]]; then
            ### start new entry to array
            ((i++));
            kl[$i]="$line";
        else
            ### continue to populate entry in array
            kl[$i]="$(echo -e "${kl[$i]}\n$line")";
        fi;
        unset field1;
    done < <(echo "$dump");

    # Pull first 'fpr:' line from each key listing
    #   note: this should be a subkey fingerprint
    #   note: sometimes a 'rvk:' line is present, necessitating array processing
    yell "DEBUG:reading key listings array..."
    for listing in "${kl[@]}"; do
        ## Process listing line-by-line to build fp (subkey fingerprint array)
        while read -r line; do            
            ### Check first field
            field1="$(echo "$line" | cut -d':' -f1)";
            if [[ $field1 == "fpr" ]]; then
                #### Save subkey fingerprint
                fp+=("$(echo "$line" | cut -d':' -f10)"); # See [1]
                break; # skip rest of listing
            else
                : # do nothing;
            fi;
        done < <(echo "$listing");
    done;

    # Output subkey fingerprint array to stdout
    for fingerprint in "${fp[@]}"; do
        echo "$fingerprint";
    done;
    
}; # list gpg pubkey subkey fingerprints
export_min_key() {
    # Desc: Writes minimal armored key to file
    # Usage: export_min_key arg1 arg2
    # Input: arg1: output dir path
    #        arg2: fingerprints
    # Depends: gpg 2.2.29
    # Example: export_min_keys /tmp/ aa11bb22cc33dd44
    # Result: aa11bb22cc33dd44.asc written
    local dir_out path_out pattern fingerprint;
    if [[ $# -ne 2 ]]; then die "ERROR:Invalid argument count:$#"; fi;
    if [[ ! -d $1 ]]; then die "ERROR:Not a directory:$1"; else
	dir_out="$(readlink -f "$1")";
    fi;
    pattern='^(0x)*([[a-fA-F0-9]){8}*$'; # match deadbeef or deadbeefdeadbeef, etc.
    if [[ ! $2 =~ $pattern ]]; then die "ERROR:Not a fingerprint:$2"; else
	fingerprint="$2";
	fingerprint="${fingerprint#0x}"; # strip leading '0x' if present
    fi;
    path_out="$dir_out"/"$fingerprint".asc;
    yell "DEBUG:Ready to write:$path_out";
    gpg --armor --export --export-options export-minimal \
	--no-emit-version --no-comments --yes \
	--output "$path_out" "$fingerprint";
} # write pubkey file
showUsage() {
    # Desc: Display script usage information
    # Usage: showUsage
    # Version 0.0.1
    # Input: none
    # Output: stdout
    # Depends: GNU-coreutils 8.30 (cat)
    cat <<'EOF'
    USAGE:
        scan_write_http_gpgkeys.sh [FILE in] [DIR out]

    DESCRIPTION:

	Download HTTP(S) resources listed in [FILE in] then performs
	regular expression search on resources for strings resembling
	gpg fingerprints. Then, exports from the default `gpg` keyring
	the corresponding public key, if the key exists, into a
	minimal ASCII-armored file in [DIR out].

    EXAMPLE:
      scan_write_http_gpgkeys.sh url_list.txt ./min_keys/
EOF
} # Display information on how to use this script.
main() {
    # Usage: write_gpgkeys.sh arg1 arg2
    # Input: arg1: file containing list of HTTP URLs
    #        arg2: dir to write minimal ASCII-armored pubkey files to
    # Output: stderr: status messages
    local file_in dir_out
    local fpr_cand fpr_known fpr_known_cand fpr_unknown_cand;
    local line_cand_full
    yell "DEBUG:start main()";

    if [[ $# -ne 2 ]]; then showUsage; exit 1; fi;
    if [[ ! -f $1 ]]; then showUsage; die "ERROR:Not a file:$1";
    else
	file_in="$1";
    fi;
    if [[ ! -d $2 ]]; then showUsage; die "ERROR:Not a directory:$2";
    else
	dir_out="$2";
    fi;

    # Get sorted list of candidate gpg fingerprints
    fpr_cand="$(list_fileurls_pgpfp "$file_in" | sed -E 's/^(0x)*//g' )";
    yell "DEBUG:fpr_cand:$fpr_cand";
    fpr_cand="$(echo "$fpr_cand" | tr '[:lower:]' '[:upper:]' | sort | uniq )";
    yell "DEBUG:fpr_cand:$fpr_cand";
    echo -e "DEBUG:fpr_cand:\n$fpr_cand" 1>&2;

    # Get sorted list of known gpg fingerprints
    fpr_known="$(list_pubkey_fingerprints | sed -E 's/^(0x)*//g' )";
    fpr_known="$(echo "$fpr_known" | tr '[:lower:]' '[:upper:]' | sort | uniq )";
    echo -e "DEBUG:fpr_known:\n$fpr_known" 1>&2;

    # Get sorted list of known gpg subkey fingerprints
    fpr_subkey_known="$(list_subkey_fingerprints | sed -E 's/^(0x)*//g' )";
    fpr_subkey_known="$(echo "$fpr_subkey_known" | tr '[:lower:]' '[:upper:]' | sort | uniq )";
    echo -e "DEBUG:fpr_subkey_known:\n$fpr_subkey_known" 1>&2;

    # Get list of known candidate fingerprints to export
    n=0;
    while read -r line_cand; do
	((n++));
	yell "DEBUG:n:$n";
	yell "DEBUG:line_cand:$line_cand";
	
	## Skip loop if $line_cand blank
	if [[ -z $line_cand ]]; then continue; fi;
	
	## Check if candidate fingerprint
	if grep "$line_cand$" < <(echo "$fpr_known") \
		1>/dev/null 2>&1; then
	    ### if candidate fingerprint is a known pubkey fingerprint
	    line_cand_full="$(grep "$line_cand$" < <(echo "$fpr_known") | head -n1)";
	    yell "DEBUG:known fingerprint  :$line_cand";
	    yell "DEBUG:line_cand_full:$line_cand_full";
	    ### grep expression exited 0
	    fpr_known_cand="$(echo -e "$fpr_known_cand"\\n"$line_cand_full")";
	    #yell "DEBUG:fpr_known_cand:$fpr_known_cand";
	    #sleep 1;
	elif grep "$line_cand" < <(echo "$fpr_subkey_known") \
		  1>/dev/null 2>&1; then
	    ### if candidate fingerprint is a known subkey fingerprint
	    line_cand_full="$(grep "$line_cand$" < <(echo "$fpr_known") | head -n1)";
	    yell "DEBUG:known subkey       :$line_cand";
	    yell "DEBUG:line_cand_full:$line_cand_full";
	    : # do not add subkey to $fpr_unknown_cand
	else
	    yell "DEBUG:unknown fingerprint:$line_cand";
	    ### grep exited non-0 ($line_cand not found in $fpr_known or found in $fpr_subkey_known)
	    fpr_unknown_cand="$(echo -e "$fpr_unknown_cand"\\n"$line_cand")";
	fi;
    done < <(echo "$fpr_cand");
    echo -e "DEBUG:fpr_known_cand:\n$fpr_known_cand" 1>&2;

    # Get list of unknown canditate fingerprints for error indication
    echo -e "DEBUG:fpr_unknown_cand:\n$fpr_unknown_cand" 1>&2;
    
    while read -r line; do
	if [[ -z $line ]]; then continue; fi;
	yell "DEBUG:NOTICE:Exporting pubkey using known fingerprint:$line";
	export_min_key "$dir_out" "$line";
    done < <(echo "$fpr_known_cand");

    if [[ -n $fpr_unknown_cand ]]; then
	echo -e "WARNING:Unknown fingerprints:\n$fpr_unknown_cand" 1>&2;
    else
	yell "DEBUG:NOTICE:No unknown fingerprints found.";
	:
    fi;

    yell "DEBUG:end main()";
}; # main program

main "$@";

# Author: Steven Baltakatei Sandoval
# License: GPLv3+
