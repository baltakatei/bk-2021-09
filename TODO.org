* Task List
** Pending
*** TODO Add pubkey Linux Kernel
- Fingerprint source: https://www.kernel.org/signature.html
- Note: Found via WikiData [[https://w.wiki/4w9R][query]].

*** TODO Add PGPainless key
- Fingerprint source: https://gh.pgpainless.org/
- Fignerprint: https://keyoxide.org/7F9116FEA90A5983936C7CFAA027DB2F3E1E118A
- Key: https://keyoxide.org/7F9116FEA90A5983936C7CFAA027DB2F3E1E118A
- Note: Java/Android library used by [[https://github.com/android-password-store/Android-Password-Store][Android Password Store]].

*** TODO Add ThinkPenguin key
- Key source: https://www.thinkpenguin.com/about
- Key: https://www.thinkpenguin.com/files/chris-thinkpenguin-gpg-public-key.asc
- Fingerprint: 
  #+begin_example
  pub   rsa4096/0xDC53C17DAFBE6895 2013-09-08 [SC] [expires: 2030-11-10]
        Key fingerprint = 8CB3 4159 C742 ADE6 AD38  9B4E DC53 C17D AFBE 6895
  uid                   [ unknown] Christopher Waid
  sub   rsa4096/0xE6A916E28DBB155A 2013-09-08 [E] [expires: 2030-11-09]
  #+end_example

** Completed
*** DONE Add Element.io key
    CLOSED: [2023-06-24 Sat 19:08]
The installation process for installing the Element.io desktop client
on Linux involves adding an ~apt~ repository and downloading a GPG key
with commands from [[https://element.io/download#linux][this]] page.

- [[https://element.io/download#linux][Page with link to GPG key]]
- [[https://packages.element.io/debian/element-io-archive-keyring.gpg][Link to GPG key]] ([[https://web.archive.org/web/20230413220109/https://packages.element.io/debian/element-io-archive-keyring.gpg][Archived 2023-04-13]])

Fingerprint:
#+begin_example
pub   rsa4096/0xD7B0B66941D01538 2019-04-15 [SC] [expires: 2033-03-13]
      Key fingerprint = 12D4 CD60 0C22 40A9 F4A8  2071 D7B0 B669 41D0 1538
uid                   [ unknown] riot.im packages <fb5ac056>
sub   rsa3072/0xC2850B265AC085BD 2019-04-15 [S] [expires: 2025-03-15]
      Key fingerprint = 7574 1890 063E 5E9A 4613  5D01 C285 0B26 5AC0 85BD
#+end_example

*** DONE Add pubkey Trisquel
    CLOSED: [2023-06-24 Sat 19:08]
- [[https://trisquel.info/en/download][Page with link to GPG key]]
- [[https://archive.trisquel.info/trisquel/trisquel-archive-signkey.gpg][Link to GPG key]]

Fingerprint:
#+begin_example
pub   dsa1024/0xB4EFB9F38D8AEBF1 2007-01-14 [SC]
      Key fingerprint = E6C2 7099 CA21 965B 734A  EA31 B4EF B9F3 8D8A EBF1
uid                   [ unknown] Trisquel GNU/Linux (Trisquel GNU/Linux signing key)
sub   elg2048/0x9D5348BEECBB0F64 2007-01-14 [E]

pub   rsa4096/0xB138CA450C05112F 2017-01-07 [SC]
      Key fingerprint = 6036 4C98 69F9 2450 421F  0C22 B138 CA45 0C05 112F
uid                   [ unknown] Trisquel GNU/Linux
sub   rsa4096/0x235C3D2B15726BD0 2017-01-07 [E]
#+end_example




** DONE Add Inkscape developer Marc Jeanmougin
   CLOSED: [2023-06-13 Tue 06:58]
- Fingerprint: via sig on Inkscape 1.2.2 AppImage [[https://web.archive.org/web/20230106211542/https://inkscape.org/release/inkscape-1.2.2/gnulinux/appimage/dl/][page]].
  #+begin_example
pub   dsa1024/0x5FCB204EF882B07A 2010-03-08 [SCA] [expires: 2023-04-09]
      Key fingerprint = 74E8 DA13 9805 5A81 20B2  76EB 5FCB 204E F882 B07A
uid                   [ unknown] Marc Jeanmougin
sub   elg4096/0x5164476ED5AC11E6 2010-03-08 [E] [expires: 2023-04-09]
  #+end_example
- Key: https://inkscape.org/~MarcJeanmougin/gpg/

*** DONE File bug on <hlink> PDF render
    CLOSED: [2023-06-13 Tue 07:06]
Date: 2022-03-13

If an ~<hlink>~ tag is used in TeXmacs in a line by itself, the
exported PDF will not make the URL a hyperlink. A workaround is to
insert some character (e.g. a space) before the ~<hlink>~ tag at the
start of the line.

- TeXmacs version 2.1.1
- GPL Ghostscript 9.54.0 (2021-03-30) (in case this is the PDF
  converter used)

Bug filed at https://savannah.gnu.org/bugs/index.php?62179
