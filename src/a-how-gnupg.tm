<TeXmacs|2.1.2>

<project|book.tm>

<style|<tuple|book|style-bk|std-frame>>

<\body>
  <\hide-preamble>
    \;

    \;

    \;
  </hide-preamble>

  <appendix|How to use <name|GnuPG>><label|a how-gnupg>

  Last updated 2022-05-18 by <name|Steven Baltakatei Sandoval>.

  \;

  <no-indent>This appendix describes in more detail how to use <name|GnuPG>.
  Examples assume use of <name|GnuPG> version <version|2.2.12>. Definitions
  of terms relevant to <name|GnuPG> are provided in <reference|a how-gnupg
  defs>. Useful commands are provided in <reference|a how-gnupg useful-cmds>.

  <\remark>
    Example code is sometimes given in the form of a <name|Bash> <def|def
    script>. Such scripts usually have a first line like
    <code-inline|#!/usr/bin/env bash> that tell your interpreter to execute
    the lines that follow as <name|Bash> commands. This is useful from a
    typography standpoint because often the length of <name|GnuPG> commands
    can exceed the recommended character limit for human
    readability.<\footnote>
      <name|Emacs>, for example, defaults to wrapping columns of text to 70.
      See <hlinkv|https://emacs.stackexchange.com/questions/36118/>.\ 
    </footnote> This document will attempt to limit line widths in code
    examples to approximately 80 characters.
  </remark>

  <section|Terms and Definitions><label|a how-gnupg defs>

  Unless otherwise indicated, terms and definitions are used as the
  <verbatim|man> page for <verbatim|gpg> (viewable using the comand:
  <code-inline|<verbatim|$ man gpg>>) uses them. For example, <name|GnuPG>
  does not use the term \Pkeychain\Q but <name|GPG Suite><\footnote>
    See <hlinkv|https://gpgtools.org/>.
  </footnote>, a popular GUI interface to <name|GnuPG> on the <name|macOS>
  operating system, does use the term in the name of its primary key
  management app called \P<name|GPG Keychain>\Q.

  <\description>
    <item*|authenticate><label|def authenticate>

    <\enumerate-numeric>
      <item>(verb) An operation performed by Alice with a <def|def
      authenticate>-flagged <def|def private-key> to authenticate herself to
      a server (e.g. to open a command line interface on a remote server via
      <verbatim|ssh>)<\footnote>
        See <hlinkv|https://mlohr.com/gpg-agent-for-ssh-authentication-update/>.
        Accessed 2022-05-17.
      </footnote>.

      <item>(noun) A <def|def capability-flag> on a <def|def primary-key> or
      <def|def subkey> indicating that the key may be used to perform the
      <def|def authenticate> operation.
    </enumerate-numeric>

    <item*|capability><label|def capability>(adjective) See <def|def flag>.

    <item*|capability flag><label|def capability-flag>(noun) See <def|def
    flag>.

    <item*|certify><label|def certify>

    <\enumerate-numeric>
      <item>(verb) An operation performed by Alice with one of her <def|def
      private-key>s (usually her <def|def primary-key>) on a <def|def
      user-id> of Bob's primary key to indicate that Alice believes Bob's
      primary key is actually his. The operation creates a <def|def
      signature> that may be included in exported copies of Bob's public key.
      Is similar to the <def|def sign> operation except it is meant to sign
      <def|def key>s, not arbitrary data or files.

      <item>(noun) A <def|def capability-flag> on a <def|def primary-key> or
      <def|def subkey> indicating that the key may be used to perform the
      <def|def certify> operation.
    </enumerate-numeric>

    \;

    <item*|decrypt><label|def decrypt>(verb) The action a <def|def
    primary-key> or <def|def subkey> with an <def|def encrypt> flag performs
    on an encrypted file in order to produce the <def|def plaintext>.

    \;

    <item*|encrypt><label|def encrypt>

    <\enumerate-numeric>
      <item>(verb) An operation performed by Alice on a file using an
      <def|def encrypt>-flagged <def|def primary-key> or <def|def subkey>
      owned by Bob in order to send Bob the file without anyone else being
      able to read the file plaintext.

      <item>(noun) A <def|def capability-flag> on a <def|def primary-key> or
      <def|def subkey> indicating that the key may be used to perform the
      <def|def encrypt> operation which decrypts<\footnote>
        See <def|def decrypt>.
      </footnote> the file.
    </enumerate-numeric>

    <item*|encrypted><label|def encrypted>(adjective) A state of a <def|def
    plaintext> after it is rendered unreadable using an <def|def
    encrypt>-flagged <def|def public-key> or a <def|def symmetric-key>. The
    <def|def plaintext> can be made readable again through use of the
    <def|def public-key>'s corresponding <def|def private-key> or a <def|def
    symmetric-key>. Such keys can <def|def decrypt> the contents to reveal
    the original plaintext.

    <item*|fingerprint><label|def fingerprint>(noun) A number, usually
    expressed in <def|def hexadecimal>, that uniquely identifies a <def|def
    public-key>. In <name|GnuPG>, this usually refers to the <def|def
    full-fingerprint> which takes the form of a 40-character string (ignoring
    spaces) that may be displayed using <code-inline|<verbatim|gpg
    --fingerprint <underline|key-id>>> (e.g. <long-id-spc|3457 A265 922A 1F38
    39DB \ 0264 A0A2 95AB DC34 69C9>). The string is derived from a
    cryptographic digest of the <def|def public-key>.<\footnote>
      See <condensed|<hlinkv|https://blog.djoproject.net/2020/05/03/main-differences-between-a-gnupg-fingerprint-a-ssh-fingerprint-and-a-keygrip/>>.
    </footnote> The <def|def long-id> is a substring of the <def|def
    full-fingerprint>.

    <item*|flag><label|def flag>(noun) (a.k.a. \P<def|def capability-flag>\Q)
    A small digital marker on a <def|def public-key> indicating to
    <name|OpenPGP> software how the key should be used. Possible capability
    flags (and their abbreviations) include: <def|def certify>
    (<verbatim|C>), <def|def sign> (<verbatim|S>), <def|def encrypt>
    (<verbatim|E>), and <def|def authenticate> (<verbatim|A>).

    Flags are set during key creation (e.g. via <code-inline|<verbatim|gpg
    --expert --full-gen-key>>) and may be modified later (e.g. via
    <code-inline|<verbatim|gpg --edit-key <underline|key-id>>>). A key
    generated using completely default settings with <name|GnuPG>
    <version|2.2.12> will consist of a <def|def primary-key> with <def|def
    sign> (<verbatim|S>) and <def|def certify> (<verbatim|C>) capability
    flags and a single <def|def subkey> with an <def|def encrypt>
    (<verbatim|E>) capability flag.

    <item*|full fingerprint><label|def full-fingerprint>(noun) In
    <name|GnuPG>, a 40-character string encoding a 160-bit number derived
    from a cryptographic digest of a <def|def public-key>. See <def|def
    fingerprint>.

    <item*|hexadecimal><label|def hexadecimal>

    <\enumerate-numeric>
      <item>(countable) A number expressed in base 16 notation (e.g.
      \P<verbatim|7155>\Q is \P<verbatim|0x1BF3>\Q in hexadecimal).

      <item>(adjective) A property of a number that is expressed in
      hexadecimal.
    </enumerate-numeric>

    <item*|interactive><label|def interactive>(adjective) A property of a
    method that provides a program input which requires the full attention of
    a user. Contrast with <def|def non-interactive>. For example, when
    <name|GnuPG> prompts the user to enter a passphrase and a user types the
    passphrase using their keyboard.

    <item*|key> <label|def key>(noun) An abstract object that can be used to
    digitally sign or decrypt data. Categories of keys used in <name|GnuPG>
    include <def|def public-key>, <def|def private-key>, and <def|def
    symmetric-key>. A public key may be marked to be used for use as a
    <def|def primary-key> or a <def|def subkey>. Depending upon context,
    <def|def key> may consist of multiple such objects (e.g. a <def|def
    primary-key> and associated <def|def subkey>(s) are modified by
    <code-inline|<verbatim|gpg --edit-key>>).

    <item*|keybox><label|def keybox>(noun) A file format used by <name|GnuPG>
    for storing public keys.<\footnote>
      See <hlinkv|https://www.gnupg.org/documentation/manuals/gnupg/kbxutil.html>.
    </footnote> See <def|def keyring>.

    <item*|key-id><label|def key-id>(noun) A string of characters that
    identifies a <def|def public-key> such as a <def|def long-id> or <def|def
    fingerprint>.

    <item*|keypair><label|def keypair>(noun) A <def|def public-key> and its
    associated <def|def private-key>.

    <item*|keyring><label|def keyring>(noun) A set of <def|def public-key>s
    and <def|def keypair>s that can be modified using <code-inline|gpg
    \U-edit-key>.

    <item*|keystore>(noun) (alt. \Pkey store\Q) A term not used by
    <name|GnuPG> but which may be refer to a collection of <def|def key>s
    that <name|GnuPG> would call a <def|def keyring>.

    <item*|long ID><label|def long-id>(noun) A 16-digit <def|def hexadecimal>
    number used to identify a <def|def public-key>, e.g. <long-id-spc|A0A2
    95AB DC34 69C9>. Its hexadecimal nature may be emphasized by prepending
    the string with the \P<verbatim|0x>\Q prefix and omitting spaces, e.g.
    <long-id|A0A295ABDC3469C9>.<\footnote>
      See <hlinkv|https://stackoverflow.com/questions/2670639/why-are-hexadecimal-numbers-prefixed-with-0x>.
    </footnote> <name|GnuPG> is not particular about whether letters in the
    <def|def long-id> are upper or lowercase, so <long-id|a0a295abdc3469c9>
    is also acceptable. Compare with <def|def short-id>.

    <item*|non-interactive><label|def non-interactive>(adjective) A property
    of a method that provides a program input which does not require the full
    attention of a user. Contrast with <def|def interactive>. For example,
    when a script uses the <code-inline|<verbatim|--batch --yes --passphrase
    <underline|string>>> options in order to automatically provide a
    passphrase <verbatim|<underline|string>> to <verbatim|gpg> unattended,
    the script may be described as <def|def non-interactive>.

    <item*|primary key><label|def primary-key>(noun) In <name|GnuPG>, a
    <def|def public-key> or <def|def keypair> that is generally used to
    <def|def certify> one's own <def|def subkey> or another person's <def|def
    primary-key>. The <def|def key> may be marked by some combination of
    <def|def certify>, <def|def sign>, <def|def encrypt>, or <def|def
    authenticate> capability flags. A <def|def primary-key> has a uniquely
    identifying <def|def fingerprint>. See <def|def long-id>.

    <item*|primary UID><label|def primary-uid>(noun) The main <def|def uid>
    (<def|def user-id>) to be used when multiple <def|def uid>s are present.
    May be set for key <long-id-spc|A0A2 95AB DC34 69C9> with:

    <code-inline|<verbatim|gpg --quick-set-primary-uid
    <with|color|brown|0xa0a295abdc3469c9> <underline|primary-user-id>>>

    <item*|primary user ID><label|def primary-user-id>(noun) See <def|def
    primary-uid>.

    <item*|plaintext><label|def plaintext>

    <\enumerate-numeric>
      <item>(noun) Data or a file before it is <def|def encrypt>ed against a
      <def|def public-key> or a <def|def symmetric-key>.

      <item>(noun) Data or a file after it has been <def|def decrypt>ed using
      a <def|def private-key> or a <def|def symmetric-key>.
    </enumerate-numeric>

    <item*|private key><label|def private-key>(noun) The <def|def key> of a
    <def|def keypair> that is used to <def|def decrypt> data that was
    encrypted with a corresponding <def|def public-key> or to <def|def sign>
    data that can be verified with the <def|def public-key>. Together with a
    <def|def public-key>, forms a <def|def keypair>. Should be
    password-protected if secured by <name|GnuPG> or PIN-protected if secured
    by a smartcard accessible to <name|GnuPG>. As of <name|GnuPG>
    <version|2.1>, <def|def private-key>s (or references to a
    smartcard-residing <def|def private-key>s) may be stored as files in the
    directory <code-inline|<verbatim|.gnupg/private-keys-v1.d>>.<rsup|<reference|gnupg_201411_v2.1-release-notes>>

    <item*|public key><label|def public-key>(noun) The <def|def key> of a
    <def|def keypair> that is used to <def|def encrypt> data for later
    <def|def decrypt>ion by a corresponding <def|def private-key> or to
    <def|def verify> data signed by the <def|def private-key>. Together with
    its <def|def private-key>, forms a <def|def keypair>. May be uniquely
    identified with a <def|def fingerprint>. Since <name|GnuPG>
    <version|2.1>, <def|def public-key>s are stored by default in a <def|def
    keybox> file at <code-inline|<verbatim|.gnupg/pubring.kbx>>.<\footnote>
      <label|gnupg_201411_v2.1-release-notes>See
      <hlinkv|https://gnupg.org/faq/whats-new-in-2.1.html>.
    </footnote>

    <item*|script><label|def script>(noun) An executable file that may run
    programs such as <verbatim|gpg>. Often used for the purpose of automating
    complex tasks such as encrypting many files at once.

    <item*|sign><label|def sign>(verb) A <def|def private-key> operation that
    produces a <def|def signature>.

    <item*|signature><label|def signature>(noun) A unique cryptographic proof
    generated by with a <def|def private-key> against some data (e.g. a file)
    that indicates the owner of the private key possessed a copy of the data.
    Often used to indicate validity of important documents or executables
    (e.g. verifying an <verbatim|.iso> file used to install an operating
    system was not corrupted or compromised by an attacker). Also known as a
    \Pdigital signature\Q.

    In <name|GnuPG>, a signature of <verbatim|file.txt> may be created and
    stored in <verbatim|file.txt.gpg> with:

    <code-inline|<verbatim|$ gpg --detach-sign --output file.txt.gpg --
    file.txt>>

    <item*|subkey><label|def subkey>(noun) In <name|GnuPG>, a <def|def
    public-key> or <def|def keypair> that is generally used to perform
    operations in the place of a <def|def primary-key> in situations where
    the risk of leaking a <def|def primary-key>'s <def|def private-key> is
    unacceptable (e.g. on a computer that could be physically stolen or
    remotely hacked). A <em|subkey> can be made mostly functionally
    equivalent to a <def|def primary-key> except for hidden software
    indicators identifying it as a <em|subkey>. It is discouraged to assign
    <def|def certify> capability to a <em|subkey>.<\footnote>
      See <condensed|<hlinkv|https://web.archive.org/web/20220517224040/https://lists.gnupg.org/pipermail/gnupg-users/2017-August/058904.html>>.
    </footnote>

    <item*|symmetric key><label|def symmetric-key>(noun) A <def|def key> that
    is used to both encrypt and decrypt. In <name|GnuPG>, a symmetric key
    consists of a passphrase that may be provided interactively or
    non-interactively. For example, <verbatim|file.txt> may be encrypted
    using the symmetric key \P<verbatim|1234>\Q using:

    <code-inline|<verbatim|gpg --symmetric --output file.txt.gpg --
    file.txt>>

    <item*|short ID><label|def short-id>(noun) An 8-digit <def|def
    hexadecimal> number similar to a <def|def long-id>. Use of <def|def
    short-id> is not recommended because, as of 2021, generating multiple
    public keys with matching <def|def short-id>s requires a negligible
    amount of computing power.<\footnote>
      See <hlinkv|https://security.stackexchange.com/questions/84280/>.
    </footnote> Compare with <def|def long-id>.

    <item*|UID><label|def uid>(noun) Abbreviation of <def|def user-id>.

    <item*|User ID><label|def user-id>(noun) (a.k.a. \P<def|def uid>\Q).
    Identification data for a <def|def primary-key>. Usually consists of a
    name and email address. \P<def|def user-id>\Q stands for \PUser
    Identification\Q and may be shortened to \P<def|def uid>\Q. When two
    people sign eachother's <name|PGP> keys, what is meant is that each uses
    a <def|def certify>-capable <def|def key> (usually their <def|def
    primary-key>) to sign one or more of eachother's <def|def user-id>s .
    Unusually, a <def|def user-id> may consist of a JPEG image file.

    <name|GnuPG> may identify a <def|def user-id> through partial or exact
    string matches.<\footnote>
      See <hlinkv|https://www.gnupg.org/documentation/manuals/gnupg/Specify-a-User-ID.html>.
    </footnote>

    See <def|def uid>. See also <def|def primary-uid>.

    <item*|verify><label|def verify>(verb) A cryptogrpahic calculation that
    determines that a <def|def private-key> was used to <def|def sign> some
    data. Required inputs are the <def|def private-key>'s corresponding
    <def|def public-key> and a <def|def signature>.

    <item*|<verbatim|-->>(command line syntax) Indicates the end of options
    passed to a command and the starting position of positional arguments.
    Also known as a \Pdouble dash\Q. Used by many other command line tools
    such as <verbatim|bash> or <verbatim|grep>. May be useful to more clearly
    indicate which arguments are NOT associated with an option flag. For
    example, the following commands are functionally equivalent but the
    latter more clearly indicates that <verbatim|file.txt> is the only
    non-option argument which <verbatim|gpg> interprets as the input:

    <code-inline|<verbatim|gpg --encrypt --output file.txt.gpg file.txt>>

    <code-inline|<verbatim|gpg --encrypt --output file.txt.gpg -- file.txt>>

    Below is a <verbatim|grep> example in which a na�ve search of
    <verbatim|file.txt> for the string <code-inline|<verbatim|-violet->>
    fails without the double dash:

    <code-inline|<\verbatim>
      $ echo "asdf-violet-asdf" \<gtr\> file.txt

      $ grep --only-matching "-violet-" file.txt

      $ grep --only-matching -- "-violet-" file.txt

      -violet-
    </verbatim>>
  </description>

  <section|Useful Commands><label|a how-gnupg useful-cmds>

  <subsection|Obtaining keys><label|a how-gnupg useful-cmds obtain>

  <subsubsection|Import a public key><label|a how-gnupg useful-cmds obtain
  import>

  The <code-inline|<verbatim|$ gpg --import -- key.asc>> command may be used
  to import a file named \P<verbatim|key.asc>\Q. If the
  <code-inline|<verbatim|$ gpg --import>> command by itself is run and a
  clipboard program is available (e.g. copy/paste), then pasting the text of
  a public key into the shell followed by pressing <kbd|ctrl-d> (i.e.
  providing an \Pend of transmission\Q character<\footnote>
    See <hlinkv|https://unix.stackexchange.com/a/110248>.
  </footnote>) will tell <verbatim|gpg> to process the pasted text.

  <subsubsection|Download from a keyserver><label|a how-gnupg useful-cmds
  obtain download>

  The <code-inline|<verbatim|$ gpg --receive-keys>> command can be used as
  shown in the example below to download a public key (e.g. <long-id-spc|4246
  8F40 09EA 8AC3>) from a keyserver (e.g. <hlinkv|keyserver.ubuntu.com>).

  <\code-wide-sm>
    <\with|hmagnified-factor|0.5>
      $ gpg --receive-keys --keyserver keyserver.ubuntu.com --
      42468f4009ea8ac3

      gpg: key 0x42468F4009EA8AC3: public key "Debian Testing CDs
      Automatic... \<less\>047be9f4\<gtr\>" imported

      gpg: Total number processed: 1

      gpg: \ \ \ \ \ \ \ \ \ \ \ \ \ \ imported: 1
    </with>
  </code-wide-sm>

  \;

  As of 2022-01-14, few keyservers provide full public keys due to an
  unsolved certificate spam problem.<\footnote>
    Hansen, Robert J.. \PSKS Keyserver Network Under Attack\Q. 2019-06-29.
    \ <hlinkv|https://gist.github.com/rjhansen/67ab921ffb4084c865b3618d6955275f>.
  </footnote>

  <\itemize>
    <item><hlink|<verbatim|keyserver.ubuntu.com>|hkps://keyserver.ubuntu.com>
    - Provides full keys.

    <item><hlink|<verbatim|keyring.debian.org>|keyring.debian.org> - Provides
    full keys of <name|Debian> developer and maintainers.

    <item><hlink|<verbatim|keys.openpgp.org>|keys.openpgp.org> - Provides
    keys without user IDs unless key owner authenticates themselves via the
    user ID email address.
  </itemize>

  <subsection|Analyzing keys><label|a how-gnupg useful-cmds analyze>

  <subsubsection|View public key fingerprint><label|a how-gnupg useful-cmds
  analyze fpr>

  <\itemize>
    <item>Show <def|def fingerprint>s of the <def|def primary-key> and
    <def|def subkey>s . The example below shows the primary fingerprint in
    <with|color|red|red>, the <def|def long-id> colored in
    <with|color|brown|brown>, <def|def user-id>s in
    <with|color|blue|blue><\footnote>
      In this publication, I may obfuscate email addresses via a
      <code-inline|<verbatim|b2sum -l32>> hash if I cannot confirm the key
      owner is okay with their email address being posted so plainly.
    </footnote>, and fingerprints of subkeys <with|color|dark green|dark
    green>.

    <\code-wide-sm>
      $ gpg --fingerprint -- <with|color|brown|0xa0a295abdc3469c9>

      pub \ \ rsa4096/<with|color|brown|0xA0A295ABDC3469C9> 2017-10-11 [C]
      [expires: 2022-07-08]

      \ \ \ \ \ \ Key fingerprint = <with|color|red|3457 A265 922A 1F38 39DB
      \ 0264 A0A2 95AB DC34 69C9>

      uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ultimate]
      <with|color|blue|Steven Sandoval \<less\>baltakatei@gmail.com\<gtr\>>

      uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ultimate]
      <with|color|blue|Steven Sandoval \<less\>baltakatei@alumni.stanford.edu\<gtr\>>

      sub \ \ rsa4096/0x6DD7D496916A1253 2018-05-16 [E] [expires: 2022-07-07]

      \ \ \ \ \ \ Key fingerprint = <with|color|dark green|5E55 5FC6 1C85
      871E 813B \ 5BCF 6DD7 D496 916A 1253>

      sub \ \ rsa4096/0x57DA57D9517E6F86 2018-05-16 [S] [expires: 2022-07-07]

      \ \ \ \ \ \ Key fingerprint = <with|color|dark green|38F9 6437 C83A
      C88E 28B7 \ A952 57DA 57D9 517E 6F86>

      sub \ \ rsa4096/0x5F9D26B9A598A2D3 2018-05-16 [A] [expires: 2022-07-07]

      \ \ \ \ \ \ Key fingerprint = <with|color|dark green|EDCA 7EE7 D09E
      7F2E 1DF6 \ A229 5F9D 26B9 A598 A2D3>
    </code-wide-sm>
  </itemize>

  <subsection|Sending keys><label|a how-gnupg useful-cmds send>

  <subsubsection|Export public key><label|a how-gnupg useful-cmds send
  to-file>

  <\itemize>
    <item>Export <def|def public-key> according to last 16 characters of
    public key <def|def fingerprint> (i.e. \P<def|def long-id>\Q, e.g.
    <long-id-spc|A0A2 95AB DC34 69C9>).

    <\code-wide-sm>
      $ gpg --export --output /tmp/key --
      <with|color|brown|0xa0a295abdc3469c9>.
    </code-wide-sm>

    <item>Export the smallest key possible. Useful to strip key of signatures
    except for self-signatures. This creates an ASCII-armored<\footnote>
      See <hlinkv|https://crypto.stackexchange.com/questions/91984/why-use-ascii-armor-for-file-encryption>.
    </footnote> text file named <code-inline|<with|color|blue|<verbatim|pubkey.asc>>>
    in the <code-inline|<with|color|magenta|<verbatim|/tmp>>> directory.

    <with|font|Linux Libertine|font-base-size|8|<\code-wide>
      #!/usr/bin/env bash

      gpg --export --export-options export-minimal \\

      \ \ \ \ --armor \\

      \ \ \ \ --output <with|color|magenta|/tmp>/<with|color|blue|pubkey.asc>
      \\

      \ \ \ \ -- \\

      \ \ \ \ <with|color|brown|0xa0a295abdc3469c9>
    </code-wide>>

    \;
  </itemize>

  <subsubsection|Upload public key><label|a how-gnupg useful-cmds send
  to-server>

  <\itemize>
    <item>Send <def|def public-key> to a keyserver using <verbatim|gpg> and a
    <def|def long-id>.

    <\code-wide-sm>
      $ gpg --send-keys --keyserver keyserver.ubuntu.com --
      <with|color|brown|0xa0a295abdc3469c9>
    </code-wide-sm>

    <\itemize>
      <item>Note: Some keyservers such as
      <hlink|<verbatim|keys.openpgp.org>|keys.openpgp.org> achieve
      reliability by requiring uploaders to verify their identity via an
      email exchange through a <def|def user-id> email address. If no
      verification is performed for a given <def|def user-id>, uploaded keys
      are shared without that <def|def user-id>.<\footnote>
        See <hlinkv|https://keys.openpgp.org/about>.
      </footnote>
    </itemize>

    <item>Send <def|def public-key> to the <hlinkv|https://keys.openpgp.org>
    keyserver using a web browser.

    <\itemize>
      <item>Export a public key file (see <reference|a how-gnupg useful-cmds
      send to-file>).

      <item>Go to <hlinkv|https://keys.openpgp.org/upload>and submit the
      public key file.

      <item>Go to <hlinkv|https://keys.openpgp.org/manage>and submit the
      email address of the public key's main <def|def user-id>.

      <item>Verify the <def|def user-id> by following the emailed
      instructions sent by <hlinkv|https://keys.openpgp.org> .
    </itemize>
  </itemize>

  <subsection|Creating keys><label|a how-gnupg useful-cmds create>

  <subsubsection|Using default settings><label|a how-gnupg useful-cmds create
  default>

  Running <code-inline|<verbatim|$ gpg --gen-key>> will guide the user to
  creating a key with default settings.

  <subsubsection|With subkeys><label|a how-gnupg useful-cmds create
  with-subkeys>

  The <code-inline|<verbatim|$ gpg --expert --full-gen-key>> command in
  combination with some modifications to the configuration file
  <code-inline|~/.gnupg/gpg.conf> may be used to create an OpenPGP key with
  subkeys. Subkeys are useful since their private components can be loaded
  onto a smartcard while keeping the primary key offline, available to create
  new subkeys. This may be desireable if a primary key is intended to be used
  over a long time period and the risk of losing an online defaultly
  configured key is unacceptable. Please see the article by <name|Thierry
  Thuron> titled \POpenPGP - The Almost Perfect Key Pair\Q for a useful
  procedure.<\footnote>
    Thuron, Thierry. \POpenPGP - The Almost Perfect Key Pair\Q. 2017-10-13.
    Eleven Labs Blog. <hlinkv|https://blog.eleven-labs.com/en/openpgp-almost-perfect-key-pair-part-1/>.
  </footnote>

  \;
</body>

<\initial>
  <\collection>
    <associate|chapter-nr|2>
    <associate|page-first|40>
    <associate|page-medium|papyrus>
    <associate|preamble|false>
    <associate|section-nr|15>
    <associate|subsection-nr|3>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|a how-gnupg|<tuple|A|41>>
    <associate|a how-gnupg defs|<tuple|A.1|41>>
    <associate|a how-gnupg useful-cmds|<tuple|A.2|43>>
    <associate|a how-gnupg useful-cmds analyze|<tuple|A.2.2|?>>
    <associate|a how-gnupg useful-cmds analyze fpr|<tuple|A.2.2.1|?>>
    <associate|a how-gnupg useful-cmds create|<tuple|A.2.4|?>>
    <associate|a how-gnupg useful-cmds create default|<tuple|A.2.4.1|?>>
    <associate|a how-gnupg useful-cmds create with-subkeys|<tuple|A.2.4.2|?>>
    <associate|a how-gnupg useful-cmds obtain|<tuple|A.2.1|?>>
    <associate|a how-gnupg useful-cmds obtain download|<tuple|A.2.1.2|?>>
    <associate|a how-gnupg useful-cmds obtain import|<tuple|A.2.1.1|?>>
    <associate|a how-gnupg useful-cmds send|<tuple|A.2.3|?>>
    <associate|a how-gnupg useful-cmds send to-file|<tuple|A.2.3.1|?>>
    <associate|a how-gnupg useful-cmds send to-server|<tuple|A.2.3.2|?>>
    <associate|auto-1|<tuple|A|41>>
    <associate|auto-10|<tuple|A.2.3.1|44>>
    <associate|auto-11|<tuple|A.2.3.2|44>>
    <associate|auto-12|<tuple|A.2.4|44>>
    <associate|auto-13|<tuple|A.2.4.1|44>>
    <associate|auto-14|<tuple|A.2.4.2|?>>
    <associate|auto-2|<tuple|A.1|41>>
    <associate|auto-3|<tuple|A.2|43>>
    <associate|auto-4|<tuple|A.2.1|43>>
    <associate|auto-5|<tuple|A.2.1.1|43>>
    <associate|auto-6|<tuple|A.2.1.2|43>>
    <associate|auto-7|<tuple|A.2.2|44>>
    <associate|auto-8|<tuple|A.2.2.1|44>>
    <associate|auto-9|<tuple|A.2.3|44>>
    <associate|def authenticate|<tuple|authenticate|41>>
    <associate|def capability|<tuple|capability|41>>
    <associate|def capability-flag|<tuple|capability flag|41>>
    <associate|def certify|<tuple|certify|41>>
    <associate|def decrypt|<tuple|decrypt|41>>
    <associate|def encrypt|<tuple|encrypt|41>>
    <associate|def encrypted|<tuple|encrypted|41>>
    <associate|def fingerprint|<tuple|fingerprint|41>>
    <associate|def flag|<tuple|flag|42>>
    <associate|def full-fingerprint|<tuple|full fingerprint|?>>
    <associate|def hexadecimal|<tuple|hexadecimal|?>>
    <associate|def interactive|<tuple|interactive|42>>
    <associate|def key|<tuple|key|42>>
    <associate|def key-id|<tuple|key-id|?>>
    <associate|def keybox|<tuple|keybox|42>>
    <associate|def keypair|<tuple|keypair|42>>
    <associate|def keyring|<tuple|keyring|42>>
    <associate|def long-id|<tuple|long ID|42>>
    <associate|def non-interactive|<tuple|non-interactive|42>>
    <associate|def plaintext|<tuple|plaintext|42>>
    <associate|def primary-key|<tuple|primary key|42>>
    <associate|def primary-uid|<tuple|primary UID|?>>
    <associate|def primary-user-id|<tuple|primary user ID|?>>
    <associate|def private-key|<tuple|private key|42>>
    <associate|def public-key|<tuple|public key|42>>
    <associate|def script|<tuple|script|42>>
    <associate|def short-id|<tuple|short ID|43>>
    <associate|def sign|<tuple|sign|42>>
    <associate|def signature|<tuple|signature|42>>
    <associate|def subkey|<tuple|subkey|43>>
    <associate|def symmetric-key|<tuple|symmetric key|43>>
    <associate|def uid|<tuple|UID|?>>
    <associate|def user-id|<tuple|User ID|?>>
    <associate|def verify|<tuple|verify|?>>
    <associate|footnote-1|<tuple|1|41>>
    <associate|footnote-A.1.1|<tuple|A.1.1|41>>
    <associate|footnote-A.1.10|<tuple|A.1.10|?>>
    <associate|footnote-A.1.2|<tuple|A.1.2|41>>
    <associate|footnote-A.1.3|<tuple|A.1.3|42>>
    <associate|footnote-A.1.4|<tuple|A.1.4|42>>
    <associate|footnote-A.1.5|<tuple|A.1.5|42>>
    <associate|footnote-A.1.6|<tuple|A.1.6|43>>
    <associate|footnote-A.1.7|<tuple|A.1.7|43>>
    <associate|footnote-A.1.8|<tuple|A.1.8|?>>
    <associate|footnote-A.1.9|<tuple|A.1.9|?>>
    <associate|footnote-A.2.1|<tuple|A.2.1|43>>
    <associate|footnote-A.2.2|<tuple|A.2.2|43>>
    <associate|footnote-A.2.3|<tuple|A.2.3|44>>
    <associate|footnote-A.2.4|<tuple|A.2.4|44>>
    <associate|footnote-A.2.5|<tuple|A.2.5|44>>
    <associate|footnote-A.2.6|<tuple|A.2.6|?>>
    <associate|footnr-1|<tuple|1|41>>
    <associate|footnr-A.1.1|<tuple|A.1.1|41>>
    <associate|footnr-A.1.10|<tuple|A.1.10|?>>
    <associate|footnr-A.1.2|<tuple|A.1.2|41>>
    <associate|footnr-A.1.3|<tuple|A.1.3|42>>
    <associate|footnr-A.1.4|<tuple|A.1.4|42>>
    <associate|footnr-A.1.5|<tuple|A.1.5|42>>
    <associate|footnr-A.1.6|<tuple|A.1.6|43>>
    <associate|footnr-A.1.7|<tuple|A.1.7|43>>
    <associate|footnr-A.1.8|<tuple|A.1.8|?>>
    <associate|footnr-A.1.9|<tuple|A.1.9|?>>
    <associate|footnr-A.2.1|<tuple|A.2.1|43>>
    <associate|footnr-A.2.2|<tuple|A.2.2|43>>
    <associate|footnr-A.2.3|<tuple|A.2.3|44>>
    <associate|footnr-A.2.4|<tuple|A.2.4|44>>
    <associate|footnr-A.2.5|<tuple|A.2.5|44>>
    <associate|footnr-A.2.6|<tuple|A.2.6|?>>
    <associate|gnupg_201411_v2.1-release-notes|<tuple|A.1.7|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|Appendix
      A<space|2spc>How to use <with|font-shape|<quote|small-caps>|GnuPG>>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      A.1<space|2spc>Terms and Definitions
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2>

      A.2<space|2spc>Useful Commands <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-3>

      <with|par-left|<quote|1tab>|A.2.1<space|2spc>Obtaining keys
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-4>>

      <with|par-left|<quote|2tab>|A.2.1.1<space|2spc>Import a public key
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-5>>

      <with|par-left|<quote|2tab>|A.2.1.2<space|2spc>Download from a
      keyserver <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-6>>

      <with|par-left|<quote|1tab>|A.2.2<space|2spc>Analyzing keys
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-7>>

      <with|par-left|<quote|2tab>|A.2.2.1<space|2spc>View public key
      fingerprint <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-8>>

      <with|par-left|<quote|1tab>|A.2.3<space|2spc>Sending keys
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-9>>

      <with|par-left|<quote|2tab>|A.2.3.1<space|2spc>Export public key
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-10>>

      <with|par-left|<quote|2tab>|A.2.3.2<space|2spc>Upload public key
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-11>>

      <with|par-left|<quote|1tab>|A.2.4<space|2spc>Creating keys
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-12>>

      <with|par-left|<quote|2tab>|A.2.4.1<space|2spc>Using default settings
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-13>>

      <with|par-left|<quote|2tab>|A.2.4.2<space|2spc>With subkeys
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-14>>
    </associate>
  </collection>
</auxiliary>