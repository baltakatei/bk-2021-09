<TeXmacs|2.1.1>

<style|<tuple|book|style-bk>>

<\body>
  <doc-data|<doc-title|Notable Public Keys>|<doc-misc|Version
  <verbatim|0.4.0>>|<doc-author|<author-data|<author-name|Steven Baltakatei
  Sandoval>|<author-homepage|https://reboil.com>|<author-email|baltakatei@gmail.com>>>>

  <include|c0.tm>

  <\table-of-contents|toc>
    Front Matter <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-1>

    <vspace*|1fn><with|font-series|bold|math-font-series|bold|1<space|2spc>Trust
    through Stories> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-2><vspace|0.5fn>

    1.1<space|2spc>Summary <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-3>

    1.2<space|2spc>Background <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-4>

    1.3<space|2spc>Purpose <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-5>

    <vspace*|1fn><with|font-series|bold|math-font-series|bold|2<space|2spc>List
    of Public Keys> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-6><vspace|0.5fn>

    2.1<space|2spc><with|font-shape|small-caps|Bitcoin Core>
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-7>

    <with|par-left|1tab|2.1.1<space|2spc>Background
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-8>>

    <with|par-left|1tab|2.1.2<space|2spc>History
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-15>>

    <with|par-left|1tab|2.1.3<space|2spc>Public Key Details
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-16>>

    <with|par-left|2tab|2.1.3.1<space|2spc>Binary Signing Keys (v25.0)
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-17>>

    <with|par-left|2tab|2.1.3.2<space|2spc>Binary Signing Keys (v23.0)
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-19>>

    <with|par-left|2tab|2.1.3.3<space|2spc>Binary Signing Keys (v22.0)
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-21>>

    <with|par-left|2tab|2.1.3.4<space|2spc>Binary Signing Key
    (v0.11.0\Uv0.21.2) (<with|hmagnified-factor|0.75|color|brown|<hgroup|<with|font-effects|hmagnify=0.75|<with|font-family|tt|language|verbatim|90C8
    019E 36C2 E964>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-23>>

    <with|par-left|2tab|2.1.3.5<space|2spc>Binary Signing Key
    (v0.9.3\Uv0.10.2) (<with|hmagnified-factor|0.75|color|brown|<hgroup|<with|font-effects|hmagnify=0.75|<with|font-family|tt|language|verbatim|7481
    0B01 2346 C9A6>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-25>>

    <with|par-left|2tab|2.1.3.6<space|2spc>Binary Signing Key
    (<with|font-shape|small-caps|v0.8.6>\Uv0.9.2.1)
    (<with|hmagnified-factor|0.75|color|brown|<hgroup|<with|font-effects|hmagnify=0.75|<with|font-family|tt|language|verbatim|29D9
    EE6B 1FC7 30C1>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-27>>

    <with|par-left|2tab|2.1.3.7<space|2spc><with|font-shape|small-caps|Satoshi
    Nakamoto> (<with|hmagnified-factor|0.75|color|brown|<hgroup|<with|font-effects|hmagnify=0.75|<with|font-family|tt|language|verbatim|18C0
    9E86 5EC9 48A1>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-29>>

    2.2<space|2spc><with|font-shape|small-caps|Cryptomator>
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-34>

    <with|par-left|1tab|2.2.1<space|2spc>Background
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-35>>

    <with|par-left|1tab|2.2.2<space|2spc>History
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-41>>

    <with|par-left|1tab|2.2.3<space|2spc>Public Key Details
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-42>>

    <with|par-left|2tab|2.2.3.1<space|2spc>Binary signing key (v1.5.8\U )
    (<with|hmagnified-factor|0.75|color|brown|<hgroup|<with|font-effects|hmagnify=0.75|<with|font-family|tt|language|verbatim|615D
    449F E6E6 A235>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-43>>

    <with|par-left|2tab|2.2.3.2<space|2spc>Binary signing key ( \Uv1.5.7)
    (<with|hmagnified-factor|0.75|color|brown|<hgroup|<with|font-effects|hmagnify=0.75|<with|font-family|tt|language|verbatim|509C
    9D63 34C8 0F11>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-45>>

    2.3<space|2spc><with|font-shape|small-caps|Debian>
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-48>

    <with|par-left|1tab|2.3.1<space|2spc>Background
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-49>>

    <with|par-left|1tab|2.3.2<space|2spc>History
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-52>>

    <with|par-left|1tab|2.3.3<space|2spc>Public Key Details
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-55>>

    <with|par-left|2tab|2.3.3.1<space|2spc>Installation Image Signature Keys
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-56>>

    <with|par-left|2tab|2.3.3.2<space|2spc>Verbose key details
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-58>>

    <with|par-left|4tab|Key 1999-01-30 (<with|hmagnified-factor|0.75|color|brown|<hgroup|<with|font-effects|hmagnify=0.75|<with|font-family|tt|language|verbatim|7C3B
    7970 88C7 C1F7>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-59><vspace|0.15fn>>

    <with|par-left|4tab|Key 2000-09-16 (<with|hmagnified-factor|0.75|color|brown|<hgroup|<with|font-effects|hmagnify=0.75|<with|font-family|tt|language|verbatim|72FD
    C205 F6A3 2A8E>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-61><vspace|0.15fn>>

    <with|par-left|4tab|Key 2004-06-20 (<with|hmagnified-factor|0.75|color|brown|<hgroup|<with|font-effects|hmagnify=0.75|<with|font-family|tt|language|verbatim|F82E
    5CC0 4B2B 2B9E>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-62><vspace|0.15fn>>

    <with|par-left|4tab|Key 2009-05-21 (<with|hmagnified-factor|0.75|color|brown|<hgroup|<with|font-effects|hmagnify=0.75|<with|font-family|tt|language|verbatim|39BE
    2D72 5CEE 3195>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-63><vspace|0.15fn>>

    <with|par-left|4tab|Key 2009-10-03 (<with|hmagnified-factor|0.75|color|brown|<hgroup|<with|font-effects|hmagnify=0.75|<with|font-family|tt|language|verbatim|9880
    21A9 64E6 EA7D>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-64><vspace|0.15fn>>

    <with|par-left|4tab|Key 2011-01-05 (<with|hmagnified-factor|0.75|color|brown|<hgroup|<with|font-effects|hmagnify=0.75|<with|font-family|tt|language|verbatim|DA87
    E80D 6294 BE9B>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-65><vspace|0.15fn>>

    <with|par-left|4tab|Key 2011-03-09 (<with|hmagnified-factor|0.75|color|brown|<hgroup|<with|font-effects|hmagnify=0.75|<with|font-family|tt|language|verbatim|6F95
    B499 6CA7 B5A6>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-66><vspace|0.15fn>>

    <with|par-left|4tab|Key 2013-05-06 (<with|hmagnified-factor|0.75|color|brown|<hgroup|<with|font-effects|hmagnify=0.75|<with|font-family|tt|language|verbatim|510A
    D6B9 AD11 CF6A>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-67><vspace|0.15fn>>

    <with|par-left|4tab|Key 2014-01-03 (<with|hmagnified-factor|0.75|color|brown|<hgroup|<with|font-effects|hmagnify=0.75|<with|font-family|tt|language|verbatim|1239
    00F2 A9B2 6DF5>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-68><vspace|0.15fn>>

    <with|par-left|4tab|Key 2014-04-15 (<with|hmagnified-factor|0.75|color|brown|<hgroup|<with|font-effects|hmagnify=0.75|<with|font-family|tt|language|verbatim|4246
    8F40 09EA 8AC3>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-69><vspace|0.15fn>>

    2.4<space|2spc><with|font-shape|small-caps|Electrum>
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-71>

    <with|par-left|1tab|2.4.1<space|2spc>Background
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-72>>

    <with|par-left|1tab|2.4.2<space|2spc>History
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-79>>

    <with|par-left|1tab|2.4.3<space|2spc>Public Key Details
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-80>>

    <with|par-left|2tab|2.4.3.1<space|2spc><with|font-shape|small-caps|Thomas
    Voegtlin> signing key (<with|font-shape|small-caps|Electrum>
    v1.7\U<space|1em>) (<with|hmagnified-factor|0.75|color|brown|<hgroup|<with|font-effects|hmagnify=0.75|<with|font-family|tt|language|verbatim|2BD5
    824B 7F94 70E6>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-81>>

    <with|par-left|2tab|2.4.3.2<space|2spc><with|font-shape|small-caps|Stephan
    \PEmzy\Q Oeste> signing key (<with|font-shape|small-caps|Electrum>
    v4.1.5\U<space|1em>) (<with|hmagnified-factor|0.75|color|brown|<hgroup|<with|font-effects|hmagnify=0.75|<with|font-family|tt|language|verbatim|2EBB
    056F D847 F8A7>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-83>>

    <with|par-left|2tab|2.4.3.3<space|2spc><with|font-shape|small-caps|SomberNight>
    signing key (<with|font-shape|small-caps|Electrum> v3.2.0\U<space|1em>)
    (<with|hmagnified-factor|0.75|color|brown|<hgroup|<with|font-effects|hmagnify=0.75|<with|font-family|tt|language|verbatim|CA9E
    EEC4 3DF9 11DC>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-85>>

    <with|par-left|2tab|2.4.3.4<space|2spc><with|font-shape|small-caps|SomberNight>
    signing key (<with|font-shape|small-caps|ElectrumX>)
    (<with|hmagnified-factor|0.75|color|brown|<hgroup|<with|font-effects|hmagnify=0.75|<with|font-family|tt|language|verbatim|E7B7
    48CD AF5E 5ED9>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-87>>

    2.5<space|2spc><with|font-shape|small-caps|Element>
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-90>

    <with|par-left|1tab|2.5.1<space|2spc>Background
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-91>>

    <with|par-left|1tab|2.5.2<space|2spc>History
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-97>>

    <with|par-left|1tab|2.5.3<space|2spc>Public Key Details
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-98>>

    <with|par-left|2tab|2.5.3.1<space|2spc>Signing key (2019\U)
    (<with|hmagnified-factor|0.75|color|brown|<hgroup|<with|font-effects|hmagnify=0.75|<with|font-family|tt|language|verbatim|D7B0
    B669 41D0 1538>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-99>>

    2.6<space|2spc><with|font-shape|small-caps|F-Droid>
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-102>

    <with|par-left|1tab|2.6.1<space|2spc>Background
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-103>>

    <with|par-left|1tab|2.6.2<space|2spc>History
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-109>>

    <with|par-left|1tab|2.6.3<space|2spc>Public Key Details
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-110>>

    <with|par-left|2tab|2.6.3.1<space|2spc><with|font-shape|small-caps|Android>
    client binary release signing key (2017\U )
    (<with|hmagnified-factor|0.75|color|brown|<hgroup|<with|font-effects|hmagnify=0.75|<with|font-family|tt|language|verbatim|41E7
    044E 1DBA 2E89>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-111>>

    <with|par-left|2tab|2.6.3.2<space|2spc><with|font-shape|small-caps|Android>
    client <with|font-shape|small-caps|Git> repository signing key (2017\U )
    (<with|hmagnified-factor|0.75|color|brown|<hgroup|<with|font-effects|hmagnify=0.75|<with|font-family|tt|language|verbatim|74DC
    A8A3 6C52 F833>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-113>>

    <with|par-left|2tab|2.6.3.3<space|2spc><with|font-shape|small-caps|Android>
    client <with|font-shape|small-caps|Git> repository signing key (2015\U )
    (<with|hmagnified-factor|0.75|color|brown|<hgroup|<with|font-effects|hmagnify=0.75|<with|font-family|tt|language|verbatim|E9E2
    8DEA 00AA 5556>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-115>>

    <with|par-left|2tab|2.6.3.4<space|2spc><with|font-shape|small-caps|Android>
    client APK signing key (2010\U2023) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-117>>

    2.7<space|2spc><with|font-shape|small-caps|Freedombox>
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-119>

    <with|par-left|1tab|2.7.1<space|2spc>Background
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-120>>

    <with|par-left|1tab|2.7.2<space|2spc>History
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-126>>

    <with|par-left|1tab|2.7.3<space|2spc>Public Key Details
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-127>>

    <with|par-left|2tab|2.7.3.1<space|2spc>Signing key (2015\U2019)
    (<with|hmagnified-factor|0.75|color|brown|<hgroup|<with|font-effects|hmagnify=0.75|<with|font-family|tt|language|verbatim|36C3
    6144 0C9B C971>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-128>>

    <with|par-left|2tab|2.7.3.2<space|2spc>Signing key (2016\U2017)
    (<with|hmagnified-factor|0.75|color|brown|<hgroup|<with|font-effects|hmagnify=0.75|<with|font-family|tt|language|verbatim|77C0
    C75E 7B65 0808>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-130>>

    <with|par-left|2tab|2.7.3.3<space|2spc>Signing key (2018\U2022)
    (<with|hmagnified-factor|0.75|color|brown|<hgroup|<with|font-effects|hmagnify=0.75|<with|font-family|tt|language|verbatim|5D41
    53D6 FE18 8FC8>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-132>>

    2.8<space|2spc><with|font-shape|small-caps|GitHub>
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-135>

    <with|par-left|1tab|2.8.1<space|2spc>Background
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-136>>

    <with|par-left|1tab|2.8.2<space|2spc>History
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-139>>

    <with|par-left|1tab|2.8.3<space|2spc>Public Key Details
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-143>>

    <with|par-left|2tab|2.8.3.1<space|2spc>Web-flow commit signing
    (<with|hmagnified-factor|0.75|color|brown|<hgroup|<with|font-effects|hmagnify=0.75|<with|font-family|tt|language|verbatim|4AEE
    18F8 3AFD EB23>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-144>>

    2.9<space|2spc><with|font-shape|small-caps|GnuPG>
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-147>

    <with|par-left|1tab|2.9.1<space|2spc>Background
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-148>>

    <with|par-left|1tab|2.9.2<space|2spc>History
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-154>>

    <with|par-left|1tab|2.9.3<space|2spc>Public Key Details
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-155>>

    <with|par-left|2tab|2.9.3.1<space|2spc>Release signing key -
    <with|font-shape|small-caps|Werner Koch> (2020\U)
    (<with|hmagnified-factor|0.75|color|brown|<hgroup|<with|font-effects|hmagnify=0.75|<with|font-family|tt|language|verbatim|5288
    97B8 2640 3ADA>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-157>>

    <with|par-left|2tab|2.9.3.2<space|2spc>Release signing key -
    <with|font-shape|small-caps|Niibe Yutaka> (2021\U)
    (<with|hmagnified-factor|0.75|color|brown|<hgroup|<with|font-effects|hmagnify=0.75|<with|font-family|tt|language|verbatim|E98E
    9B2D 19C6 C8BD>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-159>>

    <with|par-left|2tab|2.9.3.3<space|2spc>Release signing key -
    <with|font-shape|small-caps|Andre Heinecke> (2017\U)
    (<with|hmagnified-factor|0.75|color|brown|<hgroup|<with|font-effects|hmagnify=0.75|<with|font-family|tt|language|verbatim|BCEF
    7E29 4B09 2E28>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-161>>

    <with|par-left|2tab|2.9.3.4<space|2spc>Release signing key -
    <with|font-shape|small-caps|GnuPG.com> (2021\U)
    (<with|hmagnified-factor|0.75|color|brown|<hgroup|<with|font-effects|hmagnify=0.75|<with|font-family|tt|language|verbatim|549E
    695E 905B A208>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-163>>

    2.10<space|2spc><with|font-shape|small-caps|Inkscape>
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-166>

    <with|par-left|1tab|2.10.1<space|2spc>Background
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-167>>

    <with|par-left|1tab|2.10.2<space|2spc>History
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-169>>

    <with|par-left|1tab|2.10.3<space|2spc>Public Key Details
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-170>>

    <with|par-left|2tab|2.10.3.1<space|2spc>Signing key ( v0.92\Uv1.2)
    (<with|hmagnified-factor|0.75|color|brown|<hgroup|<with|font-effects|hmagnify=0.75|<with|font-family|tt|language|verbatim|5FCB
    204E F882 B07A>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-171>>

    2.11<space|2spc><with|font-shape|small-caps|KeePassXC>
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-174>

    <with|par-left|1tab|2.11.1<space|2spc>Background
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-175>>

    <with|par-left|1tab|2.11.2<space|2spc>History
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-178>>

    <with|par-left|1tab|2.11.3<space|2spc>Public Key Details
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-179>>

    <with|par-left|2tab|2.11.3.1<space|2spc>Signing key (2017\U )
    (<with|hmagnified-factor|0.75|color|brown|<hgroup|<with|font-effects|hmagnify=0.75|<with|font-family|tt|language|verbatim|CFB4
    C216 6397 D0D2>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-180>>

    <with|par-left|2tab|2.11.3.2<space|2spc>Developer key (2021)
    (<with|hmagnified-factor|0.75|color|brown|<hgroup|<with|font-effects|hmagnify=0.75|<with|font-family|tt|language|verbatim|CFB4
    C216 6397 D0D2>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-182>>

    2.12<space|2spc><with|font-shape|small-caps|Qubes OS>
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-185>

    <with|par-left|1tab|2.12.1<space|2spc>Background
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-186>>

    <with|par-left|1tab|2.12.2<space|2spc>History
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-191>>

    <with|par-left|1tab|2.12.3<space|2spc>Public Key Details
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-192>>

    <with|par-left|2tab|2.12.3.1<space|2spc>Qubes Master Signing Key
    (<with|hmagnified-factor|0.75|color|brown|<hgroup|<with|font-effects|hmagnify=0.75|<with|font-family|tt|language|verbatim|<with|font-family|tt|DDFA
    1A3E 3687 9494>>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-193>>

    <with|par-left|2tab|2.12.3.2<space|2spc>Release 4 Signing Key
    (<with|hmagnified-factor|0.75|color|brown|<hgroup|<with|font-effects|hmagnify=0.75|<with|font-family|tt|language|verbatim|1848
    792F 9E27 95E9>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-195>>

    <with|par-left|2tab|2.12.3.3<space|2spc>Release 3 Signing Key
    (<with|hmagnified-factor|0.75|color|brown|<hgroup|<with|font-effects|hmagnify=0.75|<with|font-family|tt|language|verbatim|CB11
    CA1D 03FA 5082>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-197>>

    <with|par-left|2tab|2.12.3.4<space|2spc>Release 2 Signing Key
    (<with|hmagnified-factor|0.75|color|brown|<hgroup|<with|font-effects|hmagnify=0.75|<with|font-family|tt|language|verbatim|0C73
    B9D4 0A40 E458>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-199>>

    <with|par-left|2tab|2.12.3.5<space|2spc>Release 1 Signing Key
    (<with|hmagnified-factor|0.75|color|brown|<hgroup|<with|font-effects|hmagnify=0.75|<with|font-family|tt|language|verbatim|EA01
    201B 2110 93A7>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-201>>

    2.13<space|2spc><with|font-shape|small-caps|RaspiBlitz>
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-204>

    <with|par-left|1tab|2.13.1<space|2spc>Background
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-205>>

    <with|par-left|1tab|2.13.2<space|2spc>History
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-210>>

    <with|par-left|1tab|2.13.3<space|2spc>Public Key Details
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-212>>

    <with|par-left|2tab|2.13.3.1<space|2spc><with|font-shape|small-caps|Christian
    \Prootzol\Q Rotzoll> (<with|hmagnified-factor|0.75|color|brown|<hgroup|<with|font-effects|hmagnify=0.75|<with|font-family|tt|language|verbatim|1C73
    060C 7C17 6461>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-213>>

    2.14<space|2spc><with|font-shape|small-caps|Satoshi Labs>
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-216>

    <with|par-left|1tab|2.14.1<space|2spc>Background
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-217>>

    <with|par-left|1tab|2.14.2<space|2spc>History
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-225>>

    <with|par-left|1tab|2.14.3<space|2spc>Public Key Details
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-226>>

    <with|par-left|2tab|2.14.3.1<space|2spc><with|font-shape|small-caps|Pavol
    Rusn�k> (<with|hmagnified-factor|0.75|color|brown|<hgroup|<with|font-effects|hmagnify=0.75|<with|font-family|tt|language|verbatim|91F3
    B339 B9A0 2A3D>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-227>>

    <with|par-left|2tab|2.14.3.2<space|2spc>2020 Signing Key
    (<with|hmagnified-factor|0.75|color|brown|<hgroup|<with|font-effects|hmagnify=0.75|<with|font-family|tt|language|verbatim|26A3
    A566 62F0 E7E2>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-229>>

    <with|par-left|2tab|2.14.3.3<space|2spc>2021 Signing Key
    (<with|hmagnified-factor|0.75|color|brown|<hgroup|<with|font-effects|hmagnify=0.75|<with|font-family|tt|language|verbatim|E21B
    6950 A2EC B65C>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-231>>

    2.15<space|2spc><with|font-shape|small-caps|Tails>
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-234>

    <with|par-left|1tab|2.15.1<space|2spc>Background
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-235>>

    <with|par-left|1tab|2.15.2<space|2spc>History
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-242>>

    <with|par-left|1tab|2.15.3<space|2spc>Public Key Details
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-243>>

    <with|par-left|2tab|2.15.3.1<space|2spc>Signing key (2015\U<space|1em>)
    (<with|hmagnified-factor|0.75|color|brown|<hgroup|<with|font-effects|hmagnify=0.75|<with|font-family|tt|language|verbatim|DBB8
    02B2 58AC D84F>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-244>>

    <with|par-left|2tab|2.15.3.2<space|2spc>Mailing list key
    (2009\U<space|1em>) (<with|hmagnified-factor|0.75|color|brown|<hgroup|<with|font-effects|hmagnify=0.75|<with|font-family|tt|language|verbatim|1D29
    75ED F93E 735F>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-246>>

    <with|par-left|2tab|2.15.3.3<space|2spc>Signing key (2010\U2015)
    (<with|hmagnified-factor|0.75|color|brown|<hgroup|<with|font-effects|hmagnify=0.75|<with|font-family|tt|language|verbatim|1202
    821C BE2C D9C1>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-248>>

    2.16<space|2spc><with|font-shape|small-caps|Tor Browser>
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-251>

    <with|par-left|1tab|2.16.1<space|2spc>Background
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-252>>

    <with|par-left|1tab|2.16.2<space|2spc>History
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-255>>

    <with|par-left|1tab|2.16.3<space|2spc>Public Key Details
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-257>>

    <with|par-left|2tab|2.16.3.1<space|2spc>Release Signing Key (2015\U )
    (<with|hmagnified-factor|0.75|color|brown|<hgroup|<with|font-effects|hmagnify=0.75|<with|font-family|tt|language|verbatim|4E2C
    6E87 9329 8290>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-258>>

    2.17<space|2spc><with|font-shape|small-caps|Trisquel>
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-261>

    <with|par-left|1tab|2.17.1<space|2spc>Background
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-262>>

    <with|par-left|1tab|2.17.2<space|2spc>History
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-265>>

    <with|par-left|1tab|2.17.3<space|2spc>Public Key Details
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-266>>

    <with|par-left|2tab|2.17.3.1<space|2spc>Signing key (Trisquel 11 Aramo)
    (<with|hmagnified-factor|0.75|color|brown|<hgroup|<with|font-effects|hmagnify=0.75|<with|font-family|tt|language|verbatim|F5DA
    AAF7 4AD4 C938>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-267>>

    2.18<space|2spc><with|font-shape|small-caps|VeraCrypt>
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-270>

    <with|par-left|1tab|2.18.1<space|2spc>Background
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-271>>

    <with|par-left|1tab|2.18.2<space|2spc>History
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-274>>

    <with|par-left|1tab|2.18.3<space|2spc>Public Key Details
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-276>>

    <with|par-left|2tab|2.18.3.1<space|2spc>Signing key (2018\U )
    (<with|hmagnified-factor|0.75|color|brown|<hgroup|<with|font-effects|hmagnify=0.75|<with|font-family|tt|language|verbatim|821A
    CD02 680D 16DE>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-277>>

    <with|par-left|2tab|2.18.3.2<space|2spc>Signing key (2014\U2018)
    (<with|hmagnified-factor|0.75|color|brown|<hgroup|<with|font-effects|hmagnify=0.75|<with|font-family|tt|language|verbatim|EB55
    9C7C 54DD D393>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-279>>

    2.19<space|2spc><with|font-shape|small-caps|Youtube-dl>
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-283>

    <with|par-left|1tab|2.19.1<space|2spc>Background
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-284>>

    <with|par-left|1tab|2.19.2<space|2spc>History
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-287>>

    <with|par-left|1tab|2.19.3<space|2spc>Public Key Details
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-289>>

    <with|par-left|2tab|2.19.3.1<space|2spc>Binary signing key.
    <with|font-shape|small-caps|Sergey M.>
    (<with|hmagnified-factor|0.75|color|brown|<hgroup|<with|font-effects|hmagnify=0.75|<with|font-family|tt|language|verbatim|2C39
    3E0F 18A9 236D>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-290>>

    <with|par-left|2tab|2.19.3.2<space|2spc>Binary signing key.
    <with|font-shape|small-caps|Philipp Hagemeister>
    (<with|hmagnified-factor|0.75|color|brown|<hgroup|<with|font-effects|hmagnify=0.75|<with|font-family|tt|language|verbatim|F5EA
    B582 FAFB 085C>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-292>>

    <with|par-left|2tab|2.19.3.3<space|2spc>Binary signing key.
    <with|font-shape|small-caps|Philipp Hagemeister>
    (<with|hmagnified-factor|0.75|color|brown|<hgroup|<with|font-effects|hmagnify=0.75|<with|font-family|tt|language|verbatim|DB4B
    54CB A482 6A18>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-295>>

    <with|par-left|2tab|2.19.3.4<space|2spc>Binary signing key.
    <with|font-shape|small-caps|Filippo Valsorda>
    (<with|hmagnified-factor|0.75|color|brown|<hgroup|<with|font-effects|hmagnify=0.75|<with|font-family|tt|language|verbatim|EBF0
    1804 BCF0 5F6B>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-298>>

    2.20<space|2spc><with|font-shape|small-caps|Yt-dlp>
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-302>

    <with|par-left|1tab|2.20.1<space|2spc>Background
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-303>>

    <with|par-left|1tab|2.20.2<space|2spc>History
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-306>>

    <with|par-left|1tab|2.20.3<space|2spc>Public Key Details
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-307>>

    <with|par-left|2tab|2.20.3.1<space|2spc>Signing key (2023\U )
    (<with|hmagnified-factor|0.75|color|brown|<hgroup|<with|font-effects|hmagnify=0.75|<with|font-family|tt|language|verbatim|57CF
    6593 3B5A 7581>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-308>>

    <with|par-left|2tab|2.20.3.2<space|2spc>Commit signing key (2023\U )
    (<with|hmagnified-factor|0.75|color|brown|<hgroup|<with|font-effects|hmagnify=0.75|<with|font-family|tt|language|verbatim|7EEE
    9E1E 817D 0A39>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-310>>

    <vspace*|1fn><with|font-series|bold|math-font-series|bold|Appendix
    A<space|2spc>How to use <with|font-shape|small-caps|GnuPG>>
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-313><vspace|0.5fn>

    A.1<space|2spc>Terms and Definitions <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-314>

    A.2<space|2spc>Useful Commands <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-315>

    <with|par-left|1tab|A.2.1<space|2spc>Obtaining keys
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-316>>

    <with|par-left|2tab|A.2.1.1<space|2spc>Import a public key
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-317>>

    <with|par-left|2tab|A.2.1.2<space|2spc>Download from a keyserver
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-318>>

    <with|par-left|1tab|A.2.2<space|2spc>Analyzing keys
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-319>>

    <with|par-left|2tab|A.2.2.1<space|2spc>View public key fingerprint
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-320>>

    <with|par-left|1tab|A.2.3<space|2spc>Sending keys
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-321>>

    <with|par-left|2tab|A.2.3.1<space|2spc>Export public key
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-322>>

    <with|par-left|2tab|A.2.3.2<space|2spc>Upload public key
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-323>>

    <with|par-left|1tab|A.2.4<space|2spc>Creating keys
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-324>>

    <with|par-left|2tab|A.2.4.1<space|2spc>Using default settings
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-325>>

    <with|par-left|2tab|A.2.4.2<space|2spc>With subkeys
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-326>>

    <vspace*|1fn><with|font-series|bold|math-font-series|bold|Bibliography>
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-327><vspace|0.5fn>

    <vspace*|1fn><with|font-series|bold|math-font-series|bold|Index>
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-328><vspace|0.5fn>
  </table-of-contents>

  <include|c1.tm>

  <include|c2.tm>

  <include|stories/c2-bitcoin-core.tm>

  <include|stories/c2-cryptomator.tm>

  <include|stories/c2-debian.tm>

  <include|stories/c2-electrum.tm>

  <include|stories/c2-element.tm>

  <include|stories/c2-fdroid.tm>

  <include|stories/c2-freedombox.tm>

  <include|stories/c2-github.tm>

  <include|stories/c2-gnupg.tm>

  <include|stories/c2-inkscape.tm>

  <include|stories/c2-keepassxc.tm>

  <include|stories/c2-qubesos.tm>

  <include|stories/c2-raspiblitz.tm>

  <include|stories/c2-satoshi-labs.tm>

  <include|stories/c2-tails.tm>

  <include|stories/c2-tor-browser.tm>

  <include|stories/c2-trisquel.tm>

  <include|stories/c2-veracrypt.tm>

  <include|stories/c2-youtube-dl.tm>

  <include|stories/c2-yt-dlp.tm>

  <include|a-how-gnupg.tm>

  <\bibliography|bib|tm-plain|book.bib>
    <\bib-list|2>
      <bibitem*|1><label|bib-nyt_20110215_moglen-founds-fbx>Jim Dwyer.
      <newblock>Decentralizing the Internet So Big Brother Can't Find You.
      <newblock><with|font-shape|italic|New York Times>, February
      2018.<newblock>

      <bibitem*|2><label|bib-nyt_20180604_ms-buys-github>Steve Lohr.
      <newblock>Microsoft Buys GitHub for $7.5 Billion, Moving to Grow in
      Coding's New Era. <newblock><with|font-shape|italic|New York Times>,
      June 2018.<newblock>
    </bib-list>
  </bibliography>

  <\the-index|idx>
    <index+1*|DSA, algorithm>

    <index+2|DSA, algorithm|weakness|<pageref|auto-31>>

    <index+1*|Keys>

    <index+2*|Keys|Organization>

    <index+3*|Keys|Organization|Riot.im>

    <index+4|Keys|Organization|Riot.im|<with|font-base-size|8|<with|font-family|tt|language|verbatim|0xD7B0B66941D01538>>|<pageref|auto-100>>

    <index+2*|Keys|Organizations>

    <index+3*|Keys|Organizations|Cryptomator>

    <index+4|Keys|Organizations|Cryptomator|<with|font-base-size|8|<with|font-family|tt|language|verbatim|0x509C9D6334C80F11>>|<pageref|auto-46>>

    <index+4|Keys|Organizations|Cryptomator|<with|font-base-size|8|<with|font-family|tt|language|verbatim|0x615D449FE6E6A235>>|<pageref|auto-44>>

    <index+3*|Keys|Organizations|F-Droid>

    <index+4*|Keys|Organizations|F-Droid|Signing>

    <index+5|Keys|Organizations|F-Droid|Signing|<with|font-base-size|8|<with|font-family|tt|language|verbatim|0x41E7044E1DBA2E89>>
    (2014\U)|<pageref|auto-112>>

    <index+3*|Keys|Organizations|GitHub>

    <index+4|Keys|Organizations|GitHub|<with|font-base-size|8|<with|font-family|tt|language|verbatim|0x4AEE18F83AFDEB23>>|<pageref|auto-145>>

    <index+3*|Keys|Organizations|GnuPG.com>

    <index+4|Keys|Organizations|GnuPG.com|<with|font-base-size|8|<with|font-family|tt|language|verbatim|0x549E695E905BA208>>|<pageref|auto-164>>

    <index+3*|Keys|Organizations|<with|font-shape|small-caps|KeePassXC>>

    <index+4|Keys|Organizations|<with|font-shape|small-caps|KeePassXC>|<with|font-base-size|8|<with|font-family|tt|language|verbatim|0x105D8D57BB9746BD>>|<pageref|auto-183>>

    <index+4|Keys|Organizations|<with|font-shape|small-caps|KeePassXC>|<with|font-base-size|8|<with|font-family|tt|language|verbatim|0xCFB4C2166397D0D2>>|<pageref|auto-181>>

    <index+3*|Keys|Organizations|Qubes OS>

    <index+4|Keys|Organizations|Qubes OS|<with|font-base-size|8|<with|font-family|tt|language|verbatim|0xDDFA1A3E36879494>>|<pageref|auto-194>>

    <index+3*|Keys|Organizations|Qubes OS (Release 1 Signing Key)>

    <index+4|Keys|Organizations|Qubes OS (Release 1 Signing
    Key)|<with|font-base-size|8|<with|font-family|tt|language|verbatim|0xEA01201B211093A7>>|<pageref|auto-202>>

    <index+3*|Keys|Organizations|Qubes OS (Release 2 Signing Key)>

    <index+4|Keys|Organizations|Qubes OS (Release 2 Signing
    Key)|<with|font-base-size|8|<with|font-family|tt|language|verbatim|0x0C73B9D40A40E458>>|<pageref|auto-200>>

    <index+3*|Keys|Organizations|Qubes OS (Release 3 Signing Key)>

    <index+4|Keys|Organizations|Qubes OS (Release 3 Signing
    Key)|<with|font-base-size|8|<with|font-family|tt|language|verbatim|0xCB11CA1D03FA5082>>|<pageref|auto-198>>

    <index+3*|Keys|Organizations|Qubes OS (Release 4 Signing Key)>

    <index+4|Keys|Organizations|Qubes OS (Release 4 Signing
    Key)|<with|font-base-size|8|<with|font-family|tt|language|verbatim|0x1848792F9E2795E9>>|<pageref|auto-196>>

    <index+3*|Keys|Organizations|Satoshi Labs>

    <index+4*|Keys|Organizations|Satoshi Labs|Signing>

    <index+5|Keys|Organizations|Satoshi Labs|Signing|<with|font-base-size|8|<with|font-family|tt|language|verbatim|0x26A3A56662F0E7E2>>
    (2020)|<pageref|auto-230>>

    <index+5|Keys|Organizations|Satoshi Labs|Signing|<with|font-base-size|8|<with|font-family|tt|language|verbatim|0xE21B6950A2ECB65C>>
    (2021)|<pageref|auto-232>>

    <index+3*|Keys|Organizations|Tails>

    <index+4*|Keys|Organizations|Tails|Mailing list>

    <index+5|Keys|Organizations|Tails|Mailing
    list|<with|font-base-size|8|<with|font-family|tt|language|verbatim|0x1D2975EDF93E735F>>
    (2009\U )|<pageref|auto-247>>

    <index+4*|Keys|Organizations|Tails|Signing>

    <index+5|Keys|Organizations|Tails|Signing|<with|font-base-size|8|<with|font-family|tt|language|verbatim|0x1202821CBE2CD9C1>>
    (2010\U2015)|<pageref|auto-249>>

    <index+5|Keys|Organizations|Tails|Signing|<with|font-base-size|8|<with|font-family|tt|language|verbatim|0xDBB802B258ACD84F>>
    (2015\U )|<pageref|auto-245>>

    <index+3*|Keys|Organizations|Tor Browser>

    <index+4*|Keys|Organizations|Tor Browser|Signing>

    <index+5|Keys|Organizations|Tor Browser|Signing|<with|font-base-size|8|<with|font-family|tt|language|verbatim|0x4E2C6E8793298290>>
    (2015\U )|<pageref|auto-259>>

    <index+3*|Keys|Organizations|Trisquel>

    <index+4|Keys|Organizations|Trisquel|<with|font-base-size|8|<with|font-family|tt|language|verbatim|0xF5DAAAF74AD4C938>>|<pageref|auto-268>>

    <index+3*|Keys|Organizations|Veracrypt>

    <index+4*|Keys|Organizations|Veracrypt|Signing>

    <index+5|Keys|Organizations|Veracrypt|Signing|<with|font-base-size|8|<with|font-family|tt|language|verbatim|0x821ACD02680D16DE>>
    (2018\U )|<pageref|auto-278>>

    <index+5|Keys|Organizations|Veracrypt|Signing|<with|font-base-size|8|<with|font-family|tt|language|verbatim|0xEB559C7C54DDD393>>
    (2014\U2018)|<pageref|auto-280>>

    <index+2*|Keys|People>

    <index+3*|Keys|People|Adapa, Sunil Mohan>

    <index+4|Keys|People|Adapa, Sunil Mohan|<with|font-base-size|8|<with|font-family|tt|language|verbatim|0x36C361440C9BC971>>|<pageref|auto-129>>

    <index+3*|Keys|People|Andresen, Gavin>

    <index+4|Keys|People|Andresen, Gavin|<with|font-base-size|8|<with|font-family|tt|language|verbatim|0x29D9EE6B1FC730C1>>|<pageref|auto-28>>

    <index+3*|Keys|People|Grote, Torsten>

    <index+4|Keys|People|Grote, Torsten|<with|font-base-size|8|<with|font-family|tt|language|verbatim|0x74DCA8A36C52F833>>|<pageref|auto-114>>

    <index+3*|Keys|People|Hagemann, Tobias>

    <index+4|Keys|People|Hagemann, Tobias|<with|font-base-size|8|<with|font-family|tt|language|verbatim|0x69CEFAD519598989>>|<pageref|auto-40>>

    <index+3*|Keys|People|Hagemeister, Philipp>

    <index+4|Keys|People|Hagemeister, Philipp|<with|font-base-size|8|<with|font-family|tt|language|verbatim|0xDB4B54CBA4826A18>>|<pageref|auto-296>>

    <index+4|Keys|People|Hagemeister, Philipp|<with|font-base-size|8|<with|font-family|tt|language|verbatim|0xF5EAB582FAFB085C>>|<pageref|auto-293>>

    <index+3*|Keys|People|Heinecke, Andre>

    <index+4|Keys|People|Heinecke, Andre|<with|font-base-size|8|<with|font-family|tt|language|verbatim|0xBCEF7E294B092E28>>|<pageref|auto-162>>

    <index+3*|Keys|People|Jeanmougin, Marc>

    <index+4|Keys|People|Jeanmougin, Marc|<with|font-base-size|8|<with|font-family|tt|language|verbatim|0x5FCB204EF882B07A>>|<pageref|auto-172>>

    <index+3*|Keys|People|Koch, Werner>

    <index+4|Keys|People|Koch, Werner|<with|font-base-size|8|<with|font-family|tt|language|verbatim|0x528897B826403ADA>>|<pageref|auto-158>>

    <index+3*|Keys|People|D., Sergey>

    <index+4|Keys|People|D., Sergey|<with|font-base-size|8|<with|font-family|tt|language|verbatim|0x2C393E0F18A9236D>>|<pageref|auto-291>>

    <index+3*|Keys|People|Nakamoto, Satoshi>

    <index+4|Keys|People|Nakamoto, Satoshi|<with|font-base-size|8|<with|font-family|tt|language|verbatim|0x18C09E865EC948A1>>|<pageref|auto-30>>

    <index+3*|Keys|People|Oeste, Stephan \Pemzy\Q>

    <index+4|Keys|People|Oeste, Stephan \Pemzy\Q|<with|font-base-size|8|<with|font-family|tt|language|verbatim|0x2EBB056FD847F8A7>>|<pageref|auto-84>>

    <index+3*|Keys|People|pukkandan>

    <index+4|Keys|People|pukkandan|<with|font-base-size|8|<with|font-family|tt|language|verbatim|0x7EEE9E1E817D0A39>>|<pageref|auto-311>>

    <index+3*|Keys|People|Rotzoll, Christian
    \P<with|font-family|tt|language|verbatim|rootzol>\Q>

    <index+4|Keys|People|Rotzoll, Christian
    \P<with|font-family|tt|language|verbatim|rootzol>\Q|<with|font-base-size|8|<with|font-family|tt|language|verbatim|0x1C73060C7C176461>>|<pageref|auto-214>>

    <index+3*|Keys|People|Rusn�k, Pavol \PStick\Q>

    <index+4|Keys|People|Rusn�k, Pavol \PStick\Q|<with|font-base-size|8|<with|font-family|tt|language|verbatim|0x91F3B339B9A02A3D>>|<pageref|auto-228>>

    <index+3*|Keys|People|Sawicki, Simon>

    <index+4|Keys|People|Sawicki, Simon|<with|font-base-size|8|<with|font-family|tt|language|verbatim|0x57CF65933B5A7581>>|<pageref|auto-309>>

    <index+3*|Keys|People|Schrenk, Armin>

    <index+4|Keys|People|Schrenk, Armin|<with|font-base-size|8|<with|font-family|tt|language|verbatim|0x748E55D51F5B3FBC>>|<pageref|auto-39>>

    <index+3*|Keys|People|SomberNight>

    <index+4|Keys|People|SomberNight|<with|font-base-size|8|<with|font-family|tt|language|verbatim|0x0xCA9EEEC43DF911DC>>|<pageref|auto-86>>

    <index+4|Keys|People|SomberNight|<with|font-base-size|8|<with|font-family|tt|language|verbatim|0xE7B748CDAF5E5ED9>>|<pageref|auto-88>>

    <index+3*|Keys|People|Steiner, Hans-Christoph>

    <index+4|Keys|People|Steiner, Hans-Christoph|<with|font-base-size|8|<with|font-family|tt|language|verbatim|0xE9E28DEA00AA5556>>|<pageref|auto-116>>

    <index+3*|Keys|People|Stenzel, Sebastian>

    <index+4|Keys|People|Stenzel, Sebastian|<with|font-base-size|8|<with|font-family|tt|language|verbatim|0x667B866EA8240A09>>|<pageref|auto-38>>

    <index+3*|Keys|People|Valleroy, James>

    <index+4|Keys|People|Valleroy, James|<with|font-base-size|8|<with|font-family|tt|language|verbatim|0x77C0C75E7B650808>>|<pageref|auto-131>,
    <pageref|auto-133>>

    <index+3*|Keys|People|Valsorda, Filippo>

    <index+4|Keys|People|Valsorda, Filippo|<with|font-base-size|8|<with|font-family|tt|language|verbatim|0xEBF01804BCF05F6B>>|<pageref|auto-299>>

    <index+3*|Keys|People|van der Laan, Wladimir J.>

    <index+4|Keys|People|van der Laan, Wladimir
    J.|<with|font-base-size|8|<with|font-family|tt|language|verbatim|0x74810B012346C9A6>>
    (personal; 2011\U )|<pageref|auto-26>>

    <index+4|Keys|People|van der Laan, Wladimir
    J.|<with|font-base-size|8|<with|font-family|tt|language|verbatim|0x90C8019E36C2E964>>
    <with|font-effects|hextended=0.8|(Bitcoin Core;
    2015\U2022)>|<pageref|auto-24>>

    <index+3*|Keys|People|Voegtlin, Thomas>

    <index+4|Keys|People|Voegtlin, Thomas|<with|font-base-size|8|<with|font-family|tt|language|verbatim|0x2BD5824B7F9470E6>>|<pageref|auto-82>>

    <index+3*|Keys|People|Yutaka, Niibe>

    <index+4|Keys|People|Yutaka, Niibe|<with|font-base-size|8|<with|font-family|tt|language|verbatim|0xE98E9B2D19C6C8BD>>|<pageref|auto-160>>

    <index+1|Logical Awesome, LLC|<pageref|auto-140>>

    <index+1|Microsoft|<pageref|auto-138>>

    <index+1*|Organization>

    <index+2|Organization|Amdocs|<pageref|auto-94>>

    <index+1*|Organizations>

    <index+2|Organizations|Bitcoin Foundation|<pageref|auto-32>>

    <index+2|Organizations|Freedombox Foundation|<pageref|auto-124>>

    <index+2|Organizations|Google Play|<pageref|auto-105>>

    <index+2|Organizations|Invisible Things Labs|<pageref|auto-188>>

    <index+2|Organizations|KeePassXC Team|<pageref|auto-177>>

    <index+2|Organizations|QuarksLab|<pageref|auto-275>, <pageref|auto-281>>

    <index+1*|People>

    <index+2|People|Andresen, Gavin|<pageref|auto-13>>

    <index+2|People|Antonopoulos, Andreas|<pageref|auto-211>>

    <index+2|People|Baumann, Daniel|<pageref|auto-54>>

    <index+2|People|Cross, Jonathan|<pageref|auto-142>>

    <index+2|People|Garcia, Ricardo|<pageref|auto-288>>

    <index+2|People|Grote, Torsten|<pageref|auto-108>>

    <index+2|People|Gultnieks, Ciaran|<pageref|auto-106>>

    <index+2|People|Hagemeister, Philipp|<pageref|auto-294>,
    <pageref|auto-297>>

    <index+2|People|Hodgeson, Mathew|<pageref|auto-96>>

    <index+2|People|Idrassi, Mounir|<pageref|auto-273>>

    <index+2|People|Joanna Rutkowska|<pageref|auto-189>>

    <index+2|People|Koch, Werner|<pageref|auto-153>>

    <index+2|People|Le Pape, Amandine|<pageref|auto-95>>

    <index+2|People|M., Sergey|<pageref|auto-286>>

    <index+2|People|Mantinan, Santiago Garcia|<pageref|auto-53>>

    <index+2|People|Marek Marczykowski-G�recki|<pageref|auto-190>>

    <index+2|People|McIntyre, Steve|<pageref|auto-60>>

    <index+2|People|Miles, Gary|<pageref|auto-93>>

    <index+2|People|Moglen, Eben|<pageref|auto-125>>

    <index+2|People|Murdoch, Steven J.|<pageref|auto-256>>

    <index+2|People|Murdock, Ian Ashley|<pageref|auto-51>>

    <index+2|People|Nakamoto, Satoshi|<pageref|auto-11>>

    <index+2|People|Palatinus, Marek \PSlush\Q|<pageref|auto-220>>

    <index+2|People|pukkandan|<pageref|auto-305>>

    <index+2|People|Rodriguez, Ruben|<pageref|auto-264>>

    <index+2|People|Rotzoll, Christian \P<with|font-family|tt|language|verbatim|rootzol>\Q|<pageref|auto-209>>

    <index+2|People|Rusn�k, Pavol \PStick\Q|<pageref|auto-222>>

    <index+2|People|Slush (<with|font-shape|small-caps|Satoshi Labs>
    developer)|<pageref|auto-221>>

    <index+2|People|SomberNight|<pageref|auto-78>>

    <index+2|People|Steiner, Hans-Christoph|<pageref|auto-107>>

    <index+2|People|Stick (<with|font-shape|small-caps|Satoshi Labs>
    developer)|<pageref|auto-223>>

    <index+2|People|Todd, Peter|<pageref|auto-12>>

    <index+2|People|Valsorda, Filippo|<pageref|auto-300>>

    <index+2|People|van der Laan, Wladimir J.|<pageref|auto-14>>

    <index+2|People|Voegtlin, Thomas|<pageref|auto-74>>

    <index+2|People|Vranova, Alena|<pageref|auto-224>>

    <index+2|People|Wanstrath, Chris|<pageref|auto-141>>

    <index+2|People|Zimmermann, Phil|<pageref|auto-152>>

    <index+1|Raspberry Pi|<pageref|auto-123>>

    <index+1*|Software>

    <index+2|Software|Amnesia|<pageref|auto-239>>

    <index+2|Software|Android|<pageref|auto-76>>

    <index+2|Software|App Name|<pageref|auto-150>>

    <index+2|Software|Bitcoin|<pageref|auto-10>, <pageref|auto-208>>

    <index+2|Software|Bitcoin Core|<strong|<pageref|auto-9>>\U<strong|<pageref|auto-33>>,
    <pageref|auto-75>>

    <index+2|Software|Cryptomator|<strong|<pageref|auto-36>>\U<strong|<pageref|auto-47>>>

    <index+2|Software|Debian|<strong|<pageref|auto-50>>\U<strong|<pageref|auto-70>>,
    <pageref|auto-122>, <pageref|auto-237>>

    <index+2|Software|Dropbox|<pageref|auto-37>>

    <index+2|Software|Electrum|<strong|<pageref|auto-73>>\U<strong|<pageref|auto-89>>>

    <index+2|Software|ElectrumX|<pageref|auto-77>>

    <index+2|Software|Element|<strong|<pageref|auto-92>>\U<strong|<pageref|auto-101>>>

    <index+2|Software|F-Droid|<strong|<pageref|auto-104>>\U<strong|<pageref|auto-118>>>

    <index+2|Software|Freedombox|<strong|<pageref|auto-121>>\U<strong|<pageref|auto-134>>>

    <index+2|Software|Gentoo|<pageref|auto-241>>

    <index+2|Software|GitHub|<strong|<pageref|auto-137>>\U<strong|<pageref|auto-146>>>

    <index+2|Software|GnuPG|<strong|<pageref|auto-149>>\U<strong|<pageref|auto-165>>>

    <index+2|Software|Incognito|<pageref|auto-240>>

    <index+2|Software|Inkscape|<strong|<pageref|auto-168>>\U<strong|<pageref|auto-173>>>

    <index+2|Software|KeePassXC|<strong|<pageref|auto-176>>\U<strong|<pageref|auto-184>>>

    <index+2|Software|Lightning Network|<pageref|auto-207>>

    <index+2|Software|OpenPGP|<pageref|auto-151>>

    <index+2|Software|Qubes OS|<strong|<pageref|auto-187>>\U<strong|<pageref|auto-203>>>

    <index+2|Software|RaspiBlitz|<strong|<pageref|auto-206>>\U<strong|<pageref|auto-215>>>

    <index+2|Software|Satoshi Labs|<strong|<pageref|auto-218>>\U<strong|<pageref|auto-233>>>

    <index+2|Software|Tails|<strong|<pageref|auto-236>>\U<strong|<pageref|auto-250>>>

    <index+2|Software|Tor|<pageref|auto-238>>

    <index+2|Software|Tor Browser|<strong|<pageref|auto-253>>\U<strong|<pageref|auto-260>>,
    <pageref|auto-254>>

    <index+2|Software|Trezor|<pageref|auto-219>>

    <index+2|Software|Trisquel|<strong|<pageref|auto-263>>\U<strong|<pageref|auto-269>>>

    <index+2|Software|Veracrypt|<strong|<pageref|auto-272>>\U<strong|<pageref|auto-282>>>

    <index+2|Software|Youtube-dl|<strong|<pageref|auto-285>>\U<strong|<pageref|auto-301>>>

    <index+2|Software|Yt-dlp|<strong|<pageref|auto-304>>\U<strong|<pageref|auto-312>>>
  </the-index>
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
    <associate|preamble|false>
    <associate|project-flag|true>
  </collection>
</initial>

<\attachments>
  <\collection>
    <\associate|bib-bibliography>
      <\db-entry|+2OGmCC541hAS4NeY|article|nyt_20110215_moglen-founds-fbx>
        <db-field|contributor|baltakatei>

        <db-field|modus|imported>

        <db-field|date|1668104521>
      <|db-entry>
        <db-field|author|Jim <name|Dwyer>>

        <db-field|title|Decentralizing the Internet So Big Brother Can't Find
        You>

        <db-field|journal|New York Times>

        <db-field|year|2018>

        <db-field|month|February>
      </db-entry>

      <\db-entry|+2OGmCC541hAS4NeZ|article|nyt_20180604_ms-buys-github>
        <db-field|contributor|baltakatei>

        <db-field|modus|imported>

        <db-field|date|1668104521>
      <|db-entry>
        <db-field|author|Steve <name|Lohr>>

        <db-field|title|Microsoft Buys GitHub for $7.5 Billion, Moving to
        Grow in Coding's New Era>

        <db-field|journal|New York Times>

        <db-field|year|2018>

        <db-field|month|June>

        <db-field||>
      </db-entry>
    </associate>
  </collection>
</attachments>

<\references>
  <\collection>
    <associate|20220513-fbx-sig-dates|<tuple|2.7.5|27|stories/c2-freedombox.tm>>
    <associate|a how-gnupg|<tuple|A|49|a-how-gnupg.tm>>
    <associate|a how-gnupg defs|<tuple|A.1|49|a-how-gnupg.tm>>
    <associate|a how-gnupg useful-cmds|<tuple|A.2|52|a-how-gnupg.tm>>
    <associate|a how-gnupg useful-cmds analyze|<tuple|A.2.2|53|a-how-gnupg.tm>>
    <associate|a how-gnupg useful-cmds analyze
    fpr|<tuple|A.2.2.1|53|a-how-gnupg.tm>>
    <associate|a how-gnupg useful-cmds create|<tuple|A.2.4|54|a-how-gnupg.tm>>
    <associate|a how-gnupg useful-cmds create
    default|<tuple|A.2.4.1|54|a-how-gnupg.tm>>
    <associate|a how-gnupg useful-cmds create
    with-subkeys|<tuple|A.2.4.2|54|a-how-gnupg.tm>>
    <associate|a how-gnupg useful-cmds obtain|<tuple|A.2.1|52|a-how-gnupg.tm>>
    <associate|a how-gnupg useful-cmds obtain
    download|<tuple|A.2.1.2|52|a-how-gnupg.tm>>
    <associate|a how-gnupg useful-cmds obtain
    import|<tuple|A.2.1.1|52|a-how-gnupg.tm>>
    <associate|a how-gnupg useful-cmds send|<tuple|A.2.3|53|a-how-gnupg.tm>>
    <associate|a how-gnupg useful-cmds send
    to-file|<tuple|A.2.3.1|53|a-how-gnupg.tm>>
    <associate|a how-gnupg useful-cmds send
    to-server|<tuple|A.2.3.2|53|a-how-gnupg.tm>>
    <associate|auto-1|<tuple|?|3|c0.tm>>
    <associate|auto-10|<tuple|Software|12|stories/c2-bitcoin-core.tm>>
    <associate|auto-100|<tuple|<tuple|Keys|Organization|Riot.im|0xD7B0B66941D01538>|24|stories/c2-element.tm>>
    <associate|auto-101|<tuple|<tuple|Software|Element>|24|stories/c2-element.tm>>
    <associate|auto-102|<tuple|2.6|25|stories/c2-fdroid.tm>>
    <associate|auto-103|<tuple|2.6.1|25|stories/c2-fdroid.tm>>
    <associate|auto-104|<tuple|<tuple|Software|F-Droid>|25|stories/c2-fdroid.tm>>
    <associate|auto-105|<tuple|Organizations|25|stories/c2-fdroid.tm>>
    <associate|auto-106|<tuple|People|25|stories/c2-fdroid.tm>>
    <associate|auto-107|<tuple|People|25|stories/c2-fdroid.tm>>
    <associate|auto-108|<tuple|People|25|stories/c2-fdroid.tm>>
    <associate|auto-109|<tuple|2.6.2|25|stories/c2-fdroid.tm>>
    <associate|auto-11|<tuple|People|12|stories/c2-bitcoin-core.tm>>
    <associate|auto-110|<tuple|2.6.3|25|stories/c2-fdroid.tm>>
    <associate|auto-111|<tuple|2.6.3.1|25|stories/c2-fdroid.tm>>
    <associate|auto-112|<tuple|<tuple|Keys|Organizations|F-Droid|Signing|0x41E7044E1DBA2E89
    (2014\U)>|25|stories/c2-fdroid.tm>>
    <associate|auto-113|<tuple|2.6.3.2|25|stories/c2-fdroid.tm>>
    <associate|auto-114|<tuple|<tuple|Keys|People|Grote,
    Torsten|0x74DCA8A36C52F833>|25|stories/c2-fdroid.tm>>
    <associate|auto-115|<tuple|2.6.3.3|26|stories/c2-fdroid.tm>>
    <associate|auto-116|<tuple|<tuple|Keys|People|Steiner,
    Hans-Christoph|0xE9E28DEA00AA5556>|26|stories/c2-fdroid.tm>>
    <associate|auto-117|<tuple|2.6.3.4|26|stories/c2-fdroid.tm>>
    <associate|auto-118|<tuple|<tuple|Software|F-Droid>|26|stories/c2-fdroid.tm>>
    <associate|auto-119|<tuple|2.7|27|stories/c2-freedombox.tm>>
    <associate|auto-12|<tuple|People|12|stories/c2-bitcoin-core.tm>>
    <associate|auto-120|<tuple|2.7.1|27|stories/c2-freedombox.tm>>
    <associate|auto-121|<tuple|<tuple|Software|Freedombox>|27|stories/c2-freedombox.tm>>
    <associate|auto-122|<tuple|Software|27|stories/c2-freedombox.tm>>
    <associate|auto-123|<tuple|Raspberry Pi|27|stories/c2-freedombox.tm>>
    <associate|auto-124|<tuple|Organizations|27|stories/c2-freedombox.tm>>
    <associate|auto-125|<tuple|People|27|stories/c2-freedombox.tm>>
    <associate|auto-126|<tuple|2.7.2|27|stories/c2-freedombox.tm>>
    <associate|auto-127|<tuple|2.7.3|27|stories/c2-freedombox.tm>>
    <associate|auto-128|<tuple|2.7.3.1|27|stories/c2-freedombox.tm>>
    <associate|auto-129|<tuple|<tuple|Keys|People|Adapa, Sunil
    Mohan|0x36C361440C9BC971>|27|stories/c2-freedombox.tm>>
    <associate|auto-13|<tuple|People|12|stories/c2-bitcoin-core.tm>>
    <associate|auto-130|<tuple|2.7.3.2|28|stories/c2-freedombox.tm>>
    <associate|auto-131|<tuple|<tuple|Keys|People|Valleroy,
    James|0x77C0C75E7B650808>|28|stories/c2-freedombox.tm>>
    <associate|auto-132|<tuple|2.7.3.3|28|stories/c2-freedombox.tm>>
    <associate|auto-133|<tuple|<tuple|Keys|People|Valleroy,
    James|0x77C0C75E7B650808>|28|stories/c2-freedombox.tm>>
    <associate|auto-134|<tuple|<tuple|Software|Freedombox>|28|stories/c2-freedombox.tm>>
    <associate|auto-135|<tuple|2.8|29|stories/c2-github.tm>>
    <associate|auto-136|<tuple|2.8.1|29|stories/c2-github.tm>>
    <associate|auto-137|<tuple|<tuple|Software|GitHub>|29|stories/c2-github.tm>>
    <associate|auto-138|<tuple|Microsoft|29|stories/c2-github.tm>>
    <associate|auto-139|<tuple|2.8.2|29|stories/c2-github.tm>>
    <associate|auto-14|<tuple|People|12|stories/c2-bitcoin-core.tm>>
    <associate|auto-140|<tuple|Logical Awesome, LLC|29|stories/c2-github.tm>>
    <associate|auto-141|<tuple|People|29|stories/c2-github.tm>>
    <associate|auto-142|<tuple|People|29|stories/c2-github.tm>>
    <associate|auto-143|<tuple|2.8.3|29|stories/c2-github.tm>>
    <associate|auto-144|<tuple|2.8.3.1|29|stories/c2-github.tm>>
    <associate|auto-145|<tuple|<tuple|Keys|Organizations|Github|0x4AEE18F83AFDEB23>|29|stories/c2-github.tm>>
    <associate|auto-146|<tuple|<tuple|Software|GitHub>|29|stories/c2-github.tm>>
    <associate|auto-147|<tuple|2.9|30|stories/c2-gnupg.tm>>
    <associate|auto-148|<tuple|2.9.1|30|stories/c2-gnupg.tm>>
    <associate|auto-149|<tuple|<tuple|Software|GnuPG>|30|stories/c2-gnupg.tm>>
    <associate|auto-15|<tuple|2.1.2|12|stories/c2-bitcoin-core.tm>>
    <associate|auto-150|<tuple|Software|30|stories/c2-gnupg.tm>>
    <associate|auto-151|<tuple|Software|30|stories/c2-gnupg.tm>>
    <associate|auto-152|<tuple|People|30|stories/c2-gnupg.tm>>
    <associate|auto-153|<tuple|People|30|stories/c2-gnupg.tm>>
    <associate|auto-154|<tuple|2.9.2|30|stories/c2-gnupg.tm>>
    <associate|auto-155|<tuple|2.9.3|31|stories/c2-gnupg.tm>>
    <associate|auto-156|<tuple|2.9.22|31|stories/c2-gnupg.tm>>
    <associate|auto-157|<tuple|2.9.3.1|31|stories/c2-gnupg.tm>>
    <associate|auto-158|<tuple|<tuple|Keys|People|Koch,
    Werner|0x528897B826403ADA>|31|stories/c2-gnupg.tm>>
    <associate|auto-159|<tuple|2.9.3.2|31|stories/c2-gnupg.tm>>
    <associate|auto-16|<tuple|2.1.3|13|stories/c2-bitcoin-core.tm>>
    <associate|auto-160|<tuple|<tuple|Keys|People|Yutaka,
    Niibe|0xE98E9B2D19C6C8BD>|31|stories/c2-gnupg.tm>>
    <associate|auto-161|<tuple|2.9.3.3|31|stories/c2-gnupg.tm>>
    <associate|auto-162|<tuple|<tuple|Keys|People|Heinecke,
    Andre|0xBCEF7E294B092E28>|31|stories/c2-gnupg.tm>>
    <associate|auto-163|<tuple|2.9.3.4|31|stories/c2-gnupg.tm>>
    <associate|auto-164|<tuple|<tuple|Keys|Organizations|GnuPG.com|0x549E695E905BA208>|31|stories/c2-gnupg.tm>>
    <associate|auto-165|<tuple|<tuple|Software|GnuPG>|31|stories/c2-gnupg.tm>>
    <associate|auto-166|<tuple|2.10|32|stories/c2-inkscape.tm>>
    <associate|auto-167|<tuple|2.10.1|32|stories/c2-inkscape.tm>>
    <associate|auto-168|<tuple|<tuple|Software|Inkscape>|32|stories/c2-inkscape.tm>>
    <associate|auto-169|<tuple|2.10.2|32|stories/c2-inkscape.tm>>
    <associate|auto-17|<tuple|2.1.3.1|13|stories/c2-bitcoin-core.tm>>
    <associate|auto-170|<tuple|2.10.3|32|stories/c2-inkscape.tm>>
    <associate|auto-171|<tuple|2.10.3.1|32|stories/c2-inkscape.tm>>
    <associate|auto-172|<tuple|<tuple|Keys|People|Jeanmougin,
    Marc|0x5FCB204EF882B07A>|32|stories/c2-inkscape.tm>>
    <associate|auto-173|<tuple|<tuple|Software|Inkscape>|32|stories/c2-inkscape.tm>>
    <associate|auto-174|<tuple|2.11|33|stories/c2-keepassxc.tm>>
    <associate|auto-175|<tuple|2.11.1|33|stories/c2-keepassxc.tm>>
    <associate|auto-176|<tuple|<tuple|Software|KeePassXC>|33|stories/c2-keepassxc.tm>>
    <associate|auto-177|<tuple|Organizations|33|stories/c2-keepassxc.tm>>
    <associate|auto-178|<tuple|2.11.2|33|stories/c2-keepassxc.tm>>
    <associate|auto-179|<tuple|2.11.3|33|stories/c2-keepassxc.tm>>
    <associate|auto-18|<tuple|2.1.1|13|stories/c2-bitcoin-core.tm>>
    <associate|auto-180|<tuple|2.11.3.1|33|stories/c2-keepassxc.tm>>
    <associate|auto-181|<tuple|<tuple|Keys|Organizations|KeePassXC|0xCFB4C2166397D0D2>|33|stories/c2-keepassxc.tm>>
    <associate|auto-182|<tuple|2.11.3.2|33|stories/c2-keepassxc.tm>>
    <associate|auto-183|<tuple|<tuple|Keys|Organizations|KeePassXC|0x105D8D57BB9746BD>|33|stories/c2-keepassxc.tm>>
    <associate|auto-184|<tuple|<tuple|Software|KeePassXC>|33|stories/c2-keepassxc.tm>>
    <associate|auto-185|<tuple|2.12|34|stories/c2-qubesos.tm>>
    <associate|auto-186|<tuple|2.12.1|34|stories/c2-qubesos.tm>>
    <associate|auto-187|<tuple|<tuple|Software|Qubes
    OS>|34|stories/c2-qubesos.tm>>
    <associate|auto-188|<tuple|Organizations|34|stories/c2-qubesos.tm>>
    <associate|auto-189|<tuple|People|34|stories/c2-qubesos.tm>>
    <associate|auto-19|<tuple|2.1.3.2|13|stories/c2-bitcoin-core.tm>>
    <associate|auto-190|<tuple|People|34|stories/c2-qubesos.tm>>
    <associate|auto-191|<tuple|2.12.2|34|stories/c2-qubesos.tm>>
    <associate|auto-192|<tuple|2.12.3|34|stories/c2-qubesos.tm>>
    <associate|auto-193|<tuple|2.12.3.1|34|stories/c2-qubesos.tm>>
    <associate|auto-194|<tuple|<tuple|Keys|Organizations|Qubes
    OS|0xDDFA1A3E36879494>|34|stories/c2-qubesos.tm>>
    <associate|auto-195|<tuple|2.12.3.2|34|stories/c2-qubesos.tm>>
    <associate|auto-196|<tuple|<tuple|Keys|Organizations|Qubes OS (Release 4
    Signing Key)|0x1848792F9E2795E9>|34|stories/c2-qubesos.tm>>
    <associate|auto-197|<tuple|2.12.3.3|34|stories/c2-qubesos.tm>>
    <associate|auto-198|<tuple|<tuple|Keys|Organizations|Qubes OS (Release 3
    Signing Key)|0xCB11CA1D03FA5082>|34|stories/c2-qubesos.tm>>
    <associate|auto-199|<tuple|2.12.3.4|35|stories/c2-qubesos.tm>>
    <associate|auto-2|<tuple|1|9|c1.tm>>
    <associate|auto-20|<tuple|2.1.2|13|stories/c2-bitcoin-core.tm>>
    <associate|auto-200|<tuple|<tuple|Keys|Organizations|Qubes OS (Release 2
    Signing Key)|0x0C73B9D40A40E458>|35|stories/c2-qubesos.tm>>
    <associate|auto-201|<tuple|2.12.3.5|35|stories/c2-qubesos.tm>>
    <associate|auto-202|<tuple|<tuple|Keys|Organizations|Qubes OS (Release 1
    Signing Key)|0xEA01201B211093A7>|35|stories/c2-qubesos.tm>>
    <associate|auto-203|<tuple|<tuple|Software|Qubes
    OS>|35|stories/c2-qubesos.tm>>
    <associate|auto-204|<tuple|2.13|36|stories/c2-raspiblitz.tm>>
    <associate|auto-205|<tuple|2.13.1|36|stories/c2-raspiblitz.tm>>
    <associate|auto-206|<tuple|<tuple|Software|Raspiblitz>|36|stories/c2-raspiblitz.tm>>
    <associate|auto-207|<tuple|Software|36|stories/c2-raspiblitz.tm>>
    <associate|auto-208|<tuple|Software|36|stories/c2-raspiblitz.tm>>
    <associate|auto-209|<tuple|People|36|stories/c2-raspiblitz.tm>>
    <associate|auto-21|<tuple|2.1.3.3|14|stories/c2-bitcoin-core.tm>>
    <associate|auto-210|<tuple|2.13.2|36|stories/c2-raspiblitz.tm>>
    <associate|auto-211|<tuple|People|36|stories/c2-raspiblitz.tm>>
    <associate|auto-212|<tuple|2.13.3|36|stories/c2-raspiblitz.tm>>
    <associate|auto-213|<tuple|2.13.3.1|36|stories/c2-raspiblitz.tm>>
    <associate|auto-214|<tuple|<tuple|Keys|People|Rotzoll, Christian
    Rootzol|>|36|stories/c2-raspiblitz.tm>>
    <associate|auto-215|<tuple|<tuple|Software|Raspiblitz>|36|stories/c2-raspiblitz.tm>>
    <associate|auto-216|<tuple|2.14|37|stories/c2-satoshi-labs.tm>>
    <associate|auto-217|<tuple|2.14.1|37|stories/c2-satoshi-labs.tm>>
    <associate|auto-218|<tuple|<tuple|Software|Satoshi
    Labs>|37|stories/c2-satoshi-labs.tm>>
    <associate|auto-219|<tuple|Software|37|stories/c2-satoshi-labs.tm>>
    <associate|auto-22|<tuple|2.1.3|14|stories/c2-bitcoin-core.tm>>
    <associate|auto-220|<tuple|People|37|stories/c2-satoshi-labs.tm>>
    <associate|auto-221|<tuple|People|37|stories/c2-satoshi-labs.tm>>
    <associate|auto-222|<tuple|People|37|stories/c2-satoshi-labs.tm>>
    <associate|auto-223|<tuple|People|37|stories/c2-satoshi-labs.tm>>
    <associate|auto-224|<tuple|People|37|stories/c2-satoshi-labs.tm>>
    <associate|auto-225|<tuple|2.14.2|37|stories/c2-satoshi-labs.tm>>
    <associate|auto-226|<tuple|2.14.3|37|stories/c2-satoshi-labs.tm>>
    <associate|auto-227|<tuple|2.14.3.1|37|stories/c2-satoshi-labs.tm>>
    <associate|auto-228|<tuple|<tuple|Keys|People|Rusn�k, Pavol
    \PStick\Q|0x91F3B339B9A02A3D>|37|stories/c2-satoshi-labs.tm>>
    <associate|auto-229|<tuple|2.14.3.2|38|stories/c2-satoshi-labs.tm>>
    <associate|auto-23|<tuple|2.1.3.4|14|stories/c2-bitcoin-core.tm>>
    <associate|auto-230|<tuple|<tuple|Keys|Organizations|Satoshi
    Labs|Signing|0x26A3A56662F0E7E2 (2020)>|38|stories/c2-satoshi-labs.tm>>
    <associate|auto-231|<tuple|2.14.3.3|38|stories/c2-satoshi-labs.tm>>
    <associate|auto-232|<tuple|<tuple|Keys|Organizations|Satoshi
    Labs|Signing|0xE21B6950A2ECB65C (2021)>|38|stories/c2-satoshi-labs.tm>>
    <associate|auto-233|<tuple|<tuple|Software|Satoshi
    Labs>|38|stories/c2-satoshi-labs.tm>>
    <associate|auto-234|<tuple|2.15|39|stories/c2-tails.tm>>
    <associate|auto-235|<tuple|2.15.1|39|stories/c2-tails.tm>>
    <associate|auto-236|<tuple|<tuple|Software|Tails>|39|stories/c2-tails.tm>>
    <associate|auto-237|<tuple|Software|39|stories/c2-tails.tm>>
    <associate|auto-238|<tuple|Software|39|stories/c2-tails.tm>>
    <associate|auto-239|<tuple|Software|39|stories/c2-tails.tm>>
    <associate|auto-24|<tuple|<tuple|Keys|People|van der Laan, Wladimir
    J.|0x90C8019E36C2E964 (Bitcoin Core; 2015\U2022)>|14|stories/c2-bitcoin-core.tm>>
    <associate|auto-240|<tuple|Software|39|stories/c2-tails.tm>>
    <associate|auto-241|<tuple|Software|39|stories/c2-tails.tm>>
    <associate|auto-242|<tuple|2.15.2|39|stories/c2-tails.tm>>
    <associate|auto-243|<tuple|2.15.3|39|stories/c2-tails.tm>>
    <associate|auto-244|<tuple|2.15.3.1|39|stories/c2-tails.tm>>
    <associate|auto-245|<tuple|<tuple|Keys|Organizations|Tails|Signing|0xDBB802B258ACD84F
    (2015\U )>|39|stories/c2-tails.tm>>
    <associate|auto-246|<tuple|2.15.3.2|40|stories/c2-tails.tm>>
    <associate|auto-247|<tuple|<tuple|Keys|Organizations|Tails|Mailing
    list|0x1D2975EDF93E735F (2009\U )>|40|stories/c2-tails.tm>>
    <associate|auto-248|<tuple|2.15.3.3|40|stories/c2-tails.tm>>
    <associate|auto-249|<tuple|<tuple|Keys|Organizations|Tails|Signing|0x1202821CBE2CD9C1
    (2010\U2015)>|40|stories/c2-tails.tm>>
    <associate|auto-25|<tuple|2.1.3.5|14|stories/c2-bitcoin-core.tm>>
    <associate|auto-250|<tuple|<tuple|Software|Tails>|40|stories/c2-tails.tm>>
    <associate|auto-251|<tuple|2.16|41|stories/c2-tor-browser.tm>>
    <associate|auto-252|<tuple|2.16.1|41|stories/c2-tor-browser.tm>>
    <associate|auto-253|<tuple|<tuple|Software|Tor
    Browser>|41|stories/c2-tor-browser.tm>>
    <associate|auto-254|<tuple|Software|41|stories/c2-tor-browser.tm>>
    <associate|auto-255|<tuple|2.16.2|41|stories/c2-tor-browser.tm>>
    <associate|auto-256|<tuple|People|41|stories/c2-tor-browser.tm>>
    <associate|auto-257|<tuple|2.16.3|41|stories/c2-tor-browser.tm>>
    <associate|auto-258|<tuple|2.16.3.1|41|stories/c2-tor-browser.tm>>
    <associate|auto-259|<tuple|<tuple|Keys|Organizations|Tor
    Browser|Signing|0x4E2C6E8793298290 (2015\U
    )>|41|stories/c2-tor-browser.tm>>
    <associate|auto-26|<tuple|<tuple|Keys|People|van der Laan, Wladimir
    J.|0x74810B012346C9A6 (personal; 2011\U
    )>|14|stories/c2-bitcoin-core.tm>>
    <associate|auto-260|<tuple|<tuple|Software|Tor
    Browser>|41|stories/c2-tor-browser.tm>>
    <associate|auto-261|<tuple|2.17|42|stories/c2-trisquel.tm>>
    <associate|auto-262|<tuple|2.17.1|42|stories/c2-trisquel.tm>>
    <associate|auto-263|<tuple|<tuple|Software|Trisquel>|42|stories/c2-trisquel.tm>>
    <associate|auto-264|<tuple|People|42|stories/c2-trisquel.tm>>
    <associate|auto-265|<tuple|2.17.2|42|stories/c2-trisquel.tm>>
    <associate|auto-266|<tuple|2.17.3|42|stories/c2-trisquel.tm>>
    <associate|auto-267|<tuple|2.17.3.1|42|stories/c2-trisquel.tm>>
    <associate|auto-268|<tuple|<tuple|Keys|Organizations|Trisquel|0xF5DAAAF74AD4C938>|42|stories/c2-trisquel.tm>>
    <associate|auto-269|<tuple|<tuple|Software|Trisquel>|42|stories/c2-trisquel.tm>>
    <associate|auto-27|<tuple|2.1.3.6|14|stories/c2-bitcoin-core.tm>>
    <associate|auto-270|<tuple|2.18|43|stories/c2-veracrypt.tm>>
    <associate|auto-271|<tuple|2.18.1|43|stories/c2-veracrypt.tm>>
    <associate|auto-272|<tuple|<tuple|Software|Veracrypt>|43|stories/c2-veracrypt.tm>>
    <associate|auto-273|<tuple|People|43|stories/c2-veracrypt.tm>>
    <associate|auto-274|<tuple|2.18.2|43|stories/c2-veracrypt.tm>>
    <associate|auto-275|<tuple|Organizations|43|stories/c2-veracrypt.tm>>
    <associate|auto-276|<tuple|2.18.3|43|stories/c2-veracrypt.tm>>
    <associate|auto-277|<tuple|2.18.3.1|43|stories/c2-veracrypt.tm>>
    <associate|auto-278|<tuple|<tuple|Keys|Organizations|Veracrypt|Signing|0x821ACD02680D16DE
    (2018\U )>|43|stories/c2-veracrypt.tm>>
    <associate|auto-279|<tuple|2.18.3.2|44|stories/c2-veracrypt.tm>>
    <associate|auto-28|<tuple|<tuple|Keys|People|Andresen,
    Gavin|0x29D9EE6B1FC730C1>|14|stories/c2-bitcoin-core.tm>>
    <associate|auto-280|<tuple|<tuple|Keys|Organizations|Veracrypt|Signing|0xEB559C7C54DDD393
    (2014\U2018)>|44|stories/c2-veracrypt.tm>>
    <associate|auto-281|<tuple|Organizations|44|stories/c2-veracrypt.tm>>
    <associate|auto-282|<tuple|<tuple|Software|Veracrypt>|44|stories/c2-veracrypt.tm>>
    <associate|auto-283|<tuple|2.19|45|stories/c2-youtube-dl.tm>>
    <associate|auto-284|<tuple|2.19.1|45|stories/c2-youtube-dl.tm>>
    <associate|auto-285|<tuple|<tuple|Software|Youtube-dl>|45|stories/c2-youtube-dl.tm>>
    <associate|auto-286|<tuple|People|45|stories/c2-youtube-dl.tm>>
    <associate|auto-287|<tuple|2.19.2|45|stories/c2-youtube-dl.tm>>
    <associate|auto-288|<tuple|People|45|stories/c2-youtube-dl.tm>>
    <associate|auto-289|<tuple|2.19.3|45|stories/c2-youtube-dl.tm>>
    <associate|auto-29|<tuple|2.1.3.7|15|stories/c2-bitcoin-core.tm>>
    <associate|auto-290|<tuple|2.19.3.1|45|stories/c2-youtube-dl.tm>>
    <associate|auto-291|<tuple|<tuple|Keys|People|M.,
    Sergey|0x2C393E0F18A9236D>|45|stories/c2-youtube-dl.tm>>
    <associate|auto-292|<tuple|2.19.3.2|45|stories/c2-youtube-dl.tm>>
    <associate|auto-293|<tuple|<tuple|Keys|People|Hagemeister,
    Philipp|0xF5EAB582FAFB085C>|45|stories/c2-youtube-dl.tm>>
    <associate|auto-294|<tuple|People|45|stories/c2-youtube-dl.tm>>
    <associate|auto-295|<tuple|2.19.3.3|46|stories/c2-youtube-dl.tm>>
    <associate|auto-296|<tuple|<tuple|Keys|People|Hagemeister,
    Philipp|0xDB4B54CBA4826A18>|46|stories/c2-youtube-dl.tm>>
    <associate|auto-297|<tuple|People|46|stories/c2-youtube-dl.tm>>
    <associate|auto-298|<tuple|2.19.3.4|46|stories/c2-youtube-dl.tm>>
    <associate|auto-299|<tuple|<tuple|Keys|People|Valsorda,
    Filippo|0xEBF01804BCF05F6B>|46|stories/c2-youtube-dl.tm>>
    <associate|auto-3|<tuple|1.1|9|c1.tm>>
    <associate|auto-30|<tuple|<tuple|Keys|People|Nakamoto,
    Satoshi|0x18C09E865EC948A1>|15|stories/c2-bitcoin-core.tm>>
    <associate|auto-300|<tuple|People|46|stories/c2-youtube-dl.tm>>
    <associate|auto-301|<tuple|<tuple|Software|Youtube-dl>|46|stories/c2-youtube-dl.tm>>
    <associate|auto-302|<tuple|2.20|47|stories/c2-yt-dlp.tm>>
    <associate|auto-303|<tuple|2.20.1|47|stories/c2-yt-dlp.tm>>
    <associate|auto-304|<tuple|<tuple|Software|Yt-dlp>|47|stories/c2-yt-dlp.tm>>
    <associate|auto-305|<tuple|People|47|stories/c2-yt-dlp.tm>>
    <associate|auto-306|<tuple|2.20.2|47|stories/c2-yt-dlp.tm>>
    <associate|auto-307|<tuple|2.20.3|47|stories/c2-yt-dlp.tm>>
    <associate|auto-308|<tuple|2.20.3.1|47|stories/c2-yt-dlp.tm>>
    <associate|auto-309|<tuple|<tuple|Keys|People|Sawicki,
    Simon|0x57CF65933B5A7581>|47|stories/c2-yt-dlp.tm>>
    <associate|auto-31|<tuple|DSA, algorithm|15|stories/c2-bitcoin-core.tm>>
    <associate|auto-310|<tuple|2.20.3.2|47|stories/c2-yt-dlp.tm>>
    <associate|auto-311|<tuple|<tuple|Keys|People|pukkandan|0x7EEE9E1E817D0A39>|47|stories/c2-yt-dlp.tm>>
    <associate|auto-312|<tuple|<tuple|Software|Yt-dlp>|47|stories/c2-yt-dlp.tm>>
    <associate|auto-313|<tuple|A|49|a-how-gnupg.tm>>
    <associate|auto-314|<tuple|A.1|49|a-how-gnupg.tm>>
    <associate|auto-315|<tuple|A.2|52|a-how-gnupg.tm>>
    <associate|auto-316|<tuple|A.2.1|52|a-how-gnupg.tm>>
    <associate|auto-317|<tuple|A.2.1.1|52|a-how-gnupg.tm>>
    <associate|auto-318|<tuple|A.2.1.2|52|a-how-gnupg.tm>>
    <associate|auto-319|<tuple|A.2.2|53|a-how-gnupg.tm>>
    <associate|auto-32|<tuple|Organizations|15|stories/c2-bitcoin-core.tm>>
    <associate|auto-320|<tuple|A.2.2.1|53|a-how-gnupg.tm>>
    <associate|auto-321|<tuple|A.2.3|53|a-how-gnupg.tm>>
    <associate|auto-322|<tuple|A.2.3.1|53|a-how-gnupg.tm>>
    <associate|auto-323|<tuple|A.2.3.2|53|a-how-gnupg.tm>>
    <associate|auto-324|<tuple|A.2.4|54|a-how-gnupg.tm>>
    <associate|auto-325|<tuple|A.2.4.1|54|a-how-gnupg.tm>>
    <associate|auto-326|<tuple|A.2.4.2|54|a-how-gnupg.tm>>
    <associate|auto-327|<tuple|A.2.6|55>>
    <associate|auto-328|<tuple|2|57>>
    <associate|auto-33|<tuple|<tuple|Software|Bitcoin
    Core>|15|stories/c2-bitcoin-core.tm>>
    <associate|auto-34|<tuple|2.2|16|stories/c2-cryptomator.tm>>
    <associate|auto-35|<tuple|2.2.1|16|stories/c2-cryptomator.tm>>
    <associate|auto-36|<tuple|<tuple|Software|Cryptomator>|16|stories/c2-cryptomator.tm>>
    <associate|auto-37|<tuple|Software|16|stories/c2-cryptomator.tm>>
    <associate|auto-38|<tuple|<tuple|Keys|People|Stenzel,
    Sebastian|0x667B866EA8240A09>|16|stories/c2-cryptomator.tm>>
    <associate|auto-39|<tuple|<tuple|Keys|People|Schrenk,
    Armin|0x748E55D51F5B3FBC>|16|stories/c2-cryptomator.tm>>
    <associate|auto-4|<tuple|1.2|9|c1.tm>>
    <associate|auto-40|<tuple|<tuple|Keys|People|Hagemann,
    Tobias|0x69CEFAD519598989>|16|stories/c2-cryptomator.tm>>
    <associate|auto-41|<tuple|2.2.2|16|stories/c2-cryptomator.tm>>
    <associate|auto-42|<tuple|2.2.3|16|stories/c2-cryptomator.tm>>
    <associate|auto-43|<tuple|2.2.3.1|16|stories/c2-cryptomator.tm>>
    <associate|auto-44|<tuple|<tuple|Keys|Organizations|Cryptomator|0x615D449FE6E6A235>|16|stories/c2-cryptomator.tm>>
    <associate|auto-45|<tuple|2.2.3.2|17|stories/c2-cryptomator.tm>>
    <associate|auto-46|<tuple|<tuple|Keys|Organizations|Cryptomator|0x509C9D6334C80F11>|17|stories/c2-cryptomator.tm>>
    <associate|auto-47|<tuple|<tuple|Software|Cryptomator>|17|stories/c2-cryptomator.tm>>
    <associate|auto-48|<tuple|2.3|18|stories/c2-debian.tm>>
    <associate|auto-49|<tuple|2.3.1|18|stories/c2-debian.tm>>
    <associate|auto-5|<tuple|1.3|9|c1.tm>>
    <associate|auto-50|<tuple|<tuple|Software|Debian>|18|stories/c2-debian.tm>>
    <associate|auto-51|<tuple|People|18|stories/c2-debian.tm>>
    <associate|auto-52|<tuple|2.3.2|18|stories/c2-debian.tm>>
    <associate|auto-53|<tuple|People|18|stories/c2-debian.tm>>
    <associate|auto-54|<tuple|People|18|stories/c2-debian.tm>>
    <associate|auto-55|<tuple|2.3.3|18|stories/c2-debian.tm>>
    <associate|auto-56|<tuple|2.3.3.1|18|stories/c2-debian.tm>>
    <associate|auto-57|<tuple|2.3.10|19|stories/c2-debian.tm>>
    <associate|auto-58|<tuple|2.3.3.2|19|stories/c2-debian.tm>>
    <associate|auto-59|<tuple|2.3.3.2.1|19|stories/c2-debian.tm>>
    <associate|auto-6|<tuple|2|11|c2.tm>>
    <associate|auto-60|<tuple|People|19|stories/c2-debian.tm>>
    <associate|auto-61|<tuple|2.3.3.2.2|19|stories/c2-debian.tm>>
    <associate|auto-62|<tuple|2.3.3.2.3|20|stories/c2-debian.tm>>
    <associate|auto-63|<tuple|2.3.3.2.4|20|stories/c2-debian.tm>>
    <associate|auto-64|<tuple|2.3.3.2.5|20|stories/c2-debian.tm>>
    <associate|auto-65|<tuple|2.3.3.2.6|20|stories/c2-debian.tm>>
    <associate|auto-66|<tuple|2.3.3.2.7|20|stories/c2-debian.tm>>
    <associate|auto-67|<tuple|2.3.3.2.8|20|stories/c2-debian.tm>>
    <associate|auto-68|<tuple|2.3.3.2.9|21|stories/c2-debian.tm>>
    <associate|auto-69|<tuple|2.3.3.2.10|21|stories/c2-debian.tm>>
    <associate|auto-7|<tuple|2.1|12|stories/c2-bitcoin-core.tm>>
    <associate|auto-70|<tuple|<tuple|Software|Debian>|21|stories/c2-debian.tm>>
    <associate|auto-71|<tuple|2.4|22|stories/c2-electrum.tm>>
    <associate|auto-72|<tuple|2.4.1|22|stories/c2-electrum.tm>>
    <associate|auto-73|<tuple|<tuple|Software|Electrum>|22|stories/c2-electrum.tm>>
    <associate|auto-74|<tuple|People|22|stories/c2-electrum.tm>>
    <associate|auto-75|<tuple|Software|22|stories/c2-electrum.tm>>
    <associate|auto-76|<tuple|Software|22|stories/c2-electrum.tm>>
    <associate|auto-77|<tuple|Software|22|stories/c2-electrum.tm>>
    <associate|auto-78|<tuple|People|22|stories/c2-electrum.tm>>
    <associate|auto-79|<tuple|2.4.2|22|stories/c2-electrum.tm>>
    <associate|auto-8|<tuple|2.1.1|12|stories/c2-bitcoin-core.tm>>
    <associate|auto-80|<tuple|2.4.3|22|stories/c2-electrum.tm>>
    <associate|auto-81|<tuple|2.4.3.1|22|stories/c2-electrum.tm>>
    <associate|auto-82|<tuple|<tuple|Keys|People|Voegtlin,
    Thomas|0x2BD5824B7F9470E6>|22|stories/c2-electrum.tm>>
    <associate|auto-83|<tuple|2.4.3.2|22|stories/c2-electrum.tm>>
    <associate|auto-84|<tuple|<tuple|Keys|People|Oeste, Stephan
    \Pemzy\Q|0x2EBB056FD847F8A7>|22|stories/c2-electrum.tm>>
    <associate|auto-85|<tuple|2.4.3.3|23|stories/c2-electrum.tm>>
    <associate|auto-86|<tuple|<tuple|Keys|People|SomberNight|0xCA9EEEC43DF911DC>|23|stories/c2-electrum.tm>>
    <associate|auto-87|<tuple|2.4.3.4|23|stories/c2-electrum.tm>>
    <associate|auto-88|<tuple|<tuple|Keys|People|SomberNight|0xE7B748CDAF5E5ED9>|23|stories/c2-electrum.tm>>
    <associate|auto-89|<tuple|<tuple|Software|Electrum>|23|stories/c2-electrum.tm>>
    <associate|auto-9|<tuple|<tuple|Software|Bitcoin
    Core>|12|stories/c2-bitcoin-core.tm>>
    <associate|auto-90|<tuple|2.5|24|stories/c2-element.tm>>
    <associate|auto-91|<tuple|2.5.1|24|stories/c2-element.tm>>
    <associate|auto-92|<tuple|<tuple|Software|Element>|24|stories/c2-element.tm>>
    <associate|auto-93|<tuple|People|24|stories/c2-element.tm>>
    <associate|auto-94|<tuple|Organization|24|stories/c2-element.tm>>
    <associate|auto-95|<tuple|People|24|stories/c2-element.tm>>
    <associate|auto-96|<tuple|People|24|stories/c2-element.tm>>
    <associate|auto-97|<tuple|2.5.2|24|stories/c2-element.tm>>
    <associate|auto-98|<tuple|2.5.3|24|stories/c2-element.tm>>
    <associate|auto-99|<tuple|2.5.3.1|24|stories/c2-element.tm>>
    <associate|bib-nyt_20110215_moglen-founds-fbx|<tuple|1|55>>
    <associate|bib-nyt_20180604_ms-buys-github|<tuple|2|55>>
    <associate|c1 trust-through-stories|<tuple|1|9|c1.tm>>
    <associate|c2 bitcoin-core|<tuple|2.1|12|stories/c2-bitcoin-core.tm>>
    <associate|c2 bitcoin-core-bg|<tuple|2.1.1|12|stories/c2-bitcoin-core.tm>>
    <associate|c2 bitcoin-core-hist|<tuple|2.1.2|12|stories/c2-bitcoin-core.tm>>
    <associate|c2 bitcoin-core-pk|<tuple|2.1.3|13|stories/c2-bitcoin-core.tm>>
    <associate|c2 bitcoin-core-pk bcc-22.0-sigs|<tuple|2.1.3|14|stories/c2-bitcoin-core.tm>>
    <associate|c2 bitcoin-core-pk bcc-23.0-sigs|<tuple|2.1.2|13|stories/c2-bitcoin-core.tm>>
    <associate|c2 bitcoin-core-pk bcc-25.0-sigs|<tuple|2.1.1|13|stories/c2-bitcoin-core.tm>>
    <associate|c2 cryptomator|<tuple|2.2|16|stories/c2-cryptomator.tm>>
    <associate|c2 cryptomator-bg|<tuple|2.2.1|16|stories/c2-cryptomator.tm>>
    <associate|c2 cryptomator-hist|<tuple|2.2.2|16|stories/c2-cryptomator.tm>>
    <associate|c2 cryptomator-pk|<tuple|2.2.3|16|stories/c2-cryptomator.tm>>
    <associate|c2 debian|<tuple|2.3|18|stories/c2-debian.tm>>
    <associate|c2 debian-bg|<tuple|2.3.1|18|stories/c2-debian.tm>>
    <associate|c2 debian-pk|<tuple|2.3.3|18|stories/c2-debian.tm>>
    <associate|c2 debian-pk-verbose|<tuple|2.3.3.2|19|stories/c2-debian.tm>>
    <associate|c2 electrum|<tuple|2.4|22|stories/c2-electrum.tm>>
    <associate|c2 electrum-bg|<tuple|2.4.1|22|stories/c2-electrum.tm>>
    <associate|c2 electrum-hist|<tuple|2.4.2|22|stories/c2-electrum.tm>>
    <associate|c2 electrum-pk|<tuple|2.4.3|22|stories/c2-electrum.tm>>
    <associate|c2 element|<tuple|2.5|24|stories/c2-element.tm>>
    <associate|c2 element-bg|<tuple|2.5.1|24|stories/c2-element.tm>>
    <associate|c2 element-hist|<tuple|2.5.2|24|stories/c2-element.tm>>
    <associate|c2 element-pk|<tuple|2.5.3|24|stories/c2-element.tm>>
    <associate|c2 fdroid|<tuple|2.6|25|stories/c2-fdroid.tm>>
    <associate|c2 fdroid-bg|<tuple|2.6.1|25|stories/c2-fdroid.tm>>
    <associate|c2 fdroid-hist|<tuple|2.6.2|25|stories/c2-fdroid.tm>>
    <associate|c2 fdroid-pk|<tuple|2.6.3|25|stories/c2-fdroid.tm>>
    <associate|c2 fdroid-pk-apk|<tuple|2.6.3.4|26|stories/c2-fdroid.tm>>
    <associate|c2 fdroid-pk-apk-pgp|<tuple|2.6.3.1|25|stories/c2-fdroid.tm>>
    <associate|c2 fdroid-pk-git|<tuple|2.6.3.3|26|stories/c2-fdroid.tm>>
    <associate|c2 freedombox|<tuple|2.7|27|stories/c2-freedombox.tm>>
    <associate|c2 freedombox-bg|<tuple|2.7.1|27|stories/c2-freedombox.tm>>
    <associate|c2 freedombox-hist|<tuple|2.7.2|27|stories/c2-freedombox.tm>>
    <associate|c2 freedombox-pk|<tuple|2.7.3|27|stories/c2-freedombox.tm>>
    <associate|c2 github|<tuple|2.8|29|stories/c2-github.tm>>
    <associate|c2 github-bg|<tuple|2.8.1|29|stories/c2-github.tm>>
    <associate|c2 github-hist|<tuple|2.8.2|29|stories/c2-github.tm>>
    <associate|c2 github-pk|<tuple|2.8.3|29|stories/c2-github.tm>>
    <associate|c2 gnupg|<tuple|2.9|30|stories/c2-gnupg.tm>>
    <associate|c2 gnupg-bg|<tuple|2.9.1|30|stories/c2-gnupg.tm>>
    <associate|c2 gnupg-hist|<tuple|2.9.2|30|stories/c2-gnupg.tm>>
    <associate|c2 gnupg-pk|<tuple|2.9.3|31|stories/c2-gnupg.tm>>
    <associate|c2 gnupg-pk-andre-c8bd|<tuple|2.9.3.3|31|stories/c2-gnupg.tm>>
    <associate|c2 gnupg-pk-gnupgcom-a208|<tuple|2.9.3.4|31|stories/c2-gnupg.tm>>
    <associate|c2 gnupg-pk-niibe-c8bd|<tuple|2.9.3.2|31|stories/c2-gnupg.tm>>
    <associate|c2 gnupg-pk-werner-3ada|<tuple|2.9.3.1|31|stories/c2-gnupg.tm>>
    <associate|c2 inkscape|<tuple|2.10|32|stories/c2-inkscape.tm>>
    <associate|c2 inkscape-bg|<tuple|2.10.1|32|stories/c2-inkscape.tm>>
    <associate|c2 inkscape-hist|<tuple|2.10.2|32|stories/c2-inkscape.tm>>
    <associate|c2 inkscape-pk|<tuple|2.10.3|32|stories/c2-inkscape.tm>>
    <associate|c2 keepassxc|<tuple|2.11|33|stories/c2-keepassxc.tm>>
    <associate|c2 keepassxc-bg|<tuple|2.11.1|33|stories/c2-keepassxc.tm>>
    <associate|c2 keepassxc-hist|<tuple|2.11.2|33|stories/c2-keepassxc.tm>>
    <associate|c2 keepassxc-pk|<tuple|2.11.3|33|stories/c2-keepassxc.tm>>
    <associate|c2 list-pubkeys|<tuple|2|11|c2.tm>>
    <associate|c2 qubesos|<tuple|2.12|34|stories/c2-qubesos.tm>>
    <associate|c2 qubesos-bg|<tuple|2.12.1|34|stories/c2-qubesos.tm>>
    <associate|c2 qubesos-hist|<tuple|2.12.2|34|stories/c2-qubesos.tm>>
    <associate|c2 qubesos-pk|<tuple|2.12.3|34|stories/c2-qubesos.tm>>
    <associate|c2 raspiblitz|<tuple|2.13|36|stories/c2-raspiblitz.tm>>
    <associate|c2 raspiblitz-bg|<tuple|2.13.1|36|stories/c2-raspiblitz.tm>>
    <associate|c2 raspiblitz-hist|<tuple|2.13.2|36|stories/c2-raspiblitz.tm>>
    <associate|c2 raspiblitz-pk|<tuple|2.13.3|36|stories/c2-raspiblitz.tm>>
    <associate|c2 satoshi-labs|<tuple|2.14|37|stories/c2-satoshi-labs.tm>>
    <associate|c2 satoshi-labs-bg|<tuple|2.14.1|37|stories/c2-satoshi-labs.tm>>
    <associate|c2 satoshi-labs-hist|<tuple|2.14.2|37|stories/c2-satoshi-labs.tm>>
    <associate|c2 satoshi-labs-pk|<tuple|2.14.3|37|stories/c2-satoshi-labs.tm>>
    <associate|c2 tab debian cd keys|<tuple|2.3.1|19|stories/c2-debian.tm>>
    <associate|c2 tab gnupg signing keys|<tuple|2.9.1|31|stories/c2-gnupg.tm>>
    <associate|c2 tails|<tuple|2.15|39|stories/c2-tails.tm>>
    <associate|c2 tails-bg|<tuple|2.15.1|39|stories/c2-tails.tm>>
    <associate|c2 tails-hist|<tuple|2.15.2|39|stories/c2-tails.tm>>
    <associate|c2 tails-pk|<tuple|2.15.3|39|stories/c2-tails.tm>>
    <associate|c2 tails-pk-mailinglist-2009|<tuple|2.15.3.2|40|stories/c2-tails.tm>>
    <associate|c2 tails-pk-sign-2010|<tuple|2.15.3.3|40|stories/c2-tails.tm>>
    <associate|c2 tails-pk-sign-2015|<tuple|2.15.3.1|39|stories/c2-tails.tm>>
    <associate|c2 tor-browser|<tuple|2.16|41|stories/c2-tor-browser.tm>>
    <associate|c2 tor-browser-bg|<tuple|2.16.1|41|stories/c2-tor-browser.tm>>
    <associate|c2 tor-browser-hist|<tuple|2.16.2|41|stories/c2-tor-browser.tm>>
    <associate|c2 tor-browser-pk|<tuple|2.16.3|41|stories/c2-tor-browser.tm>>
    <associate|c2 trisquel|<tuple|2.17|42|stories/c2-trisquel.tm>>
    <associate|c2 trisquel-bg|<tuple|2.17.1|42|stories/c2-trisquel.tm>>
    <associate|c2 trisquel-hist|<tuple|2.17.2|42|stories/c2-trisquel.tm>>
    <associate|c2 trisquel-pk|<tuple|2.17.3|42|stories/c2-trisquel.tm>>
    <associate|c2 veracrypt|<tuple|2.18|43|stories/c2-veracrypt.tm>>
    <associate|c2 veracrypt-bg|<tuple|2.18.1|43|stories/c2-veracrypt.tm>>
    <associate|c2 veracrypt-hist|<tuple|2.18.2|43|stories/c2-veracrypt.tm>>
    <associate|c2 veracrypt-pk|<tuple|2.18.3|43|stories/c2-veracrypt.tm>>
    <associate|c2 ytdl|<tuple|2.19|45|stories/c2-youtube-dl.tm>>
    <associate|c2 ytdl-bg|<tuple|2.19.1|45|stories/c2-youtube-dl.tm>>
    <associate|c2 ytdl-hist|<tuple|2.19.2|45|stories/c2-youtube-dl.tm>>
    <associate|c2 ytdl-pk|<tuple|2.19.3|45|stories/c2-youtube-dl.tm>>
    <associate|c2 ytdlp|<tuple|2.20|47|stories/c2-yt-dlp.tm>>
    <associate|c2 ytdlp-bg|<tuple|2.20.1|47|stories/c2-yt-dlp.tm>>
    <associate|c2 ytdlp-hist|<tuple|2.20.2|47|stories/c2-yt-dlp.tm>>
    <associate|c2 ytdlp-pk|<tuple|2.20.3|47|stories/c2-yt-dlp.tm>>
    <associate|def authenticate|<tuple|authenticate|49|a-how-gnupg.tm>>
    <associate|def capability|<tuple|capability|49|a-how-gnupg.tm>>
    <associate|def capability-flag|<tuple|capability flag|49|a-how-gnupg.tm>>
    <associate|def certify|<tuple|certify|49|a-how-gnupg.tm>>
    <associate|def decrypt|<tuple|decrypt|50|a-how-gnupg.tm>>
    <associate|def encrypt|<tuple|encrypt|50|a-how-gnupg.tm>>
    <associate|def encrypted|<tuple|encrypted|50|a-how-gnupg.tm>>
    <associate|def fingerprint|<tuple|fingerprint|50|a-how-gnupg.tm>>
    <associate|def flag|<tuple|flag|50|a-how-gnupg.tm>>
    <associate|def full-fingerprint|<tuple|full
    fingerprint|50|a-how-gnupg.tm>>
    <associate|def hexadecimal|<tuple|hexadecimal|50|a-how-gnupg.tm>>
    <associate|def interactive|<tuple|interactive|50|a-how-gnupg.tm>>
    <associate|def key|<tuple|key|50|a-how-gnupg.tm>>
    <associate|def key-id|<tuple|key-id|50|a-how-gnupg.tm>>
    <associate|def keybox|<tuple|keybox|50|a-how-gnupg.tm>>
    <associate|def keypair|<tuple|keypair|50|a-how-gnupg.tm>>
    <associate|def keyring|<tuple|keyring|50|a-how-gnupg.tm>>
    <associate|def long-id|<tuple|long ID|51|a-how-gnupg.tm>>
    <associate|def non-interactive|<tuple|non-interactive|51|a-how-gnupg.tm>>
    <associate|def plaintext|<tuple|plaintext|51|a-how-gnupg.tm>>
    <associate|def primary-key|<tuple|primary key|51|a-how-gnupg.tm>>
    <associate|def primary-uid|<tuple|primary UID|51|a-how-gnupg.tm>>
    <associate|def primary-user-id|<tuple|primary user ID|51|a-how-gnupg.tm>>
    <associate|def private-key|<tuple|private key|51|a-how-gnupg.tm>>
    <associate|def public-key|<tuple|public key|51|a-how-gnupg.tm>>
    <associate|def script|<tuple|script|51|a-how-gnupg.tm>>
    <associate|def short-id|<tuple|short ID|52|a-how-gnupg.tm>>
    <associate|def sign|<tuple|sign|51|a-how-gnupg.tm>>
    <associate|def signature|<tuple|signature|51|a-how-gnupg.tm>>
    <associate|def subkey|<tuple|subkey|51|a-how-gnupg.tm>>
    <associate|def symmetric-key|<tuple|symmetric key|52|a-how-gnupg.tm>>
    <associate|def uid|<tuple|UID|52|a-how-gnupg.tm>>
    <associate|def user-id|<tuple|User ID|52|a-how-gnupg.tm>>
    <associate|def verify|<tuple|verify|52|a-how-gnupg.tm>>
    <associate|element_20190509_pubkey|<tuple|2.5.8|24|stories/c2-element.tm>>
    <associate|fdroid_20230614_signingkeys|<tuple|2.6.7|26|stories/c2-fdroid.tm>>
    <associate|footnote-1.3.1|<tuple|1.3.1|10|c1.tm>>
    <associate|footnote-2.0.1|<tuple|2.0.1|11|c2.tm>>
    <associate|footnote-2.1.1|<tuple|2.1.1|12|stories/c2-bitcoin-core.tm>>
    <associate|footnote-2.1.10|<tuple|2.1.10|12|stories/c2-bitcoin-core.tm>>
    <associate|footnote-2.1.11|<tuple|2.1.11|12|stories/c2-bitcoin-core.tm>>
    <associate|footnote-2.1.12|<tuple|2.1.12|12|stories/c2-bitcoin-core.tm>>
    <associate|footnote-2.1.13|<tuple|2.1.13|12|stories/c2-bitcoin-core.tm>>
    <associate|footnote-2.1.14|<tuple|2.1.14|13|stories/c2-bitcoin-core.tm>>
    <associate|footnote-2.1.15|<tuple|2.1.15|13|stories/c2-bitcoin-core.tm>>
    <associate|footnote-2.1.16|<tuple|2.1.16|13|stories/c2-bitcoin-core.tm>>
    <associate|footnote-2.1.17|<tuple|2.1.17|13|stories/c2-bitcoin-core.tm>>
    <associate|footnote-2.1.18|<tuple|2.1.18|13|stories/c2-bitcoin-core.tm>>
    <associate|footnote-2.1.19|<tuple|2.1.19|13|stories/c2-bitcoin-core.tm>>
    <associate|footnote-2.1.2|<tuple|2.1.2|12|stories/c2-bitcoin-core.tm>>
    <associate|footnote-2.1.20|<tuple|2.1.20|13|stories/c2-bitcoin-core.tm>>
    <associate|footnote-2.1.21|<tuple|2.1.21|13|stories/c2-bitcoin-core.tm>>
    <associate|footnote-2.1.22|<tuple|2.1.22|13|stories/c2-bitcoin-core.tm>>
    <associate|footnote-2.1.23|<tuple|2.1.23|13|stories/c2-bitcoin-core.tm>>
    <associate|footnote-2.1.24|<tuple|2.1.24|14|stories/c2-bitcoin-core.tm>>
    <associate|footnote-2.1.25|<tuple|2.1.25|14|stories/c2-bitcoin-core.tm>>
    <associate|footnote-2.1.26|<tuple|2.1.26|14|stories/c2-bitcoin-core.tm>>
    <associate|footnote-2.1.27|<tuple|2.1.27|15|stories/c2-bitcoin-core.tm>>
    <associate|footnote-2.1.28|<tuple|2.1.28|15|stories/c2-bitcoin-core.tm>>
    <associate|footnote-2.1.29|<tuple|2.1.29|15|stories/c2-bitcoin-core.tm>>
    <associate|footnote-2.1.3|<tuple|2.1.3|?|stories/c2-bitcoin-core.tm>>
    <associate|footnote-2.1.4|<tuple|2.1.4|12|stories/c2-bitcoin-core.tm>>
    <associate|footnote-2.1.5|<tuple|2.1.5|12|stories/c2-bitcoin-core.tm>>
    <associate|footnote-2.1.6|<tuple|2.1.6|12|stories/c2-bitcoin-core.tm>>
    <associate|footnote-2.1.7|<tuple|2.1.7|12|stories/c2-bitcoin-core.tm>>
    <associate|footnote-2.1.8|<tuple|2.1.8|12|stories/c2-bitcoin-core.tm>>
    <associate|footnote-2.1.9|<tuple|2.1.9|12|stories/c2-bitcoin-core.tm>>
    <associate|footnote-2.10.1|<tuple|2.10.1|32|stories/c2-inkscape.tm>>
    <associate|footnote-2.10.2|<tuple|2.10.2|32|stories/c2-inkscape.tm>>
    <associate|footnote-2.10.3|<tuple|2.10.3|32|stories/c2-inkscape.tm>>
    <associate|footnote-2.10.4|<tuple|2.10.4|32|stories/c2-inkscape.tm>>
    <associate|footnote-2.10.5|<tuple|2.10.5|32|stories/c2-inkscape.tm>>
    <associate|footnote-2.11.1|<tuple|2.11.1|33|stories/c2-keepassxc.tm>>
    <associate|footnote-2.11.2|<tuple|2.11.2|33|stories/c2-keepassxc.tm>>
    <associate|footnote-2.11.3|<tuple|2.11.3|33|stories/c2-keepassxc.tm>>
    <associate|footnote-2.11.4|<tuple|2.11.4|33|stories/c2-keepassxc.tm>>
    <associate|footnote-2.11.5|<tuple|2.11.5|33|stories/c2-keepassxc.tm>>
    <associate|footnote-2.11.6|<tuple|2.11.6|33|stories/c2-keepassxc.tm>>
    <associate|footnote-2.11.7|<tuple|2.11.7|33|stories/c2-keepassxc.tm>>
    <associate|footnote-2.11.8|<tuple|2.11.8|33|stories/c2-keepassxc.tm>>
    <associate|footnote-2.12.1|<tuple|2.12.1|34|stories/c2-qubesos.tm>>
    <associate|footnote-2.12.2|<tuple|2.12.2|34|stories/c2-qubesos.tm>>
    <associate|footnote-2.12.3|<tuple|2.12.3|34|stories/c2-qubesos.tm>>
    <associate|footnote-2.12.4|<tuple|2.12.4|34|stories/c2-qubesos.tm>>
    <associate|footnote-2.12.5|<tuple|2.12.5|34|stories/c2-qubesos.tm>>
    <associate|footnote-2.12.6|<tuple|2.12.6|34|stories/c2-qubesos.tm>>
    <associate|footnote-2.12.7|<tuple|2.12.7|34|stories/c2-qubesos.tm>>
    <associate|footnote-2.12.8|<tuple|2.12.8|35|stories/c2-qubesos.tm>>
    <associate|footnote-2.12.9|<tuple|2.12.9|35|stories/c2-qubesos.tm>>
    <associate|footnote-2.13.1|<tuple|2.13.1|36|stories/c2-raspiblitz.tm>>
    <associate|footnote-2.13.2|<tuple|2.13.2|36|stories/c2-raspiblitz.tm>>
    <associate|footnote-2.13.3|<tuple|2.13.3|36|stories/c2-raspiblitz.tm>>
    <associate|footnote-2.13.4|<tuple|2.13.4|36|stories/c2-raspiblitz.tm>>
    <associate|footnote-2.13.5|<tuple|2.13.5|36|stories/c2-raspiblitz.tm>>
    <associate|footnote-2.13.6|<tuple|2.13.6|36|stories/c2-raspiblitz.tm>>
    <associate|footnote-2.13.7|<tuple|2.13.7|36|stories/c2-raspiblitz.tm>>
    <associate|footnote-2.13.8|<tuple|2.13.8|36|stories/c2-raspiblitz.tm>>
    <associate|footnote-2.14.1|<tuple|2.14.1|37|stories/c2-satoshi-labs.tm>>
    <associate|footnote-2.14.10|<tuple|2.14.10|37|stories/c2-satoshi-labs.tm>>
    <associate|footnote-2.14.11|<tuple|2.14.11|37|stories/c2-satoshi-labs.tm>>
    <associate|footnote-2.14.12|<tuple|2.14.12|37|stories/c2-satoshi-labs.tm>>
    <associate|footnote-2.14.13|<tuple|2.14.13|38|stories/c2-satoshi-labs.tm>>
    <associate|footnote-2.14.14|<tuple|2.14.14|38|stories/c2-satoshi-labs.tm>>
    <associate|footnote-2.14.2|<tuple|2.14.2|37|stories/c2-satoshi-labs.tm>>
    <associate|footnote-2.14.3|<tuple|2.14.3|37|stories/c2-satoshi-labs.tm>>
    <associate|footnote-2.14.4|<tuple|2.14.4|37|stories/c2-satoshi-labs.tm>>
    <associate|footnote-2.14.5|<tuple|2.14.5|37|stories/c2-satoshi-labs.tm>>
    <associate|footnote-2.14.6|<tuple|2.14.6|37|stories/c2-satoshi-labs.tm>>
    <associate|footnote-2.14.7|<tuple|2.14.7|37|stories/c2-satoshi-labs.tm>>
    <associate|footnote-2.14.8|<tuple|2.14.8|37|stories/c2-satoshi-labs.tm>>
    <associate|footnote-2.14.9|<tuple|2.14.9|37|stories/c2-satoshi-labs.tm>>
    <associate|footnote-2.15.1|<tuple|2.15.1|39|stories/c2-tails.tm>>
    <associate|footnote-2.15.10|<tuple|2.15.10|39|stories/c2-tails.tm>>
    <associate|footnote-2.15.11|<tuple|2.15.11|39|stories/c2-tails.tm>>
    <associate|footnote-2.15.12|<tuple|2.15.12|39|stories/c2-tails.tm>>
    <associate|footnote-2.15.13|<tuple|2.15.13|39|stories/c2-tails.tm>>
    <associate|footnote-2.15.14|<tuple|2.15.14|39|stories/c2-tails.tm>>
    <associate|footnote-2.15.15|<tuple|2.15.15|40|stories/c2-tails.tm>>
    <associate|footnote-2.15.16|<tuple|2.15.16|40|stories/c2-tails.tm>>
    <associate|footnote-2.15.17|<tuple|2.15.17|40|stories/c2-tails.tm>>
    <associate|footnote-2.15.18|<tuple|2.15.18|40|stories/c2-tails.tm>>
    <associate|footnote-2.15.19|<tuple|2.15.19|40|stories/c2-tails.tm>>
    <associate|footnote-2.15.2|<tuple|2.15.2|39|stories/c2-tails.tm>>
    <associate|footnote-2.15.20|<tuple|2.15.20|40|stories/c2-tails.tm>>
    <associate|footnote-2.15.21|<tuple|2.15.21|40|stories/c2-tails.tm>>
    <associate|footnote-2.15.3|<tuple|2.15.3|39|stories/c2-tails.tm>>
    <associate|footnote-2.15.4|<tuple|2.15.4|39|stories/c2-tails.tm>>
    <associate|footnote-2.15.5|<tuple|2.15.5|39|stories/c2-tails.tm>>
    <associate|footnote-2.15.6|<tuple|2.15.6|39|stories/c2-tails.tm>>
    <associate|footnote-2.15.7|<tuple|2.15.7|39|stories/c2-tails.tm>>
    <associate|footnote-2.15.8|<tuple|2.15.8|39|stories/c2-tails.tm>>
    <associate|footnote-2.15.9|<tuple|2.15.9|39|stories/c2-tails.tm>>
    <associate|footnote-2.16.1|<tuple|2.16.1|41|stories/c2-tor-browser.tm>>
    <associate|footnote-2.16.2|<tuple|2.16.2|41|stories/c2-tor-browser.tm>>
    <associate|footnote-2.16.3|<tuple|2.16.3|41|stories/c2-tor-browser.tm>>
    <associate|footnote-2.16.4|<tuple|2.16.4|41|stories/c2-tor-browser.tm>>
    <associate|footnote-2.16.5|<tuple|2.16.5|41|stories/c2-tor-browser.tm>>
    <associate|footnote-2.16.6|<tuple|2.16.6|41|stories/c2-tor-browser.tm>>
    <associate|footnote-2.16.7|<tuple|2.16.7|41|stories/c2-tor-browser.tm>>
    <associate|footnote-2.17.1|<tuple|2.17.1|42|stories/c2-trisquel.tm>>
    <associate|footnote-2.17.2|<tuple|2.17.2|42|stories/c2-trisquel.tm>>
    <associate|footnote-2.17.3|<tuple|2.17.3|42|stories/c2-trisquel.tm>>
    <associate|footnote-2.17.4|<tuple|2.17.4|42|stories/c2-trisquel.tm>>
    <associate|footnote-2.18.1|<tuple|2.18.1|43|stories/c2-veracrypt.tm>>
    <associate|footnote-2.18.2|<tuple|2.18.2|43|stories/c2-veracrypt.tm>>
    <associate|footnote-2.18.3|<tuple|2.18.3|43|stories/c2-veracrypt.tm>>
    <associate|footnote-2.18.4|<tuple|2.18.4|43|stories/c2-veracrypt.tm>>
    <associate|footnote-2.18.5|<tuple|2.18.5|43|stories/c2-veracrypt.tm>>
    <associate|footnote-2.18.6|<tuple|2.18.6|43|stories/c2-veracrypt.tm>>
    <associate|footnote-2.18.7|<tuple|2.18.7|43|stories/c2-veracrypt.tm>>
    <associate|footnote-2.18.8|<tuple|2.18.8|43|stories/c2-veracrypt.tm>>
    <associate|footnote-2.18.9|<tuple|2.18.9|44|stories/c2-veracrypt.tm>>
    <associate|footnote-2.19.1|<tuple|2.19.1|45|stories/c2-youtube-dl.tm>>
    <associate|footnote-2.19.10|<tuple|2.19.10|45|stories/c2-youtube-dl.tm>>
    <associate|footnote-2.19.2|<tuple|2.19.2|45|stories/c2-youtube-dl.tm>>
    <associate|footnote-2.19.3|<tuple|2.19.3|45|stories/c2-youtube-dl.tm>>
    <associate|footnote-2.19.4|<tuple|2.19.4|45|stories/c2-youtube-dl.tm>>
    <associate|footnote-2.19.5|<tuple|2.19.5|45|stories/c2-youtube-dl.tm>>
    <associate|footnote-2.19.6|<tuple|2.19.6|45|stories/c2-youtube-dl.tm>>
    <associate|footnote-2.19.7|<tuple|2.19.7|45|stories/c2-youtube-dl.tm>>
    <associate|footnote-2.19.8|<tuple|2.19.8|45|stories/c2-youtube-dl.tm>>
    <associate|footnote-2.19.9|<tuple|2.19.9|45|stories/c2-youtube-dl.tm>>
    <associate|footnote-2.2.1|<tuple|2.2.1|16|stories/c2-cryptomator.tm>>
    <associate|footnote-2.2.10|<tuple|2.2.10|16|stories/c2-cryptomator.tm>>
    <associate|footnote-2.2.11|<tuple|2.2.11|16|stories/c2-cryptomator.tm>>
    <associate|footnote-2.2.12|<tuple|2.2.12|16|stories/c2-cryptomator.tm>>
    <associate|footnote-2.2.2|<tuple|2.2.2|16|stories/c2-cryptomator.tm>>
    <associate|footnote-2.2.3|<tuple|2.2.3|16|stories/c2-cryptomator.tm>>
    <associate|footnote-2.2.4|<tuple|2.2.4|16|stories/c2-cryptomator.tm>>
    <associate|footnote-2.2.5|<tuple|2.2.5|16|stories/c2-cryptomator.tm>>
    <associate|footnote-2.2.6|<tuple|2.2.6|16|stories/c2-cryptomator.tm>>
    <associate|footnote-2.2.7|<tuple|2.2.7|16|stories/c2-cryptomator.tm>>
    <associate|footnote-2.2.8|<tuple|2.2.8|16|stories/c2-cryptomator.tm>>
    <associate|footnote-2.2.9|<tuple|2.2.9|16|stories/c2-cryptomator.tm>>
    <associate|footnote-2.20.1|<tuple|2.20.1|47|stories/c2-yt-dlp.tm>>
    <associate|footnote-2.20.2|<tuple|2.20.2|47|stories/c2-yt-dlp.tm>>
    <associate|footnote-2.20.3|<tuple|2.20.3|47|stories/c2-yt-dlp.tm>>
    <associate|footnote-2.20.4|<tuple|2.20.4|47|stories/c2-yt-dlp.tm>>
    <associate|footnote-2.3.1|<tuple|2.3.1|18|stories/c2-debian.tm>>
    <associate|footnote-2.3.10|<tuple|2.3.10|19|stories/c2-debian.tm>>
    <associate|footnote-2.3.11|<tuple|2.3.11|19|stories/c2-debian.tm>>
    <associate|footnote-2.3.12|<tuple|2.3.12|19|stories/c2-debian.tm>>
    <associate|footnote-2.3.13|<tuple|2.3.13|19|stories/c2-debian.tm>>
    <associate|footnote-2.3.14|<tuple|2.3.14|19|stories/c2-debian.tm>>
    <associate|footnote-2.3.2|<tuple|2.3.2|18|stories/c2-debian.tm>>
    <associate|footnote-2.3.3|<tuple|2.3.3|18|stories/c2-debian.tm>>
    <associate|footnote-2.3.4|<tuple|2.3.4|19|stories/c2-debian.tm>>
    <associate|footnote-2.3.5|<tuple|2.3.5|19|stories/c2-debian.tm>>
    <associate|footnote-2.3.6|<tuple|2.3.6|19|stories/c2-debian.tm>>
    <associate|footnote-2.3.7|<tuple|2.3.7|19|stories/c2-debian.tm>>
    <associate|footnote-2.3.8|<tuple|2.3.8|19|stories/c2-debian.tm>>
    <associate|footnote-2.3.9|<tuple|2.3.9|19|stories/c2-debian.tm>>
    <associate|footnote-2.4.1|<tuple|2.4.1|22|stories/c2-electrum.tm>>
    <associate|footnote-2.4.10|<tuple|2.4.10|22|stories/c2-electrum.tm>>
    <associate|footnote-2.4.11|<tuple|2.4.11|23|stories/c2-electrum.tm>>
    <associate|footnote-2.4.12|<tuple|2.4.12|23|stories/c2-electrum.tm>>
    <associate|footnote-2.4.13|<tuple|2.4.13|23|stories/c2-electrum.tm>>
    <associate|footnote-2.4.14|<tuple|2.4.14|23|stories/c2-electrum.tm>>
    <associate|footnote-2.4.2|<tuple|2.4.2|22|stories/c2-electrum.tm>>
    <associate|footnote-2.4.3|<tuple|2.4.3|22|stories/c2-electrum.tm>>
    <associate|footnote-2.4.4|<tuple|2.4.4|22|stories/c2-electrum.tm>>
    <associate|footnote-2.4.5|<tuple|2.4.5|22|stories/c2-electrum.tm>>
    <associate|footnote-2.4.6|<tuple|2.4.6|22|stories/c2-electrum.tm>>
    <associate|footnote-2.4.7|<tuple|2.4.7|22|stories/c2-electrum.tm>>
    <associate|footnote-2.4.8|<tuple|2.4.8|22|stories/c2-electrum.tm>>
    <associate|footnote-2.4.9|<tuple|2.4.9|22|stories/c2-electrum.tm>>
    <associate|footnote-2.5.1|<tuple|2.5.1|24|stories/c2-element.tm>>
    <associate|footnote-2.5.10|<tuple|2.5.10|24|stories/c2-element.tm>>
    <associate|footnote-2.5.2|<tuple|2.5.2|24|stories/c2-element.tm>>
    <associate|footnote-2.5.3|<tuple|2.5.3|24|stories/c2-element.tm>>
    <associate|footnote-2.5.4|<tuple|2.5.4|24|stories/c2-element.tm>>
    <associate|footnote-2.5.5|<tuple|2.5.5|24|stories/c2-element.tm>>
    <associate|footnote-2.5.6|<tuple|2.5.6|24|stories/c2-element.tm>>
    <associate|footnote-2.5.7|<tuple|2.5.7|24|stories/c2-element.tm>>
    <associate|footnote-2.5.8|<tuple|2.5.8|24|stories/c2-element.tm>>
    <associate|footnote-2.5.9|<tuple|2.5.9|24|stories/c2-element.tm>>
    <associate|footnote-2.6.1|<tuple|2.6.1|25|stories/c2-fdroid.tm>>
    <associate|footnote-2.6.2|<tuple|2.6.2|25|stories/c2-fdroid.tm>>
    <associate|footnote-2.6.3|<tuple|2.6.3|25|stories/c2-fdroid.tm>>
    <associate|footnote-2.6.4|<tuple|2.6.4|25|stories/c2-fdroid.tm>>
    <associate|footnote-2.6.5|<tuple|2.6.5|25|stories/c2-fdroid.tm>>
    <associate|footnote-2.6.6|<tuple|2.6.6|26|stories/c2-fdroid.tm>>
    <associate|footnote-2.6.7|<tuple|2.6.7|26|stories/c2-fdroid.tm>>
    <associate|footnote-2.7.1|<tuple|2.7.1|27|stories/c2-freedombox.tm>>
    <associate|footnote-2.7.2|<tuple|2.7.2|27|stories/c2-freedombox.tm>>
    <associate|footnote-2.7.3|<tuple|2.7.3|27|stories/c2-freedombox.tm>>
    <associate|footnote-2.7.4|<tuple|2.7.4|27|stories/c2-freedombox.tm>>
    <associate|footnote-2.7.5|<tuple|2.7.5|27|stories/c2-freedombox.tm>>
    <associate|footnote-2.8.1|<tuple|2.8.1|29|stories/c2-github.tm>>
    <associate|footnote-2.8.10|<tuple|2.8.10|29|stories/c2-github.tm>>
    <associate|footnote-2.8.11|<tuple|2.8.11|29|stories/c2-github.tm>>
    <associate|footnote-2.8.2|<tuple|2.8.2|29|stories/c2-github.tm>>
    <associate|footnote-2.8.3|<tuple|2.8.3|29|stories/c2-github.tm>>
    <associate|footnote-2.8.4|<tuple|2.8.4|29|stories/c2-github.tm>>
    <associate|footnote-2.8.5|<tuple|2.8.5|29|stories/c2-github.tm>>
    <associate|footnote-2.8.6|<tuple|2.8.6|29|stories/c2-github.tm>>
    <associate|footnote-2.8.7|<tuple|2.8.7|29|stories/c2-github.tm>>
    <associate|footnote-2.8.8|<tuple|2.8.8|29|stories/c2-github.tm>>
    <associate|footnote-2.8.9|<tuple|2.8.9|29|stories/c2-github.tm>>
    <associate|footnote-2.9.1|<tuple|2.9.1|30|stories/c2-gnupg.tm>>
    <associate|footnote-2.9.10|<tuple|2.9.10|30|stories/c2-gnupg.tm>>
    <associate|footnote-2.9.11|<tuple|2.9.11|30|stories/c2-gnupg.tm>>
    <associate|footnote-2.9.12|<tuple|2.9.12|30|stories/c2-gnupg.tm>>
    <associate|footnote-2.9.13|<tuple|2.9.13|30|stories/c2-gnupg.tm>>
    <associate|footnote-2.9.14|<tuple|2.9.14|30|stories/c2-gnupg.tm>>
    <associate|footnote-2.9.15|<tuple|2.9.15|31|stories/c2-gnupg.tm>>
    <associate|footnote-2.9.16|<tuple|2.9.16|31|stories/c2-gnupg.tm>>
    <associate|footnote-2.9.17|<tuple|2.9.17|31|stories/c2-gnupg.tm>>
    <associate|footnote-2.9.18|<tuple|2.9.18|31|stories/c2-gnupg.tm>>
    <associate|footnote-2.9.19|<tuple|2.9.19|31|stories/c2-gnupg.tm>>
    <associate|footnote-2.9.2|<tuple|2.9.2|30|stories/c2-gnupg.tm>>
    <associate|footnote-2.9.20|<tuple|2.9.20|31|stories/c2-gnupg.tm>>
    <associate|footnote-2.9.21|<tuple|2.9.21|31|stories/c2-gnupg.tm>>
    <associate|footnote-2.9.22|<tuple|2.9.22|31|stories/c2-gnupg.tm>>
    <associate|footnote-2.9.23|<tuple|2.9.23|31|stories/c2-gnupg.tm>>
    <associate|footnote-2.9.3|<tuple|2.9.3|30|stories/c2-gnupg.tm>>
    <associate|footnote-2.9.4|<tuple|2.9.4|30|stories/c2-gnupg.tm>>
    <associate|footnote-2.9.5|<tuple|2.9.5|30|stories/c2-gnupg.tm>>
    <associate|footnote-2.9.6|<tuple|2.9.6|30|stories/c2-gnupg.tm>>
    <associate|footnote-2.9.7|<tuple|2.9.7|30|stories/c2-gnupg.tm>>
    <associate|footnote-2.9.8|<tuple|2.9.8|30|stories/c2-gnupg.tm>>
    <associate|footnote-2.9.9|<tuple|2.9.9|30|stories/c2-gnupg.tm>>
    <associate|footnote-A.0.1|<tuple|A.0.1|49|a-how-gnupg.tm>>
    <associate|footnote-A.1.1|<tuple|A.1.1|49|a-how-gnupg.tm>>
    <associate|footnote-A.1.10|<tuple|A.1.10|52|a-how-gnupg.tm>>
    <associate|footnote-A.1.2|<tuple|A.1.2|49|a-how-gnupg.tm>>
    <associate|footnote-A.1.3|<tuple|A.1.3|50|a-how-gnupg.tm>>
    <associate|footnote-A.1.4|<tuple|A.1.4|50|a-how-gnupg.tm>>
    <associate|footnote-A.1.5|<tuple|A.1.5|50|a-how-gnupg.tm>>
    <associate|footnote-A.1.6|<tuple|A.1.6|51|a-how-gnupg.tm>>
    <associate|footnote-A.1.7|<tuple|A.1.7|51|a-how-gnupg.tm>>
    <associate|footnote-A.1.8|<tuple|A.1.8|51|a-how-gnupg.tm>>
    <associate|footnote-A.1.9|<tuple|A.1.9|52|a-how-gnupg.tm>>
    <associate|footnote-A.2.1|<tuple|A.2.1|52|a-how-gnupg.tm>>
    <associate|footnote-A.2.2|<tuple|A.2.2|53|a-how-gnupg.tm>>
    <associate|footnote-A.2.3|<tuple|A.2.3|53|a-how-gnupg.tm>>
    <associate|footnote-A.2.4|<tuple|A.2.4|53|a-how-gnupg.tm>>
    <associate|footnote-A.2.5|<tuple|A.2.5|53|a-how-gnupg.tm>>
    <associate|footnote-A.2.6|<tuple|A.2.6|54|a-how-gnupg.tm>>
    <associate|footnr-1.3.1|<tuple|1.3.1|10|c1.tm>>
    <associate|footnr-2.0.1|<tuple|2.0.1|11|c2.tm>>
    <associate|footnr-2.1.1|<tuple|2.1.1|12|stories/c2-bitcoin-core.tm>>
    <associate|footnr-2.1.10|<tuple|2.1.10|12|stories/c2-bitcoin-core.tm>>
    <associate|footnr-2.1.11|<tuple|2.1.11|12|stories/c2-bitcoin-core.tm>>
    <associate|footnr-2.1.12|<tuple|2.1.12|12|stories/c2-bitcoin-core.tm>>
    <associate|footnr-2.1.13|<tuple|2.1.13|12|stories/c2-bitcoin-core.tm>>
    <associate|footnr-2.1.14|<tuple|2.1.14|13|stories/c2-bitcoin-core.tm>>
    <associate|footnr-2.1.15|<tuple|2.1.15|13|stories/c2-bitcoin-core.tm>>
    <associate|footnr-2.1.16|<tuple|2.1.16|13|stories/c2-bitcoin-core.tm>>
    <associate|footnr-2.1.17|<tuple|2.1.17|13|stories/c2-bitcoin-core.tm>>
    <associate|footnr-2.1.18|<tuple|2.1.18|13|stories/c2-bitcoin-core.tm>>
    <associate|footnr-2.1.19|<tuple|2.1.19|13|stories/c2-bitcoin-core.tm>>
    <associate|footnr-2.1.20|<tuple|2.1.20|13|stories/c2-bitcoin-core.tm>>
    <associate|footnr-2.1.21|<tuple|2.1.21|13|stories/c2-bitcoin-core.tm>>
    <associate|footnr-2.1.22|<tuple|2.1.22|13|stories/c2-bitcoin-core.tm>>
    <associate|footnr-2.1.23|<tuple|2.1.23|13|stories/c2-bitcoin-core.tm>>
    <associate|footnr-2.1.24|<tuple|2.1.24|14|stories/c2-bitcoin-core.tm>>
    <associate|footnr-2.1.25|<tuple|2.1.25|14|stories/c2-bitcoin-core.tm>>
    <associate|footnr-2.1.26|<tuple|2.1.26|14|stories/c2-bitcoin-core.tm>>
    <associate|footnr-2.1.27|<tuple|2.1.27|15|stories/c2-bitcoin-core.tm>>
    <associate|footnr-2.1.28|<tuple|2.1.28|15|stories/c2-bitcoin-core.tm>>
    <associate|footnr-2.1.29|<tuple|2.1.29|15|stories/c2-bitcoin-core.tm>>
    <associate|footnr-2.1.3|<tuple|People|12|stories/c2-bitcoin-core.tm>>
    <associate|footnr-2.1.4|<tuple|2.1.4|12|stories/c2-bitcoin-core.tm>>
    <associate|footnr-2.1.5|<tuple|2.1.5|12|stories/c2-bitcoin-core.tm>>
    <associate|footnr-2.1.6|<tuple|2.1.6|12|stories/c2-bitcoin-core.tm>>
    <associate|footnr-2.1.7|<tuple|2.1.7|12|stories/c2-bitcoin-core.tm>>
    <associate|footnr-2.1.8|<tuple|2.1.8|12|stories/c2-bitcoin-core.tm>>
    <associate|footnr-2.1.9|<tuple|2.1.9|12|stories/c2-bitcoin-core.tm>>
    <associate|footnr-2.10.1|<tuple|2.10.1|32|stories/c2-inkscape.tm>>
    <associate|footnr-2.10.2|<tuple|2.10.2|32|stories/c2-inkscape.tm>>
    <associate|footnr-2.10.3|<tuple|2.10.3|32|stories/c2-inkscape.tm>>
    <associate|footnr-2.10.4|<tuple|2.10.4|32|stories/c2-inkscape.tm>>
    <associate|footnr-2.10.5|<tuple|2.10.5|32|stories/c2-inkscape.tm>>
    <associate|footnr-2.11.1|<tuple|2.11.1|33|stories/c2-keepassxc.tm>>
    <associate|footnr-2.11.2|<tuple|2.11.2|33|stories/c2-keepassxc.tm>>
    <associate|footnr-2.11.3|<tuple|2.11.3|33|stories/c2-keepassxc.tm>>
    <associate|footnr-2.11.4|<tuple|2.11.4|33|stories/c2-keepassxc.tm>>
    <associate|footnr-2.11.5|<tuple|2.11.5|33|stories/c2-keepassxc.tm>>
    <associate|footnr-2.11.6|<tuple|2.11.6|33|stories/c2-keepassxc.tm>>
    <associate|footnr-2.11.7|<tuple|2.11.7|33|stories/c2-keepassxc.tm>>
    <associate|footnr-2.11.8|<tuple|2.11.8|33|stories/c2-keepassxc.tm>>
    <associate|footnr-2.12.1|<tuple|2.12.1|34|stories/c2-qubesos.tm>>
    <associate|footnr-2.12.2|<tuple|2.12.2|34|stories/c2-qubesos.tm>>
    <associate|footnr-2.12.3|<tuple|2.12.3|34|stories/c2-qubesos.tm>>
    <associate|footnr-2.12.4|<tuple|2.12.4|34|stories/c2-qubesos.tm>>
    <associate|footnr-2.12.5|<tuple|2.12.5|34|stories/c2-qubesos.tm>>
    <associate|footnr-2.12.6|<tuple|2.12.6|34|stories/c2-qubesos.tm>>
    <associate|footnr-2.12.7|<tuple|2.12.7|34|stories/c2-qubesos.tm>>
    <associate|footnr-2.12.8|<tuple|2.12.8|35|stories/c2-qubesos.tm>>
    <associate|footnr-2.12.9|<tuple|2.12.9|35|stories/c2-qubesos.tm>>
    <associate|footnr-2.13.1|<tuple|2.13.1|36|stories/c2-raspiblitz.tm>>
    <associate|footnr-2.13.2|<tuple|2.13.2|36|stories/c2-raspiblitz.tm>>
    <associate|footnr-2.13.3|<tuple|2.13.3|36|stories/c2-raspiblitz.tm>>
    <associate|footnr-2.13.4|<tuple|2.13.4|36|stories/c2-raspiblitz.tm>>
    <associate|footnr-2.13.5|<tuple|2.13.5|36|stories/c2-raspiblitz.tm>>
    <associate|footnr-2.13.6|<tuple|2.13.6|36|stories/c2-raspiblitz.tm>>
    <associate|footnr-2.13.7|<tuple|2.13.7|36|stories/c2-raspiblitz.tm>>
    <associate|footnr-2.13.8|<tuple|2.13.8|36|stories/c2-raspiblitz.tm>>
    <associate|footnr-2.14.1|<tuple|2.14.1|37|stories/c2-satoshi-labs.tm>>
    <associate|footnr-2.14.10|<tuple|2.14.10|37|stories/c2-satoshi-labs.tm>>
    <associate|footnr-2.14.11|<tuple|2.14.11|37|stories/c2-satoshi-labs.tm>>
    <associate|footnr-2.14.12|<tuple|2.14.12|37|stories/c2-satoshi-labs.tm>>
    <associate|footnr-2.14.13|<tuple|2.14.13|38|stories/c2-satoshi-labs.tm>>
    <associate|footnr-2.14.14|<tuple|2.14.14|38|stories/c2-satoshi-labs.tm>>
    <associate|footnr-2.14.2|<tuple|2.14.2|37|stories/c2-satoshi-labs.tm>>
    <associate|footnr-2.14.3|<tuple|2.14.3|37|stories/c2-satoshi-labs.tm>>
    <associate|footnr-2.14.4|<tuple|2.14.4|37|stories/c2-satoshi-labs.tm>>
    <associate|footnr-2.14.5|<tuple|2.14.5|37|stories/c2-satoshi-labs.tm>>
    <associate|footnr-2.14.6|<tuple|2.14.6|37|stories/c2-satoshi-labs.tm>>
    <associate|footnr-2.14.7|<tuple|2.14.7|37|stories/c2-satoshi-labs.tm>>
    <associate|footnr-2.14.8|<tuple|2.14.8|37|stories/c2-satoshi-labs.tm>>
    <associate|footnr-2.14.9|<tuple|2.14.9|37|stories/c2-satoshi-labs.tm>>
    <associate|footnr-2.15.1|<tuple|2.15.1|39|stories/c2-tails.tm>>
    <associate|footnr-2.15.10|<tuple|2.15.10|39|stories/c2-tails.tm>>
    <associate|footnr-2.15.11|<tuple|2.15.11|39|stories/c2-tails.tm>>
    <associate|footnr-2.15.12|<tuple|2.15.12|39|stories/c2-tails.tm>>
    <associate|footnr-2.15.13|<tuple|2.15.13|39|stories/c2-tails.tm>>
    <associate|footnr-2.15.14|<tuple|2.15.14|39|stories/c2-tails.tm>>
    <associate|footnr-2.15.15|<tuple|2.15.15|40|stories/c2-tails.tm>>
    <associate|footnr-2.15.16|<tuple|2.15.16|40|stories/c2-tails.tm>>
    <associate|footnr-2.15.17|<tuple|2.15.17|40|stories/c2-tails.tm>>
    <associate|footnr-2.15.18|<tuple|2.15.18|40|stories/c2-tails.tm>>
    <associate|footnr-2.15.19|<tuple|2.15.19|40|stories/c2-tails.tm>>
    <associate|footnr-2.15.2|<tuple|2.15.2|39|stories/c2-tails.tm>>
    <associate|footnr-2.15.20|<tuple|2.15.20|40|stories/c2-tails.tm>>
    <associate|footnr-2.15.21|<tuple|2.15.21|40|stories/c2-tails.tm>>
    <associate|footnr-2.15.3|<tuple|2.15.3|39|stories/c2-tails.tm>>
    <associate|footnr-2.15.4|<tuple|2.15.4|39|stories/c2-tails.tm>>
    <associate|footnr-2.15.5|<tuple|2.15.5|39|stories/c2-tails.tm>>
    <associate|footnr-2.15.6|<tuple|2.15.6|39|stories/c2-tails.tm>>
    <associate|footnr-2.15.7|<tuple|2.15.7|39|stories/c2-tails.tm>>
    <associate|footnr-2.15.8|<tuple|2.15.8|39|stories/c2-tails.tm>>
    <associate|footnr-2.15.9|<tuple|2.15.9|39|stories/c2-tails.tm>>
    <associate|footnr-2.16.1|<tuple|2.16.1|41|stories/c2-tor-browser.tm>>
    <associate|footnr-2.16.2|<tuple|2.16.2|41|stories/c2-tor-browser.tm>>
    <associate|footnr-2.16.3|<tuple|2.16.3|41|stories/c2-tor-browser.tm>>
    <associate|footnr-2.16.4|<tuple|2.16.4|41|stories/c2-tor-browser.tm>>
    <associate|footnr-2.16.5|<tuple|2.16.5|41|stories/c2-tor-browser.tm>>
    <associate|footnr-2.16.6|<tuple|2.16.6|41|stories/c2-tor-browser.tm>>
    <associate|footnr-2.16.7|<tuple|2.16.7|41|stories/c2-tor-browser.tm>>
    <associate|footnr-2.17.1|<tuple|2.17.1|42|stories/c2-trisquel.tm>>
    <associate|footnr-2.17.2|<tuple|2.17.2|42|stories/c2-trisquel.tm>>
    <associate|footnr-2.17.3|<tuple|2.17.3|42|stories/c2-trisquel.tm>>
    <associate|footnr-2.17.4|<tuple|2.17.4|42|stories/c2-trisquel.tm>>
    <associate|footnr-2.18.1|<tuple|2.18.1|43|stories/c2-veracrypt.tm>>
    <associate|footnr-2.18.2|<tuple|2.18.2|43|stories/c2-veracrypt.tm>>
    <associate|footnr-2.18.3|<tuple|2.18.3|43|stories/c2-veracrypt.tm>>
    <associate|footnr-2.18.4|<tuple|2.18.4|43|stories/c2-veracrypt.tm>>
    <associate|footnr-2.18.5|<tuple|2.18.5|43|stories/c2-veracrypt.tm>>
    <associate|footnr-2.18.6|<tuple|2.18.6|43|stories/c2-veracrypt.tm>>
    <associate|footnr-2.18.7|<tuple|2.18.7|43|stories/c2-veracrypt.tm>>
    <associate|footnr-2.18.8|<tuple|2.18.8|43|stories/c2-veracrypt.tm>>
    <associate|footnr-2.18.9|<tuple|2.18.9|44|stories/c2-veracrypt.tm>>
    <associate|footnr-2.19.1|<tuple|2.19.1|45|stories/c2-youtube-dl.tm>>
    <associate|footnr-2.19.10|<tuple|2.19.10|45|stories/c2-youtube-dl.tm>>
    <associate|footnr-2.19.2|<tuple|2.19.2|45|stories/c2-youtube-dl.tm>>
    <associate|footnr-2.19.3|<tuple|2.19.3|45|stories/c2-youtube-dl.tm>>
    <associate|footnr-2.19.4|<tuple|2.19.4|45|stories/c2-youtube-dl.tm>>
    <associate|footnr-2.19.5|<tuple|2.19.5|45|stories/c2-youtube-dl.tm>>
    <associate|footnr-2.19.6|<tuple|2.19.6|45|stories/c2-youtube-dl.tm>>
    <associate|footnr-2.19.7|<tuple|2.19.7|45|stories/c2-youtube-dl.tm>>
    <associate|footnr-2.19.8|<tuple|2.19.8|45|stories/c2-youtube-dl.tm>>
    <associate|footnr-2.19.9|<tuple|2.19.9|45|stories/c2-youtube-dl.tm>>
    <associate|footnr-2.2.1|<tuple|2.2.1|16|stories/c2-cryptomator.tm>>
    <associate|footnr-2.2.10|<tuple|2.2.10|16|stories/c2-cryptomator.tm>>
    <associate|footnr-2.2.11|<tuple|2.2.11|16|stories/c2-cryptomator.tm>>
    <associate|footnr-2.2.12|<tuple|2.2.12|16|stories/c2-cryptomator.tm>>
    <associate|footnr-2.2.2|<tuple|2.2.2|16|stories/c2-cryptomator.tm>>
    <associate|footnr-2.2.3|<tuple|2.2.3|16|stories/c2-cryptomator.tm>>
    <associate|footnr-2.2.4|<tuple|2.2.4|16|stories/c2-cryptomator.tm>>
    <associate|footnr-2.2.5|<tuple|2.2.5|16|stories/c2-cryptomator.tm>>
    <associate|footnr-2.2.6|<tuple|2.2.6|16|stories/c2-cryptomator.tm>>
    <associate|footnr-2.2.7|<tuple|2.2.7|16|stories/c2-cryptomator.tm>>
    <associate|footnr-2.2.8|<tuple|2.2.8|16|stories/c2-cryptomator.tm>>
    <associate|footnr-2.2.9|<tuple|2.2.9|16|stories/c2-cryptomator.tm>>
    <associate|footnr-2.20.1|<tuple|2.20.1|47|stories/c2-yt-dlp.tm>>
    <associate|footnr-2.20.2|<tuple|2.20.2|47|stories/c2-yt-dlp.tm>>
    <associate|footnr-2.20.3|<tuple|2.20.3|47|stories/c2-yt-dlp.tm>>
    <associate|footnr-2.20.4|<tuple|2.20.4|47|stories/c2-yt-dlp.tm>>
    <associate|footnr-2.3.1|<tuple|2.3.1|18|stories/c2-debian.tm>>
    <associate|footnr-2.3.10|<tuple|2.3.10|19|stories/c2-debian.tm>>
    <associate|footnr-2.3.11|<tuple|2.3.11|19|stories/c2-debian.tm>>
    <associate|footnr-2.3.12|<tuple|2.3.12|19|stories/c2-debian.tm>>
    <associate|footnr-2.3.13|<tuple|2.3.13|19|stories/c2-debian.tm>>
    <associate|footnr-2.3.14|<tuple|2.3.14|19|stories/c2-debian.tm>>
    <associate|footnr-2.3.2|<tuple|2.3.2|18|stories/c2-debian.tm>>
    <associate|footnr-2.3.3|<tuple|2.3.3|18|stories/c2-debian.tm>>
    <associate|footnr-2.3.4|<tuple|2.3.6|19|stories/c2-debian.tm>>
    <associate|footnr-2.3.5|<tuple|2.3.6|19|stories/c2-debian.tm>>
    <associate|footnr-2.3.6|<tuple|2.3.6|19|stories/c2-debian.tm>>
    <associate|footnr-2.3.7|<tuple|2.3.7|19|stories/c2-debian.tm>>
    <associate|footnr-2.3.8|<tuple|2.3.8|19|stories/c2-debian.tm>>
    <associate|footnr-2.3.9|<tuple|2.3.9|19|stories/c2-debian.tm>>
    <associate|footnr-2.4.1|<tuple|2.4.1|22|stories/c2-electrum.tm>>
    <associate|footnr-2.4.10|<tuple|2.4.10|22|stories/c2-electrum.tm>>
    <associate|footnr-2.4.11|<tuple|2.4.11|23|stories/c2-electrum.tm>>
    <associate|footnr-2.4.12|<tuple|2.4.12|23|stories/c2-electrum.tm>>
    <associate|footnr-2.4.13|<tuple|2.4.13|23|stories/c2-electrum.tm>>
    <associate|footnr-2.4.14|<tuple|2.4.14|23|stories/c2-electrum.tm>>
    <associate|footnr-2.4.2|<tuple|2.4.2|22|stories/c2-electrum.tm>>
    <associate|footnr-2.4.3|<tuple|2.4.3|22|stories/c2-electrum.tm>>
    <associate|footnr-2.4.4|<tuple|2.4.4|22|stories/c2-electrum.tm>>
    <associate|footnr-2.4.5|<tuple|2.4.5|22|stories/c2-electrum.tm>>
    <associate|footnr-2.4.6|<tuple|2.4.6|22|stories/c2-electrum.tm>>
    <associate|footnr-2.4.7|<tuple|2.4.7|22|stories/c2-electrum.tm>>
    <associate|footnr-2.4.8|<tuple|2.4.8|22|stories/c2-electrum.tm>>
    <associate|footnr-2.4.9|<tuple|2.4.9|22|stories/c2-electrum.tm>>
    <associate|footnr-2.5.1|<tuple|2.5.1|24|stories/c2-element.tm>>
    <associate|footnr-2.5.10|<tuple|2.5.10|24|stories/c2-element.tm>>
    <associate|footnr-2.5.2|<tuple|2.5.2|24|stories/c2-element.tm>>
    <associate|footnr-2.5.3|<tuple|2.5.3|24|stories/c2-element.tm>>
    <associate|footnr-2.5.4|<tuple|2.5.4|24|stories/c2-element.tm>>
    <associate|footnr-2.5.5|<tuple|2.5.5|24|stories/c2-element.tm>>
    <associate|footnr-2.5.6|<tuple|2.5.6|24|stories/c2-element.tm>>
    <associate|footnr-2.5.7|<tuple|2.5.7|24|stories/c2-element.tm>>
    <associate|footnr-2.5.8|<tuple|2.5.8|24|stories/c2-element.tm>>
    <associate|footnr-2.5.9|<tuple|2.5.9|24|stories/c2-element.tm>>
    <associate|footnr-2.6.1|<tuple|2.6.1|25|stories/c2-fdroid.tm>>
    <associate|footnr-2.6.2|<tuple|2.6.2|25|stories/c2-fdroid.tm>>
    <associate|footnr-2.6.3|<tuple|2.6.3|25|stories/c2-fdroid.tm>>
    <associate|footnr-2.6.4|<tuple|2.6.4|25|stories/c2-fdroid.tm>>
    <associate|footnr-2.6.5|<tuple|2.6.5|25|stories/c2-fdroid.tm>>
    <associate|footnr-2.6.6|<tuple|2.6.6|26|stories/c2-fdroid.tm>>
    <associate|footnr-2.6.7|<tuple|2.6.7|26|stories/c2-fdroid.tm>>
    <associate|footnr-2.7.1|<tuple|2.7.1|27|stories/c2-freedombox.tm>>
    <associate|footnr-2.7.2|<tuple|2.7.2|27|stories/c2-freedombox.tm>>
    <associate|footnr-2.7.3|<tuple|2.7.3|27|stories/c2-freedombox.tm>>
    <associate|footnr-2.7.4|<tuple|2.7.4|27|stories/c2-freedombox.tm>>
    <associate|footnr-2.7.5|<tuple|2.7.5|27|stories/c2-freedombox.tm>>
    <associate|footnr-2.8.1|<tuple|2.8.1|29|stories/c2-github.tm>>
    <associate|footnr-2.8.10|<tuple|2.8.10|29|stories/c2-github.tm>>
    <associate|footnr-2.8.11|<tuple|2.8.11|29|stories/c2-github.tm>>
    <associate|footnr-2.8.2|<tuple|2.8.2|29|stories/c2-github.tm>>
    <associate|footnr-2.8.3|<tuple|2.8.3|29|stories/c2-github.tm>>
    <associate|footnr-2.8.4|<tuple|2.8.4|29|stories/c2-github.tm>>
    <associate|footnr-2.8.5|<tuple|2.8.5|29|stories/c2-github.tm>>
    <associate|footnr-2.8.6|<tuple|2.8.6|29|stories/c2-github.tm>>
    <associate|footnr-2.8.7|<tuple|2.8.7|29|stories/c2-github.tm>>
    <associate|footnr-2.8.8|<tuple|2.8.8|29|stories/c2-github.tm>>
    <associate|footnr-2.8.9|<tuple|2.8.9|29|stories/c2-github.tm>>
    <associate|footnr-2.9.1|<tuple|2.9.1|30|stories/c2-gnupg.tm>>
    <associate|footnr-2.9.10|<tuple|2.9.10|30|stories/c2-gnupg.tm>>
    <associate|footnr-2.9.11|<tuple|2.9.11|30|stories/c2-gnupg.tm>>
    <associate|footnr-2.9.12|<tuple|2.9.12|30|stories/c2-gnupg.tm>>
    <associate|footnr-2.9.13|<tuple|2.9.13|30|stories/c2-gnupg.tm>>
    <associate|footnr-2.9.14|<tuple|2.9.14|30|stories/c2-gnupg.tm>>
    <associate|footnr-2.9.15|<tuple|2.9.16|31|stories/c2-gnupg.tm>>
    <associate|footnr-2.9.16|<tuple|2.9.16|31|stories/c2-gnupg.tm>>
    <associate|footnr-2.9.17|<tuple|2.9.17|31|stories/c2-gnupg.tm>>
    <associate|footnr-2.9.18|<tuple|2.9.18|31|stories/c2-gnupg.tm>>
    <associate|footnr-2.9.19|<tuple|2.9.19|31|stories/c2-gnupg.tm>>
    <associate|footnr-2.9.2|<tuple|2.9.2|30|stories/c2-gnupg.tm>>
    <associate|footnr-2.9.20|<tuple|2.9.20|31|stories/c2-gnupg.tm>>
    <associate|footnr-2.9.21|<tuple|2.9.21|31|stories/c2-gnupg.tm>>
    <associate|footnr-2.9.22|<tuple|2.9.22|31|stories/c2-gnupg.tm>>
    <associate|footnr-2.9.23|<tuple|2.9.23|31|stories/c2-gnupg.tm>>
    <associate|footnr-2.9.3|<tuple|2.9.3|30|stories/c2-gnupg.tm>>
    <associate|footnr-2.9.4|<tuple|2.9.4|30|stories/c2-gnupg.tm>>
    <associate|footnr-2.9.5|<tuple|2.9.5|30|stories/c2-gnupg.tm>>
    <associate|footnr-2.9.6|<tuple|2.9.6|30|stories/c2-gnupg.tm>>
    <associate|footnr-2.9.7|<tuple|2.9.7|30|stories/c2-gnupg.tm>>
    <associate|footnr-2.9.8|<tuple|2.9.8|30|stories/c2-gnupg.tm>>
    <associate|footnr-2.9.9|<tuple|2.9.9|30|stories/c2-gnupg.tm>>
    <associate|footnr-A.0.1|<tuple|A.0.1|49|a-how-gnupg.tm>>
    <associate|footnr-A.1.1|<tuple|A.1.1|49|a-how-gnupg.tm>>
    <associate|footnr-A.1.10|<tuple|A.1.10|52|a-how-gnupg.tm>>
    <associate|footnr-A.1.2|<tuple|A.1.2|49|a-how-gnupg.tm>>
    <associate|footnr-A.1.3|<tuple|A.1.3|50|a-how-gnupg.tm>>
    <associate|footnr-A.1.4|<tuple|A.1.4|50|a-how-gnupg.tm>>
    <associate|footnr-A.1.5|<tuple|A.1.5|50|a-how-gnupg.tm>>
    <associate|footnr-A.1.6|<tuple|A.1.6|51|a-how-gnupg.tm>>
    <associate|footnr-A.1.7|<tuple|A.1.7|51|a-how-gnupg.tm>>
    <associate|footnr-A.1.8|<tuple|A.1.8|51|a-how-gnupg.tm>>
    <associate|footnr-A.1.9|<tuple|A.1.9|52|a-how-gnupg.tm>>
    <associate|footnr-A.2.1|<tuple|A.2.1|52|a-how-gnupg.tm>>
    <associate|footnr-A.2.2|<tuple|A.2.2|53|a-how-gnupg.tm>>
    <associate|footnr-A.2.3|<tuple|A.2.3|53|a-how-gnupg.tm>>
    <associate|footnr-A.2.4|<tuple|A.2.4|53|a-how-gnupg.tm>>
    <associate|footnr-A.2.5|<tuple|A.2.5|53|a-how-gnupg.tm>>
    <associate|footnr-A.2.6|<tuple|A.2.6|54|a-how-gnupg.tm>>
    <associate|gnupg_201411_v2.1-release-notes|<tuple|A.1.7|51|a-how-gnupg.tm>>
    <associate|part:a-how-gnupg.tm|<tuple|<tuple|Software|Yt-dlp>|47>>
    <associate|part:c0.tm|<tuple|?|3>>
    <associate|part:c1.tm|<tuple|?|9>>
    <associate|part:c2.tm|<tuple|1.3.1|11>>
    <associate|part:stories/c2-bitcoin-core.tm|<tuple|2.0.1|11>>
    <associate|part:stories/c2-cryptomator.tm|<tuple|<tuple|Software|Bitcoin
    Core>|16>>
    <associate|part:stories/c2-debian.tm|<tuple|<tuple|Software|Cryptomator>|18>>
    <associate|part:stories/c2-electrum.tm|<tuple|<tuple|Software|Debian>|22>>
    <associate|part:stories/c2-element.tm|<tuple|<tuple|Software|Electrum>|24>>
    <associate|part:stories/c2-fdroid.tm|<tuple|<tuple|Software|Element>|25>>
    <associate|part:stories/c2-freedombox.tm|<tuple|<tuple|Software|F-Droid>|27>>
    <associate|part:stories/c2-github.tm|<tuple|<tuple|Software|Freedombox>|28>>
    <associate|part:stories/c2-gnupg.tm|<tuple|<tuple|Software|GitHub>|30>>
    <associate|part:stories/c2-inkscape.tm|<tuple|<tuple|Software|GnuPG>|32>>
    <associate|part:stories/c2-keepassxc.tm|<tuple|<tuple|Software|Inkscape>|33>>
    <associate|part:stories/c2-qubesos.tm|<tuple|<tuple|Software|KeePassXC>|34>>
    <associate|part:stories/c2-raspiblitz.tm|<tuple|<tuple|Software|Qubes
    OS>|35>>
    <associate|part:stories/c2-satoshi-labs.tm|<tuple|<tuple|Software|Raspiblitz>|37>>
    <associate|part:stories/c2-tails.tm|<tuple|<tuple|Software|Satoshi
    Labs>|39>>
    <associate|part:stories/c2-tor-browser.tm|<tuple|<tuple|Software|Tails>|41>>
    <associate|part:stories/c2-trisquel.tm|<tuple|<tuple|Software|Tor
    Browser>|42>>
    <associate|part:stories/c2-veracrypt.tm|<tuple|<tuple|Software|Trisquel>|43>>
    <associate|part:stories/c2-youtube-dl.tm|<tuple|<tuple|Software|Veracrypt>|45>>
    <associate|part:stories/c2-yt-dlp.tm|<tuple|<tuple|Software|Youtube-dl>|47>>
    <associate|trisquel_20230322_aramo|<tuple|2.17.3|42|stories/c2-trisquel.tm>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|bib>
      nyt_20110215_moglen-founds-fbx

      nyt_20110215_moglen-founds-fbx

      nyt_20180604_ms-buys-github

      nyt_20180604_ms-buys-github
    </associate>
    <\associate|idx>
      <tuple|<tuple|Software|Bitcoin Core>|strong|c2 bitcoin-core
      idx1|<tuple|Software|Bitcoin Core>|<pageref|auto-9>>

      <tuple|<tuple|Software|Bitcoin>|<pageref|auto-10>>

      <tuple|<tuple|People|Nakamoto, Satoshi>|<pageref|auto-11>>

      <tuple|<tuple|People|Todd, Peter>|<pageref|auto-12>>

      <tuple|<tuple|People|Andresen, Gavin>|<pageref|auto-13>>

      <tuple|<tuple|People|van der Laan, Wladimir J.>|<pageref|auto-14>>

      <tuple|<tuple|Keys|People|van der Laan, Wladimir J.|0x90C8019E36C2E964
      (Bitcoin Core; 2015\U2022)>|||<tuple|Keys|People|van der Laan, Wladimir
      J.|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x90C8019E36C2E964>>
      <with|font-effects|<quote|hextended=0.8>|(Bitcoin Core;
      2015\U2022)>>|<pageref|auto-24>>

      <tuple|<tuple|Keys|People|van der Laan, Wladimir J.|0x74810B012346C9A6
      (personal; 2011\U )>|||<tuple|Keys|People|van der Laan, Wladimir
      J.|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x74810B012346C9A6>>
      (personal; 2011\U )>|<pageref|auto-26>>

      <tuple|<tuple|Keys|People|Andresen,
      Gavin|0x29D9EE6B1FC730C1>|||<tuple|Keys|People|Andresen,
      Gavin|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x29D9EE6B1FC730C1>>>|<pageref|auto-28>>

      <tuple|<tuple|Keys|People|Nakamoto,
      Satoshi|0x18C09E865EC948A1>|||<tuple|Keys|People|Nakamoto,
      Satoshi|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x18C09E865EC948A1>>>|<pageref|auto-30>>

      <tuple|<tuple|DSA, algorithm|weakness>|<pageref|auto-31>>

      <tuple|<tuple|Organizations|Bitcoin Foundation>|<pageref|auto-32>>

      <tuple|<tuple|Software|Bitcoin Core>|strong|c2 bitcoin-core
      idx1|<tuple|Software|Bitcoin Core>|<pageref|auto-33>>

      <tuple|<tuple|Software|Cryptomator>|strong|c2 cryptomator
      idx1|<tuple|Software|Cryptomator>|<pageref|auto-36>>

      <tuple|<tuple|Software|Dropbox>|<pageref|auto-37>>

      <tuple|<tuple|Keys|People|Stenzel, Sebastian|0x667B866EA8240A09>|||<tuple|Keys|People|Stenzel,
      Sebastian|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x667B866EA8240A09>>>|<pageref|auto-38>>

      <tuple|<tuple|Keys|People|Schrenk, Armin|0x748E55D51F5B3FBC>|||<tuple|Keys|People|Schrenk,
      Armin|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x748E55D51F5B3FBC>>>|<pageref|auto-39>>

      <tuple|<tuple|Keys|People|Hagemann,
      Tobias|0x69CEFAD519598989>|||<tuple|Keys|People|Hagemann,
      Tobias|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x69CEFAD519598989>>>|<pageref|auto-40>>

      <tuple|<tuple|Keys|Organizations|Cryptomator|0x615D449FE6E6A235>|||<tuple|Keys|Organizations|Cryptomator|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x615D449FE6E6A235>>>|<pageref|auto-44>>

      <tuple|<tuple|Keys|Organizations|Cryptomator|0x509C9D6334C80F11>|||<tuple|Keys|Organizations|Cryptomator|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x509C9D6334C80F11>>>|<pageref|auto-46>>

      <tuple|<tuple|Software|Cryptomator>|strong|c2 cryptomator
      idx1|<tuple|Software|Cryptomator>|<pageref|auto-47>>

      <tuple|<tuple|Software|Debian>|strong|c2 debian
      idx1|<tuple|Software|Debian>|<pageref|auto-50>>

      <tuple|<tuple|People|Murdock, Ian Ashley>|<pageref|auto-51>>

      <tuple|<tuple|People|Mantinan, Santiago Garcia>|<pageref|auto-53>>

      <tuple|<tuple|People|Baumann, Daniel>|<pageref|auto-54>>

      <tuple|<tuple|People|McIntyre, Steve>|<pageref|auto-60>>

      <tuple|<tuple|Software|Debian>|strong|c2 debian
      idx1|<tuple|Software|Debian>|<pageref|auto-70>>

      <tuple|<tuple|Software|Electrum>|strong|c2 electrum
      idx1|<tuple|Software|Electrum>|<pageref|auto-73>>

      <tuple|<tuple|People|Voegtlin, Thomas>|<pageref|auto-74>>

      <tuple|<tuple|Software|Bitcoin Core>|<pageref|auto-75>>

      <tuple|<tuple|Software|Android>|<pageref|auto-76>>

      <tuple|<tuple|Software|ElectrumX>|<pageref|auto-77>>

      <tuple|<tuple|People|SomberNight>|<pageref|auto-78>>

      <tuple|<tuple|Keys|People|Voegtlin,
      Thomas|0x2BD5824B7F9470E6>|||<tuple|Keys|People|Voegtlin,
      Thomas|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x2BD5824B7F9470E6>>>|<pageref|auto-82>>

      <tuple|<tuple|Keys|People|Oeste, Stephan
      \Pemzy\Q|0x2EBB056FD847F8A7>|||<tuple|Keys|People|Oeste, Stephan
      \Pemzy\Q|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x2EBB056FD847F8A7>>>|<pageref|auto-84>>

      <tuple|<tuple|Keys|People|SomberNight|0xCA9EEEC43DF911DC>|||<tuple|Keys|People|SomberNight|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x0xCA9EEEC43DF911DC>>>|<pageref|auto-86>>

      <tuple|<tuple|Keys|People|SomberNight|0xE7B748CDAF5E5ED9>|||<tuple|Keys|People|SomberNight|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0xE7B748CDAF5E5ED9>>>|<pageref|auto-88>>

      <tuple|<tuple|Software|Electrum>|strong|c2 electrum
      idx1|<tuple|Software|Electrum>|<pageref|auto-89>>

      <tuple|<tuple|Software|Element>|strong|c2 element
      idx1|<tuple|Software|Element>|<pageref|auto-92>>

      <tuple|<tuple|People|Miles, Gary>|<pageref|auto-93>>

      <tuple|<tuple|Organization|Amdocs>|<pageref|auto-94>>

      <tuple|<tuple|People|Le Pape, Amandine>|<pageref|auto-95>>

      <tuple|<tuple|People|Hodgeson, Mathew>|<pageref|auto-96>>

      <tuple|<tuple|Keys|Organization|Riot.im|0xD7B0B66941D01538>|||<tuple|Keys|Organization|Riot.im|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0xD7B0B66941D01538>>>|<pageref|auto-100>>

      <tuple|<tuple|Software|Element>|strong|c2 element
      idx1|<tuple|Software|Element>|<pageref|auto-101>>

      <tuple|<tuple|Software|F-Droid>|strong|c2 fdroid
      idx1|<tuple|Software|F-Droid>|<pageref|auto-104>>

      <tuple|<tuple|Organizations|Google Play>|<pageref|auto-105>>

      <tuple|<tuple|People|Gultnieks, Ciaran>|<pageref|auto-106>>

      <tuple|<tuple|People|Steiner, Hans-Christoph>|<pageref|auto-107>>

      <tuple|<tuple|People|Grote, Torsten>|<pageref|auto-108>>

      <tuple|<tuple|Keys|Organizations|F-Droid|Signing|0x41E7044E1DBA2E89
      (2014\U)>|||<tuple|Keys|Organizations|F-Droid|Signing|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x41E7044E1DBA2E89>>
      (2014\U)>|<pageref|auto-112>>

      <tuple|<tuple|Keys|People|Grote, Torsten|0x74DCA8A36C52F833>|||<tuple|Keys|People|Grote,
      Torsten|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x74DCA8A36C52F833>>>|<pageref|auto-114>>

      <tuple|<tuple|Keys|People|Steiner, Hans-Christoph|0xE9E28DEA00AA5556>|||<tuple|Keys|People|Steiner,
      Hans-Christoph|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0xE9E28DEA00AA5556>>>|<pageref|auto-116>>

      <tuple|<tuple|Software|F-Droid>|strong|c2 fdroid
      idx1|<tuple|Software|F-Droid>|<pageref|auto-118>>

      <tuple|<tuple|Software|Freedombox>|strong|c2 freedombox
      idx1|<tuple|Software|Freedombox>|<pageref|auto-121>>

      <tuple|<tuple|Software|Debian>|<pageref|auto-122>>

      <tuple|<tuple|Raspberry Pi>|<pageref|auto-123>>

      <tuple|<tuple|Organizations|Freedombox Foundation>|<pageref|auto-124>>

      <tuple|<tuple|People|Moglen, Eben>|<pageref|auto-125>>

      <tuple|<tuple|Keys|People|Adapa, Sunil
      Mohan|0x36C361440C9BC971>|||<tuple|Keys|People|Adapa, Sunil
      Mohan|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x36C361440C9BC971>>>|<pageref|auto-129>>

      <tuple|<tuple|Keys|People|Valleroy,
      James|0x77C0C75E7B650808>|||<tuple|Keys|People|Valleroy,
      James|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x77C0C75E7B650808>>>|<pageref|auto-131>>

      <tuple|<tuple|Keys|People|Valleroy,
      James|0x77C0C75E7B650808>|||<tuple|Keys|People|Valleroy,
      James|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x77C0C75E7B650808>>>|<pageref|auto-133>>

      <tuple|<tuple|Software|Freedombox>|strong|c2 freedombox
      idx1|<tuple|Software|Freedombox>|<pageref|auto-134>>

      <tuple|<tuple|Software|GitHub>|strong|c2 github
      idx1|<tuple|Software|GitHub>|<pageref|auto-137>>

      <tuple|<tuple|Microsoft>|<pageref|auto-138>>

      <tuple|<tuple|Logical Awesome, LLC>|<pageref|auto-140>>

      <tuple|<tuple|People|Wanstrath, Chris>|<pageref|auto-141>>

      <tuple|<tuple|People|Cross, Jonathan>|<pageref|auto-142>>

      <tuple|<tuple|Keys|Organizations|Github|0x4AEE18F83AFDEB23>|||<tuple|Keys|Organizations|GitHub|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x4AEE18F83AFDEB23>>>|<pageref|auto-145>>

      <tuple|<tuple|Software|GitHub>|strong|c2 github
      idx1|<tuple|Software|GitHub>|<pageref|auto-146>>

      <tuple|<tuple|Software|GnuPG>|strong|c2 gnupg
      idx1|<tuple|Software|GnuPG>|<pageref|auto-149>>

      <tuple|<tuple|Software|App Name>|<pageref|auto-150>>

      <tuple|<tuple|Software|OpenPGP>|<pageref|auto-151>>

      <tuple|<tuple|People|Zimmermann, Phil>|<pageref|auto-152>>

      <tuple|<tuple|People|Koch, Werner>|<pageref|auto-153>>

      <tuple|<tuple|Keys|People|Koch, Werner|0x528897B826403ADA>|||<tuple|Keys|People|Koch,
      Werner|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x528897B826403ADA>>>|<pageref|auto-158>>

      <tuple|<tuple|Keys|People|Yutaka, Niibe|0xE98E9B2D19C6C8BD>|||<tuple|Keys|People|Yutaka,
      Niibe|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0xE98E9B2D19C6C8BD>>>|<pageref|auto-160>>

      <tuple|<tuple|Keys|People|Heinecke,
      Andre|0xBCEF7E294B092E28>|||<tuple|Keys|People|Heinecke,
      Andre|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0xBCEF7E294B092E28>>>|<pageref|auto-162>>

      <tuple|<tuple|Keys|Organizations|GnuPG.com|0x549E695E905BA208>|||<tuple|Keys|Organizations|GnuPG.com|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x549E695E905BA208>>>|<pageref|auto-164>>

      <tuple|<tuple|Software|GnuPG>|strong|c2 gnupg
      idx1|<tuple|Software|GnuPG>|<pageref|auto-165>>

      <tuple|<tuple|Software|Inkscape>|strong|c2 inkscape
      idx1|<tuple|Software|Inkscape>|<pageref|auto-168>>

      <tuple|<tuple|Keys|People|Jeanmougin,
      Marc|0x5FCB204EF882B07A>|||<tuple|Keys|People|Jeanmougin,
      Marc|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x5FCB204EF882B07A>>>|<pageref|auto-172>>

      <tuple|<tuple|Software|Inkscape>|strong|c2 inkscape
      idx1|<tuple|Software|Inkscape>|<pageref|auto-173>>

      <tuple|<tuple|Software|KeePassXC>|strong|c2 keepassxc
      idx1|<tuple|Software|KeePassXC>|<pageref|auto-176>>

      <tuple|<tuple|Organizations|KeePassXC Team>|<pageref|auto-177>>

      <tuple|<tuple|Keys|Organizations|KeePassXC|0xCFB4C2166397D0D2>|||<tuple|Keys|Organizations|<with|font-shape|<quote|small-caps>|KeePassXC>|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0xCFB4C2166397D0D2>>>|<pageref|auto-181>>

      <tuple|<tuple|Keys|Organizations|KeePassXC|0x105D8D57BB9746BD>|||<tuple|Keys|Organizations|<with|font-shape|<quote|small-caps>|KeePassXC>|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x105D8D57BB9746BD>>>|<pageref|auto-183>>

      <tuple|<tuple|Software|KeePassXC>|strong|c2 keepassxc
      idx1|<tuple|Software|KeePassXC>|<pageref|auto-184>>

      <tuple|<tuple|Software|Qubes OS>|strong|c2 qubesos
      idx1|<tuple|Software|Qubes OS>|<pageref|auto-187>>

      <tuple|<tuple|Organizations|Invisible Things Labs>|<pageref|auto-188>>

      <tuple|<tuple|People|Joanna Rutkowska>|<pageref|auto-189>>

      <tuple|<tuple|People|Marek Marczykowski-G�recki>|<pageref|auto-190>>

      <tuple|<tuple|Keys|Organizations|Qubes
      OS|0xDDFA1A3E36879494>|||<tuple|Keys|Organizations|Qubes
      OS|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0xDDFA1A3E36879494>>>|<pageref|auto-194>>

      <tuple|<tuple|Keys|Organizations|Qubes OS (Release 4 Signing
      Key)|0x1848792F9E2795E9>|||<tuple|Keys|Organizations|Qubes OS (Release
      4 Signing Key)|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x1848792F9E2795E9>>>|<pageref|auto-196>>

      <tuple|<tuple|Keys|Organizations|Qubes OS (Release 3 Signing
      Key)|0xCB11CA1D03FA5082>|||<tuple|Keys|Organizations|Qubes OS (Release
      3 Signing Key)|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0xCB11CA1D03FA5082>>>|<pageref|auto-198>>

      <tuple|<tuple|Keys|Organizations|Qubes OS (Release 2 Signing
      Key)|0x0C73B9D40A40E458>|||<tuple|Keys|Organizations|Qubes OS (Release
      2 Signing Key)|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x0C73B9D40A40E458>>>|<pageref|auto-200>>

      <tuple|<tuple|Keys|Organizations|Qubes OS (Release 1 Signing
      Key)|0xEA01201B211093A7>|||<tuple|Keys|Organizations|Qubes OS (Release
      1 Signing Key)|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0xEA01201B211093A7>>>|<pageref|auto-202>>

      <tuple|<tuple|Software|Qubes OS>|strong|c2 qubesos
      idx1|<tuple|Software|Qubes OS>|<pageref|auto-203>>

      <tuple|<tuple|Software|Raspiblitz>|strong|c2 raspiblitz
      idx1|<tuple|Software|RaspiBlitz>|<pageref|auto-206>>

      <tuple|<tuple|Software|Lightning Network>|<pageref|auto-207>>

      <tuple|<tuple|Software|Bitcoin>|<pageref|auto-208>>

      <tuple|<tuple|People|Rotzoll, Christian
      \P<with|font-family|<quote|tt>|language|<quote|verbatim>|rootzol>\Q>|<pageref|auto-209>>

      <tuple|<tuple|People|Antonopoulos, Andreas>|<pageref|auto-211>>

      <tuple|<tuple|Keys|People|Rotzoll, Christian
      Rootzol|>|||<tuple|Keys|People|Rotzoll, Christian
      \P<with|font-family|<quote|tt>|language|<quote|verbatim>|rootzol>\Q|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x1C73060C7C176461>>>|<pageref|auto-214>>

      <tuple|<tuple|Software|Raspiblitz>|strong|c2 raspiblitz
      idx1|<tuple|Software|RaspiBlitz>|<pageref|auto-215>>

      <tuple|<tuple|Software|Satoshi Labs>|strong|c2 satoshi-labs
      idx1|<tuple|Software|Satoshi Labs>|<pageref|auto-218>>

      <tuple|<tuple|Software|Trezor>|<pageref|auto-219>>

      <tuple|<tuple|People|Palatinus, Marek \PSlush\Q>|<pageref|auto-220>>

      <tuple|<tuple|People|Slush (<with|font-shape|<quote|small-caps>|Satoshi
      Labs> developer)>|<pageref|auto-221>>

      <tuple|<tuple|People|Rusn�k, Pavol \PStick\Q>|<pageref|auto-222>>

      <tuple|<tuple|People|Stick (<with|font-shape|<quote|small-caps>|Satoshi
      Labs> developer)>|<pageref|auto-223>>

      <tuple|<tuple|People|Vranova, Alena>|<pageref|auto-224>>

      <tuple|<tuple|Keys|People|Rusn�k, Pavol
      \PStick\Q|0x91F3B339B9A02A3D>|||<tuple|Keys|People|Rusn�k, Pavol
      \PStick\Q|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x91F3B339B9A02A3D>>>|<pageref|auto-228>>

      <tuple|<tuple|Keys|Organizations|Satoshi
      Labs|Signing|0x26A3A56662F0E7E2 (2020)>|||<tuple|Keys|Organizations|Satoshi
      Labs|Signing|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x26A3A56662F0E7E2>>
      (2020)>|<pageref|auto-230>>

      <tuple|<tuple|Keys|Organizations|Satoshi
      Labs|Signing|0xE21B6950A2ECB65C (2021)>|||<tuple|Keys|Organizations|Satoshi
      Labs|Signing|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0xE21B6950A2ECB65C>>
      (2021)>|<pageref|auto-232>>

      <tuple|<tuple|Software|Satoshi Labs>|strong|c2 satoshi-labs
      idx1|<tuple|Software|Satoshi Labs>|<pageref|auto-233>>

      <tuple|<tuple|Software|Tails>|strong|c2 tails
      idx1|<tuple|Software|Tails>|<pageref|auto-236>>

      <tuple|<tuple|Software|Debian>|<pageref|auto-237>>

      <tuple|<tuple|Software|Tor>|<pageref|auto-238>>

      <tuple|<tuple|Software|Amnesia>|<pageref|auto-239>>

      <tuple|<tuple|Software|Incognito>|<pageref|auto-240>>

      <tuple|<tuple|Software|Gentoo>|<pageref|auto-241>>

      <tuple|<tuple|Keys|Organizations|Tails|Signing|0xDBB802B258ACD84F
      (2015\U )>|||<tuple|Keys|Organizations|Tails|Signing|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0xDBB802B258ACD84F>>
      (2015\U )>|<pageref|auto-245>>

      <tuple|<tuple|Keys|Organizations|Tails|Mailing list|0x1D2975EDF93E735F
      (2009\U )>|||<tuple|Keys|Organizations|Tails|Mailing
      list|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x1D2975EDF93E735F>>
      (2009\U )>|<pageref|auto-247>>

      <tuple|<tuple|Keys|Organizations|Tails|Signing|0x1202821CBE2CD9C1
      (2010\U2015)>|||<tuple|Keys|Organizations|Tails|Signing|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x1202821CBE2CD9C1>>
      (2010\U2015)>|<pageref|auto-249>>

      <tuple|<tuple|Software|Tails>|strong|c2 tails
      idx1|<tuple|Software|Tails>|<pageref|auto-250>>

      <tuple|<tuple|Software|Tor Browser>|strong|c2 tor-browser
      idx1|<tuple|Software|Tor Browser>|<pageref|auto-253>>

      <tuple|<tuple|Software|Tor Browser>|<pageref|auto-254>>

      <tuple|<tuple|People|Murdoch, Steven J.>|<pageref|auto-256>>

      <tuple|<tuple|Keys|Organizations|Tor Browser|Signing|0x4E2C6E8793298290
      (2015\U )>|||<tuple|Keys|Organizations|Tor
      Browser|Signing|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x4E2C6E8793298290>>
      (2015\U )>|<pageref|auto-259>>

      <tuple|<tuple|Software|Tor Browser>|strong|c2 tor-browser
      idx1|<tuple|Software|Tor Browser>|<pageref|auto-260>>

      <tuple|<tuple|Software|Trisquel>|strong|c2 trisquel
      idx1|<tuple|Software|Trisquel>|<pageref|auto-263>>

      <tuple|<tuple|People|Rodriguez, Ruben>|<pageref|auto-264>>

      <tuple|<tuple|Keys|Organizations|Trisquel|0xF5DAAAF74AD4C938>|||<tuple|Keys|Organizations|Trisquel|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0xF5DAAAF74AD4C938>>>|<pageref|auto-268>>

      <tuple|<tuple|Software|Trisquel>|strong|c2 trisquel
      idx1|<tuple|Software|Trisquel>|<pageref|auto-269>>

      <tuple|<tuple|Software|Veracrypt>|strong|c2 veracrypt
      idx1|<tuple|Software|Veracrypt>|<pageref|auto-272>>

      <tuple|<tuple|People|Idrassi, Mounir>|<pageref|auto-273>>

      <tuple|<tuple|Organizations|QuarksLab>|<pageref|auto-275>>

      <tuple|<tuple|Keys|Organizations|Veracrypt|Signing|0x821ACD02680D16DE
      (2018\U )>|||<tuple|Keys|Organizations|Veracrypt|Signing|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x821ACD02680D16DE>>
      (2018\U )>|<pageref|auto-278>>

      <tuple|<tuple|Keys|Organizations|Veracrypt|Signing|0xEB559C7C54DDD393
      (2014\U2018)>|||<tuple|Keys|Organizations|Veracrypt|Signing|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0xEB559C7C54DDD393>>
      (2014\U2018)>|<pageref|auto-280>>

      <tuple|<tuple|Organizations|QuarksLab>|<pageref|auto-281>>

      <tuple|<tuple|Software|Veracrypt>|strong|c2 veracrypt
      idx1|<tuple|Software|Veracrypt>|<pageref|auto-282>>

      <tuple|<tuple|Software|Youtube-dl>|strong|c2 ytdl
      idx1|<tuple|Software|Youtube-dl>|<pageref|auto-285>>

      <tuple|<tuple|People|M., Sergey>|<pageref|auto-286>>

      <tuple|<tuple|People|Garcia, Ricardo>|<pageref|auto-288>>

      <tuple|<tuple|Keys|People|M., Sergey|0x2C393E0F18A9236D>|||<tuple|Keys|People|D.,
      Sergey|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x2C393E0F18A9236D>>>|<pageref|auto-291>>

      <tuple|<tuple|Keys|People|Hagemeister,
      Philipp|0xF5EAB582FAFB085C>|||<tuple|Keys|People|Hagemeister,
      Philipp|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0xF5EAB582FAFB085C>>>|<pageref|auto-293>>

      <tuple|<tuple|People|Hagemeister, Philipp>|<pageref|auto-294>>

      <tuple|<tuple|Keys|People|Hagemeister,
      Philipp|0xDB4B54CBA4826A18>|||<tuple|Keys|People|Hagemeister,
      Philipp|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0xDB4B54CBA4826A18>>>|<pageref|auto-296>>

      <tuple|<tuple|People|Hagemeister, Philipp>|<pageref|auto-297>>

      <tuple|<tuple|Keys|People|Valsorda,
      Filippo|0xEBF01804BCF05F6B>|||<tuple|Keys|People|Valsorda,
      Filippo|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0xEBF01804BCF05F6B>>>|<pageref|auto-299>>

      <tuple|<tuple|People|Valsorda, Filippo>|<pageref|auto-300>>

      <tuple|<tuple|Software|Youtube-dl>|strong|c2 ytdl
      idx1|<tuple|Software|Youtube-dl>|<pageref|auto-301>>

      <tuple|<tuple|Software|Yt-dlp>|strong|c2 ytdlp
      idx1|<tuple|Software|Yt-dlp>|<pageref|auto-304>>

      <tuple|<tuple|People|pukkandan>|<pageref|auto-305>>

      <tuple|<tuple|Keys|People|Sawicki, Simon|0x57CF65933B5A7581>|||<tuple|Keys|People|Sawicki,
      Simon|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x57CF65933B5A7581>>>|<pageref|auto-309>>

      <tuple|<tuple|Keys|People|pukkandan|0x7EEE9E1E817D0A39>|||<tuple|Keys|People|pukkandan|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x7EEE9E1E817D0A39>>>|<pageref|auto-311>>

      <tuple|<tuple|Software|Yt-dlp>|strong|c2 ytdlp
      idx1|<tuple|Software|Yt-dlp>|<pageref|auto-312>>
    </associate>
    <\associate|parts>
      <tuple|c0.tm|chapter-nr|0|section-nr|0|subsection-nr|0>

      <tuple|c1.tm|chapter-nr|0|section-nr|0|subsection-nr|0>

      <tuple|c2.tm|chapter-nr|1|section-nr|3|subsection-nr|0>

      <tuple|stories/c2-bitcoin-core.tm|chapter-nr|2|section-nr|0|subsection-nr|0>

      <tuple|stories/c2-cryptomator.tm|chapter-nr|2|section-nr|1|subsection-nr|3>

      <tuple|stories/c2-debian.tm|chapter-nr|2|section-nr|2|subsection-nr|3>

      <tuple|stories/c2-electrum.tm|chapter-nr|2|section-nr|3|subsection-nr|3>

      <tuple|stories/c2-element.tm|chapter-nr|2|section-nr|4|subsection-nr|3>

      <tuple|stories/c2-fdroid.tm|chapter-nr|2|section-nr|5|subsection-nr|3>

      <tuple|stories/c2-freedombox.tm|chapter-nr|2|section-nr|6|subsection-nr|3>

      <tuple|stories/c2-github.tm|chapter-nr|2|section-nr|7|subsection-nr|3>

      <tuple|stories/c2-gnupg.tm|chapter-nr|2|section-nr|8|subsection-nr|3>

      <tuple|stories/c2-inkscape.tm|chapter-nr|2|section-nr|9|subsection-nr|3>

      <tuple|stories/c2-keepassxc.tm|chapter-nr|2|section-nr|10|subsection-nr|3>

      <tuple|stories/c2-qubesos.tm|chapter-nr|2|section-nr|11|subsection-nr|3>

      <tuple|stories/c2-raspiblitz.tm|chapter-nr|2|section-nr|12|subsection-nr|3>

      <tuple|stories/c2-satoshi-labs.tm|chapter-nr|2|section-nr|13|subsection-nr|3>

      <tuple|stories/c2-tails.tm|chapter-nr|2|section-nr|14|subsection-nr|3>

      <tuple|stories/c2-tor-browser.tm|chapter-nr|2|section-nr|15|subsection-nr|3>

      <tuple|stories/c2-trisquel.tm|chapter-nr|2|section-nr|16|subsection-nr|3>

      <tuple|stories/c2-veracrypt.tm|chapter-nr|2|section-nr|17|subsection-nr|3>

      <tuple|stories/c2-youtube-dl.tm|chapter-nr|2|section-nr|18|subsection-nr|3>

      <tuple|stories/c2-yt-dlp.tm|chapter-nr|2|section-nr|19|subsection-nr|3>

      <tuple|a-how-gnupg.tm|chapter-nr|2|section-nr|20|subsection-nr|3>
    </associate>
    <\associate|table>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|2.1.1>|>
        Fingerprints (primary key long IDs) of
        <with|font-shape|<quote|small-caps>|OpenPGP> keys that signed hashes
        of <with|font-shape|<quote|small-caps>|Bitcoin Core> version
        <with|font-shape|<quote|italic>|25.0> release files, sorted by long
        ID.
      </surround>|<pageref|auto-18>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|2.1.2>|>
        Fingerprints (primary key long IDs) of
        <with|font-shape|<quote|small-caps>|OpenPGP> keys that signed hashes
        of <with|font-shape|<quote|small-caps>|Bitcoin Core> version
        <with|font-shape|<quote|italic>|23.0> release files, sorted by long
        ID.
      </surround>|<pageref|auto-20>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|2.1.3>|>
        Fingerprints (primary key long IDs) of
        <with|font-shape|<quote|small-caps>|OpenPGP> keys that signed hashes
        of <with|font-shape|<quote|small-caps>|Bitcoin Core> version
        <with|font-shape|<quote|italic>|22.0> release files, sorted by long
        ID.
      </surround>|<pageref|auto-22>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|2.3.1>|>
        A list of keys used to sign <with|font-shape|<quote|small-caps>|Debian>
        installation images. Keys identified from
        <with|font-shape|<quote|small-caps>|Internet Archive> snapshots of
        \ <locus|<id|%3E1CFC8-4ECAC58--2198AF60>|<link|hyperlink|<id|%3E1CFC8-4ECAC58--2198AF60>|<url|https://debian.org/CD/verify>>|<with|font-family|<quote|tt>|language|<quote|verbatim>|<with|font-effects|<quote|hextended=0.8>|https://debian.org/CD/verify>>
        >.

        <surround|||<with|font-size|<quote|0.771>|<surround|<locus|<id|%3E1CFC8-92ECCA0--689E3CFF>|<link|hyperlink|<id|%3E1CFC8-92ECCA0--689E3CFF>|<url|#footnr-2.3.4>>|2.3.4>.
        |<hidden-binding|<tuple|footnote-2.3.4>|2.3.4>|See
        <locus|<id|%3E1CFC8-4ECAC58--2198AF60>|<link|hyperlink|<id|%3E1CFC8-4ECAC58--2198AF60>|<url|https://web.archive.org/web/20110413065857/http://www.debian.org/CD/verify>>|<with|font-family|<quote|tt>|language|<quote|verbatim>|<with|font-effects|<quote|hextended=0.8>|https://web.archive.org/web/20110413065857/http://www.debian.org/CD/verify>>
        >.>>>

        <surround|||<with|font-size|<quote|0.771>|<surround|<locus|<id|%3E1CFC8-17DD9640-7BF7EFC8>|<link|hyperlink|<id|%3E1CFC8-17DD9640-7BF7EFC8>|<url|#footnr-2.3.7>>|2.3.7>.
        |<hidden-binding|<tuple|footnote-2.3.7>|2.3.7>|See
        <locus|<id|%3E1CFC8-4ECAC58--2198AF60>|<link|hyperlink|<id|%3E1CFC8-4ECAC58--2198AF60>|<url|https://web.archive.org/web/20120815030316/http://www.debian.org:80/CD/verify>>|<with|font-family|<quote|tt>|language|<quote|verbatim>|<with|font-effects|<quote|hextended=0.8>|https://web.archive.org/web/20120815030316/http://www.debian.org:80/CD/verify>>
        >.>>>

        <surround|||<with|font-size|<quote|0.771>|<surround|<locus|<id|%3E1CFC8-17DD95C8--7572046F>|<link|hyperlink|<id|%3E1CFC8-17DD95C8--7572046F>|<url|#footnr-2.3.8>>|2.3.8>.
        |<hidden-binding|<tuple|footnote-2.3.8>|2.3.8>|See
        <locus|<id|%3E1CFC8-4ECAC58--2198AF60>|<link|hyperlink|<id|%3E1CFC8-4ECAC58--2198AF60>|<url|https://web.archive.org/web/20130813130619/http://www.debian.org/CD/verify>>|<with|font-family|<quote|tt>|language|<quote|verbatim>|<with|font-effects|<quote|hextended=0.8>|https://web.archive.org/web/20130813130619/http://www.debian.org/CD/verify>>
        >.>>>

        <surround|||<with|font-size|<quote|0.771>|<\surround|<locus|<id|%3E1CFC8-12190F40-728B47F2>|<link|hyperlink|<id|%3E1CFC8-12190F40-728B47F2>|<url|#footnr-2.3.9>>|2.3.9>.
        |<hidden-binding|<tuple|footnote-2.3.9>|2.3.9>>
          See <locus|<id|%3E1CFC8-4ECAC58--2198AF60>|<link|hyperlink|<id|%3E1CFC8-4ECAC58--2198AF60>|<url|https://web.archive.org/web/20140410065231/http://www.debian.org/CD/verify>>|<with|font-family|<quote|tt>|language|<quote|verbatim>|<with|font-effects|<quote|hextended=0.8>|https://web.archive.org/web/20140410065231/http://www.debian.org/CD/verify>>
          >.
        </surround>>>

        <surround|||<with|font-size|<quote|0.771>|<surround|<locus|<id|%3E1CFC8-12055E50--757D47C6>|<link|hyperlink|<id|%3E1CFC8-12055E50--757D47C6>|<url|#footnr-2.3.10>>|2.3.10>.
        |<hidden-binding|<tuple|footnote-2.3.10>|2.3.10>|See
        <locus|<id|%3E1CFC8-4ECAC58--2198AF60>|<link|hyperlink|<id|%3E1CFC8-4ECAC58--2198AF60>|<url|https://web.archive.org/web/20140528012106/https://www.debian.org/CD/verify>>|<with|font-family|<quote|tt>|language|<quote|verbatim>|<with|font-effects|<quote|hextended=0.8>|https://web.archive.org/web/20140528012106/https://www.debian.org/CD/verify>>
        >.>>>

        <surround|||<with|font-size|<quote|0.771>|<surround|<locus|<id|%3E1CFC8-172C53F8--C227348>|<link|hyperlink|<id|%3E1CFC8-172C53F8--C227348>|<url|#footnr-2.3.5>>|2.3.5>.
        |<hidden-binding|<tuple|footnote-2.3.5>|2.3.5>|Public key available
        at <locus|<id|%3E1CFC8-4ECAC58--2198AF60>|<link|hyperlink|<id|%3E1CFC8-4ECAC58--2198AF60>|<url|https://web.archive.org/web/20210928205206/https://www.einval.com/~steve/pgp/>>|<with|font-family|<quote|tt>|language|<quote|verbatim>|<with|font-effects|<quote|hextended=0.8>|https://web.archive.org/web/20210928205206/https://www.einval.com/~steve/pgp/>>
        >.>>>

        <surround|||<with|font-size|<quote|0.771>|<surround|<locus|<id|%3E1CFC8-1731EA90-3F48719>|<link|hyperlink|<id|%3E1CFC8-1731EA90-3F48719>|<url|#footnr-2.3.6>>|2.3.6>.
        |<hidden-binding|<tuple|footnote-2.3.6>|2.3.6>|Public key available
        at <locus|<id|%3E1CFC8-4ECAC58--2198AF60>|<link|hyperlink|<id|%3E1CFC8-4ECAC58--2198AF60>|<url|https://web.archive.org/web/20210928220426/https://reboil.com/res/2021/txt/20210928_72FDC205F6A32A8E..debian_manty.asc>>|<with|font-family|<quote|tt>|language|<quote|verbatim>|<with|font-effects|<quote|hextended=0.8>|https://web.archive.org/web/20210928220426/https://reboil.com/res/2021/txt/20210928_72FDC205F6A32A8E..debian_manty.asc>>
        >.>>>

        \;
      </surround>|<pageref|auto-57>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|2.9.1>|>
        A list of keys used to sign <with|font-shape|<quote|small-caps>|GnuPG>
        releases. Keys identified from <with|font-shape|<quote|small-caps>|Internet
        Archive> snapshots of \ <locus|<id|%3E1CFC8-4ECAC58--2198AF60>|<link|hyperlink|<id|%3E1CFC8-4ECAC58--2198AF60>|<url|http://www.gnupg.org/signature_key.html>>|<with|font-family|<quote|tt>|language|<quote|verbatim>|<with|font-effects|<quote|hextended=0.8>|http://www.gnupg.org/signature_key.html>>
        >.

        <surround|||<with|font-size|<quote|0.771>|<surround|<locus|<id|%3E1CFC8-1CC1BDE0-2DAEE53A>|<link|hyperlink|<id|%3E1CFC8-1CC1BDE0-2DAEE53A>|<url|#footnr-2.9.15>>|2.9.15>.
        |<hidden-binding|<tuple|footnote-2.9.15>|2.9.15>|Date span source:
        <locus|<id|%3E1CFC8-4ECAC58--2198AF60>|<link|hyperlink|<id|%3E1CFC8-4ECAC58--2198AF60>|<url|https://web.archive.org/web/20131123175952/http://www.gnupg.org:80/signature_key.html>>|<with|font-family|<quote|tt>|language|<quote|verbatim>|<with|font-effects|<quote|hextended=0.8>|https://web.archive.org/web/20131123175952/http://www.gnupg.org:80/signature_key.html>>
        >.>>>

        <surround|||<with|font-size|<quote|0.771>|<surround|<locus|<id|%3E1CFC8-1CC1BCF0--2DF84F88>|<link|hyperlink|<id|%3E1CFC8-1CC1BCF0--2DF84F88>|<url|#footnr-2.9.16>>|2.9.16>.
        |<hidden-binding|<tuple|footnote-2.9.16>|2.9.16>|See
        <locus|<id|%3E1CFC8-4ECAC58--2198AF60>|<link|hyperlink|<id|%3E1CFC8-4ECAC58--2198AF60>|<url|https://web.archive.org/web/20041113170551/http://www.gnupg.org/signature_key.html>>|<with|font-family|<quote|tt>|language|<quote|verbatim>|<with|font-effects|<quote|hextended=0.8>|https://web.archive.org/web/20041113170551/http://www.gnupg.org/signature_key.html>>
        >.>>>

        <surround|||<with|font-size|<quote|0.771>|<surround|<locus|<id|%3E1CFC8-1CC1BC00--7D4E949A>|<link|hyperlink|<id|%3E1CFC8-1CC1BC00--7D4E949A>|<url|#footnr-2.9.17>>|2.9.17>.
        |<hidden-binding|<tuple|footnote-2.9.17>|2.9.17>|See
        <locus|<id|%3E1CFC8-4ECAC58--2198AF60>|<link|hyperlink|<id|%3E1CFC8-4ECAC58--2198AF60>|<url|https://web.archive.org/web/20131123175952/http://www.gnupg.org:80/signature_key.html>>|<with|font-family|<quote|tt>|language|<quote|verbatim>|<with|font-effects|<quote|hextended=0.8>|https://web.archive.org/web/20131123175952/http://www.gnupg.org:80/signature_key.html>>
        >.>>>

        <surround|||<with|font-size|<quote|0.771>|<surround|<locus|<id|%3E1CFC8-1CC1BB10--7AC40951>|<link|hyperlink|<id|%3E1CFC8-1CC1BB10--7AC40951>|<url|#footnr-2.9.18>>|2.9.18>.
        |<hidden-binding|<tuple|footnote-2.9.18>|2.9.18>|See
        <locus|<id|%3E1CFC8-4ECAC58--2198AF60>|<link|hyperlink|<id|%3E1CFC8-4ECAC58--2198AF60>|<url|https://web.archive.org/web/20150503220844/https://www.gnupg.org/signature_key.html>>|<with|font-family|<quote|tt>|language|<quote|verbatim>|<with|font-effects|<quote|hextended=0.8>|https://web.archive.org/web/20150503220844/https://www.gnupg.org/signature_key.html>>
        >.>>>

        <surround|||<with|font-size|<quote|0.771>|<surround|<locus|<id|%3E1CFC8-1CC1BA20--72782F23>|<link|hyperlink|<id|%3E1CFC8-1CC1BA20--72782F23>|<url|#footnr-2.9.19>>|2.9.19>.
        |<hidden-binding|<tuple|footnote-2.9.19>|2.9.19>|See
        <locus|<id|%3E1CFC8-4ECAC58--2198AF60>|<link|hyperlink|<id|%3E1CFC8-4ECAC58--2198AF60>|<url|https://web.archive.org/web/20180515231121/https://gnupg.org/signature_key.html>>|<with|font-family|<quote|tt>|language|<quote|verbatim>|<with|font-effects|<quote|hextended=0.8>|https://web.archive.org/web/20180515231121/https://gnupg.org/signature_key.html>>
        >.>>>

        <surround|||<with|font-size|<quote|0.771>|<surround|<locus|<id|%3E1CFC8-1CC1B930-2DB7CB1D>|<link|hyperlink|<id|%3E1CFC8-1CC1B930-2DB7CB1D>|<url|#footnr-2.9.20>>|2.9.20>.
        |<hidden-binding|<tuple|footnote-2.9.20>|2.9.20>|See
        <locus|<id|%3E1CFC8-4ECAC58--2198AF60>|<link|hyperlink|<id|%3E1CFC8-4ECAC58--2198AF60>|<url|https://web.archive.org/web/20200917215036/https://gnupg.org/signature_key.html>>|<with|font-family|<quote|tt>|language|<quote|verbatim>|<with|font-effects|<quote|hextended=0.8>|https://web.archive.org/web/20200917215036/https://gnupg.org/signature_key.html>>
        >.>>>

        <surround|||<with|font-size|<quote|0.771>|<surround|<locus|<id|%3E1CFC8-1CC1B840--5593FA11>|<link|hyperlink|<id|%3E1CFC8-1CC1B840--5593FA11>|<url|#footnr-2.9.21>>|2.9.21>.
        |<hidden-binding|<tuple|footnote-2.9.21>|2.9.21>|See
        <locus|<id|%3E1CFC8-4ECAC58--2198AF60>|<link|hyperlink|<id|%3E1CFC8-4ECAC58--2198AF60>|<url|https://web.archive.org/web/20210923054234/https://www.gnupg.org/signature_key.html>>|<with|font-family|<quote|tt>|language|<quote|verbatim>|<with|font-effects|<quote|hextended=0.8>|https://web.archive.org/web/20210923054234/https://www.gnupg.org/signature_key.html>>
        >.>>>

        <surround|||<with|font-size|<quote|0.771>|<surround|<locus|<id|%3E1CFC8-1CC1B750-37991810>|<link|hyperlink|<id|%3E1CFC8-1CC1B750-37991810>|<url|#footnr-2.9.22>>|2.9.22>.
        |<hidden-binding|<tuple|footnote-2.9.22>|2.9.22>|See
        <locus|<id|%3E1CFC8-4ECAC58--2198AF60>|<link|hyperlink|<id|%3E1CFC8-4ECAC58--2198AF60>|<url|https://web.archive.org/web/20211018075758/https://gnupg.org/signature_key.html>>|<with|font-family|<quote|tt>|language|<quote|verbatim>|<with|font-effects|<quote|hextended=0.8>|https://web.archive.org/web/20211018075758/https://gnupg.org/signature_key.html>>
        >.>>>

        \;

        \;
      </surround>|<pageref|auto-156>>
    </associate>
    <\associate|toc>
      Front Matter <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|1<space|2spc>Trust
      through Stories> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2><vspace|0.5fn>

      1.1<space|2spc>Summary <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-3>

      1.2<space|2spc>Background <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-4>

      1.3<space|2spc>Purpose <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-5>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|2<space|2spc>List
      of Public Keys> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-6><vspace|0.5fn>

      2.1<space|2spc><with|font-shape|<quote|small-caps>|Bitcoin Core>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-7>

      <with|par-left|<quote|1tab>|2.1.1<space|2spc>Background
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-8>>

      <with|par-left|<quote|1tab>|2.1.2<space|2spc>History
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-15>>

      <with|par-left|<quote|1tab>|2.1.3<space|2spc>Public Key Details
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-16>>

      <with|par-left|<quote|2tab>|2.1.3.1<space|2spc>Binary Signing Keys
      (v25.0) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-17>>

      <with|par-left|<quote|2tab>|2.1.3.2<space|2spc>Binary Signing Keys
      (v23.0) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-19>>

      <with|par-left|<quote|2tab>|2.1.3.3<space|2spc>Binary Signing Keys
      (v22.0) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-21>>

      <with|par-left|<quote|2tab>|2.1.3.4<space|2spc>Binary Signing Key
      (v0.11.0\Uv0.21.2) (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|90C8
      019E 36C2 E964>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-23>>

      <with|par-left|<quote|2tab>|2.1.3.5<space|2spc>Binary Signing Key
      (v0.9.3\Uv0.10.2) (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|7481
      0B01 2346 C9A6>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-25>>

      <with|par-left|<quote|2tab>|2.1.3.6<space|2spc>Binary Signing Key
      (<with|font-shape|<quote|small-caps>|v0.8.6>\Uv0.9.2.1)
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|29D9
      EE6B 1FC7 30C1>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-27>>

      <with|par-left|<quote|2tab>|2.1.3.7<space|2spc><with|font-shape|<quote|small-caps>|Satoshi
      Nakamoto> (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|18C0
      9E86 5EC9 48A1>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-29>>

      2.2<space|2spc><with|font-shape|<quote|small-caps>|Cryptomator>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-34>

      <with|par-left|<quote|1tab>|2.2.1<space|2spc>Background
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-35>>

      <with|par-left|<quote|1tab>|2.2.2<space|2spc>History
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-41>>

      <with|par-left|<quote|1tab>|2.2.3<space|2spc>Public Key Details
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-42>>

      <with|par-left|<quote|2tab>|2.2.3.1<space|2spc>Binary signing key
      (v1.5.8\U ) (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|615D
      449F E6E6 A235>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-43>>

      <with|par-left|<quote|2tab>|2.2.3.2<space|2spc>Binary signing key (
      \Uv1.5.7) (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|509C
      9D63 34C8 0F11>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-45>>

      2.3<space|2spc><with|font-shape|<quote|small-caps>|Debian>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-48>

      <with|par-left|<quote|1tab>|2.3.1<space|2spc>Background
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-49>>

      <with|par-left|<quote|1tab>|2.3.2<space|2spc>History
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-52>>

      <with|par-left|<quote|1tab>|2.3.3<space|2spc>Public Key Details
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-55>>

      <with|par-left|<quote|2tab>|2.3.3.1<space|2spc>Installation Image
      Signature Keys <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-56>>

      <with|par-left|<quote|2tab>|2.3.3.2<space|2spc>Verbose key details
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-58>>

      <with|par-left|<quote|4tab>|Key 1999-01-30
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|7C3B
      7970 88C7 C1F7>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-59><vspace|0.15fn>>

      <with|par-left|<quote|4tab>|Key 2000-09-16
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|72FD
      C205 F6A3 2A8E>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-61><vspace|0.15fn>>

      <with|par-left|<quote|4tab>|Key 2004-06-20
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|F82E
      5CC0 4B2B 2B9E>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-62><vspace|0.15fn>>

      <with|par-left|<quote|4tab>|Key 2009-05-21
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|39BE
      2D72 5CEE 3195>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-63><vspace|0.15fn>>

      <with|par-left|<quote|4tab>|Key 2009-10-03
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|9880
      21A9 64E6 EA7D>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-64><vspace|0.15fn>>

      <with|par-left|<quote|4tab>|Key 2011-01-05
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|DA87
      E80D 6294 BE9B>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-65><vspace|0.15fn>>

      <with|par-left|<quote|4tab>|Key 2011-03-09
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|6F95
      B499 6CA7 B5A6>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-66><vspace|0.15fn>>

      <with|par-left|<quote|4tab>|Key 2013-05-06
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|510A
      D6B9 AD11 CF6A>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-67><vspace|0.15fn>>

      <with|par-left|<quote|4tab>|Key 2014-01-03
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|1239
      00F2 A9B2 6DF5>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-68><vspace|0.15fn>>

      <with|par-left|<quote|4tab>|Key 2014-04-15
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|4246
      8F40 09EA 8AC3>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-69><vspace|0.15fn>>

      2.4<space|2spc><with|font-shape|<quote|small-caps>|Electrum>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-71>

      <with|par-left|<quote|1tab>|2.4.1<space|2spc>Background
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-72>>

      <with|par-left|<quote|1tab>|2.4.2<space|2spc>History
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-79>>

      <with|par-left|<quote|1tab>|2.4.3<space|2spc>Public Key Details
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-80>>

      <with|par-left|<quote|2tab>|2.4.3.1<space|2spc><with|font-shape|<quote|small-caps>|Thomas
      Voegtlin> signing key (<with|font-shape|<quote|small-caps>|Electrum>
      v1.7\U<space|1em>) (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|2BD5
      824B 7F94 70E6>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-81>>

      <with|par-left|<quote|2tab>|2.4.3.2<space|2spc><with|font-shape|<quote|small-caps>|Stephan
      \PEmzy\Q Oeste> signing key (<with|font-shape|<quote|small-caps>|Electrum>
      v4.1.5\U<space|1em>) (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|2EBB
      056F D847 F8A7>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-83>>

      <with|par-left|<quote|2tab>|2.4.3.3<space|2spc><with|font-shape|<quote|small-caps>|SomberNight>
      signing key (<with|font-shape|<quote|small-caps>|Electrum>
      v3.2.0\U<space|1em>) (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|CA9E
      EEC4 3DF9 11DC>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-85>>

      <with|par-left|<quote|2tab>|2.4.3.4<space|2spc><with|font-shape|<quote|small-caps>|SomberNight>
      signing key (<with|font-shape|<quote|small-caps>|ElectrumX>)
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|E7B7
      48CD AF5E 5ED9>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-87>>

      2.5<space|2spc><with|font-shape|<quote|small-caps>|Element>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-90>

      <with|par-left|<quote|1tab>|2.5.1<space|2spc>Background
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-91>>

      <with|par-left|<quote|1tab>|2.5.2<space|2spc>History
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-97>>

      <with|par-left|<quote|1tab>|2.5.3<space|2spc>Public Key Details
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-98>>

      <with|par-left|<quote|2tab>|2.5.3.1<space|2spc>Signing key (2019\U)
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|D7B0
      B669 41D0 1538>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-99>>

      2.6<space|2spc><with|font-shape|<quote|small-caps>|F-Droid>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-102>

      <with|par-left|<quote|1tab>|2.6.1<space|2spc>Background
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-103>>

      <with|par-left|<quote|1tab>|2.6.2<space|2spc>History
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-109>>

      <with|par-left|<quote|1tab>|2.6.3<space|2spc>Public Key Details
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-110>>

      <with|par-left|<quote|2tab>|2.6.3.1<space|2spc><with|font-shape|<quote|small-caps>|Android>
      client binary release signing key (2017\U )
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|41E7
      044E 1DBA 2E89>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-111>>

      <with|par-left|<quote|2tab>|2.6.3.2<space|2spc><with|font-shape|<quote|small-caps>|Android>
      client <with|font-shape|<quote|small-caps>|Git> repository signing key
      (2017\U ) (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|74DC
      A8A3 6C52 F833>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-113>>

      <with|par-left|<quote|2tab>|2.6.3.3<space|2spc><with|font-shape|<quote|small-caps>|Android>
      client <with|font-shape|<quote|small-caps>|Git> repository signing key
      (2015\U ) (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|E9E2
      8DEA 00AA 5556>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-115>>

      <with|par-left|<quote|2tab>|2.6.3.4<space|2spc><with|font-shape|<quote|small-caps>|Android>
      client APK signing key (2010\U2023)
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-117>>

      2.7<space|2spc><with|font-shape|<quote|small-caps>|Freedombox>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-119>

      <with|par-left|<quote|1tab>|2.7.1<space|2spc>Background
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-120>>

      <with|par-left|<quote|1tab>|2.7.2<space|2spc>History
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-126>>

      <with|par-left|<quote|1tab>|2.7.3<space|2spc>Public Key Details
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-127>>

      <with|par-left|<quote|2tab>|2.7.3.1<space|2spc>Signing key (2015\U2019)
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|36C3
      6144 0C9B C971>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-128>>

      <with|par-left|<quote|2tab>|2.7.3.2<space|2spc>Signing key (2016\U2017)
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|77C0
      C75E 7B65 0808>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-130>>

      <with|par-left|<quote|2tab>|2.7.3.3<space|2spc>Signing key (2018\U2022)
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|5D41
      53D6 FE18 8FC8>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-132>>

      2.8<space|2spc><with|font-shape|<quote|small-caps>|GitHub>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-135>

      <with|par-left|<quote|1tab>|2.8.1<space|2spc>Background
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-136>>

      <with|par-left|<quote|1tab>|2.8.2<space|2spc>History
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-139>>

      <with|par-left|<quote|1tab>|2.8.3<space|2spc>Public Key Details
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-143>>

      <with|par-left|<quote|2tab>|2.8.3.1<space|2spc>Web-flow commit signing
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|4AEE
      18F8 3AFD EB23>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-144>>

      2.9<space|2spc><with|font-shape|<quote|small-caps>|GnuPG>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-147>

      <with|par-left|<quote|1tab>|2.9.1<space|2spc>Background
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-148>>

      <with|par-left|<quote|1tab>|2.9.2<space|2spc>History
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-154>>

      <with|par-left|<quote|1tab>|2.9.3<space|2spc>Public Key Details
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-155>>

      <with|par-left|<quote|2tab>|2.9.3.1<space|2spc>Release signing key -
      <with|font-shape|<quote|small-caps>|Werner Koch> (2020\U)
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|5288
      97B8 2640 3ADA>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-157>>

      <with|par-left|<quote|2tab>|2.9.3.2<space|2spc>Release signing key -
      <with|font-shape|<quote|small-caps>|Niibe Yutaka> (2021\U)
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|E98E
      9B2D 19C6 C8BD>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-159>>

      <with|par-left|<quote|2tab>|2.9.3.3<space|2spc>Release signing key -
      <with|font-shape|<quote|small-caps>|Andre Heinecke> (2017\U)
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|BCEF
      7E29 4B09 2E28>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-161>>

      <with|par-left|<quote|2tab>|2.9.3.4<space|2spc>Release signing key -
      <with|font-shape|<quote|small-caps>|GnuPG.com> (2021\U)
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|549E
      695E 905B A208>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-163>>

      2.10<space|2spc><with|font-shape|<quote|small-caps>|Inkscape>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-166>

      <with|par-left|<quote|1tab>|2.10.1<space|2spc>Background
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-167>>

      <with|par-left|<quote|1tab>|2.10.2<space|2spc>History
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-169>>

      <with|par-left|<quote|1tab>|2.10.3<space|2spc>Public Key Details
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-170>>

      <with|par-left|<quote|2tab>|2.10.3.1<space|2spc>Signing key (
      v0.92\Uv1.2) (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|5FCB
      204E F882 B07A>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-171>>

      2.11<space|2spc><with|font-shape|<quote|small-caps>|KeePassXC>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-174>

      <with|par-left|<quote|1tab>|2.11.1<space|2spc>Background
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-175>>

      <with|par-left|<quote|1tab>|2.11.2<space|2spc>History
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-178>>

      <with|par-left|<quote|1tab>|2.11.3<space|2spc>Public Key Details
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-179>>

      <with|par-left|<quote|2tab>|2.11.3.1<space|2spc>Signing key (2017\U )
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|CFB4
      C216 6397 D0D2>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-180>>

      <with|par-left|<quote|2tab>|2.11.3.2<space|2spc>Developer key (2021)
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|CFB4
      C216 6397 D0D2>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-182>>

      2.12<space|2spc><with|font-shape|<quote|small-caps>|Qubes OS>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-185>

      <with|par-left|<quote|1tab>|2.12.1<space|2spc>Background
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-186>>

      <with|par-left|<quote|1tab>|2.12.2<space|2spc>History
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-191>>

      <with|par-left|<quote|1tab>|2.12.3<space|2spc>Public Key Details
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-192>>

      <with|par-left|<quote|2tab>|2.12.3.1<space|2spc>Qubes Master Signing
      Key (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|<with|font-family|<quote|tt>|DDFA
      1A3E 3687 9494>>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-193>>

      <with|par-left|<quote|2tab>|2.12.3.2<space|2spc>Release 4 Signing Key
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|1848
      792F 9E27 95E9>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-195>>

      <with|par-left|<quote|2tab>|2.12.3.3<space|2spc>Release 3 Signing Key
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|CB11
      CA1D 03FA 5082>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-197>>

      <with|par-left|<quote|2tab>|2.12.3.4<space|2spc>Release 2 Signing Key
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0C73
      B9D4 0A40 E458>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-199>>

      <with|par-left|<quote|2tab>|2.12.3.5<space|2spc>Release 1 Signing Key
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|EA01
      201B 2110 93A7>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-201>>

      2.13<space|2spc><with|font-shape|<quote|small-caps>|RaspiBlitz>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-204>

      <with|par-left|<quote|1tab>|2.13.1<space|2spc>Background
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-205>>

      <with|par-left|<quote|1tab>|2.13.2<space|2spc>History
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-210>>

      <with|par-left|<quote|1tab>|2.13.3<space|2spc>Public Key Details
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-212>>

      <with|par-left|<quote|2tab>|2.13.3.1<space|2spc><with|font-shape|<quote|small-caps>|Christian
      \Prootzol\Q Rotzoll> (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|1C73
      060C 7C17 6461>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-213>>

      2.14<space|2spc><with|font-shape|<quote|small-caps>|Satoshi Labs>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-216>

      <with|par-left|<quote|1tab>|2.14.1<space|2spc>Background
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-217>>

      <with|par-left|<quote|1tab>|2.14.2<space|2spc>History
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-225>>

      <with|par-left|<quote|1tab>|2.14.3<space|2spc>Public Key Details
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-226>>

      <with|par-left|<quote|2tab>|2.14.3.1<space|2spc><with|font-shape|<quote|small-caps>|Pavol
      Rusn�k> (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|91F3
      B339 B9A0 2A3D>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-227>>

      <with|par-left|<quote|2tab>|2.14.3.2<space|2spc>2020 Signing Key
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|26A3
      A566 62F0 E7E2>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-229>>

      <with|par-left|<quote|2tab>|2.14.3.3<space|2spc>2021 Signing Key
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|E21B
      6950 A2EC B65C>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-231>>

      2.15<space|2spc><with|font-shape|<quote|small-caps>|Tails>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-234>

      <with|par-left|<quote|1tab>|2.15.1<space|2spc>Background
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-235>>

      <with|par-left|<quote|1tab>|2.15.2<space|2spc>History
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-242>>

      <with|par-left|<quote|1tab>|2.15.3<space|2spc>Public Key Details
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-243>>

      <with|par-left|<quote|2tab>|2.15.3.1<space|2spc>Signing key
      (2015\U<space|1em>) (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|DBB8
      02B2 58AC D84F>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-244>>

      <with|par-left|<quote|2tab>|2.15.3.2<space|2spc>Mailing list key
      (2009\U<space|1em>) (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|1D29
      75ED F93E 735F>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-246>>

      <with|par-left|<quote|2tab>|2.15.3.3<space|2spc>Signing key
      (2010\U2015) (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|1202
      821C BE2C D9C1>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-248>>

      2.16<space|2spc><with|font-shape|<quote|small-caps>|Tor Browser>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-251>

      <with|par-left|<quote|1tab>|2.16.1<space|2spc>Background
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-252>>

      <with|par-left|<quote|1tab>|2.16.2<space|2spc>History
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-255>>

      <with|par-left|<quote|1tab>|2.16.3<space|2spc>Public Key Details
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-257>>

      <with|par-left|<quote|2tab>|2.16.3.1<space|2spc>Release Signing Key
      (2015\U ) (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|4E2C
      6E87 9329 8290>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-258>>

      2.17<space|2spc><with|font-shape|<quote|small-caps>|Trisquel>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-261>

      <with|par-left|<quote|1tab>|2.17.1<space|2spc>Background
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-262>>

      <with|par-left|<quote|1tab>|2.17.2<space|2spc>History
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-265>>

      <with|par-left|<quote|1tab>|2.17.3<space|2spc>Public Key Details
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-266>>

      <with|par-left|<quote|2tab>|2.17.3.1<space|2spc>Signing key (Trisquel
      11 Aramo) (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|F5DA
      AAF7 4AD4 C938>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-267>>

      2.18<space|2spc><with|font-shape|<quote|small-caps>|VeraCrypt>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-270>

      <with|par-left|<quote|1tab>|2.18.1<space|2spc>Background
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-271>>

      <with|par-left|<quote|1tab>|2.18.2<space|2spc>History
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-274>>

      <with|par-left|<quote|1tab>|2.18.3<space|2spc>Public Key Details
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-276>>

      <with|par-left|<quote|2tab>|2.18.3.1<space|2spc>Signing key (2018\U )
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|821A
      CD02 680D 16DE>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-277>>

      <with|par-left|<quote|2tab>|2.18.3.2<space|2spc>Signing key
      (2014\U2018) (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|EB55
      9C7C 54DD D393>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-279>>

      2.19<space|2spc><with|font-shape|<quote|small-caps>|Youtube-dl>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-283>

      <with|par-left|<quote|1tab>|2.19.1<space|2spc>Background
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-284>>

      <with|par-left|<quote|1tab>|2.19.2<space|2spc>History
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-287>>

      <with|par-left|<quote|1tab>|2.19.3<space|2spc>Public Key Details
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-289>>

      <with|par-left|<quote|2tab>|2.19.3.1<space|2spc>Binary signing key.
      <with|font-shape|<quote|small-caps>|Sergey M.>
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|2C39
      3E0F 18A9 236D>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-290>>

      <with|par-left|<quote|2tab>|2.19.3.2<space|2spc>Binary signing key.
      <with|font-shape|<quote|small-caps>|Philipp Hagemeister>
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|F5EA
      B582 FAFB 085C>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-292>>

      <with|par-left|<quote|2tab>|2.19.3.3<space|2spc>Binary signing key.
      <with|font-shape|<quote|small-caps>|Philipp Hagemeister>
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|DB4B
      54CB A482 6A18>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-295>>

      <with|par-left|<quote|2tab>|2.19.3.4<space|2spc>Binary signing key.
      <with|font-shape|<quote|small-caps>|Filippo Valsorda>
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|EBF0
      1804 BCF0 5F6B>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-298>>

      2.20<space|2spc><with|font-shape|<quote|small-caps>|Yt-dlp>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-302>

      <with|par-left|<quote|1tab>|2.20.1<space|2spc>Background
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-303>>

      <with|par-left|<quote|1tab>|2.20.2<space|2spc>History
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-306>>

      <with|par-left|<quote|1tab>|2.20.3<space|2spc>Public Key Details
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-307>>

      <with|par-left|<quote|2tab>|2.20.3.1<space|2spc>Signing key (2023\U )
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|57CF
      6593 3B5A 7581>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-308>>

      <with|par-left|<quote|2tab>|2.20.3.2<space|2spc>Commit signing key
      (2023\U ) (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|7EEE
      9E1E 817D 0A39>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-310>>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|Appendix
      A<space|2spc>How to use <with|font-shape|<quote|small-caps>|GnuPG>>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-313><vspace|0.5fn>

      A.1<space|2spc>Terms and Definitions
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-314>

      A.2<space|2spc>Useful Commands <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-315>

      <with|par-left|<quote|1tab>|A.2.1<space|2spc>Obtaining keys
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-316>>

      <with|par-left|<quote|2tab>|A.2.1.1<space|2spc>Import a public key
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-317>>

      <with|par-left|<quote|2tab>|A.2.1.2<space|2spc>Download from a
      keyserver <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-318>>

      <with|par-left|<quote|1tab>|A.2.2<space|2spc>Analyzing keys
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-319>>

      <with|par-left|<quote|2tab>|A.2.2.1<space|2spc>View public key
      fingerprint <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-320>>

      <with|par-left|<quote|1tab>|A.2.3<space|2spc>Sending keys
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-321>>

      <with|par-left|<quote|2tab>|A.2.3.1<space|2spc>Export public key
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-322>>

      <with|par-left|<quote|2tab>|A.2.3.2<space|2spc>Upload public key
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-323>>

      <with|par-left|<quote|1tab>|A.2.4<space|2spc>Creating keys
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-324>>

      <with|par-left|<quote|2tab>|A.2.4.1<space|2spc>Using default settings
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-325>>

      <with|par-left|<quote|2tab>|A.2.4.2<space|2spc>With subkeys
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-326>>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|Bibliography>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-327><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|Index>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-328><vspace|0.5fn>
    </associate>
  </collection>
</auxiliary>