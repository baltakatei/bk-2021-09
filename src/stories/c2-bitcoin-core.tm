<TeXmacs|2.1.1>

<project|../book.tm>

<style|<tuple|book|style-bk>>

<\body>
  <\hide-preamble>
    \;

    <assign|vpk|<\macro|body>
      <\vgroup>
        <\verbatim-8pt>
          <\with|font-base-size|8>
            <arg|body>
          </with>
        </verbatim-8pt>
      </vgroup>

      \;
    </macro>>
  </hide-preamble>

  <new-page*><section|<name|Bitcoin Core>><label|c2 bitcoin-core>

  Last updated on 2023-06-12 by <name|Steven Baltakatei Sandoval>.

  <subsection|Background><label|c2 bitcoin-core-bg>

  <index-complex|<tuple|Software|Bitcoin Core>|strong|c2 bitcoin-core
  idx1|<tuple|Software|Bitcoin Core>><name|Bitcoin Core><\footnote>
    Main website: <hlinkv|https://bitcoincore.org/>.
  </footnote> is the \Preference implementation\Q of the
  <subindex|Software|Bitcoin><name|Bitcoin> protocol. It is maintained by a
  group of people who have become known as the <name|Bitcoin Core>
  developers.

  Early in the blockchain's history, the software that verified transactions
  against balances of previous transactions was a <name|Windows> executable
  known as <name|Bitcoin>. The initial release of this software was by an
  entity that called themselves <subindex|People|Nakamoto,
  Satoshi><name|Satoshi Nakamoto>.<\footnote>
    There exist various dubious theories regarding PGP key use by
    <name|Satoshi Nakamoto>.<\footnote>
      See <hlinkv|https://www.vice.com/en/article/jpgq3y/satoshis-pgp-keys-are-probably-backdated-and-point-to-a-hoax>.
    </footnote> The most likely candidate (<long-id-spc|18C0 9E86 5EC9 48A1>)
    is one signed by <name|Bitcoin Core> developers <subindex|People|Todd,
    Peter><name|Peter Todd> (<long-id-spc|7FAB 1142 67E4 FA04>) and
    <name|Wladimir J. van der Laan> (<long-id-spc|7481 0B01 2346 C9A6>).
  </footnote> Satoshi later gave up the code maintainer role of the project.
  The person who subsequently gained control was a person named
  <subindex|People|Andresen, Gavin><name|Gavin Andresen>. The software was
  rebranded from <name|Bitcoin> to <name|Bitcoin Core> at version
  <em|0.9.0>.<\footnote>
    See <hlinkv|https://bitcoin.org/en/release/v0.9.0#rebranding-to-bitcoin-core>.
  </footnote> A developer named <subindex|People|van der Laan, Wladimir
  J.><name|Wladimir J. van der Laan> became owner of the <name|OpenPGP>
  signing keys of the reference implementation starting at version
  <em|0.9.3>. <name|van der Laan> originally used a personal key
  (<long-id-spc|7481 0B01 2346 C9A6>) to sign binaries but later created a
  dedicated key (<long-id-spc|90C8 019E 36C2 E964>) to sign binaries. In
  2021, binaries were instead signed by a group of people each with their own
  personal key.

  As of 2023-06-12, the latest release of <name| Bitcoin Core> is version
  <em|25.0>.

  <subsection|History><label|c2 bitcoin-core-hist>

  <\description>
    <item*|2011-08-24>Creation date of <name|van der Laan>'s personal signing
    key <long-id-spc|7481 0B01 2346 C9A6>.

    <item*|2011-12-15>Creation date of <name|Andresen>'s dedicated code
    signing key <long-id-spc|29D9 EE6B 1FC7 30C1>.

    <item*|2013-03-23>Earliest snapshot of the <hlinkv|https://bitcoin.org>
    website on the <name|Internet Archive>.<\footnote>
      See <hlinkv|https://web.archive.org/web/20130323195546/http://bitcoin.org/en>.
    </footnote> It is a redirect to <hlinkv|https://bitcoin.org/en>.

    <item*|2013-04-11>Earliest snapshot of the
    <hlinkv|https://bitcoincore.org> website on the <name|Internet
    Archive>.<\footnote>
      See <hlinkv|https://web.archive.org/web/20130411033932/http://bitcoincore.org/>.
    </footnote>

    <item*|2013-07-27>Earliest snapshot of main <name|GitHub> repository at
    <hlinkv|https://github.com/bitcoin/bitcoin> on the <name|Internet
    Archive>.<\footnote>
      See <hlinkv|https://web.archive.org/web/20130727135658/https://github.com/bitcoin/bitcoin>.
    </footnote>

    <item*|2014-03-19>The reference client rebranded from <name|Bitcoin> to
    <name|Bitcoin Core>.

    <item*|2014-04-08><name|Gavin Andresen> steps down as lead developer.
    Hands over role to <name|Wladimir J. van der Laan>.<\footnote>
      See <hlinkv|https://www.coindesk.com/gavin-andresen-steps-bitcoins-lead-developer>.
    </footnote> <name|Andresen> maintains commit privileges to the
    <name|GitHub> repository.

    <item*|2015-06-24>Creation date of <name|van der Laan>'s dedicated code
    signing key <long-id-spc|90C8 019E 36C2 E964>.

    <item*|2016-05-02><name|Gavin Andresen>'s commit privileges revoked by
    other <name|Bitcoin Core> developers after <name|Andresen> published a
    blog post claiming <name|Craig Wright> was <name|Satoshi
    Nakamoto>.<\footnote>
      See <hlinkv|https://twitter.com/peterktodd/status/727078284345917441>,
      <hlinkv|https://laanwj.github.io/2016/05/06/hostility-scams-and-moving-forward.html>,
      <hlinkv|https://www.bbc.com/news/technology-36202904>, and
      <hlinkv|https://www.theguardian.com/technology/2016/may/06/bitcoin-project-blocks-out-gavin-andresen-over-satoshi-nakamoto-claims>.
    </footnote>

    <item*|2021-09-13><name|Bitcoin Core> version <em|22.0> published with
    change to how binary releases are signed. Releases now signed by several
    individuals<\footnote>
      For list of fingerprints, see <hlinkv|https://web.archive.org/web/20210725054312/https://github.com/bitcoin/bitcoin/blob/master/contrib/builder-keys/keys.txt>.
    </footnote> instead of <name|van der Laan>'s dedicated code signing key
    <long-id-spc|90C8 019E 36C2 E964>.<\footnote>
      See <hlinkv|https://web.archive.org/web/20210926105351/https://bitcoincore.org/en/download/>;
      specifically \PLinux verification instructions\Q.
    </footnote> Additionally, the version numbering system dropped the
    initial zero (i.e. <em|22.0> instead of <em|0.22.0>).<\footnote>
      See <hlinkv|https://web.archive.org/web/20220426204054/https://bitcoincore.org/en/releases/>.
    </footnote>

    <item*|2022-04-25><name|Bitcoin Core> version <em|23.0>
    published.<\footnote>
      See <hlinkv|https://web.archive.org/web/20220427090519/https://bitcoincore.org/en/releases/23.0/>.
    </footnote>

    <item*|2022-12-11><name|Bitcoin Core> version <em|24.0.1> published.

    <item*|2023-05-26><name|Bitcoin Core> version <em|25.0>
    published.<\footnote>
      See <hlinkv|https://web.archive.org/web/20230526151017/https://bitcoincore.org/en/releases/25.0/>.
    </footnote> Copies of public keys signing the binary releases were moved
    from the <hlinkv|github.com/bitcoin/bitcoin> repository to the
    <hlinkv|github.com/bitcoin/guix.sigs> repository.<\footnote>
      See <hlinkv|https://github.com/bitcoin-core/gui/commit/296e88225096125b08665b97715c5b8ebb1d28ec>.
    </footnote> <\footnote>
      See <hlinkv|https://github.com/bitcoin-core/guix.sigs/tree/main/builder-keys>.
    </footnote> <\footnote>
      <name|fanquake>. (2022-11-29). \Pcontrib: remove builder keys #26598\Q.
      <hlinkv|https://github.com/bitcoin/bitcoin/pull/26598>.
    </footnote>
  </description>

  <subsection|Public Key Details><label|c2 bitcoin-core-pk>

  <subsubsection|Binary Signing Keys (v25.0)>

  For version <em|25.0>, published on 2023-05-26, the fingerprints and
  primary UIDs of keys used to sign<\footnote>
    See <hlinkv|https://web.archive.org/web/20230528174630/https://bitcoincore.org/bin/bitcoin-core-25.0/SHA256SUMS.asc>.
  </footnote> the release's <verbatim|SHA256SUMS> file<\footnote>
    See <hlinkv|https://web.archive.org/web/20230528174659/https://bitcoincore.org/bin/bitcoin-core-25.0/SHA256SUMS>.
  </footnote> are listed in Table <reference|c2 bitcoin-core-pk
  bcc-25.0-sigs>.<\footnote>
    See <hlinkv|https://web.archive.org/web/20230528174136/https://bitcoincore.org/en/download/>.
  </footnote>

  <\big-table|<tabular|<tformat|||||<cwith|1|1|1|-1|cell-tborder|1ln>|||<cwith|1|1|1|-1|cell-bborder|1ln>|||<table|<row|<cell|Fingerprint>|<cell|UID
  Name>>|<row|<cell|<long-id-spc|1756 5732 E08E 5E41>>|<cell|Andrew
  Chow>>|<row|<cell|<long-id-spc|1C24 91FF EB0E F770>>|<cell|Cory
  Fields>>|<row|<cell|<long-id-spc|4101 0811 2E7E A81F>>|<cell|Hennadii
  Stepanov (GitHub key)>>|<row|<cell|<long-id-spc|7481 0B01 2346
  C9A6>>|<cell|Wladimir J. van der Laan>>|<row|<cell|<long-id-spc|8E42 5659
  3F17 7720>>|<cell|Oliver Gugger>>|<row|<cell|<long-id-spc|8F61 7F12 00A6
  D25C>>|<cell|Gloria Zhao>>|<row|<cell|<long-id-spc|9303 B33A 3052
  24CB>>|<cell|Sebastian Kung>>|<row|<cell|<long-id-spc|944D 35F9 AC3D
  B76A>>|<cell|Michael Ford (bitcoin-otc)>>|<row|<cell|<long-id-spc|A5E0 907A
  0380 E6C3>>|<cell|CoinForensics (SigningKey)>>|<row|<cell|<long-id-spc|A7BE
  B262 1678 D37D>>|<cell|vertiond>>|<row|<cell|<long-id-spc|C237 1D91 CB71
  6EA7>>|<cell|Sebastian Falbesoner (theStack)>>|<row|<cell|<long-id-spc|D11B
  D4F3 3F1D B499>>|<cell|jackielove4u>>|<row|<cell|<long-id-spc|D7CC 770B
  81FD 22A8>>|<cell|Ben Carman>>>>>>
    <label|c2 bitcoin-core-pk bcc-25.0-sigs>Fingerprints (primary key long
    IDs) of <name|OpenPGP> keys that signed hashes of <name|Bitcoin Core>
    version <em|25.0> release files, sorted by long ID.
  </big-table>

  \;

  <subsubsection|Binary Signing Keys (v23.0)>

  For version <em|23.0>, published on 2022-04-25, the fingerprints and
  primary UIDs of keys used to sign<\footnote>
    See <hlinkv|https://web.archive.org/web/20220425130207/https://bitcoincore.org/bin/bitcoin-core-23.0/SHA256SUMS.asc>.
  </footnote> the release's <verbatim|SHA256SUMS> file<\footnote>
    See <hlinkv|https://web.archive.org/web/20220425130207/https://bitcoincore.org/bin/bitcoin-core-23.0/SHA256SUMS>.
  </footnote> are listed in Table <reference|c2 bitcoin-core-pk
  bcc-23.0-sigs>.<\footnote>
    See <hlinkv|https://web.archive.org/web/20220426204017/https://bitcoincore.org/en/download/>.
  </footnote>

  <\big-table|<tabular|<tformat|||||<cwith|1|1|1|-1|cell-tborder|1ln>|||<cwith|2|2|1|-1|cell-tborder|1ln>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|-1|-1|1|-1|cell-bborder|1ln>|||<table|<row|<cell|Fingerprint>|<cell|UID
  Name>>|<row|<cell|<long-id-spc|099B AD16 3C70 FBFA>>|<cell|Will
  Clark>>|<row|<cell|<long-id-spc|0A41 BDC3 F4FA FF1C>>|<cell|Aaron Clauson
  (sipsorcery)>>|<row|<cell|<long-id-spc|1756 5732 E08E 5E41>>|<cell|Andrew
  Chow>>|<row|<cell|<long-id-spc|2EBB 056F D847 F8A7>>|<cell|Stephan Oeste
  (it)>>|<row|<cell|<long-id-spc|4101 0811 2E7E A81F>>|<cell|Hennadii
  Stepanov (GitHub key)>>|<row|<cell|<long-id-spc|7481 0B01 2346
  C9A6>>|<cell|Wladimir J. van der Laan>>|<row|<cell|<long-id-spc|8E42 5659
  3F17 7720>>|<cell|Oliver Gugger>>|<row|<cell|<long-id-spc|944D 35F9 AC3D
  B76A>>|<cell|Michael Ford (bitcoin-otc)>>|<row|<cell|<long-id-spc|BD02 9424
  21F4 889F>>|<cell|Luke Dashjr>>|<row|<cell|<long-id-spc|C37B 1C1D 44C7
  86EE>>|<cell|Duncan Dean>>|<row|<cell|<long-id-spc|D11B D4F3 3F1D
  B499>>|<cell|jackielove4u>>|<row|<cell|<long-id-spc|D7CC 770B 81FD
  22A8>>|<cell|Ben Carman>>|<row|<cell|<long-id-spc|E13F C145 CD3F
  4304>>|<cell|Antoine Poinsot>>|<row|<cell|>|<cell|>>>>>>
    <label|c2 bitcoin-core-pk bcc-23.0-sigs>Fingerprints (primary key long
    IDs) of <name|OpenPGP> keys that signed hashes of <name|Bitcoin Core>
    version <em|23.0> release files, sorted by long ID.
  </big-table>

  <subsubsection|Binary Signing Keys (v22.0)>

  Since the release of <name|Bitcoin Core> version <em|22.0> on 2019-09-13
  (Note: the version numbering system changed to drop the leading zero), the
  <verbatim|SHA256SUMS> file available at
  <hlinkv|https://bitcoincore.org/en/download> has been split into two files:

  <\description>
    <item*|<verbatim|SHA256SUMS>>Contains hashes of the binary executable
    files.

    <item*|<verbatim|SHA256SUMS.asc>>Contains multiple detached signtaures
    from different public keys.
  </description>

  For version <em|22.0>, the fingerprints and primary UIDs of these
  signatures are in Table <reference|c2 bitcoin-core-pk bcc-22.0-sigs>.

  <\big-table|<tabular|<tformat|||||<cwith|1|1|1|-1|cell-tborder|1ln>|||<cwith|2|2|1|-1|cell-tborder|1ln>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|-1|-1|1|-1|cell-bborder|1ln>|||<table|<row|<cell|Fingerprint>|<cell|UID
  Name>>|<row|<cell|<long-id-spc|099B AD16 3C70 FBFA>>|<cell|Will
  Clark>>|<row|<cell|<long-id-spc|0A41 BDC3 F4FA FF1C>>|<cell|Aaron Clauson
  (sipsorcery)>>|<row|<cell|<long-id-spc|1756 5732 E08E 5E41>>|<cell|Andrew
  Chow (Official New Key)>>|<row|<cell|<long-id-spc|2EBB 056F D847
  F8A7>>|<cell|Stephan Oeste (it)>>|<row|<cell|<long-id-spc|4101 0811 2E7E
  A81F>>|<cell|Hennadii Stepanov (GitHub key)>>|<row|<cell|<long-id-spc|7481
  0B01 2346 C9A6>>|<cell|Wladimir J. van der
  Laan>>|<row|<cell|<long-id-spc|796C 4109 063D 4EAF>>|<cell|Jon
  Atack>>|<row|<cell|<long-id-spc|8E42 5659 3F17 7720>>|<cell|Oliver
  Gugger>>|<row|<cell|<long-id-spc|944D 35F9 AC3D B76A>>|<cell|Michael Ford
  (bitcoin-otc)>>|<row|<cell|<long-id-spc|C37B 1C1D 44C7 86EE>>|<cell|Duncan
  Dean>>|<row|<cell|<long-id-spc|D7CC 770B 81FD 22A8>>|<cell|Ben
  Carman>>|<row|<cell|<long-id-spc|E13F C145 CD3F 4304>>|<cell|Antoine
  Poinsot>>|<row|<cell|>|<cell|>>>>>>
    <label|c2 bitcoin-core-pk bcc-22.0-sigs>Fingerprints (primary key long
    IDs) of <name|OpenPGP> keys that signed hashes of <name|Bitcoin Core>
    version <em|22.0> release files, sorted by long ID.
  </big-table>

  <subsubsection|Binary Signing Key (v0.11.0\Uv0.21.2) (<long-id-spc|90C8
  019E 36C2 E964>)>

  <index-complex|<tuple|Keys|People|van der Laan, Wladimir
  J.|0x90C8019E36C2E964 (Bitcoin Core; 2015\U2022)>|||<tuple|Keys|People|van
  der Laan, Wladimir J.|<verb-sm|0x90C8019E36C2E964> <condensed|(Bitcoin
  Core; 2015\U2022)>>>This key<\footnote>
    See <hlinkv|https://reboil.com/res/2021/txt/20210719_90C8019E36C2E964..bitcoin_vanderlaan.asc>
  </footnote>, owned by <name|Wladimir J. van der Laan>, was used to sign
  <name|Bitcoin Core> releases between versions <em|0.11.0> and <em|0.21.2>.

  <\vpk>
    <with|hmagnified-factor|0.9|<\hmagnified>
      pub \ \ rsa4096/0x90C8019E36C2E964 2015-06-24 [SC] [expired:
      2022-02-10]

      \ \ \ \ \ \ Key fingerprint = 01EA 5486 DE18 A882 D4C2 \ 6845 90C8 019E
      36C2 E964

      uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ expired] Wladimir J. van der
      Laan (Bitcoin Core binary...) \<less\>69ed48c5\<gtr\>

      \;
    </hmagnified>>
  </vpk>

  <subsubsection|Binary Signing Key (v0.9.3\Uv0.10.2) (<long-id-spc|7481 0B01
  2346 C9A6>)>

  <index-complex|<tuple|Keys|People|van der Laan, Wladimir
  J.|0x74810B012346C9A6 (personal; 2011\U )>|||<tuple|Keys|People|van der
  Laan, Wladimir J.|<verb-sm|0x74810B012346C9A6> (personal; 2011\U
  )>><name|Wladimir van der Laan> used his personal key<\footnote>
    See <hlinkv|https://reboil.com/res/2021/txt/20210719_74810B012346C9A6..bitcoin_vanderlaan.asc>
  </footnote> to sign <name|Bitcoin> versions v0.9.3\Uv0.10.2.

  <\vpk>
    pub \ \ rsa2048/0x74810B012346C9A6 2011-08-24 [SC] [expires: 2027-02-08]

    \ \ \ \ \ \ Key fingerprint = 71A3 B167 3540 5025 D447 \ E8F2 7481 0B01
    2346 C9A6

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] Wladimir J. van der
    Laan \<less\>b64e04a4\<gtr\>

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] Wladimir J. van der
    Laan \<less\>69ed48c5\<gtr\>

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] Wladimir J. van der
    Laan \<less\>c7357718\<gtr\>

    sub \ \ rsa2048/0x69B4C4CDC628F8F9 2017-05-17 [A] [expires: 2027-02-08]

    \ \ \ \ \ \ Key fingerprint = 53D9 74DA 0BAF FF22 B3A5 \ FB5C 69B4 C4CD
    C628 F8F9

    sub \ \ rsa2048/0xF69705ED890DE427 2011-08-24 [E]

    \ \ \ \ \ \ Key fingerprint = D01B 5D68 0154 44D2 71DA \ D33F F697 05ED
    890D E427

    sub \ \ rsa2048/0x1E4AED62986CD25D 2017-05-17 [S] [expires: 2027-02-08]

    \ \ \ \ \ \ Key fingerprint = 9DEA E0DC 7063 249F B054 \ 7468 1E4A ED62
    986C D25D
  </vpk>

  <subsubsection|Binary Signing Key (<name|v0.8.6>\Uv0.9.2.1)
  (<long-id-spc|29D9 EE6B 1FC7 30C1>)>

  <index-complex|<tuple|Keys|People|Andresen,
  Gavin|0x29D9EE6B1FC730C1>|||<tuple|Keys|People|Andresen,
  Gavin|<verb-sm|0x29D9EE6B1FC730C1>>><name|Gavin Andresen> used this
  dedicated code-signing key<\footnote>
    See <hlinkv|https://reboil.com/res/2021/txt/20210719_29D9EE6B1FC730C1..bitcoin_andresen.asc>
  </footnote> to sign <name|Bitcoin> versions v0.8.6\Uv0.9.2.1. As of
  2021-07-19, these versions and their signatures are available at
  <hlinkv|https://bitcoincore.org/bin/insecure/>.

  <\vpk>
    <with|hmagnified-factor|0.9|<\hmagnified>
      pub \ \ rsa4096/0x29D9EE6B1FC730C1 2011-12-15 [SC]

      \ \ \ \ \ \ Key fingerprint = 2664 6D99 CBAE C9B8 1982 \ EF60 29D9 EE6B
      1FC7 30C1

      uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] Gavin Andresen (CODE
      SIGNING KEY) \<less\>84976526\<gtr\>

      sub \ \ rsa4096/0x1B7BFB457BF6E212 2013-11-01 [S]

      \ \ \ \ \ \ Key fingerprint = 3D22 F497 DEAE D078 18A2 \ 219A 1B7B FB45
      7BF6 E212

      sub \ \ rsa4096/0x36E924A98E30B3ED 2011-12-15 [E]

      \ \ \ \ \ \ Key fingerprint = DC9F CD02 AB25 5459 FDA8 \ 7469 36E9 24A9
      8E30 B3ED

      \;
    </hmagnified>>
  </vpk>

  <subsubsection|<name|Satoshi Nakamoto> (<long-id-spc|18C0 9E86 5EC9 48A1>)>

  <index-complex|<tuple|Keys|People|Nakamoto,
  Satoshi|0x18C09E865EC948A1>|||<tuple|Keys|People|Nakamoto,
  Satoshi|<verb-sm|0x18C09E865EC948A1>>>The <verbatim|dsa1024> algorithm this
  key<\footnote>
    See <hlinkv|https://reboil.com/res/2021/txt/20210719_18C09E865EC948A1..bitcoin_nakamoto.asc>
  </footnote> uses is considered <subindex|DSA, algorithm|weakness>weak by
  the the <name|NIST> standard SP800-57 Part 1 Revision 5: <em|Recommendation
  for Key management>.<\footnote>
    See <hlinkv|https://doi.org/10.6028/NIST.SP.800-57pt1r5>, table 2, page
    54. <verbatim|dsa1024> keys have only offer <math|80> bits of security
    against brute force attacks.
  </footnote> The key offers only <math|80> bits of security against the
  possibility of impersonation via a brute force attack. Nevertheless, this
  key has a signature of <name|Bitcoin Core> developer <name|Peter Todd>
  (<long-id-spc|7FAB 1142 67E4 FA04>) dated 2013-10-12. <name|Todd> also
  committed the full fingerprint in a <subindex|Organizations|Bitcoin
  Foundation><name|Bitcoin Foundation> document on 2013-04-26<\footnote>
    See <condensed|<hlinkv|https://github.com/pmlaw/The-Bitcoin-Foundation-Legal-Repo/commit/fb70771a9927e04ebe5ae33c46ba6589a9703e40>>.
  </footnote>. This key also has a signature of <name|Bitcoin Core>
  maintainer <name|Vladimir J. van der Laan>'s personal key
  (<long-id-spc|7481 0B01 2346 C9A6>) dated 2013-05-10, albeit revoked on
  2016-05-02.

  <\vpk>
    <with|hmagnified-factor|0.9|<\hmagnified>
      pub \ \ dsa1024/0x18C09E865EC948A1 2008-10-30 [SC]

      \ \ \ \ \ \ Key fingerprint = DE4E FCA3 E1AB 9E41 CE96 \ CECB 18C0 9E86
      5EC9 48A1

      uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] Satoshi Nakamoto
      \<less\>6bab5993\<gtr\>

      sig 3 \ \ \ \ \ \ \ 0x18C09E865EC948A1 2008-10-30 \ Satoshi Nakamoto
      \<less\>6bab5993\<gtr\>

      sig \ \ \ \ \ \ \ \ \ 0x74810B012346C9A6 2013-05-10 \ Wladimir J. van
      der Laan \<less\>c7357718\<gtr\>

      sig 1 \ \ \ \ \ \ \ 0x7FAB114267E4FA04 2013-10-12 \ Peter Todd
      \<less\>13866f62\<gtr\>

      rev \ \ \ \ \ \ \ \ \ 0x74810B012346C9A6 2016-05-02 \ Wladimir J. van
      der Laan \<less\>c7357718\<gtr\>

      \ \ \ \ \ \ reason for revocation: User ID is no longer valid

      sub \ \ elg2048/0xCF1857E6D6AAA69F 2008-10-30 [E]

      \ \ \ \ \ \ Key fingerprint = EA4E 9C90 7F72 21B0 B37D \ 0940 CF18 57E6
      D6AA A69F

      \;
    </hmagnified>>
  </vpk>

  <index-complex|<tuple|Software|Bitcoin Core>|strong|c2 bitcoin-core
  idx1|<tuple|Software|Bitcoin Core>>
</body>

<\initial>
  <\collection>
    <associate|chapter-nr|2>
    <associate|page-first|11>
    <associate|page-medium|papyrus>
    <associate|preamble|false>
    <associate|section-nr|0>
    <associate|subsection-nr|0>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|?>>
    <associate|auto-10|<tuple|1.3|?>>
    <associate|auto-11|<tuple|1.3.1|?>>
    <associate|auto-12|<tuple|1.1|?>>
    <associate|auto-13|<tuple|1.3.2|?>>
    <associate|auto-14|<tuple|1.2|?>>
    <associate|auto-15|<tuple|1.3.3|?>>
    <associate|auto-16|<tuple|1.3|?>>
    <associate|auto-17|<tuple|1.3.4|?>>
    <associate|auto-18|<tuple|<tuple|Keys|People|van der Laan, Wladimir
    J.|0x90C8019E36C2E964 (Bitcoin Core; 2015\U2022)>|?>>
    <associate|auto-19|<tuple|1.3.5|?>>
    <associate|auto-2|<tuple|1.1|?>>
    <associate|auto-20|<tuple|<tuple|Keys|People|van der Laan, Wladimir
    J.|0x74810B012346C9A6 (personal; 2011\U )>|?>>
    <associate|auto-21|<tuple|1.3.6|?>>
    <associate|auto-22|<tuple|<tuple|Keys|People|Andresen,
    Gavin|0x29D9EE6B1FC730C1>|?>>
    <associate|auto-23|<tuple|1.3.7|?>>
    <associate|auto-24|<tuple|<tuple|Keys|People|Nakamoto,
    Satoshi|0x18C09E865EC948A1>|?>>
    <associate|auto-25|<tuple|DSA, algorithm|?>>
    <associate|auto-26|<tuple|Organizations|?>>
    <associate|auto-27|<tuple|<tuple|Software|Bitcoin Core>|?>>
    <associate|auto-3|<tuple|<tuple|Software|Bitcoin Core>|?>>
    <associate|auto-4|<tuple|Software|?>>
    <associate|auto-5|<tuple|People|?>>
    <associate|auto-6|<tuple|People|?>>
    <associate|auto-7|<tuple|People|?>>
    <associate|auto-8|<tuple|People|?>>
    <associate|auto-9|<tuple|1.2|?>>
    <associate|c2 bitcoin-core|<tuple|1|?>>
    <associate|c2 bitcoin-core-bg|<tuple|1.1|?>>
    <associate|c2 bitcoin-core-hist|<tuple|1.2|?>>
    <associate|c2 bitcoin-core-pk|<tuple|1.3|?>>
    <associate|c2 bitcoin-core-pk bcc-22.0-sigs|<tuple|1.3|?>>
    <associate|c2 bitcoin-core-pk bcc-23.0-sigs|<tuple|1.2|?>>
    <associate|c2 bitcoin-core-pk bcc-25.0-sigs|<tuple|1.1|?>>
    <associate|footnote-1.1|<tuple|1.1|?>>
    <associate|footnote-1.10|<tuple|1.10|?>>
    <associate|footnote-1.11|<tuple|1.11|?>>
    <associate|footnote-1.12|<tuple|1.12|?>>
    <associate|footnote-1.13|<tuple|1.13|?>>
    <associate|footnote-1.14|<tuple|1.14|?>>
    <associate|footnote-1.15|<tuple|1.15|?>>
    <associate|footnote-1.16|<tuple|1.16|?>>
    <associate|footnote-1.17|<tuple|1.17|?>>
    <associate|footnote-1.18|<tuple|1.18|?>>
    <associate|footnote-1.19|<tuple|1.19|?>>
    <associate|footnote-1.2|<tuple|1.2|?>>
    <associate|footnote-1.20|<tuple|1.20|?>>
    <associate|footnote-1.21|<tuple|1.21|?>>
    <associate|footnote-1.22|<tuple|1.22|?>>
    <associate|footnote-1.23|<tuple|1.23|?>>
    <associate|footnote-1.24|<tuple|1.24|?>>
    <associate|footnote-1.25|<tuple|1.25|?>>
    <associate|footnote-1.26|<tuple|1.26|?>>
    <associate|footnote-1.27|<tuple|1.27|?>>
    <associate|footnote-1.28|<tuple|1.28|?>>
    <associate|footnote-1.29|<tuple|1.29|?>>
    <associate|footnote-1.3|<tuple|1.3|?>>
    <associate|footnote-1.4|<tuple|1.4|?>>
    <associate|footnote-1.5|<tuple|1.5|?>>
    <associate|footnote-1.6|<tuple|1.6|?>>
    <associate|footnote-1.7|<tuple|1.7|?>>
    <associate|footnote-1.8|<tuple|1.8|?>>
    <associate|footnote-1.9|<tuple|1.9|?>>
    <associate|footnr-1.1|<tuple|1.1|?>>
    <associate|footnr-1.10|<tuple|1.10|?>>
    <associate|footnr-1.11|<tuple|1.11|?>>
    <associate|footnr-1.12|<tuple|1.12|?>>
    <associate|footnr-1.13|<tuple|1.13|?>>
    <associate|footnr-1.14|<tuple|1.14|?>>
    <associate|footnr-1.15|<tuple|1.15|?>>
    <associate|footnr-1.16|<tuple|1.16|?>>
    <associate|footnr-1.17|<tuple|1.17|?>>
    <associate|footnr-1.18|<tuple|1.18|?>>
    <associate|footnr-1.19|<tuple|1.19|?>>
    <associate|footnr-1.20|<tuple|1.20|?>>
    <associate|footnr-1.21|<tuple|1.21|?>>
    <associate|footnr-1.22|<tuple|1.22|?>>
    <associate|footnr-1.23|<tuple|1.23|?>>
    <associate|footnr-1.24|<tuple|1.24|?>>
    <associate|footnr-1.25|<tuple|1.25|?>>
    <associate|footnr-1.26|<tuple|1.26|?>>
    <associate|footnr-1.27|<tuple|1.27|?>>
    <associate|footnr-1.28|<tuple|1.28|?>>
    <associate|footnr-1.29|<tuple|1.29|?>>
    <associate|footnr-1.3|<tuple|People|?>>
    <associate|footnr-1.4|<tuple|1.4|?>>
    <associate|footnr-1.5|<tuple|1.5|?>>
    <associate|footnr-1.6|<tuple|1.6|?>>
    <associate|footnr-1.7|<tuple|1.7|?>>
    <associate|footnr-1.8|<tuple|1.8|?>>
    <associate|footnr-1.9|<tuple|1.9|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|idx>
      <tuple|<tuple|Software|Bitcoin Core>|strong|c2 bitcoin-core
      idx1|<tuple|Software|Bitcoin Core>|<pageref|auto-3>>

      <tuple|<tuple|Software|Bitcoin>|<pageref|auto-4>>

      <tuple|<tuple|People|Nakamoto, Satoshi>|<pageref|auto-5>>

      <tuple|<tuple|People|Todd, Peter>|<pageref|auto-6>>

      <tuple|<tuple|People|Andresen, Gavin>|<pageref|auto-7>>

      <tuple|<tuple|People|van der Laan, Wladimir J.>|<pageref|auto-8>>

      <tuple|<tuple|Keys|People|van der Laan, Wladimir J.|0x90C8019E36C2E964
      (Bitcoin Core; 2015\U2022)>|||<tuple|Keys|People|van der Laan, Wladimir
      J.|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x90C8019E36C2E964>>
      <with|font-effects|<quote|hextended=0.8>|(Bitcoin Core;
      2015\U2022)>>|<pageref|auto-18>>

      <tuple|<tuple|Keys|People|van der Laan, Wladimir J.|0x74810B012346C9A6
      (personal; 2011\U )>|||<tuple|Keys|People|van der Laan, Wladimir
      J.|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x74810B012346C9A6>>
      (personal; 2011\U )>|<pageref|auto-20>>

      <tuple|<tuple|Keys|People|Andresen,
      Gavin|0x29D9EE6B1FC730C1>|||<tuple|Keys|People|Andresen,
      Gavin|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x29D9EE6B1FC730C1>>>|<pageref|auto-22>>

      <tuple|<tuple|Keys|People|Nakamoto,
      Satoshi|0x18C09E865EC948A1>|||<tuple|Keys|People|Nakamoto,
      Satoshi|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x18C09E865EC948A1>>>|<pageref|auto-24>>

      <tuple|<tuple|DSA, algorithm|weakness>|<pageref|auto-25>>

      <tuple|<tuple|Organizations|Bitcoin Foundation>|<pageref|auto-26>>

      <tuple|<tuple|Software|Bitcoin Core>|strong|c2 bitcoin-core
      idx1|<tuple|Software|Bitcoin Core>|<pageref|auto-27>>
    </associate>
    <\associate|table>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|1.1>|>
        Fingerprints (primary key long IDs) of
        <with|font-shape|<quote|small-caps>|OpenPGP> keys that signed hashes
        of <with|font-shape|<quote|small-caps>|Bitcoin Core> version
        <with|font-shape|<quote|italic>|25.0> release files, sorted by long
        ID.
      </surround>|<pageref|auto-12>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|1.2>|>
        Fingerprints (primary key long IDs) of
        <with|font-shape|<quote|small-caps>|OpenPGP> keys that signed hashes
        of <with|font-shape|<quote|small-caps>|Bitcoin Core> version
        <with|font-shape|<quote|italic>|23.0> release files, sorted by long
        ID.
      </surround>|<pageref|auto-14>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|1.3>|>
        Fingerprints (primary key long IDs) of
        <with|font-shape|<quote|small-caps>|OpenPGP> keys that signed hashes
        of <with|font-shape|<quote|small-caps>|Bitcoin Core> version
        <with|font-shape|<quote|italic>|22.0> release files, sorted by long
        ID.
      </surround>|<pageref|auto-16>>
    </associate>
    <\associate|toc>
      1<space|2spc><with|font-shape|<quote|small-caps>|Bitcoin Core>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1>

      <with|par-left|<quote|1tab>|1.1<space|2spc>Background
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2>>

      <with|par-left|<quote|1tab>|1.2<space|2spc>History
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-9>>

      <with|par-left|<quote|1tab>|1.3<space|2spc>Public Key Details
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-10>>

      <with|par-left|<quote|2tab>|1.3.1<space|2spc>Binary Signing Keys
      (v25.0) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-11>>

      <with|par-left|<quote|2tab>|1.3.2<space|2spc>Binary Signing Keys
      (v23.0) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-13>>

      <with|par-left|<quote|2tab>|1.3.3<space|2spc>Binary Signing Keys
      (v22.0) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-15>>

      <with|par-left|<quote|2tab>|1.3.4<space|2spc>Binary Signing Key
      (v0.11.0\Uv0.21.2) (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|90C8
      019E 36C2 E964>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-17>>

      <with|par-left|<quote|2tab>|1.3.5<space|2spc>Binary Signing Key
      (v0.9.3\Uv0.10.2) (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|7481
      0B01 2346 C9A6>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-19>>

      <with|par-left|<quote|2tab>|1.3.6<space|2spc>Binary Signing Key
      (<with|font-shape|<quote|small-caps>|v0.8.6>\Uv0.9.2.1)
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|29D9
      EE6B 1FC7 30C1>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-21>>

      <with|par-left|<quote|2tab>|1.3.7<space|2spc><with|font-shape|<quote|small-caps>|Satoshi
      Nakamoto> (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|18C0
      9E86 5EC9 48A1>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-23>>
    </associate>
  </collection>
</auxiliary>