<TeXmacs|2.1.1>

<project|../book.tm>

<style|<tuple|book|style-bk>>

<\body>
  <new-page*><section|<name|Debian>><label|c2 debian>

  Last updated on 2023-06-11 by <name|Steven Baltakatei Sandoval>.

  <subsection|Background><label|c2 debian-bg>

  <index-complex|<tuple|Software|Debian>|strong|c2 debian
  idx1|<tuple|Software|Debian>><name|Debian><\footnote>
    Main website: <hlinkv|https://www.debian.org>.
  </footnote> is a free operating system from which many <name|GNU/Linux>
  systems are derived. Such derived systems include <name|Ubuntu>,
  <name|Tails> (see <reference|c2 tails>), <name|Kali Linux>, and others.

  <name|Debian> is maintained by an association of developers who use
  <name|GnuPG> keys to sign announcements of software they contribute in
  order to protect against forgeries. A <code*|git> repository containing
  <name|GnuPG> keyrings of <name|Debian> keys is available at
  <hlinkv|https://salsa.debian.org/debian-keyring/keyring> or by installation
  of the <code*|debian-keyring> package<\footnote>
    See <hlinkv|https://tracker.debian.org/pkg/debian-keyring>
  </footnote> within a <name|Debian> system.

  The <name|Debian Project> was founded in 1993 by <subindex|People|Murdock,
  Ian Ashley><name|Ian Ashley Murdock>. Various individuals have led the
  project since.<\footnote>
    For a list of <name|Debian> Project Leaders, see
    <hlinkv|https://www.debian.org/doc/manuals/project-history/leaders>.
  </footnote> As of 2023-06-11, the latest release of the operating system is
  called \P<name|Debian 12 (<hgroup|Bookworm>)>\Q.

  <subsection|History>

  <\description>
    <item*|1993-08-16>The <name|Debian Project> officially founded by
    <name|Ian Ashley Murdock>.

    <item*|1999-01-30>Creation date of the Debian CD signing key
    <long-id-spc|7C3B 7970 88C7 C1F7>.

    <item*|2000-09-16>Creation date of <subindex|People|Mantinan, Santiago
    Garcia><name|Santiago Garcia Mantinan>'s key <long-id-spc|72FD C205 F6A3
    2A8E>.

    <item*|2004-06-20>Creation date of <subindex|People|Baumann,
    Daniel><name|Daniel Baumann>'s key <long-id-spc|F82E 5CC0 4B2B 2B9E>.

    <item*|2009-10-03>Creation date of the Debian CD signing key
    <long-id-spc|9880 21A9 64E6 EA7D>.

    <item*|2011-01-05>Creation date of the Debian CD signing key
    <long-id-spc|DA87 E80D 6294 BE9B>.

    <item*|2014-04-15>Creation date of the Debian Testing CDs Automatic
    Signing Key <long-id-spc|4246 8F40 09EA 8AC3>.
  </description>

  <subsection|Public Key Details><label|c2 debian-pk>

  <subsubsection|Installation Image Signature Keys>

  The <name|Debian> website makes available images of the operating system
  that can be installed onto and executed from removable media such as
  Compact Discs (CD), Digital Versatile Disc (DVD), and Universal Serial Bus
  (USB) storage devices. A set of <name|GnuPG> public key fingerprints have
  been listed on the <verbatim|debian.org> website at
  <hlinkv|https://debian.org/CD/verify>. Table <reference|c2 tab debian cd
  keys> summarizes the creation dates, long IDs, and availabilities of these
  keys. Full fingerprints and other information may be found in section
  <reference|c2 debian-pk-verbose>.

  <\big-table|<tabular|<tformat|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|2|2|1|-1|cell-tborder|1ln>|<cwith|1|1|1|1|cell-lborder|0ln>|<cwith|1|1|5|5|cell-rborder|0ln>|<cwith|11|11|1|-1|cell-tborder|0ln>|<cwith|10|10|1|-1|cell-bborder|0ln>|<cwith|11|11|1|-1|cell-bborder|1ln>|<cwith|11|11|1|1|cell-lborder|0ln>|<cwith|11|11|5|5|cell-rborder|0ln>|<table|<row|<cell|Date>|<cell|Long
  ID>|<cell|Description>|<cell|Available>|<cell|Link>>|<row|<cell|1999-01-30>|<cell|<long-id-spc|7C3B
  7970 88C7 C1F7>>|<cell|Debian CD signing key>|<cell|2011\U2015
  <note-ref|+2S8OYb331U74E4Rd>>|<cell|<note-ref|+2S8OYb331U74E4Ri>>>|<row|<cell|2000-09-16>|<cell|<long-id-spc|72FD
  C205 F6A3 2A8E>>|<cell|Santiago Garcia Mantinan>|<cell|2011\U2015
  <note-ref|+2S8OYb331U74E4Rd>>|<cell|<note-ref|+2S8OYb331U74E4Rj>>>|<row|<cell|2004-06-20>|<cell|<long-id-spc|F82E
  5CC0 4B2B 2B9E>>|<cell|Daniel Baumann>|<cell|2011\U2015
  <note-ref|+2S8OYb331U74E4Rd>>|<cell|>>|<row|<cell|2009-05-21>|<cell|<long-id-spc|39BE
  2D72 5CEE 3195>>|<cell|Daniel Baumann>|<cell|2011\U2015
  <note-ref|+2S8OYb331U74E4Rd>>|<cell|>>|<row|<cell|2009-10-03>|<cell|<long-id-spc|9880
  21A9 64E6 EA7D>>|<cell|Debian CD signing key>|<cell|2011\U2021
  <note-ref|+2S8OYb331U74E4Rd>>|<cell|>>|<row|<cell|2011-01-05>|<cell|<long-id-spc|DA87
  E80D 6294 BE9B>>|<cell|Debian CD signing key>|<cell|2011\U2021
  <note-ref|+2S8OYb331U74E4Rd>>|<cell|<note-ref|+2S8OYb331U74E4Ri>>>|<row|<cell|2011-03-09>|<cell|<long-id-spc|6F95
  B499 6CA7 B5A6>>|<cell|Debian Live Signing Key>|<cell|2012\U2015
  <note-ref|+2S8OYb331U74E4Re>>|<cell|>>|<row|<cell|2013-05-06>|<cell|<long-id-spc|510A
  D6B9 AD11 CF6A>>|<cell|Debian Live Signing Key>|<cell|2013\U2015
  <note-ref|+2S8OYb331U74E4Rf>>|<cell|>>|<row|<cell|2014-01-03>|<cell|<long-id-spc|1239
  00F2 A9B2 6DF5>>|<cell|Live Systems Project>|<cell|2014\U2015
  <note-ref|+2S8OYb331U74E4Rg>>|<cell|>>|<row|<cell|2014-04-15>|<cell|<long-id-spc|4246
  8F40 09EA 8AC3>>|<cell|Debian Testing CDs Automatic Signing
  Key>|<cell|2014\U2022 <note-ref|+2S8OYb331U74E4Rh>>|<cell|>>>>>>
    <label|c2 tab debian cd keys>A list of keys used to sign <name|Debian>
    installation images. Keys identified from <name|Internet Archive>
    snapshots of \ <hlinkv|https://debian.org/CD/verify>.

    <note-inline|See <hlinkv|https://web.archive.org/web/20110413065857/http://www.debian.org/CD/verify>.|+2S8OYb331U74E4Rd>

    <note-inline|See <hlinkv|https://web.archive.org/web/20120815030316/http://www.debian.org:80/CD/verify>.|+2S8OYb331U74E4Re>

    <note-inline|See <hlinkv|https://web.archive.org/web/20130813130619/http://www.debian.org/CD/verify>.|+2S8OYb331U74E4Rf>

    <\note-inline>
      See <hlinkv|https://web.archive.org/web/20140410065231/http://www.debian.org/CD/verify>.
    </note-inline|+2S8OYb331U74E4Rg>

    <note-inline|See <hlinkv|https://web.archive.org/web/20140528012106/https://www.debian.org/CD/verify>.|+2S8OYb331U74E4Rh>

    <note-inline|Public key available at <hlinkv|https://web.archive.org/web/20210928205206/https://www.einval.com/~steve/pgp/>.|+2S8OYb331U74E4Ri>

    <note-inline|Public key available at <hlinkv|https://web.archive.org/web/20210928220426/https://reboil.com/res/2021/txt/20210928_72FDC205F6A32A8E..debian_manty.asc>.|+2S8OYb331U74E4Rj>

    \;
  </big-table>

  <subsubsection|Verbose key details><label|c2 debian-pk-verbose>

  <paragraph|Key 1999-01-30 (<long-id-spc|7C3B 7970 88C7 C1F7>)>

  A 1024-bit DSA key that is the earliest dated key for signing Debian CDs
  mentioned at <hlinkv|https://debian.org/CD/verify> according to the
  <name|Internet Archive> <\footnote>
    See <hlink|<verbatim|https://web.archive.org/web/20110413065857/http://www.debian.org/CD/verify>|https://web.archive.org/web/20110413065857/http://www.debian.org/CD/verify>.
  </footnote>. Mention of this key was removed from that page by the end of
  2015. A copy of this key can be found at the personal website of
  <subindex|People|McIntyre, Steve><name|Steve McIntyre>, a debian
  developer.<\footnote>
    Key <long-id-spc|7C3B 7970 88C7 C1F7> is available at
    <hlinkv|https://web.archive.org/web/20210928205229/https://www.einval.com/~steve/pgp/7C3B797088C7C1F7.asc>.
  </footnote>

  <\vpk>
    <\code>
      pub \ \ dsa1024/0x7C3B797088C7C1F7 1999-01-30 [SCA] [revoked:
      2017-01-11]

      \ \ \ \ \ \ Key fingerprint = AC65 6D79 E362 32CF 77BB \ B0E8 7C3B 7970
      88C7 C1F7

      uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ revoked] Steve McIntyre
      \<less\>313467b6\<gtr\>

      uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ revoked] Steve McIntyre
      \<less\>8b5d9233\<gtr\>

      uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ revoked] Steve McIntyre
      \<less\>032c14f6\<gtr\>

      uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ revoked] Debian CD signing
      key \<less\>047be9f4\<gtr\>
    </code>
  </vpk>

  <paragraph|Key 2000-09-16 (<long-id-spc|72FD C205 F6A3 2A8E>)>

  A 1024-bit DSA key listed as being a signing key for Debian CD images as of
  2011 at <hlinkv|https://debian.org/CD/verify>according to the
  <name|Internet Archive>. Mention of this key was removed from that page by
  the end of 2015. A copy of this key was archived from the
  <verbatim|pgp.mit.edu> keyserver.<\footnote>
    Key <long-id-spc|72FD C205 F6A3 2A8E> is available at
    <hlinkv|https://web.archive.org/web/20210928220426/https://reboil.com/res/2021/txt/20210928_72FDC205F6A32A8E..debian_manty.asc>.
  </footnote> This <verbatim|1024-bit DSA> key was deprecated in favor of a
  <verbatim|4096-bit RSA> key with fingerprint <long-id-spc|B868 8CA3 D876
  D5A3> in a signed blog post at <verbatim|blog.manty.net> .<\footnote>
    Key transition statement available at
    \ <hlinkv|https://web.archive.org/web/20150614033612/http://blog.manty.net/2014/12/transitioning-from-0xf6a32a8e-to.html>.
    To verify, use <code*|gpg \U-import> command on text copied from between
    the <code*|\<less\>listing\<gtr\>> tags. A copy of this text is also
    archived at <hlinkv|https://web.archive.org/web/20210928222521/https://reboil.com/res/2021/txt/20210928..72FDC205F6A32A8E_to_B8688CA3D876D5A3_pgp_transition_statement.txt>.
  </footnote>

  <\vpk>
    <\code>
      pub \ \ dsa1024/0x72FDC205F6A32A8E 2000-09-16 [SCA]

      \ \ \ \ \ \ Key fingerprint = 3F0A 12FC 0B55 A917 D791 \ 82D3 72FD C205
      F6A3 2A8E

      uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] Santiago Garcia
      Mantinan (manty) \<less\>65783682\<gtr\>

      uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] Santiago Garcia
      Mantinan (manty) \<less\>ad3a4b28\<gtr\>

      uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] Santiago Garcia
      Mantinan (manty) \<less\>99df45ac\<gtr\>

      sub \ \ elg1024/0x8F802C268D0EB704 2000-09-16 [E]

      \ \ \ \ \ \ Key fingerprint = 0481 1B16 A1BC E82B E985 \ 26B7 8F80 2C26
      8D0E B704
    </code>
  </vpk>

  <paragraph|Key 2004-06-20 (<long-id-spc|F82E 5CC0 4B2B 2B9E>)>

  A key listed as being a signing key for Debian CD images as of 2011 at
  <hlinkv|https://debian.org/CD/verify> according to the <name|Internet
  Archive>. Mention of this key was removed from that page by the end of
  2015.

  <\vpk>
    <\code>
      pub \ \ dsa1024/0xF82E5CC04B2B2B9E 2004-06-20 [SC] [expired:
      2015-01-01]

      \ \ \ \ \ \ Key fingerprint = 709F 54E4 ECF3 1956 2332 \ 6AE3 F82E 5CC0
      4B2B 2B9E

      uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ expired] Daniel Baumann
      \<less\>09d45987\<gtr\>
    </code>
  </vpk>

  <paragraph|Key 2009-05-21 (<long-id-spc|39BE 2D72 5CEE 3195>)>

  A key listed as being a signing key for Debian CD images as of 2011 at
  <hlinkv|https://debian.org/CD/verify> according to the <name|Internet
  Archive>.

  <\vpk>
    <\code>
      pub \ \ rsa4096/0x39BE2D725CEE3195 2009-05-21 [SC]

      \ \ \ \ \ \ Key fingerprint = D2FB 633A DDC2 0485 CBCE \ 6D12 39BE 2D72
      5CEE 3195

      uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] Daniel Baumann
      \<less\>58c7ad3d\<gtr\>

      uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] Daniel Baumann
      \<less\>1d8e385c\<gtr\>

      uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] Daniel Baumann
      \<less\>612d81a0\<gtr\>

      uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] Daniel Baumann
      \<less\>2260b338\<gtr\>

      uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] Daniel Baumann
      \<less\>8f3d581a\<gtr\>

      uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] Daniel Baumann
      \<less\>24ba8964\<gtr\>

      uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] Daniel Baumann
      \<less\>09d45987\<gtr\>

      uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] Daniel Baumann
      \<less\>5923ca47\<gtr\>

      sub \ \ rsa4096/0x2E86B0C2E7D77F65 2009-05-21 [E]

      \ \ \ \ \ \ Key fingerprint = 205A 272D 2838 238C 3058 \ C278 2E86 B0C2
      E7D7 7F65
    </code>
  </vpk>

  <paragraph|Key 2009-10-03 (<long-id-spc|9880 21A9 64E6 EA7D>)>

  A key listed as being a signing key for Debian CD images as of 2011 at
  <hlinkv|https://debian.org/CD/verify> according to the <name|Internet
  Archive>.

  <\vpk>
    <\code>
      pub \ \ rsa4096/0x988021A964E6EA7D 2009-10-03 [SC]

      \ \ \ \ \ \ Key fingerprint = 1046 0DAD 7616 5AD8 1FBC \ 0CE9 9880 21A9
      64E6 EA7D

      uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] Debian CD signing
      key \<less\>047be9f4\<gtr\>
    </code>
  </vpk>

  <paragraph|Key 2011-01-05 (<long-id-spc|DA87 E80D 6294 BE9B>)>

  A key listed as being a signing key for Debian CD images as of 2011 at
  <hlinkv|https://debian.org/CD/verify> according to the <name|Internet
  Archive>.

  <\vpk>
    <\code>
      pub \ \ rsa4096/0xDA87E80D6294BE9B 2011-01-05 [SC]

      \ \ \ \ \ \ Key fingerprint = DF9B 9C49 EAA9 2984 3258 \ 9D76 DA87 E80D
      6294 BE9B

      uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] Debian CD signing
      key \<less\>047be9f4\<gtr\>

      sub \ \ rsa4096/0x642A5AC311CD9819 2011-01-05 [E]

      \ \ \ \ \ \ Key fingerprint = 47A8 EA16 451B F5C9 B691 \ 5C64 642A 5AC3
      11CD 9819
    </code>
  </vpk>

  <paragraph|Key 2011-03-09 (<long-id-spc|6F95 B499 6CA7 B5A6>)>

  This key was mentioned at <hlinkv|https://debian.org/CD/verify> at the end
  of 2012, according to the <name|Internet Archive>.

  <\vpk>
    <\code>
      pub \ \ rsa4096/0x6F95B4996CA7B5A6 2011-03-09 [SC] [expired:
      2021-02-01]

      \ \ \ \ \ \ Key fingerprint = 696F 95F0 88E4 D359 947F \ 7AEB 6F95 B499
      6CA7 B5A6

      uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ expired] Debian Live Signing
      Key \<less\>4719e7fe\<gtr\>
    </code>
  </vpk>

  <paragraph|Key 2013-05-06 (<long-id-spc|510A D6B9 AD11 CF6A>)>

  This key was mentioned at <hlinkv|https://debian.org/CD/verify> at the end
  of 2013, according to the <name|Internet Archive>.

  <\vpk>
    <\code>
      pub \ \ rsa4096/0x510AD6B9AD11CF6A 2013-05-06 [SC]

      \ \ \ \ \ \ Key fingerprint = 1E4F 435C 4E9A 42B3 D9DF \ BE3A 510A D6B9
      AD11 CF6A

      uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] Debian Live Signing
      Key (2013) \<less\>4719e7fe\<gtr\>

      sub \ \ rsa4096/0x4E534D59B72E3E00 2013-05-06 [E]

      \ \ \ \ \ \ Key fingerprint = 407B 17AD 8CD1 B9D3 6891 \ 262B 4E53 4D59
      B72E 3E00

      \;
    </code>
  </vpk>

  \;

  <paragraph|Key 2014-01-03 (<long-id-spc|1239 00F2 A9B2 6DF5>)>

  This key was mentioned at <hlinkv|https://debian.org/CD/verify> at the end
  of 2014, according to the <name|Internet Archive>.

  <\vpk>
    pub \ \ rsa4096/0x123900F2A9B26DF5 2014-01-03 [SC] [expires: 2025-01-01]

    \ \ \ \ \ \ Key fingerprint = 8A36 A2E8 91A5 C2A9 0DEB \ 7A8B 1239 00F2
    A9B2 6DF5

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] Live Systems Project
    \<less\>4719e7fe\<gtr\>

    sub \ \ rsa4096/0xA1A89023D0125917 2014-01-03 [E] [expires: 2025-01-01]

    \ \ \ \ \ \ Key fingerprint = A9E3 8E70 E798 7E03 285E \ D9C2 A1A8 9023
    D012 5917
  </vpk>

  <paragraph|Key 2014-04-15 (<long-id-spc|4246 8F40 09EA 8AC3>)>

  This key was mentioned at <hlinkv|https://debian.org/CD/verify> at the end
  of 2014, according to the <name|Internet Archive>.

  <\vpk>
    <\code>
      <with|hmagnified-factor|0.8|<\hmagnified>
        pub \ \ rsa4096/0x42468F4009EA8AC3 2014-04-15 [SC]

        \ \ \ \ \ \ Key fingerprint = F41D 3034 2F35 4669 5F65 \ C669 4246
        8F40 09EA 8AC3

        uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] Debian Testing CDs
        Automatic Signing Key \<less\>047be9f4\<gtr\>

        sub \ \ rsa4096/0x0C5470136BD05CFB 2014-04-15 [E]

        \ \ \ \ \ \ Key fingerprint = AEE9 9CA7 0C3E C4B3 1C75 \ 2843 0C54
        7013 6BD0 5CFB
      </hmagnified>>
    </code>
  </vpk>

  <index-complex|<tuple|Software|Debian>|strong|c2 debian
  idx1|<tuple|Software|Debian>>
</body>

<\initial>
  <\collection>
    <associate|chapter-nr|2>
    <associate|page-first|18>
    <associate|page-medium|paper>
    <associate|preamble|false>
    <associate|section-nr|2>
    <associate|subsection-nr|3>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|3|18>>
    <associate|auto-10|<tuple|3.10|19>>
    <associate|auto-11|<tuple|3.3.2|19>>
    <associate|auto-12|<tuple|3.3.2.1|19>>
    <associate|auto-13|<tuple|People|19>>
    <associate|auto-14|<tuple|3.3.2.2|19>>
    <associate|auto-15|<tuple|3.3.2.3|20>>
    <associate|auto-16|<tuple|3.3.2.4|20>>
    <associate|auto-17|<tuple|3.3.2.5|20>>
    <associate|auto-18|<tuple|3.3.2.6|20>>
    <associate|auto-19|<tuple|3.3.2.7|20>>
    <associate|auto-2|<tuple|3.1|18>>
    <associate|auto-20|<tuple|3.3.2.8|20>>
    <associate|auto-21|<tuple|3.3.2.9|21>>
    <associate|auto-22|<tuple|3.3.2.10|21>>
    <associate|auto-23|<tuple|<tuple|Software|Debian>|21>>
    <associate|auto-3|<tuple|<tuple|Software|Debian>|18>>
    <associate|auto-4|<tuple|People|18>>
    <associate|auto-5|<tuple|3.2|18>>
    <associate|auto-6|<tuple|People|18>>
    <associate|auto-7|<tuple|People|18>>
    <associate|auto-8|<tuple|3.3|18>>
    <associate|auto-9|<tuple|3.3.1|18>>
    <associate|c2 debian|<tuple|3|18>>
    <associate|c2 debian-bg|<tuple|3.1|18>>
    <associate|c2 debian-pk|<tuple|3.3|18>>
    <associate|c2 debian-pk-verbose|<tuple|3.3.2|19>>
    <associate|c2 tab debian cd keys|<tuple|3.1|19>>
    <associate|footnote-3.1|<tuple|3.1|18>>
    <associate|footnote-3.10|<tuple|3.10|19>>
    <associate|footnote-3.11|<tuple|3.11|19>>
    <associate|footnote-3.12|<tuple|3.12|19>>
    <associate|footnote-3.13|<tuple|3.13|19>>
    <associate|footnote-3.14|<tuple|3.14|19>>
    <associate|footnote-3.2|<tuple|3.2|18>>
    <associate|footnote-3.3|<tuple|3.3|18>>
    <associate|footnote-3.4|<tuple|3.4|19>>
    <associate|footnote-3.5|<tuple|3.5|19>>
    <associate|footnote-3.6|<tuple|3.6|19>>
    <associate|footnote-3.7|<tuple|3.7|19>>
    <associate|footnote-3.8|<tuple|3.8|19>>
    <associate|footnote-3.9|<tuple|3.9|19>>
    <associate|footnr-3.1|<tuple|3.1|18>>
    <associate|footnr-3.10|<tuple|3.10|19>>
    <associate|footnr-3.11|<tuple|3.11|19>>
    <associate|footnr-3.12|<tuple|3.12|19>>
    <associate|footnr-3.13|<tuple|3.13|19>>
    <associate|footnr-3.14|<tuple|3.14|19>>
    <associate|footnr-3.2|<tuple|3.2|18>>
    <associate|footnr-3.3|<tuple|3.3|18>>
    <associate|footnr-3.4|<tuple|3.6|19>>
    <associate|footnr-3.5|<tuple|3.6|19>>
    <associate|footnr-3.6|<tuple|3.6|19>>
    <associate|footnr-3.7|<tuple|3.7|19>>
    <associate|footnr-3.8|<tuple|3.8|19>>
    <associate|footnr-3.9|<tuple|3.9|19>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|idx>
      <tuple|<tuple|Software|Debian>|strong|c2 debian
      idx1|<tuple|Software|Debian>|<pageref|auto-3>>

      <tuple|<tuple|People|Murdock, Ian Ashley>|<pageref|auto-4>>

      <tuple|<tuple|People|Mantinan, Santiago Garcia>|<pageref|auto-6>>

      <tuple|<tuple|People|Baumann, Daniel>|<pageref|auto-7>>

      <tuple|<tuple|People|McIntyre, Steve>|<pageref|auto-13>>

      <tuple|<tuple|Software|Debian>|strong|c2 debian
      idx1|<tuple|Software|Debian>|<pageref|auto-23>>
    </associate>
    <\associate|table>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|3.1>|>
        A list of keys used to sign <with|font-shape|<quote|small-caps>|Debian>
        installation images. Keys identified from
        <with|font-shape|<quote|small-caps>|Internet Archive> snapshots of
        \ <locus|<id|%E67CA28-79F4838>|<link|hyperlink|<id|%E67CA28-79F4838>|<url|https://debian.org/CD/verify>>|<with|font-family|<quote|tt>|language|<quote|verbatim>|<with|font-effects|<quote|hextended=0.8>|https://debian.org/CD/verify>>
        >.

        <surround|||<with|font-size|<quote|0.771>|<surround|<locus|<id|%E67CA28-79F4708>|<link|hyperlink|<id|%E67CA28-79F4708>|<url|#footnr-3.4>>|3.4>.
        |<hidden-binding|<tuple|footnote-3.4>|3.4>|See
        <locus|<id|%E67CA28-79F5048>|<link|hyperlink|<id|%E67CA28-79F5048>|<url|https://web.archive.org/web/20110413065857/http://www.debian.org/CD/verify>>|<with|font-family|<quote|tt>|language|<quote|verbatim>|<with|font-effects|<quote|hextended=0.8>|https://web.archive.org/web/20110413065857/http://www.debian.org/CD/verify>>
        >.>>>

        <surround|||<with|font-size|<quote|0.771>|<surround|<locus|<id|%E67CA28-79F52D8>|<link|hyperlink|<id|%E67CA28-79F52D8>|<url|#footnr-3.7>>|3.7>.
        |<hidden-binding|<tuple|footnote-3.7>|3.7>|See
        <locus|<id|%E67CA28-79F5158>|<link|hyperlink|<id|%E67CA28-79F5158>|<url|https://web.archive.org/web/20120815030316/http://www.debian.org:80/CD/verify>>|<with|font-family|<quote|tt>|language|<quote|verbatim>|<with|font-effects|<quote|hextended=0.8>|https://web.archive.org/web/20120815030316/http://www.debian.org:80/CD/verify>>
        >.>>>

        <surround|||<with|font-size|<quote|0.771>|<surround|<locus|<id|%E67CA28-79F5010>|<link|hyperlink|<id|%E67CA28-79F5010>|<url|#footnr-3.8>>|3.8>.
        |<hidden-binding|<tuple|footnote-3.8>|3.8>|See
        <locus|<id|%E67CA28-79F4D40>|<link|hyperlink|<id|%E67CA28-79F4D40>|<url|https://web.archive.org/web/20130813130619/http://www.debian.org/CD/verify>>|<with|font-family|<quote|tt>|language|<quote|verbatim>|<with|font-effects|<quote|hextended=0.8>|https://web.archive.org/web/20130813130619/http://www.debian.org/CD/verify>>
        >.>>>

        <surround|||<with|font-size|<quote|0.771>|<\surround|<locus|<id|%E67CA28-79F4EF8>|<link|hyperlink|<id|%E67CA28-79F4EF8>|<url|#footnr-3.9>>|3.9>.
        |<hidden-binding|<tuple|footnote-3.9>|3.9>>
          See <locus|<id|%E67CA28-79F57C8>|<link|hyperlink|<id|%E67CA28-79F57C8>|<url|https://web.archive.org/web/20140410065231/http://www.debian.org/CD/verify>>|<with|font-family|<quote|tt>|language|<quote|verbatim>|<with|font-effects|<quote|hextended=0.8>|https://web.archive.org/web/20140410065231/http://www.debian.org/CD/verify>>
          >.
        </surround>>>

        <surround|||<with|font-size|<quote|0.771>|<surround|<locus|<id|%E67CA28-79F55E0>|<link|hyperlink|<id|%E67CA28-79F55E0>|<url|#footnr-3.10>>|3.10>.
        |<hidden-binding|<tuple|footnote-3.10>|3.10>|See
        <locus|<id|%E67CA28-79F5978>|<link|hyperlink|<id|%E67CA28-79F5978>|<url|https://web.archive.org/web/20140528012106/https://www.debian.org/CD/verify>>|<with|font-family|<quote|tt>|language|<quote|verbatim>|<with|font-effects|<quote|hextended=0.8>|https://web.archive.org/web/20140528012106/https://www.debian.org/CD/verify>>
        >.>>>

        <surround|||<with|font-size|<quote|0.771>|<surround|<locus|<id|%E67CA28-79F5810>|<link|hyperlink|<id|%E67CA28-79F5810>|<url|#footnr-3.5>>|3.5>.
        |<hidden-binding|<tuple|footnote-3.5>|3.5>|Public key available at
        <locus|<id|%E67CA28-79F5490>|<link|hyperlink|<id|%E67CA28-79F5490>|<url|https://web.archive.org/web/20210928205206/https://www.einval.com/~steve/pgp/>>|<with|font-family|<quote|tt>|language|<quote|verbatim>|<with|font-effects|<quote|hextended=0.8>|https://web.archive.org/web/20210928205206/https://www.einval.com/~steve/pgp/>>
        >.>>>

        <surround|||<with|font-size|<quote|0.771>|<surround|<locus|<id|%E67CA28-79F5628>|<link|hyperlink|<id|%E67CA28-79F5628>|<url|#footnr-3.6>>|3.6>.
        |<hidden-binding|<tuple|footnote-3.6>|3.6>|Public key available at
        <locus|<id|%E67CA28-79F5700>|<link|hyperlink|<id|%E67CA28-79F5700>|<url|https://web.archive.org/web/20210928220426/https://reboil.com/res/2021/txt/20210928_72FDC205F6A32A8E..debian_manty.asc>>|<with|font-family|<quote|tt>|language|<quote|verbatim>|<with|font-effects|<quote|hextended=0.8>|https://web.archive.org/web/20210928220426/https://reboil.com/res/2021/txt/20210928_72FDC205F6A32A8E..debian_manty.asc>>
        >.>>>

        \;
      </surround>|<pageref|auto-10>>
    </associate>
    <\associate|toc>
      3<space|2spc><with|font-shape|<quote|small-caps>|Debian>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1>

      <with|par-left|<quote|1tab>|3.1<space|2spc>Background
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2>>

      <with|par-left|<quote|1tab>|3.2<space|2spc>History
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-5>>

      <with|par-left|<quote|1tab>|3.3<space|2spc>Public Key Details
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-8>>

      <with|par-left|<quote|2tab>|3.3.1<space|2spc>Installation Image
      Signature Keys <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-9>>

      <with|par-left|<quote|2tab>|3.3.2<space|2spc>Verbose key details
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-11>>

      <with|par-left|<quote|4tab>|Key 1999-01-30
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|7C3B
      7970 88C7 C1F7>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-12><vspace|0.15fn>>

      <with|par-left|<quote|4tab>|Key 2000-09-16
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|72FD
      C205 F6A3 2A8E>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-14><vspace|0.15fn>>

      <with|par-left|<quote|4tab>|Key 2004-06-20
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|F82E
      5CC0 4B2B 2B9E>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-15><vspace|0.15fn>>

      <with|par-left|<quote|4tab>|Key 2009-05-21
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|39BE
      2D72 5CEE 3195>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-16><vspace|0.15fn>>

      <with|par-left|<quote|4tab>|Key 2009-10-03
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|9880
      21A9 64E6 EA7D>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-17><vspace|0.15fn>>

      <with|par-left|<quote|4tab>|Key 2011-01-05
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|DA87
      E80D 6294 BE9B>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-18><vspace|0.15fn>>

      <with|par-left|<quote|4tab>|Key 2011-03-09
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|6F95
      B499 6CA7 B5A6>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-19><vspace|0.15fn>>

      <with|par-left|<quote|4tab>|Key 2013-05-06
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|510A
      D6B9 AD11 CF6A>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-20><vspace|0.15fn>>

      <with|par-left|<quote|4tab>|Key 2014-01-03
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|1239
      00F2 A9B2 6DF5>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-21><vspace|0.15fn>>

      <with|par-left|<quote|4tab>|Key 2014-04-15
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|4246
      8F40 09EA 8AC3>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-22><vspace|0.15fn>>
    </associate>
  </collection>
</auxiliary>