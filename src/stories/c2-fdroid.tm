<TeXmacs|2.1.1>

<project|../book.tm>

<style|<tuple|book|style-bk>>

<\body>
  <new-page*><section|<name|F-Droid>><label|c2 fdroid>

  Last updated on <date|2023-06-15> by <name|Steven Baltakatei Sandoval>.

  <subsection|Background><label|c2 fdroid-bg>

  <index-complex|<tuple|Software|F-Droid>|strong|c2 fdroid
  idx1|<tuple|Software|F-Droid>><name|F-Droid<\footnote>
    Main website: <hlinkv|https://f-droid.org>.
  </footnote>> is an <name|Android> app and repository that differentiates
  itself from the <subindex|Organizations|Google Play><name|Google Play>
  repository and app by providing only apps with free and open-source
  software (FOSS) licenses. The <name|F-Droid> client permits searching and
  downloading of FOSS apps in the <name|F-Droid> repository as well as
  downloading and verifying updates for these apps.

  The <name|F-Droid> project was founded by <subindex|People|Gultnieks,
  Ciaran><name|Ciaran Gultnieks> in 2010. As of 2023, various
  individuals<\footnote>
    See <hlinkv|https://web.archive.org/web/20220405054709/https://f-droid.org/en/about/>.
  </footnote> maintain the client but the <name|Git> repository tags are
  digitally signed using the <name|OpenPGP> keys of <subindex|People|Steiner,
  Hans-Christoph><name|Hans-Christoph Steiner> (<long-id-spc|E9E2 8DEA 00AA
  5556>) and <subindex|People|Grote, Torsten><person|Torsten Grote>
  (<long-id-spc|74DC A8A3 6C52 F833>).

  <subsection|History><label|c2 fdroid-hist>

  <\description>
    <item*|2010-10-25>Earliest <name|I.A.> snapshot of
    <hlinkv|https://f-droid.org>.

    <item*|2014-04-25>Creation date of the <name|Android> client binary
    release signing key (<long-id-spc|><long-id-spc|41E7 044E 1DBA 2E89>).

    <item*|2014-03-30>Creation date of <person|Torston Grote>'s
    <name|Android> client <name|Git> repository signing key
    (<long-id-spc|74DC A8A3 6C52 F833>).

    <item*|2015-10-31>Creation date of <person|Hans-Christoph Steiner>'s
    <name|Android> client <name|Git> repository signing key
    (<long-id-spc|E9E2 8DEA 00AA 5556>).

    <item*|2017-10-17>Earliest snapshot of <hlinkv|https://f-droid.org>that
    links to a PGP signature for the client APK.

    <item*|2017-12-20>Early I.A. mention of the <long-id-spc|41E7 044E 1DBA
    2E89> and <long-id-spc|E9E2 8DEA 00AA 5556> signing keys.<\footnote>
      See <hlinkv|https://web.archive.org/web/20171220224805/https://f-droid.org/en/docs/Release_Channels_and_Signing_Keys/>.
    </footnote>
  </description>

  <subsection|Public Key Details><label|c2 fdroid-pk>

  <subsubsection|<name|Android> client binary release signing key (2017\U )
  (<long-id-spc|41E7 044E 1DBA 2E89>)><label|c2 fdroid-pk-apk-pgp>

  <index-complex|<tuple|Keys|Organizations|F-Droid|Signing|0x41E7044E1DBA2E89
  (2014\U)>|||<tuple|Keys|Organizations|F-Droid|Signing|<verb-sm|0x41E7044E1DBA2E89>
  (2014\U)>><name|OpenPGP> key used to sign official binary release of the
  <name|F-Droid> Android client since at least as far back as
  2017.<\footnote>
    See <hlinkv|https://web.archive.org/web/20230611155440/https://f-droid.org/F-Droid.apk.asc>and
    <hlinkv|https://web.archive.org/web/20230611155429/https://f-droid.org/F-Droid.apk>.
  </footnote> <rsup|<reference|fdroid_20230614_signingkeys>>

  <\vpk>
    pub \ \ rsa4096/0x41E7044E1DBA2E89 2014-04-25 [C]

    \ \ \ \ \ \ Key fingerprint = 37D2 C987 89D8 3119 4839 \ 4E3E 41E7 044E
    1DBA 2E89

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] F-Droid
    \<less\>25ef2ab4\<gtr\>

    sub \ \ rsa3072/0x7A029E54DD5DCE7A 2014-04-25 [S] [expires: 2026-04-25]

    \ \ \ \ \ \ Key fingerprint = 802A 9799 0161 1234 6E1F \ EFF4 7A02 9E54
    DD5D CE7A

    sub \ \ rsa3072/0x5DCCB667F9BF9046 2014-04-25 [E] [expires: 2026-04-25]

    \ \ \ \ \ \ Key fingerprint = 2622 41A6 D092 3980 0242 \ 8AE6 5DCC B667
    F9BF 9046
  </vpk>

  <subsubsection|<name|Android> client <name|Git> repository signing key
  (2017\U ) (<long-id-spc|74DC A8A3 6C52 F833>)><label|c2 fdroid-pk-git>

  <index-complex|<tuple|Keys|People|Grote,
  Torsten|0x74DCA8A36C52F833>|||<tuple|Keys|People|Grote,
  Torsten|<verb-sm|0x74DCA8A36C52F833>>><name|OpenPGP >key used to sign
  <name|Git> tags of the <name|F-Droid> client source code repository since
  2017. The key is owned by by <name|Torsten Grote>.<\footnote>
    See <hlinkv|https://gitlab.com/fdroid/fdroidclient>.
  </footnote> <rsup|<reference|fdroid_20230614_signingkeys>>

  <\vpk>
    pub \ \ rsa4096/0x74DCA8A36C52F833 2014-03-30 [SC]

    \ \ \ \ \ \ Key fingerprint = 7251 9DA2 D8BD 6F38 D4A1 \ 199E 74DC A8A3
    6C52 F833

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] Torsten Grote
    \<less\>aff70df0\<gtr\>

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] Torsten Grote (Free
    Software Foundation Europe) \<less\>20854ca0\<gtr\>

    sub \ \ rsa4096/0x3E5F77D92CF891FF 2014-03-30 [S]

    \ \ \ \ \ \ Key fingerprint = 366E C348 5895 4F62 FFAD \ 095A 3E5F 77D9
    2CF8 91FF

    sub \ \ rsa4096/0x74FB2FB013112EAE 2014-03-30 [E]

    \ \ \ \ \ \ Key fingerprint = 1D87 2324 B15D A7DA 77E4 \ CAB1 74FB 2FB0
    1311 2EAE
  </vpk>

  <subsubsection|<name|Android> client <name|Git> repository signing key
  (2015\U ) (<long-id-spc|E9E2 8DEA 00AA 5556>)><label|c2 fdroid-pk-git>

  <index-complex|<tuple|Keys|People|Steiner,
  Hans-Christoph|0xE9E28DEA00AA5556>|||<tuple|Keys|People|Steiner,
  Hans-Christoph|<verb-sm|0xE9E28DEA00AA5556>>><name|OpenPGP >key used to
  sign <name|Git> tags of the <name|F-Droid> client source code repository
  since 2015. The key is owned by by <name|Hans-Christoph
  Steiner>.<\footnote>
    See <hlinkv|https://gitlab.com/fdroid/fdroidclient>.
  </footnote>

  <\vpk>
    pub \ \ rsa4096/0xE9E28DEA00AA5556 2015-10-31 [C]

    \ \ \ \ \ \ Key fingerprint = EE66 20C7 136B 0D2C 456C \ 0A4D E9E2 8DEA
    00AA 5556

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] Hans-Christoph Steiner
    \<less\>90782c16\<gtr\>

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] Hans-Christoph Steiner
    \<less\>e4e6522f\<gtr\>

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] Hans-Christoph Steiner
    \<less\>a055eb1b\<gtr\>

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] [jpeg image of size
    5375]

    sub \ \ rsa2048/0x044051F83354A28B 2015-10-31 [E] [expires: 2027-06-09]

    \ \ \ \ \ \ Key fingerprint = F9CC 6FE0 12C9 26C7 37F3 \ 74FC 0440 51F8
    3354 A28B

    sub \ \ rsa2048/0x3E177817BA1B9BFA 2015-10-31 [S] [expires: 2027-06-09]

    \ \ \ \ \ \ Key fingerprint = 9722 39DB E686 99F5 26C0 \ 6A05 3E17 7817
    BA1B 9BFA

    sub \ \ rsa2048/0x4FE01854A428189E 2015-10-31 [A]

    \ \ \ \ \ \ Key fingerprint = E70A D210 1083 51FB 03F5 \ D4FB 4FE0 1854
    A428 189E
  </vpk>

  <subsubsection|<name|Android> client APK signing key (2010\U2023)><label|c2
  fdroid-pk-apk>

  A public key used to sign the APK file used to install the <name|Android>
  client.<\footnote>
    <label|fdroid_20230614_signingkeys>See
    <hlinkv|https://f-droid.org/en/docs/Release_Channels_and_Signing_Keys/>.
  </footnote>

  <condensed|<\vpk>
    <code|<\code*>
      Owner: CN=Ciaran Gultnieks, OU=Unknown, O=Unknown, L=Wetherby,
      ST=Unknown, C=UK

      Issuer: CN=Ciaran Gultnieks, OU=Unknown, O=Unknown, L=Wetherby,
      ST=Unknown, C=UK

      Serial number: 4c49cd00

      Valid from: Fri Jul 23 13:10:24 EDT 2010 until: Tue Dec 08 12:10:24 EST
      2037

      Certificate fingerprints:

      \;

      \ \ MD5: \ 17:C5:5C:62:80:56:E1:93:E9:56:44:E9:89:79:27:86

      \ \ SHA1: 05:F2:E6:59:28:08:89:81:B3:17:FC:9A:6D:BF:E0:4B:0F:A1:3B:4E

      \ \ SHA256: 43:23:8D:51:2C:1E:5E:B2:D6:56:9F:4A:3A:FB:F5:52:34:18:B8:2E:0A:3E:D1:55:27:70:AB:B9:A9:C9:CC:AB
    </code*>>
  </vpk>>

  <index-complex|<tuple|Software|F-Droid>|strong|c2 fdroid
  idx1|<tuple|Software|F-Droid>>
</body>

<\initial>
  <\collection>
    <associate|chapter-nr|2>
    <associate|page-first|25>
    <associate|page-medium|papyrus>
    <associate|section-nr|5>
    <associate|subsection-nr|3>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|6|9>>
    <associate|auto-10|<tuple|6.3.1|9>>
    <associate|auto-11|<tuple|<tuple|Keys|Organizations|F-Droid|Signing|0x41E7044E1DBA2E89
    (2014\U)>|9>>
    <associate|auto-12|<tuple|6.3.2|9>>
    <associate|auto-13|<tuple|<tuple|Keys|People|Grote,
    Torsten|0x74DCA8A36C52F833>|10>>
    <associate|auto-14|<tuple|6.3.3|10>>
    <associate|auto-15|<tuple|<tuple|Keys|People|Steiner,
    Hans-Christoph|0xE9E28DEA00AA5556>|?>>
    <associate|auto-16|<tuple|6.3.4|?>>
    <associate|auto-17|<tuple|<tuple|Software|F-Droid>|?>>
    <associate|auto-2|<tuple|6.1|9>>
    <associate|auto-3|<tuple|<tuple|Software|F-Droid>|9>>
    <associate|auto-4|<tuple|Organizations|9>>
    <associate|auto-5|<tuple|People|9>>
    <associate|auto-6|<tuple|People|9>>
    <associate|auto-7|<tuple|People|9>>
    <associate|auto-8|<tuple|6.2|9>>
    <associate|auto-9|<tuple|6.3|9>>
    <associate|c2 fdroid|<tuple|6|9>>
    <associate|c2 fdroid-bg|<tuple|6.1|9>>
    <associate|c2 fdroid-hist|<tuple|6.2|9>>
    <associate|c2 fdroid-pk|<tuple|6.3|9>>
    <associate|c2 fdroid-pk-apk|<tuple|6.3.4|10>>
    <associate|c2 fdroid-pk-apk-pgp|<tuple|6.3.1|9>>
    <associate|c2 fdroid-pk-git|<tuple|6.3.3|9>>
    <associate|fdroid_20230614_signingkeys|<tuple|6.7|?>>
    <associate|footnote-6.1|<tuple|6.1|?>>
    <associate|footnote-6.2|<tuple|6.2|?>>
    <associate|footnote-6.3|<tuple|6.3|?>>
    <associate|footnote-6.4|<tuple|6.4|?>>
    <associate|footnote-6.5|<tuple|6.5|?>>
    <associate|footnote-6.6|<tuple|6.6|?>>
    <associate|footnote-6.7|<tuple|6.7|?>>
    <associate|footnr-6.1|<tuple|6.1|?>>
    <associate|footnr-6.2|<tuple|6.2|?>>
    <associate|footnr-6.3|<tuple|6.3|?>>
    <associate|footnr-6.4|<tuple|6.4|?>>
    <associate|footnr-6.5|<tuple|6.5|?>>
    <associate|footnr-6.6|<tuple|6.6|?>>
    <associate|footnr-6.7|<tuple|6.7|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|idx>
      <tuple|<tuple|Software|F-Droid>|strong|c2 fdroid
      idx1|<tuple|Software|F-Droid>|<pageref|auto-3>>

      <tuple|<tuple|Organizations|Google Play>|<pageref|auto-4>>

      <tuple|<tuple|People|Gultnieks, Ciaran>|<pageref|auto-5>>

      <tuple|<tuple|People|Steiner, Hans-Christoph>|<pageref|auto-6>>

      <tuple|<tuple|People|Grote, Torsten>|<pageref|auto-7>>

      <tuple|<tuple|Keys|Organizations|F-Droid|Signing|0x41E7044E1DBA2E89
      (2014\U)>|||<tuple|Keys|Organizations|F-Droid|Signing|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x41E7044E1DBA2E89>>
      (2014\U)>|<pageref|auto-11>>

      <tuple|<tuple|Keys|People|Grote, Torsten|0x74DCA8A36C52F833>|||<tuple|Keys|People|Grote,
      Torsten|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x74DCA8A36C52F833>>>|<pageref|auto-13>>

      <tuple|<tuple|Keys|People|Steiner, Hans-Christoph|0xE9E28DEA00AA5556>|||<tuple|Keys|People|Steiner,
      Hans-Christoph|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0xE9E28DEA00AA5556>>>|<pageref|auto-15>>

      <tuple|<tuple|Software|F-Droid>|strong|c2 fdroid
      idx1|<tuple|Software|F-Droid>|<pageref|auto-17>>
    </associate>
    <\associate|toc>
      6<space|2spc><with|font-shape|<quote|small-caps>|F-Droid>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1>

      <with|par-left|<quote|1tab>|6.1<space|2spc>Background
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2>>

      <with|par-left|<quote|1tab>|6.2<space|2spc>History
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-8>>

      <with|par-left|<quote|1tab>|6.3<space|2spc>Public Key Details
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-9>>

      <with|par-left|<quote|2tab>|6.3.1<space|2spc><with|font-shape|<quote|small-caps>|Android>
      client binary release signing key (2017\U2023)
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|41E7
      044E 1DBA 2E89>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-10>>

      <with|par-left|<quote|2tab>|6.3.2<space|2spc><with|font-shape|<quote|small-caps>|Android>
      client <with|font-shape|<quote|small-caps>|Git> repository signing key
      (2017\U2023) (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|74DC
      A8A3 6C52 F833>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-12>>

      <with|par-left|<quote|2tab>|6.3.3<space|2spc><with|font-shape|<quote|small-caps>|Android>
      client <with|font-shape|<quote|small-caps>|Git> repository signing key
      (2015\U2023) (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|E9E2
      8DEA 00AA 5556>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-14>>

      <with|par-left|<quote|2tab>|6.3.4<space|2spc><with|font-shape|<quote|small-caps>|Android>
      client APK signing key (2010\U2023)
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-16>>
    </associate>
  </collection>
</auxiliary>