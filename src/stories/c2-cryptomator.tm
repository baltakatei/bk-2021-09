<TeXmacs|2.1.1>

<project|../book.tm>

<style|<tuple|book|style-bk>>

<\body>
  <new-page*><section|<name|Cryptomator>><label|c2 cryptomator>

  Last updated on 2023-06-15 by <name|Steven Baltakatei Sandoval>.

  <subsection|Background><label|c2 cryptomator-bg>

  <index-complex|<tuple|Software|Cryptomator>|strong|c2 cryptomator
  idx1|<tuple|Software|Cryptomator>><name|Cryptomator><\footnote>
    Main website: <hlinkv|https://cryptomator.org/>.
  </footnote> is a cross-platform file storage privacy application. It
  permits storing files on a third-party file storage services (e.g.
  <subindex|Software|Dropbox><name|Dropbox>) in encrypted form and accessible
  to the user as a virtual mountable drive. In other words,
  <name|Cryptomator> acts as an encryption layer between a user and a file
  storage service. Compiled binary releases are available for <name|Windows>,
  <name|macOS>, <name|Linux>, <name|Android>, and <name|iOS><\footnote>
    See <hlinkv|<hlinkv|https://cryptomator.org/downloads/>>.
  </footnote>.

  As of 2021-12-22, the latest version of <name|Cryptomator> is version
  <em|1.6.5 (Hotfix)> available on <name|GitHub>.<\footnote>
    See <hlinkv|https://github.com/cryptomator/cryptomator/releases/tag/1.6.5>.
  </footnote> Judging from commit signatures of the <name|GitHub>
  repository<\footnote>
    See <hlinkv|https://github.com/cryptomator/cryptomator>.
  </footnote>, the main developers appear to be
  <index-complex|<tuple|Keys|People|Stenzel,
  Sebastian|0x667B866EA8240A09>|||<tuple|Keys|People|Stenzel,
  Sebastian|<verb-sm|0x667B866EA8240A09>>><name|Sebastian Stenzel>
  (<long-id-spc|667B 866E A824 0A09>) <index-complex|<tuple|Keys|People|Schrenk,
  Armin|0x748E55D51F5B3FBC>|||<tuple|Keys|People|Schrenk,
  Armin|<verb-sm|0x748E55D51F5B3FBC>>><name|Armin Schrenk> (<long-id-spc|748E
  55D5 1F5B 3FBC>), and <index-complex|<tuple|Keys|People|Hagemann,
  Tobias|0x69CEFAD519598989>|||<tuple|Keys|People|Hagemann,
  Tobias|<verb-sm|0x69CEFAD519598989>>><name|Tobias Hagemann>
  (<long-id-spc|69CE FAD5 1959 8989>).<\footnote>
    See <hlinkv|https://github.com/orgs/cryptomator/people>.
  </footnote>

  <subsection|History><label|c2 cryptomator-hist>

  <\description>
    <item*|2015-01-01>First snapshot of <hlinkv|https://cryptomator.org>captured
    on the <name|Internet Archive>.<\footnote>
      See <hlinkv|https://web.archive.org/web/20150101033915/http://cryptomator.org/>.
    </footnote> Signature of latest version of <name|Cryptomator>
    (<em|1.4.11>) uses PGP key <long-id-spc|509C 9D63 34C8 0F11>.<\footnote>
      Signature file at: <hlinkv|https://web.archive.org/web/20210502041159/https://dl.bintray.com/cryptomator/cryptomator/1.4.11/cryptomator-1.4.11-x86_64.AppImage.asc>.
      Signed file at: <hlinkv|https://web.archive.org/web/20210502115653/https://dl.bintray.com/cryptomator/cryptomator/1.4.11/cryptomator-1.4.11-x86_64.AppImage>.
    </footnote>

    <item*|2018-06-17>Binary signing PGP key <long-id-spc|509C 9D63 34C8
    0F11> published as <name|GitHub> gist.<\footnote>
      See <hlinkv|https://gist.github.com/cryptobot/8ccf8fd686d0c2d8381b69126bb3f2f8/9fdeef62bddf9edf7b73f61f42423f1f123d3218>.
    </footnote> Key used to sign <name|Cryptomator> versions prior to
    <em|1.5.8>.

    <item*|2020-09-01>Binary signing PGP key <long-id-spc|615D 449F E6E6
    A235> published as <name|GitHub> gist.<\footnote>
      See <hlinkv|https://gist.github.com/cryptobot/211111cf092037490275f39d408f461a/1a8e133a1d7e6ae4eb2bcc0830e4567393e5162a>.
    </footnote> Key used to sign <name|Cryptomator> version <em|1.5.8> onward
    (as of 2021-12-22).

    <item*|2020-09-02>Old binary signing PGP key <long-id-spc|615D 449F E6E6
    A235> signed by new PGP key <long-id-spc|509C 9D63 34C8 0F11>.<\footnote>
      See <hlinkv|https://gist.github.com/cryptobot/211111cf092037490275f39d408f461a/d416c6f0d35506116436cbe2f872baa217f3f72a>.
      Verify with <code-inline|<verbatim|$ gpg --import>> and
      <code-inline|<verbatim|$ gpg --list-signatures>> to show the signature
      (<outlined-engrave|highlighted>):

      <\vpk>
        pub \ \ rsa4096/0x615D449FE6E6A235 2020-08-18 [SC] [expires:
        2031-01-01]

        \ \ \ \ \ \ Key fingerprint = 5811 7AFA 1F85 B3EE C154 \ 677D 615D
        449F E6E6 A235

        uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] Cryptobot
        \<less\>d0228975\<gtr\>

        sig 3 \ \ \ \ \ \ \ 0x615D449FE6E6A235 2020-08-18 \ Cryptobot
        \<less\>d0228975\<gtr\>

        <\outlined-engrave>
          sig \ \ \ \ \ \ \ \ \ 0x509C9D6334C80F11 2020-09-02 \ Cryptobot
          (Release Manager) \<less\>d0228975\<gtr\>
        </outlined-engrave>

        \;
      </vpk>
    </footnote> Notice of revocation of old key and signing of new key by old
    key posted in <name|GitHub> issue thread.<\footnote>
      See <hlinkv|https://github.com/cryptomator/cryptomator.github.io/issues/25#issuecomment-685308263>.
    </footnote>
  </description>

  <subsection|Public Key Details><label|c2 cryptomator-pk>

  <\vgroup>
    <subsubsection|Binary signing key (v1.5.8\U ) (<long-id-spc|615D 449F
    E6E6 A235>)>

    <index-complex|<tuple|Keys|Organizations|Cryptomator|0x615D449FE6E6A235>|||<tuple|Keys|Organizations|Cryptomator|<verb-sm|0x615D449FE6E6A235>>>PGP
    key used to sign compiled binary releases of <name|Cryptomator> after
    version <em|1.5.8> (as of 2023-06-15). The key and fingerprint are
    available on the Linux download page.<\footnote>
      See <hlinkv|https://web.archive.org/web/20230314175327/https://cryptomator.org/downloads/linux/thanks/>.
    </footnote>

    <\vpk>
      pub \ \ rsa4096/0x615D449FE6E6A235 2020-08-18 [SC] [expires:
      2031-01-01]

      \ \ \ \ \ \ Key fingerprint = 5811 7AFA 1F85 B3EE C154 \ 677D 615D 449F
      E6E6 A235

      uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] Cryptobot
      \<less\>d0228975\<gtr\>
    </vpk>
  </vgroup>

  <\vgroup>
    <subsubsection|Binary signing key ( \Uv1.5.7) (<long-id-spc|509C 9D63
    34C8 0F11>)>

    <index-complex|<tuple|Keys|Organizations|Cryptomator|0x509C9D6334C80F11>|||<tuple|Keys|Organizations|Cryptomator|<verb-sm|0x509C9D6334C80F11>>>PGP
    key used to sign compiled binary releases of <name|Cryptomator> prior to
    version <em|1.5.8>.

    <\vpk>
      pub \ \ rsa4096/0x509C9D6334C80F11 2016-06-24 [SC] [revoked:
      2020-08-18]

      \ \ \ \ \ \ Key fingerprint = 5054 3A3D A4B1 DB81 DA3E \ 79CB 509C 9D63
      34C8 0F11

      uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ revoked] Cryptobot (Release
      Manager) \<less\>d0228975\<gtr\>
    </vpk>
  </vgroup>

  <index-complex|<tuple|Software|Cryptomator>|strong|c2 cryptomator
  idx1|<tuple|Software|Cryptomator>>
</body>

<\initial>
  <\collection>
    <associate|chapter-nr|2>
    <associate|page-first|16>
    <associate|page-medium|papyrus>
    <associate|preamble|false>
    <associate|section-nr|1>
    <associate|subsection-nr|3>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|2|16>>
    <associate|auto-10|<tuple|2.3.1|16>>
    <associate|auto-11|<tuple|<tuple|Keys|Organizations|Cryptomator|0x615D449FE6E6A235>|16>>
    <associate|auto-12|<tuple|2.3.2|17>>
    <associate|auto-13|<tuple|<tuple|Keys|Organizations|Cryptomator|0x509C9D6334C80F11>|17>>
    <associate|auto-14|<tuple|<tuple|Software|Cryptomator>|17>>
    <associate|auto-2|<tuple|2.1|16>>
    <associate|auto-3|<tuple|<tuple|Software|Cryptomator>|16>>
    <associate|auto-4|<tuple|Software|16>>
    <associate|auto-5|<tuple|<tuple|Keys|People|Stenzel,
    Sebastian|0x667B866EA8240A09>|16>>
    <associate|auto-6|<tuple|<tuple|Keys|People|Schrenk,
    Armin|0x748E55D51F5B3FBC>|16>>
    <associate|auto-7|<tuple|<tuple|Keys|People|Hagemann,
    Tobias|0x69CEFAD519598989>|16>>
    <associate|auto-8|<tuple|2.2|16>>
    <associate|auto-9|<tuple|2.3|16>>
    <associate|c2 cryptomator|<tuple|2|16>>
    <associate|c2 cryptomator-bg|<tuple|2.1|16>>
    <associate|c2 cryptomator-hist|<tuple|2.2|16>>
    <associate|c2 cryptomator-pk|<tuple|2.3|16>>
    <associate|footnote-1|<tuple|1|?>>
    <associate|footnote-2.1|<tuple|2.1|16>>
    <associate|footnote-2.10|<tuple|2.10|16>>
    <associate|footnote-2.11|<tuple|2.11|17>>
    <associate|footnote-2.12|<tuple|2.12|?>>
    <associate|footnote-2.2|<tuple|2.2|16>>
    <associate|footnote-2.3|<tuple|2.3|16>>
    <associate|footnote-2.4|<tuple|2.4|16>>
    <associate|footnote-2.5|<tuple|2.5|16>>
    <associate|footnote-2.6|<tuple|2.6|16>>
    <associate|footnote-2.7|<tuple|2.7|16>>
    <associate|footnote-2.8|<tuple|2.8|16>>
    <associate|footnote-2.9|<tuple|2.9|16>>
    <associate|footnr-2.1|<tuple|2.1|16>>
    <associate|footnr-2.10|<tuple|2.10|16>>
    <associate|footnr-2.11|<tuple|2.11|17>>
    <associate|footnr-2.12|<tuple|2.12|?>>
    <associate|footnr-2.2|<tuple|2.2|16>>
    <associate|footnr-2.3|<tuple|2.3|16>>
    <associate|footnr-2.4|<tuple|2.4|16>>
    <associate|footnr-2.5|<tuple|2.5|16>>
    <associate|footnr-2.6|<tuple|2.6|16>>
    <associate|footnr-2.7|<tuple|2.7|16>>
    <associate|footnr-2.8|<tuple|2.8|16>>
    <associate|footnr-2.9|<tuple|2.9|16>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|idx>
      <tuple|<tuple|Software|Cryptomator>|strong|c2 cryptomator
      idx1|<tuple|Software|Cryptomator>|<pageref|auto-3>>

      <tuple|<tuple|Software|Dropbox>|<pageref|auto-4>>

      <tuple|<tuple|Keys|People|Stenzel, Sebastian|0x667B866EA8240A09>|||<tuple|Keys|People|Stenzel,
      Sebastian|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x667B866EA8240A09>>>|<pageref|auto-5>>

      <tuple|<tuple|Keys|People|Schrenk, Armin|0x748E55D51F5B3FBC>|||<tuple|Keys|People|Schrenk,
      Armin|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x748E55D51F5B3FBC>>>|<pageref|auto-6>>

      <tuple|<tuple|Keys|People|Hagemann,
      Tobias|0x69CEFAD519598989>|||<tuple|Keys|People|Hagemann,
      Tobias|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x69CEFAD519598989>>>|<pageref|auto-7>>

      <tuple|<tuple|Keys|Organizations|Cryptomator|0x509C9D6334C80F11>|||<tuple|Keys|Organizations|Cryptomator|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x509C9D6334C80F11>>>|<pageref|auto-11>>

      <tuple|<tuple|Keys|Organizations|Cryptomator|0x615D449FE6E6A235>|||<tuple|Keys|Organizations|Cryptomator|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x615D449FE6E6A235>>>|<pageref|auto-13>>

      <tuple|<tuple|Software|Cryptomator>|strong|c2 cryptomator
      idx1|<tuple|Software|Cryptomator>|<pageref|auto-14>>
    </associate>
    <\associate|toc>
      2<space|2spc><with|font-shape|<quote|small-caps>|Cryptomator>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1>

      <with|par-left|<quote|1tab>|2.1<space|2spc>Background
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2>>

      <with|par-left|<quote|1tab>|2.2<space|2spc>History
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-8>>

      <with|par-left|<quote|1tab>|2.3<space|2spc>Public Key Details
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-9>>

      <with|par-left|<quote|2tab>|2.3.1<space|2spc>Binary signing key (
      \Uv1.5.7) (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|509C
      9D63 34C8 0F11>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-10>>

      <with|par-left|<quote|2tab>|2.3.2<space|2spc>Binary signing key
      (v1.5.8\U ) (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|615D
      449F E6E6 A235>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-12>>
    </associate>
  </collection>
</auxiliary>