<TeXmacs|2.1.1>

<project|../book.tm>

<style|<tuple|book|style-bk>>

<\body>
  <new-page*><section|<name|Element>><label|c2 element>

  Last updated on 2023-06-16 by <person|Steven Baltakatei Sandoval>.

  <subsection|Background><label|c2 element-bg>

  <index-complex|<tuple|Software|Element>|strong|c2 element
  idx1|<tuple|Software|Element>><name|Element<\footnote>
    Main website: <hlinkv|https://element.io>.
  </footnote>> is a <acronym|FOSS> end-to-end encrypted instant messaging
  application designed to work with <name|Matrix> servers. The application is
  developed and maintained by the <name|Element> company founded by
  <subindex|People|Miles, Gary><name|Gary Miles> (an
  <subindex|Organization|Amdocs><name|Amdocs> executive), <subindex|People|Le
  Pape, Amandine><name|Amandine Le Pape>, and <subindex|People|Hodgeson,
  Mathew><name|Mathew Hodgeson> (the technical lead of
  <name|Matrix>).<\footnote>
    \PRiot wants to be like Slack, but with the flexibility of an underlying
    open source platform\Q. (<date|2016-09-19>). <em|Tech Crunch>. Accessed
    <date|2023-06-15>. <hlinkv|https://techcrunch.com/2016/09/19/riot-wants-to-be-like-slack-but-with-the-flexibility-of-an-underlying-open-source-platform/>.
  </footnote> <\footnote>
    <name|Walker, Claire> (<date|2020-10-29>). \PElement: Founder Story\Q.
    <em|Notion>. Accessed <date|2023-06-15>.
    <hlinkv|https://www.notion.vc/resources/element-founder-story>.
  </footnote> <name|Element> is published under the <name|Apache 2.0>
  license.<\footnote>
    See <hlinkv|https://github.com/vector-im/element-web/blob/develop/LICENSE>.
  </footnote>

  <subsection|History><label|c2 element-hist>

  <\description>
    <item*|2016-06-09>The instant messaging client is officially launched
    under the name <name|Vector>.<\footnote>
      \PSay Hello to Vector!\Q. (<date|2016-06-09>). <em|medium.com>.
      Archived from the original on <date|2017-01-16> .
      <hlinkv|https://web.archive.org/web/20170116100943/https://medium.com/@RiotChat/say-hello-to-vector-2d33b23a787>.
    </footnote>

    <item*|2016-09-19><name|Vector> is rebranded <name|Riot>.<\footnote>
      \PLet's Riot!\Q. (<date|2016-09-19>). <em|medium.com>. Archived from
      the original on <date|2017-02-23>.
    </footnote>

    <item*|2016-09-20>First <name|I.A.> snapshot of
    <hlinkv|https://riot.im>.<\footnote>
      See <hlinkv|https://web.archive.org/web/20160920122722/https://riot.im/>.
    </footnote>

    <item*|2019-04-15>Creation date of signing key <long-id-spc|D7B0 B669
    41D0 1538>.

    <item*|2019-05-09>Early <name|I.A.> snapshot of signing key
    <long-id-spc|D7B0 B669 41D0 1538>.<\footnote>
      <label|element_20190509_pubkey>See <hlinkv|https://web.archive.org/web/20190509202504/https://packages.riot.im/debian/riot-im-archive-keyring.asc>.
    </footnote>

    <item*|2020-07-15><name|Riot> is rebranded to <name|Element>.<\footnote>
      <name|Wohlscheid, John Paul>. (2020-07-16). \PDecentralized Messaging
      App Riot Rebrands to Element\Q. <em|itsfoss.com>. Accessed 2023-06-15.
      <hlinkv|https://itsfoss.com/riot-to-element/>.
    </footnote>
  </description>

  <subsection|Public Key Details><label|c2 element-pk>

  <subsubsection|Signing key (2019\U) (<long-id-spc|D7B0 B669 41D0 1538>)>

  <index-complex|<tuple|Keys|Organization|Riot.im|0xD7B0B66941D01538>|||<tuple|Keys|Organization|Riot.im|<verb-sm|0xD7B0B66941D01538>>>Key
  used by <name|Element.io> developers (formerly <name|Riot.im>) to sign
  releases since at least 2019.<\footnote>
    See <hlinkv|https://element.io/download#linux>.
  </footnote> <rsup|<reference|element_20190509_pubkey>>

  <\vpk>
    pub \ \ rsa4096/0xD7B0B66941D01538 2019-04-15 [SC] [expires: 2033-03-13]

    \ \ \ \ \ \ Key fingerprint = 12D4 CD60 0C22 40A9 F4A8 \ 2071 D7B0 B669
    41D0 1538

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] riot.im packages
    \<less\>fb5ac056\<gtr\>

    sub \ \ rsa3072/0xC2850B265AC085BD 2019-04-15 [S] [expires: 2025-03-15]

    \ \ \ \ \ \ Key fingerprint = 7574 1890 063E 5E9A 4613 \ 5D01 C285 0B26
    5AC0 85BD
  </vpk>

  <index-complex|<tuple|Software|Element>|strong|c2 element
  idx1|<tuple|Software|Element>>
</body>

<\initial>
  <\collection>
    <associate|chapter-nr|2>
    <associate|page-first|24>
    <associate|page-medium|paper>
    <associate|section-nr|4>
    <associate|subsection-nr|3>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|5|24>>
    <associate|auto-10|<tuple|5.3.1|24>>
    <associate|auto-11|<tuple|<tuple|Keys|Organization|Riot.im|0xD7B0B66941D01538>|24>>
    <associate|auto-12|<tuple|<tuple|Software|Element>|24>>
    <associate|auto-2|<tuple|5.1|24>>
    <associate|auto-3|<tuple|<tuple|Software|Element>|24>>
    <associate|auto-4|<tuple|People|24>>
    <associate|auto-5|<tuple|Organization|24>>
    <associate|auto-6|<tuple|People|24>>
    <associate|auto-7|<tuple|People|24>>
    <associate|auto-8|<tuple|5.2|24>>
    <associate|auto-9|<tuple|5.3|24>>
    <associate|c2 element|<tuple|5|24>>
    <associate|c2 element-bg|<tuple|5.1|24>>
    <associate|c2 element-hist|<tuple|5.2|24>>
    <associate|c2 element-pk|<tuple|5.3|24>>
    <associate|element_20190509_pubkey|<tuple|5.8|24>>
    <associate|footnote-5.1|<tuple|5.1|24>>
    <associate|footnote-5.10|<tuple|5.10|24>>
    <associate|footnote-5.2|<tuple|5.2|24>>
    <associate|footnote-5.3|<tuple|5.3|24>>
    <associate|footnote-5.4|<tuple|5.4|24>>
    <associate|footnote-5.5|<tuple|5.5|24>>
    <associate|footnote-5.6|<tuple|5.6|24>>
    <associate|footnote-5.7|<tuple|5.7|24>>
    <associate|footnote-5.8|<tuple|5.8|24>>
    <associate|footnote-5.9|<tuple|5.9|24>>
    <associate|footnr-5.1|<tuple|5.1|24>>
    <associate|footnr-5.10|<tuple|5.10|24>>
    <associate|footnr-5.2|<tuple|5.2|24>>
    <associate|footnr-5.3|<tuple|5.3|24>>
    <associate|footnr-5.4|<tuple|5.4|24>>
    <associate|footnr-5.5|<tuple|5.5|24>>
    <associate|footnr-5.6|<tuple|5.6|24>>
    <associate|footnr-5.7|<tuple|5.7|24>>
    <associate|footnr-5.8|<tuple|5.8|24>>
    <associate|footnr-5.9|<tuple|5.9|24>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|idx>
      <tuple|<tuple|Software|Element>|strong|c2 element
      idx1|<tuple|Software|Element>|<pageref|auto-3>>

      <tuple|<tuple|People|Miles, Gary>|<pageref|auto-4>>

      <tuple|<tuple|Organization|Amdocs>|<pageref|auto-5>>

      <tuple|<tuple|People|Le Pape, Amandine>|<pageref|auto-6>>

      <tuple|<tuple|People|Hodgeson, Mathew>|<pageref|auto-7>>

      <tuple|<tuple|Keys|Organization|Riot.im|0xD7B0B66941D01538>|||<tuple|Keys|Organization|Riot.im|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0xD7B0B66941D01538>>>|<pageref|auto-11>>

      <tuple|<tuple|Software|Element>|strong|c2 element
      idx1|<tuple|Software|Element>|<pageref|auto-12>>
    </associate>
    <\associate|toc>
      5<space|2spc><with|font-shape|<quote|small-caps>|Element>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1>

      <with|par-left|<quote|1tab>|5.1<space|2spc>Background
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2>>

      <with|par-left|<quote|1tab>|5.2<space|2spc>History
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-8>>

      <with|par-left|<quote|1tab>|5.3<space|2spc>Public Key Details
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-9>>

      <with|par-left|<quote|2tab>|5.3.1<space|2spc>Signing key (2019\U2023)
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|D7B0
      B669 41D0 1538>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-10>>
    </associate>
  </collection>
</auxiliary>