<TeXmacs|2.1.1>

<project|../book.tm>

<style|<tuple|book|style-bk>>

<\body>
  <new-page*><section|<name|Trisquel>><label|c2 trisquel>

  Last updated on 2023-06-13 by <person|Steven Baltakatei Sandoval>.

  <subsection|Background><label|c2 trisquel-bg>

  <index-complex|<tuple|Software|Trisquel>|strong|c2 trisquel
  idx1|<tuple|Software|Trisquel>><name|Trisquel<\footnote>
    Main website: <hlinkv|https://trisquel.info>.
  </footnote>> is an <name|Ubuntu>-derived operating system maintained by
  <subindex|People|Rodriguez, Ruben><person|Ruben Rogriguez>
  (<long-id-spc|46A7 0073 E4E5 0D4E>).<\footnote>
    <person|Nestor, Marius>. (2023-03-20). \PTrisquel GNU/Linux 11.0 LTS
    Released with GNU Linux-Libre 5.15 Kernel, MATE 1.26\Q. <em|9to5Linux>.
    <hlinkv|https://9to5linux.com/trisquel-gnu-linux-11-0-lts-released-with-gnu-linux-libre-5-15-kernel-mate-1-26>.
  </footnote> <\footnote>
    <label|trisquel_20230322_aramo><person|Rodriguez, Ruben>. (2023-03-22).
    \PTrisquel 11.0 \PAramo\Q release announcement\Q. <em|trisquel.info>.
    <hlinkv|https://listas.trisquel.info/pipermail/trisquel-announce/2023-March/000027.html>.
  </footnote>

  <subsection|History><label|c2 trisquel-hist>

  <\description>
    <item*|2009-04-17>Earliest <name|I.A.> snapshot of
    <hlinkv|http://trisquel.info>.<\footnote>
      See <hlinkv|https://web.archive.org/web/20090417041206/http://trisquel.info/>.
    </footnote>

    <item*|2023-03-22>Trisquel 11 Aramo was
    released.<rsup|<reference|trisquel_20230322_aramo>>
  </description>

  <subsection|Public Key Details><label|c2 trisquel-pk>

  <subsubsection|Signing key (Trisquel 11 Aramo) (<long-id-spc|F5DA AAF7 4AD4
  C938>)>

  <index-complex|<tuple|Keys|Organizations|Trisquel|0xF5DAAAF74AD4C938>|||<tuple|Keys|Organizations|Trisquel|<verb-sm|0xF5DAAAF74AD4C938>>>Key
  used by the Trisquel developers to sign the Trisquel 11 Aramo release of
  2023.

  <\condensed>
    <\vpk>
      pub \ \ rsa4096/0xF5DAAAF74AD4C938 2022-10-19 [SC]

      \ \ \ \ \ \ Key fingerprint = D24D DAC9 226D 5BA5 E9F3 \ BED3 F5DA AAF7
      4AD4 C938

      uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] Trisquel GNU/Linux
      Archive Automatic Signing Key (11/aramo) \<less\>08d28017\<gtr\>
    </vpk>
  </condensed>

  \;

  <index-complex|<tuple|Software|Trisquel>|strong|c2 trisquel
  idx1|<tuple|Software|Trisquel>>
</body>

<\initial>
  <\collection>
    <associate|chapter-nr|2>
    <associate|page-first|42>
    <associate|page-medium|paper>
    <associate|section-nr|16>
    <associate|subsection-nr|3>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|17|42>>
    <associate|auto-2|<tuple|17.1|42>>
    <associate|auto-3|<tuple|<tuple|Software|Trisquel>|42>>
    <associate|auto-4|<tuple|People|42>>
    <associate|auto-5|<tuple|17.2|42>>
    <associate|auto-6|<tuple|17.3|42>>
    <associate|auto-7|<tuple|17.3.1|42>>
    <associate|auto-8|<tuple|<tuple|Keys|Organizations|Trisquel|0xF5DAAAF74AD4C938>|42>>
    <associate|auto-9|<tuple|<tuple|Software|Trisquel>|42>>
    <associate|c2 trisquel|<tuple|17|42>>
    <associate|c2 trisquel-bg|<tuple|17.1|42>>
    <associate|c2 trisquel-hist|<tuple|17.2|42>>
    <associate|c2 trisquel-pk|<tuple|17.3|42>>
    <associate|footnote-17.1|<tuple|17.1|42>>
    <associate|footnote-17.2|<tuple|17.2|42>>
    <associate|footnote-17.3|<tuple|17.3|42>>
    <associate|footnote-17.4|<tuple|17.4|42>>
    <associate|footnr-17.1|<tuple|17.1|42>>
    <associate|footnr-17.2|<tuple|17.2|42>>
    <associate|footnr-17.3|<tuple|17.3|42>>
    <associate|footnr-17.4|<tuple|17.4|42>>
    <associate|trisquel_20230322_aramo|<tuple|17.3|42>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|idx>
      <tuple|<tuple|Software|Trisquel>|strong|c2 trisquel
      idx1|<tuple|Software|Trisquel>|<pageref|auto-3>>

      <tuple|<tuple|People|Rodriguez, Ruben>|<pageref|auto-4>>

      <tuple|<tuple|Keys|Organizations|Trisquel|0xF5DAAAF74AD4C938>|||<tuple|Keys|Organizations|Trisquel|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0xF5DAAAF74AD4C938>>>|<pageref|auto-8>>

      <tuple|<tuple|Software|Trisquel>|strong|c2 trisquel
      idx1|<tuple|Software|Trisquel>|<pageref|auto-9>>
    </associate>
    <\associate|toc>
      17<space|2spc><with|font-shape|<quote|small-caps>|Trisquel>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1>

      <with|par-left|<quote|1tab>|17.1<space|2spc>Background
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2>>

      <with|par-left|<quote|1tab>|17.2<space|2spc>History
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-5>>

      <with|par-left|<quote|1tab>|17.3<space|2spc>Public Key Details
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-6>>

      <with|par-left|<quote|2tab>|17.3.1<space|2spc>Signing key (Trisquel 11
      Aramo) (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|F5DA
      AAF7 4AD4 C938>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-7>>
    </associate>
  </collection>
</auxiliary>