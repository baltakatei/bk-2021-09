<TeXmacs|2.1.1>

<project|../book.tm>

<style|<tuple|book|style-bk>>

<\body>
  <new-page*><section|<name|Satoshi Labs>><label|c2 satoshi-labs>

  Last updated on 2023-06-12 by <name|Steven Baltakatei Sandoval>.

  <subsection|Background><label|c2 satoshi-labs-bg>

  <index-complex|<tuple|Software|Satoshi Labs>|strong|c2 satoshi-labs
  idx1|<tuple|Software|Satoshi Labs>><name|Satoshi Labs><\footnote>
    Main website: <hlinkv|https://satoshilabs.com/>.
  </footnote> is a company that produces cryptocurrency hardware wallets
  called <subindex|Software|Trezor><name|Trezor><\footnote>
    Trezor website: <hlinkv|https://trezor.io/>.
  </footnote>. These devices enable a user to privately manage their private
  keys necessary to create transactions. Publishing transactions and viewing
  current balances typically requires software running on a computer
  connected to the internet. <name|Satoshi Labs> uses an OpenPGP key to sign
  these software packages published on their website
  <hlinkv|https://trezor.io>.

  <name|Satoshi Labs> was founded in 2013 by <subindex|People|Palatinus,
  Marek \PSlush\Q><subindex|People|Slush (<name|Satoshi Labs>
  developer)><name|Marek \PSlush\Q Palatinus>, <subindex|People|Rusn�k, Pavol
  \PStick\Q><subindex|People|Stick (<name|Satoshi Labs>
  developer)><name|Pavol \PStick\Q Rusn�k>, and <subindex|People|Vranova,
  Alena><name|Alena Vranova>.<\footnote>
    See <hlinkv|https://web.archive.org/web/20140627154535/http://satoshilabs.com/team/>.
  </footnote> It is based in Prague, Czech Republic.

  As of 2022-01-03, the primary <name|Trezor> program requiring verification
  is <name|Trezor Suite>.

  <subsection|History><label|c2 satoshi-labs-hist>

  <\description>
    <item*|2012-03-07>Creation date of <name|Pavol Rusn�k>'s personal PGP key
    (<long-id-spc|91F3 B339 B9A0 2A3D>).

    <item*|2014-07-18>First snapshot of <hlinkv|https://mytrezor.com>appears
    on the <name|Internet Archive>.<\footnote>
      See <hlinkv|https://web.archive.org/web/20140718104157/https://mytrezor.com/>.
    </footnote>

    <item*|2017-01-11><hlinkv|mytrezor.com>, <hlinkv|buytrezor.com>, and
    other domains migrated to <hlinkv|https://trezor.io>.<\footnote>
      See <hlinkv|https://web.archive.org/web/20201111170337/https://blog.trezor.io/new-trezor-io-55cf687c88d5?gi=3481ee5b4637>.
    </footnote>

    <item*|2017-01-28>The first snapshot of <hlinkv|https://trezor.io>
    appears on the <name|Internet Archive>.<\footnote>
      See <hlinkv|https://web.archive.org/web/20170128023418/https://trezor.io/>.
    </footnote>

    <item*|2020-10-20>Creation date of the 2020 signing key
    (<long-id-spc|26A3 A566 62F0 E7E2>).

    <item*|2021-01-04>Creation date of the 2021 signing key
    (<long-id-spc|E21B 6950 A2EC B65C>).

    <item*|2021-07-14><name|Trezor Suite> launched<\footnote>
      See <hlinkv|https://blog.trezor.io/trezor-suite-launches-8958c1d37d33>.
    </footnote> in order to replace an older web wallet
    implementation.<\footnote>
      See <hlinkv|https://github.com/trezor-graveyard>.
    </footnote>
  </description>

  <subsection|Public Key Details><label|c2 satoshi-labs-pk>

  <subsubsection|<name|Pavol Rusn�k> (<long-id-spc|91F3 B339 B9A0 2A3D>)>

  <index-complex|<tuple|Keys|People|Rusn�k, Pavol
  \PStick\Q|0x91F3B339B9A02A3D>|||<tuple|Keys|People|Rusn�k, Pavol
  \PStick\Q|<verb-sm|0x91F3B339B9A02A3D>>>A key<\footnote>
    Download key at <hlinkv|https://rusnak.io/public/pgp.txt>.
  </footnote> used by a developer named <name|Pavol \PStick\Q
  Rusn�k>.<\footnote>
    Twitter: <hlinkv|https://twitter.com/pavolrusnak>.
  </footnote> This key has been used to sign <name|Trezor> software in the
  past<\footnote>
    See <hlinkv|https://github.com/trezor/trezord-go/issues/211>.
  </footnote> such as <name|Trezor Bridge<\footnote>
    See <hlinkv|https://github.com/trezor/webwallet-data/tree/master/bridge>.
  </footnote>> and other various <name|GitHub> commits.

  <\vpk>
    pub \ \ rsa4096/0x91F3B339B9A02A3D 2012-03-07 [SC] [expires: 2024-06-16]

    \ \ \ \ \ \ Key fingerprint = 86E6 792F C27B FD47 8860 \ C110 91F3 B339
    B9A0 2A3D

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] Pavol Rusn�k
    \<less\>343a72bf\<gtr\>

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] Pavol Rusn�k
    \<less\>707f7617\<gtr\>

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] Pavol Rusn�k
    \<less\>5144f42a\<gtr\>

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] Pavol Rusn�k
    \<less\>5aef3feb\<gtr\>

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] [jpeg image of size
    2449]

    sub \ \ rsa4096/0x22AF226D38DC1F4D 2012-03-07 [E] [expires: 2024-06-16]

    \ \ \ \ \ \ Key fingerprint = E177 6F65 0601 E596 9E7F \ 9E25 22AF 226D
    38DC 1F4D
  </vpk>

  <subsubsection|2020 Signing Key (<long-id-spc|26A3 A566 62F0 E7E2>)>

  <index-complex|<tuple|Keys|Organizations|Satoshi
  Labs|Signing|0x26A3A56662F0E7E2 (2020)>|||<tuple|Keys|Organizations|Satoshi
  Labs|Signing|<verb-sm|0x26A3A56662F0E7E2> (2020)>>A key<\footnote>
    Download key at <hlinkv|https://trezor.io/security/satoshilabs-2020-signing-key.asc>.
  </footnote> used to sign the software required by a PC to communicate with
  the <name|Trezor> product line. Expired as of 2021-01-01.

  <\vpk>
    pub \ \ rsa4096/0x26A3A56662F0E7E2 2020-10-20 [SC] [expired: 2021-01-01]

    \ \ \ \ \ \ Key fingerprint = 5406 7D8B BF00 5541 81B5 \ AB8F 26A3 A566
    62F0 E7E2

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ expired] SatoshiLabs 2020
    Signing Key
  </vpk>

  <subsubsection|2021 Signing Key (<long-id-spc|E21B 6950 A2EC B65C>)>

  <index-complex|<tuple|Keys|Organizations|Satoshi
  Labs|Signing|0xE21B6950A2ECB65C (2021)>|||<tuple|Keys|Organizations|Satoshi
  Labs|Signing|<verb-sm|0xE21B6950A2ECB65C> (2021)>>A key<\footnote>
    Download key at <hlinkv|https://trezor.io/security/satoshilabs-2021-signing-key.asc>.
  </footnote> used to sign the software required by a PC to communicate with
  the Trezor product line.

  <\vpk>
    pub \ \ rsa4096/0xE21B6950A2ECB65C 2021-01-04 [SC]

    \ \ \ \ \ \ Key fingerprint = EB48 3B26 B078 A4AA 1B6F \ 425E E21B 6950
    A2EC B65C

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] SatoshiLabs 2021
    Signing Key
  </vpk>

  <index-complex|<tuple|Software|Satoshi Labs>|strong|c2 satoshi-labs
  idx1|<tuple|Software|Satoshi Labs>>
</body>

<\initial>
  <\collection>
    <associate|chapter-nr|2>
    <associate|page-first|36>
    <associate|page-medium|paper>
    <associate|section-nr|12>
    <associate|subsection-nr|3>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|13|36>>
    <associate|auto-10|<tuple|13.2|36>>
    <associate|auto-11|<tuple|13.3|36>>
    <associate|auto-12|<tuple|13.3.1|36>>
    <associate|auto-13|<tuple|<tuple|Keys|People|Rusn�k, Pavol
    \PStick\Q|0x91F3B339B9A02A3D>|36>>
    <associate|auto-14|<tuple|13.3.2|37>>
    <associate|auto-15|<tuple|<tuple|Keys|Organizations|Satoshi
    Labs|Signing|0x26A3A56662F0E7E2 (2020)>|37>>
    <associate|auto-16|<tuple|13.3.3|37>>
    <associate|auto-17|<tuple|<tuple|Keys|Organizations|Satoshi
    Labs|Signing|0xE21B6950A2ECB65C (2021)>|37>>
    <associate|auto-18|<tuple|<tuple|Software|Satoshi Labs>|37>>
    <associate|auto-2|<tuple|13.1|36>>
    <associate|auto-3|<tuple|<tuple|Software|Satoshi Labs>|36>>
    <associate|auto-4|<tuple|Software|36>>
    <associate|auto-5|<tuple|People|36>>
    <associate|auto-6|<tuple|People|36>>
    <associate|auto-7|<tuple|People|36>>
    <associate|auto-8|<tuple|People|36>>
    <associate|auto-9|<tuple|People|36>>
    <associate|c2 satoshi-labs|<tuple|13|36>>
    <associate|c2 satoshi-labs-bg|<tuple|13.1|36>>
    <associate|c2 satoshi-labs-hist|<tuple|13.2|36>>
    <associate|c2 satoshi-labs-pk|<tuple|13.3|36>>
    <associate|footnote-13.1|<tuple|13.1|36>>
    <associate|footnote-13.10|<tuple|13.10|36>>
    <associate|footnote-13.11|<tuple|13.11|36>>
    <associate|footnote-13.12|<tuple|13.12|36>>
    <associate|footnote-13.13|<tuple|13.13|37>>
    <associate|footnote-13.14|<tuple|13.14|37>>
    <associate|footnote-13.2|<tuple|13.2|36>>
    <associate|footnote-13.3|<tuple|13.3|36>>
    <associate|footnote-13.4|<tuple|13.4|36>>
    <associate|footnote-13.5|<tuple|13.5|36>>
    <associate|footnote-13.6|<tuple|13.6|36>>
    <associate|footnote-13.7|<tuple|13.7|36>>
    <associate|footnote-13.8|<tuple|13.8|36>>
    <associate|footnote-13.9|<tuple|13.9|36>>
    <associate|footnr-13.1|<tuple|13.1|36>>
    <associate|footnr-13.10|<tuple|13.10|36>>
    <associate|footnr-13.11|<tuple|13.11|36>>
    <associate|footnr-13.12|<tuple|13.12|36>>
    <associate|footnr-13.13|<tuple|13.13|37>>
    <associate|footnr-13.14|<tuple|13.14|37>>
    <associate|footnr-13.2|<tuple|13.2|36>>
    <associate|footnr-13.3|<tuple|13.3|36>>
    <associate|footnr-13.4|<tuple|13.4|36>>
    <associate|footnr-13.5|<tuple|13.5|36>>
    <associate|footnr-13.6|<tuple|13.6|36>>
    <associate|footnr-13.7|<tuple|13.7|36>>
    <associate|footnr-13.8|<tuple|13.8|36>>
    <associate|footnr-13.9|<tuple|13.9|36>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|idx>
      <tuple|<tuple|Software|Satoshi Labs>|strong|c2 satoshi-labs
      idx1|<tuple|Software|Satoshi Labs>|<pageref|auto-3>>

      <tuple|<tuple|Software|Trezor>|<pageref|auto-4>>

      <tuple|<tuple|People|Palatinus, Marek \PSlush\Q>|<pageref|auto-5>>

      <tuple|<tuple|People|Slush (<with|font-shape|<quote|small-caps>|Satoshi
      Labs> developer)>|<pageref|auto-6>>

      <tuple|<tuple|People|Rusn�k, Pavol \PStick\Q>|<pageref|auto-7>>

      <tuple|<tuple|People|Stick (<with|font-shape|<quote|small-caps>|Satoshi
      Labs> developer)>|<pageref|auto-8>>

      <tuple|<tuple|People|Vranova, Alena>|<pageref|auto-9>>

      <tuple|<tuple|Keys|People|Rusn�k, Pavol
      \PStick\Q|0x91F3B339B9A02A3D>|||<tuple|Keys|People|Rusn�k, Pavol
      \PStick\Q|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x91F3B339B9A02A3D>>>|<pageref|auto-13>>

      <tuple|<tuple|Keys|Organizations|Satoshi
      Labs|Signing|0x26A3A56662F0E7E2 (2020)>|||<tuple|Keys|Organizations|Satoshi
      Labs|Signing|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x26A3A56662F0E7E2>>
      (2020)>|<pageref|auto-15>>

      <tuple|<tuple|Keys|Organizations|Satoshi
      Labs|Signing|0xE21B6950A2ECB65C (2021)>|||<tuple|Keys|Organizations|Satoshi
      Labs|Signing|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0xE21B6950A2ECB65C>>
      (2021)>|<pageref|auto-17>>

      <tuple|<tuple|Software|Satoshi Labs>|strong|c2 satoshi-labs
      idx1|<tuple|Software|Satoshi Labs>|<pageref|auto-18>>
    </associate>
    <\associate|toc>
      13<space|2spc><with|font-shape|<quote|small-caps>|Satoshi Labs>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1>

      <with|par-left|<quote|1tab>|13.1<space|2spc>Background
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2>>

      <with|par-left|<quote|1tab>|13.2<space|2spc>History
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-10>>

      <with|par-left|<quote|1tab>|13.3<space|2spc>Public Key Details
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-11>>

      <with|par-left|<quote|2tab>|13.3.1<space|2spc><with|font-shape|<quote|small-caps>|Pavol
      Rusn�k> (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|91F3
      B339 B9A0 2A3D>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-12>>

      <with|par-left|<quote|2tab>|13.3.2<space|2spc>2020 Signing Key
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|26A3
      A566 62F0 E7E2>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-14>>

      <with|par-left|<quote|2tab>|13.3.3<space|2spc>2021 Signing Key
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|E21B
      6950 A2EC B65C>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-16>>
    </associate>
  </collection>
</auxiliary>