<TeXmacs|2.1>

<style|<tuple|book|style-bk>>

<\body>
  <new-page*><section|<TeXmacs>><label|c2 texmacs>

  <subsection|Background><label|c2 texmacs-bg>

  Formally known as, <name|GNU <TeXmacs>>, this program is a
  WYSIWYG<\footnote>
    \PWhat you see is what you get\Q. See
    <hlink|<verbatim|https://w.wiki/3eQb>|https://w.wiki/3eQb>
  </footnote> technical document editor written and maintained by <name|Joris
  van der Hoeven> and a group of developers.

  <subsection|History>

  <subsection|Public Key Details><label|c2 texmacs-pk>

  <name|Joris van der Hoeven> has published this <verbatim|dsa1024> public
  key.

  <\verbatim-8pt>
    pub \ \ dsa1024/0x17C9DC43D33314EC 2005-05-24 [SCA]

    \ \ \ \ \ \ Key fingerprint = 13F1 0942 AC31 62F9 043D \ 4099 17C9 DC43
    D333 14EC

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] Joris van der Hoeven
    (Mathematician...) \<less\>vdhoeven@texmacs.org\<gtr\>

    sub \ \ elg2048/0xA146596EE54A68EE 2005-05-24 [E]
  </verbatim-8pt>

  \;

  As of 2021-07-17, the <TeXmacs> website recommends a procedure for
  installing <TeXmacs> onto GNU/Linux operating systems that involves
  configuring <name|apt> to automatically download and install the software.
  The recommended commands instruct <name|apt> to import the following public
  key.

  <\vpk>
    pub \ \ rsa2048/0x8CADE14440A2D6D6 2019-07-16 [SC] [expires: 2023-07-15]

    \ \ \ \ \ \ Key fingerprint = 129F DE27 280D 2D8E F79E \ 2395 8CAD E144
    40A2 D6D6

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] TeXmacs Team
    \<less\>contact@texmacs.org\<gtr\>
  </vpk>
</body>

<\initial>
  <\collection>
    <associate|chapter-nr|1>
    <associate|page-first|9>
    <associate|page-medium|paper>
    <associate|section-nr|0>
    <associate|subsection-nr|0>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|9>>
    <associate|auto-2|<tuple|1.1|9>>
    <associate|auto-3|<tuple|1.2|9>>
    <associate|auto-4|<tuple|1.3|9>>
    <associate|c2 texmacs|<tuple|1|9>>
    <associate|c2 texmacs-bg|<tuple|1.1|9>>
    <associate|c2 texmacs-pk|<tuple|1.3|9>>
    <associate|footnote-1.1|<tuple|1.1|9>>
    <associate|footnr-1.1|<tuple|1.1|9>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|toc>
      1<space|2spc>T<rsub|<space|-0.4spc><move|<resize|<with|math-level|<quote|0>|E>||||0.5fn>|0fn|-0.1fn>><space|-0.4spc>X<rsub|<space|-0.4spc><move|<resize|M<space|-0.2spc>A<space|-0.4spc>CS||||0.5fn>|0fn|-0.1fn>>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1>

      <with|par-left|<quote|1tab>|1.1<space|2spc>Background
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2>>

      <with|par-left|<quote|1tab>|1.2<space|2spc>History
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-3>>

      <with|par-left|<quote|1tab>|1.3<space|2spc>Public Key Details
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-4>>
    </associate>
  </collection>
</auxiliary>