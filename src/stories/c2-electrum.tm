<TeXmacs|2.1.1>

<project|../book.tm>

<style|<tuple|book|style-bk>>

<\body>
  <new-page*><section|<name|Electrum>><label|c2 electrum>

  Last updated on 2023-06-12 by <person|Steven Baltakatei Sandoval>.

  <subsection|Background><label|c2 electrum-bg>

  <index-complex|<tuple|Software|Electrum>|strong|c2 electrum
  idx1|<tuple|Software|Electrum>><name|Electrum<\footnote>
    Main website: <hlinkv|https://electrum.org>.
  </footnote>> is a <name|Bitcoin> wallet program first published by
  <subindex|People|Voegtlin, Thomas><person|Thomas Voegtlin>
  (<long-id-spc|2BD5 824B 7F94 70E6>) in 2013.<\footnote>
    <name|Thomas Voegtlin>'s <name|GitHub> page:
    <hlinkv|https://github.com/ecdsa>.
  </footnote> The program permits creating and digitally signing and
  verifying <name|Bitcoin> transactions as well as messages. In contrast to
  <subindex|Software|Bitcoin Core><name|Bitcoin Core>, the main
  <name|Electrum> program is a \Pthin client\Q designed to require less CPU
  power and storage capacity in order to be able to run on mobile devices
  such as <subindex|Software|Android><name|Android> phones. It therefore
  requires trusting an <name|Electrum> server to provide it with current
  transaction data from the <name|Bitcoin> blockchain. One such server is
  <subindex|Software|ElectrumX><name|ElectrumX><\footnote>
    See <hlinkv|https://github.com/spesmilo/electrumx/>.
  </footnote>, maintained by <subindex|People|SomberNight><person|SomberNight>
  (<long-id-spc|E7B7 48CD AF5E 5ED9>, <long-id-spc|CA9E EEC4 3DF9 11DC>).

  As of 2022-07-12, the latest release of <name|Electrum> that is available
  is version <em|4.2.2>.

  <subsection|History><label|c2 electrum-hist>

  <\description>
    <item*|2011-12-14>Early <name|I.A.> mention of <person|Thomas Voegtlin>'s
    <long-id-spc|2BD5 824B 7F94 70E6> key on
    <hlinkv|http://bitcoin-otc.com>.<\footnote>
      See <hlinkv|https://web.archive.org/web/20111214231353/http://bitcoin-otc.com/viewgpg.php?nick=ThomasV>.
    </footnote>

    <item*|2013-01-09>Earliest <name|I.A.> snapshot of
    <hlinkv|https://electrum.org/download.html>available.<\footnote>
      See <hlinkv|https://web.archive.org/web/20130109155542/http://electrum.org/download.html>.
    </footnote>

    <item*|2014-09-13>Earliest <name|I.A.> snapshot of
    <hlinkv|https://github.com/spesmilo/electrum>available.<\footnote>
      See <hlinkv|https://web.archive.org/web/20140913002252/https://github.com/spesmilo/electrum>.
    </footnote>

    <item*|2018-03-31>Creation date of <person|SomberNight>'s
    <long-id-spc|E7B7 48CD AF5E 5ED9> key.

    <item*|2019-10-23>Creation date of <person|Stephan \PEmzy\Q Oeste>'s
    <long-id-spc|2EBB 056F D847 F8A7> key.

    <item*|2021-06-15>Creation date of <person|SomberNight>'s
    <long-id-spc|CA9E EEC4 3DF9 11DC> key.
  </description>

  <subsection|Public Key Details><label|c2 electrum-pk>

  <subsubsection|<person|Thomas Voegtlin> signing key (<name|Electrum>
  v1.7\U<space|1em>) (<long-id-spc|2BD5 824B 7F94 70E6>)>

  <index-complex|<tuple|Keys|People|Voegtlin,
  Thomas|0x2BD5824B7F9470E6>|||<tuple|Keys|People|Voegtlin,
  Thomas|<verb-sm|0x2BD5824B7F9470E6>>><name|OpenPGP> key used by
  <person|Thomas Voegtlin> (a.k.a. \P<verbatim|ThomasV>\Q) to sign
  <name|Electrum> client releases since as early as version <em|1.7> in
  2013<\footnote>
    See <hlinkv|https://web.archive.org/web/20130320184418/http://electrum.org/download.html>.
  </footnote>. A copy of the key can be downloaded from the <name|Electrum>
  website.<\footnote>
    See <hlinkv|https://raw.githubusercontent.com/spesmilo/electrum/master/pubkeys/ThomasV.asc>.
  </footnote>

  <\vpk>
    pub \ \ rsa4096/0x2BD5824B7F9470E6 2011-06-15 [SC]

    \ \ \ \ \ \ Key fingerprint = 6694 D8DE 7BE8 EE56 31BE \ D950 2BD5 824B
    7F94 70E6

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] Thomas Voegtlin
    (https://electrum.org) \<less\>cab2cac8\<gtr\>

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] ThomasV
    \<less\>45704e55\<gtr\>

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] Thomas Voegtlin
    \<less\>45704e55\<gtr\>

    sub \ \ rsa4096/0x1A25C4602021CD84 2011-06-15 [E]

    \ \ \ \ \ \ Key fingerprint = 0A40 B328 1212 5B08 FCBF \ 90EC 1A25 C460
    2021 CD84
  </vpk>

  <subsubsection|<person|Stephan \PEmzy\Q Oeste> signing key (<name|Electrum>
  v4.1.5\U<space|1em>) (<long-id-spc|2EBB 056F D847 F8A7>)>

  <index-complex|<tuple|Keys|People|Oeste, Stephan
  \Pemzy\Q|0x2EBB056FD847F8A7>|||<tuple|Keys|People|Oeste, Stephan
  \Pemzy\Q|<verb-sm|0x2EBB056FD847F8A7>>><name|OpenPGP> key used by
  <name|Stephan \PEmzy\Q Oeste> to sign <name|Electrum> client releases since
  at least version <em|4.1.5>.<\footnote>
    See <hlinkv|https://github.com/spesmilo/electrum-signatures/commit/1b177c1cfcf17a4770f0806bd2aff552c11b12f9>.
  </footnote> The key is downloadable from the <name|Electrum> project's
  <name|Git> repository.<\footnote>
    See <hlinkv|https://raw.githubusercontent.com/spesmilo/electrum/master/pubkeys/Emzy.asc>.
  </footnote>

  <\vpk>
    pub \ \ rsa4096/0x2EBB056FD847F8A7 2019-10-23 [C]

    \ \ \ \ \ \ Key fingerprint = 9EDA FF80 E080 6596 04F4 \ A76B 2EBB 056F
    D847 F8A7

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] Stephan Oeste (it)
    \<less\>2e3078dc\<gtr\>

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] Emzy E. (emzy)
    \<less\>10670594\<gtr\>

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] Stephan Oeste
    (Master-key) \<less\>3d27d629\<gtr\>

    sub \ \ rsa4096/0x918A89D210E96167 2019-10-23 [A] [expires: 2026-10-12]

    \ \ \ \ \ \ Key fingerprint = 3442 1905 5D59 453C D505 \ 31CD 918A 89D2
    10E9 6167

    sub \ \ rsa4096/0x70596D7FF6B55417 2019-10-23 [E] [expires: 2026-10-12]

    \ \ \ \ \ \ Key fingerprint = 28EB 13F9 FD58 CE86 EAAB \ 0914 7059 6D7F
    F6B5 5417

    sub \ \ rsa4096/0x3152347D07DA627C 2019-10-23 [S] [expires: 2026-10-12]

    \ \ \ \ \ \ Key fingerprint = 637D B1E2 3370 F84A FF88 \ CCE0 3152 347D
    07DA 627C
  </vpk>

  <subsubsection|<person|SomberNight> signing key (<name|Electrum>
  v3.2.0\U<space|1em>) (<long-id-spc|CA9E EEC4 3DF9 11DC>)>

  <index-complex|<tuple|Keys|People|SomberNight|0xCA9EEEC43DF911DC>|||<tuple|Keys|People|SomberNight|<verb-sm|0x0xCA9EEEC43DF911DC>>><name|OpenPGP>
  key used by <person|SomberNight> (a.k.a. \P<verbatim|ghost43>\Q) to sign
  some <name|Electrum> client releases since at least version
  <em|3.2.0>.<\footnote>
    See <hlinkv|https://github.com/spesmilo/electrum-signatures/commit/efacae3014e38627e268588ed33cf92b9f65f1a7>.
  </footnote> The key is downloadable from the <name|Electrum> project's
  <name|Git> repository.<\footnote>
    See <hlinkv|https://raw.githubusercontent.com/spesmilo/electrum/master/pubkeys/sombernight_releasekey.asc>.
  </footnote>

  <\vpk>
    pub \ \ rsa4096/0xCA9EEEC43DF911DC 2021-06-15 [SC]

    \ \ \ \ \ \ Key fingerprint = 0EED CFD5 CAFB 4590 6734 \ 9B23 CA9E EEC4
    3DF9 11DC

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] SomberNight/ghost43
    (Electrum RELEASE signing key) \<less\>dbf39566\<gtr\>
  </vpk>

  <subsubsection|<person|SomberNight> signing key (<name|ElectrumX>)
  (<long-id-spc|E7B7 48CD AF5E 5ED9>)>

  <index-complex|<tuple|Keys|People|SomberNight|0xE7B748CDAF5E5ED9>|||<tuple|Keys|People|SomberNight|<verb-sm|0xE7B748CDAF5E5ED9>>><name|OpenPGP>
  key used by <name|SomberNight> (a.k.a. \P<verbatim|ghost43>\Q<\footnote>
    <name|SomberNight>'s <name|GitHub> page:
    <hlinkv|https://github.com/SomberNight>.
  </footnote>) to sign <name|ElectrumX> (an <name|Electrum> server) commits
  on <name|GitHub><\footnote>
    See <hlinkv|https://github.com/spesmilo/electrumx/commit/914938264e5621ea8980be6d3e69964e7f219d16>.
  </footnote>. Note, this key is different from another key
  (<long-id-spc|CA9E EEC4 3DF9 11DC>) used by <name|SomberNight> to sign
  <name|Electrum> releases.

  <\vpk>
    pub \ \ rsa4096/0xE7B748CDAF5E5ED9 2018-03-31 [SC]

    \ \ \ \ \ \ Key fingerprint = 4AD6 4339 DFA0 5E20 B3F6 \ AD51 E7B7 48CD
    AF5E 5ED9

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] SomberNight
    \<less\>dbf39566\<gtr\>

    sub \ \ rsa4096/0xB38EBEDF07698B05 2018-03-31 [E]

    \ \ \ \ \ \ Key fingerprint = 9145 6DB8 FDF2 16A9 3C37 \ F0A9 B38E BEDF
    0769 8B05

    sub \ \ rsa4096/0xB33B5F232C6271E9 2018-03-31 [S]

    \ \ \ \ \ \ Key fingerprint = 6D7A 2116 DA90 9E00 AC21 \ 108B B33B 5F23
    2C62 71E9
  </vpk>

  <index-complex|<tuple|Software|Electrum>|strong|c2 electrum
  idx1|<tuple|Software|Electrum>>
</body>

<\initial>
  <\collection>
    <associate|chapter-nr|2>
    <associate|page-first|22>
    <associate|page-medium|paper>
    <associate|section-nr|3>
    <associate|subsection-nr|3>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|4|22>>
    <associate|auto-10|<tuple|4.3|22>>
    <associate|auto-11|<tuple|4.3.1|22>>
    <associate|auto-12|<tuple|<tuple|Keys|People|Voegtlin,
    Thomas|0x2BD5824B7F9470E6>|22>>
    <associate|auto-13|<tuple|4.3.2|22>>
    <associate|auto-14|<tuple|<tuple|Keys|People|Oeste, Stephan
    \Pemzy\Q|0x2EBB056FD847F8A7>|22>>
    <associate|auto-15|<tuple|4.3.3|23>>
    <associate|auto-16|<tuple|<tuple|Keys|People|SomberNight|0xCA9EEEC43DF911DC>|23>>
    <associate|auto-17|<tuple|4.3.4|23>>
    <associate|auto-18|<tuple|<tuple|Keys|People|SomberNight|0xE7B748CDAF5E5ED9>|23>>
    <associate|auto-19|<tuple|<tuple|Software|Electrum>|23>>
    <associate|auto-2|<tuple|4.1|22>>
    <associate|auto-3|<tuple|<tuple|Software|Electrum>|22>>
    <associate|auto-4|<tuple|People|22>>
    <associate|auto-5|<tuple|Software|22>>
    <associate|auto-6|<tuple|Software|22>>
    <associate|auto-7|<tuple|Software|22>>
    <associate|auto-8|<tuple|People|22>>
    <associate|auto-9|<tuple|4.2|22>>
    <associate|c2 electrum|<tuple|4|22>>
    <associate|c2 electrum-bg|<tuple|4.1|22>>
    <associate|c2 electrum-hist|<tuple|4.2|22>>
    <associate|c2 electrum-pk|<tuple|4.3|22>>
    <associate|footnote-4.1|<tuple|4.1|22>>
    <associate|footnote-4.10|<tuple|4.10|22>>
    <associate|footnote-4.11|<tuple|4.11|23>>
    <associate|footnote-4.12|<tuple|4.12|23>>
    <associate|footnote-4.13|<tuple|4.13|23>>
    <associate|footnote-4.14|<tuple|4.14|23>>
    <associate|footnote-4.2|<tuple|4.2|22>>
    <associate|footnote-4.3|<tuple|4.3|22>>
    <associate|footnote-4.4|<tuple|4.4|22>>
    <associate|footnote-4.5|<tuple|4.5|22>>
    <associate|footnote-4.6|<tuple|4.6|22>>
    <associate|footnote-4.7|<tuple|4.7|22>>
    <associate|footnote-4.8|<tuple|4.8|22>>
    <associate|footnote-4.9|<tuple|4.9|22>>
    <associate|footnr-4.1|<tuple|4.1|22>>
    <associate|footnr-4.10|<tuple|4.10|22>>
    <associate|footnr-4.11|<tuple|4.11|23>>
    <associate|footnr-4.12|<tuple|4.12|23>>
    <associate|footnr-4.13|<tuple|4.13|23>>
    <associate|footnr-4.14|<tuple|4.14|23>>
    <associate|footnr-4.2|<tuple|4.2|22>>
    <associate|footnr-4.3|<tuple|4.3|22>>
    <associate|footnr-4.4|<tuple|4.4|22>>
    <associate|footnr-4.5|<tuple|4.5|22>>
    <associate|footnr-4.6|<tuple|4.6|22>>
    <associate|footnr-4.7|<tuple|4.7|22>>
    <associate|footnr-4.8|<tuple|4.8|22>>
    <associate|footnr-4.9|<tuple|4.9|22>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|idx>
      <tuple|<tuple|Software|Electrum>|strong|c2 electrum
      idx1|<tuple|Software|Electrum>|<pageref|auto-3>>

      <tuple|<tuple|People|Voegtlin, Thomas>|<pageref|auto-4>>

      <tuple|<tuple|Software|Bitcoin Core>|<pageref|auto-5>>

      <tuple|<tuple|Software|Android>|<pageref|auto-6>>

      <tuple|<tuple|Software|ElectrumX>|<pageref|auto-7>>

      <tuple|<tuple|People|SomberNight>|<pageref|auto-8>>

      <tuple|<tuple|Keys|People|Voegtlin,
      Thomas|0x2BD5824B7F9470E6>|||<tuple|Keys|People|Voegtlin,
      Thomas|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x2BD5824B7F9470E6>>>|<pageref|auto-12>>

      <tuple|<tuple|Keys|People|Oeste, Stephan
      \Pemzy\Q|0x2EBB056FD847F8A7>|||<tuple|Keys|People|Oeste, Stephan
      \Pemzy\Q|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x2EBB056FD847F8A7>>>|<pageref|auto-14>>

      <tuple|<tuple|Keys|People|SomberNight|0xCA9EEEC43DF911DC>|||<tuple|Keys|People|SomberNight|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x0xCA9EEEC43DF911DC>>>|<pageref|auto-16>>

      <tuple|<tuple|Keys|People|SomberNight|0xE7B748CDAF5E5ED9>|||<tuple|Keys|People|SomberNight|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0xE7B748CDAF5E5ED9>>>|<pageref|auto-18>>

      <tuple|<tuple|Software|Electrum>|strong|c2 electrum
      idx1|<tuple|Software|Electrum>|<pageref|auto-19>>
    </associate>
    <\associate|toc>
      4<space|2spc><with|font-shape|<quote|small-caps>|Electrum>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1>

      <with|par-left|<quote|1tab>|4.1<space|2spc>Background
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2>>

      <with|par-left|<quote|1tab>|4.2<space|2spc>History
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-9>>

      <with|par-left|<quote|1tab>|4.3<space|2spc>Public Key Details
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-10>>

      <with|par-left|<quote|2tab>|4.3.1<space|2spc><with|font-shape|<quote|small-caps>|Thomas
      Voegtlin> signing key (<with|font-shape|<quote|small-caps>|Electrum>
      v1.7\U<space|1em>) (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|2BD5
      824B 7F94 70E6>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-11>>

      <with|par-left|<quote|2tab>|4.3.2<space|2spc><with|font-shape|<quote|small-caps>|Stephan
      \PEmzy\Q Oeste> signing key (<with|font-shape|<quote|small-caps>|Electrum>
      v4.1.5\U<space|1em>) (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|2EBB
      056F D847 F8A7>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-13>>

      <with|par-left|<quote|2tab>|4.3.3<space|2spc><with|font-shape|<quote|small-caps>|SomberNight>
      signing key (<with|font-shape|<quote|small-caps>|Electrum>
      v3.2.0\U<space|1em>) (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|CA9E
      EEC4 3DF9 11DC>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-15>>

      <with|par-left|<quote|2tab>|4.3.4<space|2spc><with|font-shape|<quote|small-caps>|SomberNight>
      signing key (<with|font-shape|<quote|small-caps>|ElectrumX>)
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|E7B7
      48CD AF5E 5ED9>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-17>>
    </associate>
  </collection>
</auxiliary>