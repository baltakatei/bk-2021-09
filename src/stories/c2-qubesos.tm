<TeXmacs|2.1.1>

<project|../book.tm>

<style|<tuple|book|style-bk>>

<\body>
  <new-page*><section|<name|Qubes OS>><label|c2 qubesos>

  Last updated on 2023-06-12 by <name|Steven Baltakatei Sandoval>.

  <subsection|Background><label|c2 qubesos-bg>

  <index-complex|<tuple|Software|Qubes OS>|strong|c2 qubesos
  idx1|<tuple|Software|Qubes OS>><name|Qubes OS<\footnote>
    Main website: <hlinkv|https://www.qubes-os.org/>.
  </footnote>> is a privacy-focused operating system made by
  <subindex|Organizations|Invisible Things Labs><name|Invisible Things Labs>.
  Privacy is enhanced by isolating programs so each runs in its own virtual
  machine environment. The project uses <name|OpenPGP> to sign release
  files<\footnote>
    See <hlinkv|https://www.qubes-os.org/downloads/>.
  </footnote>.

  The project was founded in 2010 by <subindex|People|Joanna
  Rutkowska><name|Joanna Rutkowska> (<long-id-spc|5FA6 C3E4 D9AF BB99>). As
  of 2022, the project lead is <subindex|People|Marek
  Marczykowski-G�recki><name|Marek Marczykowski-G�recki> (<long-id-spc|DB8F
  D31C CAD7 D72C>).

  <subsection|History><label|c2 qubesos-hist>

  <\description>
    <item*|2010-04-01>Creation date of the Qubes Master Signing Key
    (<long-id-spc|<code*|DDFA 1A3E 3687 9494>>; a.k.a. QMSK).

    <item*|2010-04-09>First snapshot on IA of
    <hlinkv|https://qubes-os.org><\footnote>
      See <hlinkv|https://web.archive.org/web/20100409054657/http://qubes-os.org/Home.html>.
    </footnote>.

    <item*|2010-04-12>Early publication on IA of the QMSK
    (<long-id-spc|<code*|DDFA 1A3E 3687 9494>>) full fingerprint.<\footnote>
      See <hlinkv|https://web.archive.org/web/20100412080416/http://www.qubes-os.org/trac/wiki/VerifyingSignatures>.
    </footnote>

    <item*|2012-03-31>Creation date of the Release 1 signing key
    (<long-id-spc|EA01 201B 2110 93A7>).

    <item*|2012-11-15>Creation date of the Release 2 signing key
    (<long-id-spc|0C73 B9D4 0A40 E458>).

    <item*|2014-11-19>Creation date of the Release 3 signing key
    (<long-id-spc|CB11 CA1D 03FA 5082>).

    <item*|2017-03-06>Creation date of the Release 4 signing key
    (<long-id-spc|1848 792F 9E27 95E9>).
  </description>

  <subsection|Public Key Details><label|c2 qubesos-pk>

  <subsubsection|Qubes Master Signing Key (<long-id-spc|<code*|DDFA 1A3E 3687
  9494>>)>

  <index-complex|<tuple|Keys|Organizations|Qubes
  OS|0xDDFA1A3E36879494>|||<tuple|Keys|Organizations|Qubes
  OS|<verb-sm|0xDDFA1A3E36879494>>>Key used by the Qubes OS Project to sign
  keys of official team members and release signing keys. A procedure for
  downloading and verifying this key is available on the <name|Qubes OS>
  website.<\footnote>
    See <hlinkv|https://www.qubes-os.org/security/verifying-signatures/>.
  </footnote>

  <\vpk>
    pub \ \ rsa4096/0xDDFA1A3E36879494 2010-04-01 [SC]

    \ \ \ \ \ \ Key fingerprint = 427F 11FD 0FAA 4B08 0123 \ F01C DDFA 1A3E
    3687 9494

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] Qubes Master Signing
    Key
  </vpk>

  <subsubsection|Release 4 Signing Key (<long-id-spc|1848 792F 9E27 95E9>)>

  <index-complex|<tuple|Keys|Organizations|Qubes OS (Release 4 Signing
  Key)|0x1848792F9E2795E9>|||<tuple|Keys|Organizations|Qubes OS (Release 4
  Signing Key)|<verb-sm|0x1848792F9E2795E9>>>Key used to sign Release 4 of
  <name|Qubes OS><\footnote>
    See <hlinkv|https://www.qubes-os.org/doc/releases/4.0/release-notes/>.
  </footnote>.

  <\vpk>
    pub \ \ rsa4096/0x1848792F9E2795E9 2017-03-06 [SC]

    \ \ \ \ \ \ Key fingerprint = 5817 A43B 283D E5A9 181A \ 522E 1848 792F
    9E27 95E9

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] Qubes OS Release 4
    Signing Key
  </vpk>

  <subsubsection|Release 3 Signing Key (<long-id-spc|CB11 CA1D 03FA 5082>)>

  <index-complex|<tuple|Keys|Organizations|Qubes OS (Release 3 Signing
  Key)|0xCB11CA1D03FA5082>|||<tuple|Keys|Organizations|Qubes OS (Release 3
  Signing Key)|<verb-sm|0xCB11CA1D03FA5082>>>Key used to sign Release 3 of
  <name|Qubes OS><\footnote>
    See <hlinkv|https://www.qubes-os.org/doc/releases/3.0/release-notes/>.
  </footnote>.

  <\vpk>
    pub \ \ rsa4096/0xCB11CA1D03FA5082 2014-11-19 [SC]

    \ \ \ \ \ \ Key fingerprint = C522 61BE 0A82 3221 D94C \ A1D1 CB11 CA1D
    03FA 5082

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] Qubes OS Release 3
    Signing Key
  </vpk>

  <subsubsection|Release 2 Signing Key (<long-id-spc|0C73 B9D4 0A40 E458>)>

  <index-complex|<tuple|Keys|Organizations|Qubes OS (Release 2 Signing
  Key)|0x0C73B9D40A40E458>|||<tuple|Keys|Organizations|Qubes OS (Release 2
  Signing Key)|<verb-sm|0x0C73B9D40A40E458>>>Key used to sign Release 2 of
  <name|Qubes OS><\footnote>
    See <hlinkv|https://blog.invisiblethings.org/2014/09/26/announcing-qubes-os-release-2.html>.
  </footnote>.

  <\vpk>
    pub \ \ rsa4096/0x0C73B9D40A40E458 2012-11-15 [SC]

    \ \ \ \ \ \ Key fingerprint = 3F01 DEF4 9719 158E F862 \ 66F8 0C73 B9D4
    0A40 E458

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] Qubes OS Release 2
    Signing Key
  </vpk>

  <subsubsection|Release 1 Signing Key (<long-id-spc|EA01 201B 2110 93A7>)>

  <index-complex|<tuple|Keys|Organizations|Qubes OS (Release 1 Signing
  Key)|0xEA01201B211093A7>|||<tuple|Keys|Organizations|Qubes OS (Release 1
  Signing Key)|<verb-sm|0xEA01201B211093A7>>>Key used to sign Release 1 of
  <name|Qubes OS><\footnote>
    See <hlinkv|https://blog.invisiblethings.org/2012/09/03/introducing-qubes-10.html>.
  </footnote>.

  <\vpk>
    pub \ \ rsa4096/0xEA01201B211093A7 2012-03-31 [SC]

    \ \ \ \ \ \ Key fingerprint = FFED 4FD8 E49E 79F3 9C83 \ FD81 EA01 201B
    2110 93A7

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] Qubes OS Release 1
    Signing Key
  </vpk>

  \;

  <index-complex|<tuple|Software|Qubes OS>|strong|c2 qubesos
  idx1|<tuple|Software|Qubes OS>>
</body>

<\initial>
  <\collection>
    <associate|chapter-nr|2>
    <associate|page-first|33>
    <associate|page-medium|papyrus>
    <associate|section-nr|10>
    <associate|subsection-nr|3>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|11|9>>
    <associate|auto-10|<tuple|<tuple|Keys|Organizations|Qubes
    OS|0xDDFA1A3E36879494>|?>>
    <associate|auto-11|<tuple|11.3.2|?>>
    <associate|auto-12|<tuple|<tuple|Keys|Organizations|Qubes OS (Release 4
    Signing Key)|0x1848792F9E2795E9>|?>>
    <associate|auto-13|<tuple|11.3.3|?>>
    <associate|auto-14|<tuple|<tuple|Keys|Organizations|Qubes OS (Release 3
    Signing Key)|0xCB11CA1D03FA5082>|?>>
    <associate|auto-15|<tuple|11.3.4|?>>
    <associate|auto-16|<tuple|<tuple|Keys|Organizations|Qubes OS (Release 2
    Signing Key)|0x0C73B9D40A40E458>|?>>
    <associate|auto-17|<tuple|11.3.5|?>>
    <associate|auto-18|<tuple|<tuple|Keys|Organizations|Qubes OS (Release 1
    Signing Key)|0xEA01201B211093A7>|?>>
    <associate|auto-19|<tuple|<tuple|Software|Qubes OS>|?>>
    <associate|auto-2|<tuple|11.1|9>>
    <associate|auto-3|<tuple|<tuple|Software|Qubes OS>|9>>
    <associate|auto-4|<tuple|Organizations|9>>
    <associate|auto-5|<tuple|People|9>>
    <associate|auto-6|<tuple|People|?>>
    <associate|auto-7|<tuple|11.2|?>>
    <associate|auto-8|<tuple|11.3|?>>
    <associate|auto-9|<tuple|11.3.1|?>>
    <associate|c2 qubesos|<tuple|11|?>>
    <associate|c2 qubesos-bg|<tuple|11.1|?>>
    <associate|c2 qubesos-hist|<tuple|11.2|?>>
    <associate|c2 qubesos-pk|<tuple|11.3|?>>
    <associate|footnote-11.1|<tuple|11.1|?>>
    <associate|footnote-11.2|<tuple|11.2|?>>
    <associate|footnote-11.3|<tuple|11.3|?>>
    <associate|footnote-11.4|<tuple|11.4|?>>
    <associate|footnote-11.5|<tuple|11.5|?>>
    <associate|footnote-11.6|<tuple|11.6|?>>
    <associate|footnote-11.7|<tuple|11.7|?>>
    <associate|footnote-11.8|<tuple|11.8|?>>
    <associate|footnote-11.9|<tuple|11.9|?>>
    <associate|footnr-11.1|<tuple|11.1|?>>
    <associate|footnr-11.2|<tuple|11.2|?>>
    <associate|footnr-11.3|<tuple|11.3|?>>
    <associate|footnr-11.4|<tuple|11.4|?>>
    <associate|footnr-11.5|<tuple|11.5|?>>
    <associate|footnr-11.6|<tuple|11.6|?>>
    <associate|footnr-11.7|<tuple|11.7|?>>
    <associate|footnr-11.8|<tuple|11.8|?>>
    <associate|footnr-11.9|<tuple|11.9|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|idx>
      <tuple|<tuple|Software|Qubes OS>|strong|c2 qubesos
      idx1|<tuple|Software|Qubes OS>|<pageref|auto-3>>

      <tuple|<tuple|Organizations|Invisible Things Labs>|<pageref|auto-4>>

      <tuple|<tuple|People|Joanna Rutkowska>|<pageref|auto-5>>

      <tuple|<tuple|People|Marek Marczykowski-G�recki>|<pageref|auto-6>>

      <tuple|<tuple|Keys|Organizations|Qubes
      OS|0xDDFA1A3E36879494>|||<tuple|Keys|Organizations|Qubes
      OS|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0xDDFA1A3E36879494>>>|<pageref|auto-10>>

      <tuple|<tuple|Keys|Organizations|Qubes OS (Release 4 Signing
      Key)|0x1848792F9E2795E9>|||<tuple|Keys|Organizations|Qubes OS (Release
      4 Signing Key)|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x1848792F9E2795E9>>>|<pageref|auto-12>>

      <tuple|<tuple|Keys|Organizations|Qubes OS (Release 3 Signing
      Key)|0xCB11CA1D03FA5082>|||<tuple|Keys|Organizations|Qubes OS (Release
      3 Signing Key)|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0xCB11CA1D03FA5082>>>|<pageref|auto-14>>

      <tuple|<tuple|Keys|Organizations|Qubes OS (Release 2 Signing
      Key)|0x0C73B9D40A40E458>|||<tuple|Keys|Organizations|Qubes OS (Release
      2 Signing Key)|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x0C73B9D40A40E458>>>|<pageref|auto-16>>

      <tuple|<tuple|Keys|Organizations|Qubes OS (Release 1 Signing
      Key)|0xEA01201B211093A7>|||<tuple|Keys|Organizations|Qubes OS (Release
      1 Signing Key)|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0xEA01201B211093A7>>>|<pageref|auto-18>>

      <tuple|<tuple|Software|Qubes OS>|strong|c2 qubesos
      idx1|<tuple|Software|Qubes OS>|<pageref|auto-19>>
    </associate>
    <\associate|toc>
      11<space|2spc><with|font-shape|<quote|small-caps>|Qubes OS>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1>

      <with|par-left|<quote|1tab>|11.1<space|2spc>Background
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2>>

      <with|par-left|<quote|1tab>|11.2<space|2spc>History
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-7>>

      <with|par-left|<quote|1tab>|11.3<space|2spc>Public Key Details
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-8>>

      <with|par-left|<quote|2tab>|11.3.1<space|2spc>Qubes Master Signing Key
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|<with|font-family|<quote|tt>|DDFA
      1A3E 3687 9494>>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-9>>

      <with|par-left|<quote|2tab>|11.3.2<space|2spc>Release 4 Signing Key
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|1848
      792F 9E27 95E9>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-11>>

      <with|par-left|<quote|2tab>|11.3.3<space|2spc>Release 3 Signing Key
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|CB11
      CA1D 03FA 5082>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-13>>

      <with|par-left|<quote|2tab>|11.3.4<space|2spc>Release 2 Signing Key
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0C73
      B9D4 0A40 E458>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-15>>

      <with|par-left|<quote|2tab>|11.3.5<space|2spc>Release 1 Signing Key
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|EA01
      201B 2110 93A7>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-17>>
    </associate>
  </collection>
</auxiliary>