<TeXmacs|2.1.1>

<project|../book.tm>

<style|<tuple|book|style-bk>>

<\body>
  <\hide-preamble>
    \;

    \;
  </hide-preamble>

  <new-page*><section|<name|RaspiBlitz>><label|c2 raspiblitz>

  Last updated on 2023-06-15 by <name|Steven Baltakatei Sandoval>.

  <subsection|Background><label|c2 raspiblitz-bg>

  <index-complex|<tuple|Software|Raspiblitz>|strong|c2 raspiblitz
  idx1|<tuple|Software|RaspiBlitz>><name|RaspiBlitz><\footnote>
    Main website: <hlinkv|https://raspiblitz.org/>.
  </footnote> is a software package designed to facilitate operation of a
  <subindex|Software|Lightning Network><name|Lightning Network> and
  <subindex|Software|Bitcoin><name|Bitcoin> node. The software is version
  controlled using <name|git>, with the main git repository available at
  <name|GitHub>.<\footnote>
    See <hlinkv|https://github.com/rootzoll/raspiblitz>.
  </footnote> As of 2023-06-15, the principal maintainer appears to be
  <subindex|People|Rotzoll, Christian \P<verbatim|rootzol>\Q><name|Christian
  \Prootzol\Q Rotzoll><\footnote>
    Their public key <verb-sm|0x1C73060C7C176461> is available at:
    <hlinkv|https://keybase.io/rootzoll>.
  </footnote>.

  <subsection|History><label|c2 raspiblitz-hist>

  <\description>
    <item*|2019-09-03>The creation date of <verbatim|rootzol>'s
    <long-id-spc|1C73 060C 7C17 6461> public key.

    <item*|2019-09-05><name|rootzol> added their public key fingerprint
    <long-id-spc|1C73 060C 7C17 6461> to the FAQ of the <name|RaspiBlitz>
    <name|GitHub> repository.<\footnote>
      See <hlinkv|https://github.com/rootzoll/raspiblitz/commit/75ebdd8d571cccc427b5d023a25c6e2e9e8a2da2>.
    </footnote> They linked their <hlink|<verbatim|keybase.io>|https://keybase.io>
    page as a source of the public key.

    <item*|2020-10-31>The first snapshot of the
    <hlink|<verbatim|raspiblitz.org>|https://raspiblitz.org> website appeared
    on the Internet Archive.<\footnote>
      See <hlinkv|https://web.archive.org/web/20201031223643/https://raspiblitz.org/><hlink||https://web.archive.org/web/20201031223643/https://raspiblitz.org/>.
    </footnote>

    <item*|2021-02-07><subindex|People|Antonopoulos, Andreas>Andreas
    Antonopoulos posted a YouTube video identifying <name|RaspiBlitz> as a
    popular Bitcoin full node software package.<\footnote>
      See <hlinkv|https://www.youtube.com/watch?v=AXUfwvhr3lg&t=26m27s><hlink||https://www.youtube.com/watch?v=AXUfwvhr3lg&t=25m53s>.
    </footnote>

    <item*|2021-05-18><name|rootzol> added their public key fingerprint
    <long-id-spc|1C73 060C 7C17 6461> to the <verbatim|README> of the
    <name|RaspiBlitz> <name|GitHub> repository.
  </description>

  <subsection|Public Key Details><label|c2 raspiblitz-pk>

  <\vgroup>
    <subsubsection|<name|Christian \Prootzol\Q Rotzoll> (<long-id-spc|1C73
    060C 7C17 6461>)>

    <index-complex|<tuple|Keys|People|Rotzoll, Christian
    Rootzol|>|||<tuple|Keys|People|Rotzoll, Christian
    \P<verbatim|rootzol>\Q|<verb-sm|0x1C73060C7C176461>>><name|rootzol>'s PGP
    key<\footnote>
      See <hlinkv|https://reboil.com/res/2021/txt/20210719_0x1C73060C7C176461..raspiblitz_rootzol.asc>
    </footnote> may be downloaded from their Keybase page.<\footnote>
      See <hlinkv|https://keybase.io/rootzoll/pgp_keys.asc>.
    </footnote>. Their fingerprint information is as follows:

    <\vpk>
      pub \ \ rsa4096/0x1C73060C7C176461 2019-09-03 [C]

      \ \ \ \ \ \ Key fingerprint = 92A7 46AE 33A3 C186 D014 \ BF5C 1C73 060C
      7C17 6461

      uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] Christian Rotzoll
      \<less\>bca61f43\<gtr\>

      sub \ \ rsa4096/0xAA9DD1B5CC5647DA 2019-09-03 [S] [expires: 2023-10-28]

      \ \ \ \ \ \ Key fingerprint = C0EE 6145 31A4 16B4 CDB7 \ A2D7 AA9D D1B5
      CC56 47DA

      sub \ \ rsa4096/0xD40D94E6C7C9B4D9 2019-09-03 [E] [expires: 2023-10-28]

      \ \ \ \ \ \ Key fingerprint = A7D4 DA62 95EC 365E 4CCF \ 05FC D40D 94E6
      C7C9 B4D9

      sub \ \ rsa4096/0x1C29DC2F8D764F9A 2019-09-03 [A] [expires: 2023-10-28]

      \ \ \ \ \ \ Key fingerprint = AA80 506C FCAB 3405 84C4 \ 9F0E 1C29 DC2F
      8D76 4F9A
    </vpk>
  </vgroup>

  <index-complex|<tuple|Software|Raspiblitz>|strong|c2 raspiblitz
  idx1|<tuple|Software|RaspiBlitz>>
</body>

<\initial>
  <\collection>
    <associate|chapter-nr|2>
    <associate|page-first|35>
    <associate|page-medium|paper>
    <associate|preamble|false>
    <associate|section-nr|12>
    <associate|subsection-nr|3>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|13|35>>
    <associate|auto-10|<tuple|13.3.1|35>>
    <associate|auto-11|<tuple|<tuple|Keys|People|Rotzoll, Christian
    Rootzol|>|35>>
    <associate|auto-12|<tuple|<tuple|Software|Raspiblitz>|35>>
    <associate|auto-2|<tuple|13.1|35>>
    <associate|auto-3|<tuple|<tuple|Software|Raspiblitz>|35>>
    <associate|auto-4|<tuple|Software|35>>
    <associate|auto-5|<tuple|Software|35>>
    <associate|auto-6|<tuple|People|35>>
    <associate|auto-7|<tuple|13.2|35>>
    <associate|auto-8|<tuple|People|35>>
    <associate|auto-9|<tuple|13.3|35>>
    <associate|c2 raspiblitz|<tuple|13|35>>
    <associate|c2 raspiblitz-bg|<tuple|13.1|35>>
    <associate|c2 raspiblitz-hist|<tuple|13.2|35>>
    <associate|c2 raspiblitz-pk|<tuple|13.3|35>>
    <associate|footnote-13.1|<tuple|13.1|35>>
    <associate|footnote-13.2|<tuple|13.2|35>>
    <associate|footnote-13.3|<tuple|13.3|35>>
    <associate|footnote-13.4|<tuple|13.4|35>>
    <associate|footnote-13.5|<tuple|13.5|35>>
    <associate|footnote-13.6|<tuple|13.6|35>>
    <associate|footnote-13.7|<tuple|13.7|35>>
    <associate|footnote-13.8|<tuple|13.8|35>>
    <associate|footnr-13.1|<tuple|13.1|35>>
    <associate|footnr-13.2|<tuple|13.2|35>>
    <associate|footnr-13.3|<tuple|13.3|35>>
    <associate|footnr-13.4|<tuple|13.4|35>>
    <associate|footnr-13.5|<tuple|13.5|35>>
    <associate|footnr-13.6|<tuple|13.6|35>>
    <associate|footnr-13.7|<tuple|13.7|35>>
    <associate|footnr-13.8|<tuple|13.8|35>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|idx>
      <tuple|<tuple|Software|Raspiblitz>|strong|c2 raspiblitz
      idx1|<tuple|Software|RaspiBlitz>|<pageref|auto-3>>

      <tuple|<tuple|Software|Lightning Network>|<pageref|auto-4>>

      <tuple|<tuple|Software|Bitcoin>|<pageref|auto-5>>

      <tuple|<tuple|People|Rotzoll, Christian
      \P<with|font-family|<quote|tt>|language|<quote|verbatim>|rootzol>\Q>|<pageref|auto-6>>

      <tuple|<tuple|People|Antonopoulos, Andreas>|<pageref|auto-8>>

      <tuple|<tuple|Keys|People|Rotzoll, Christian
      Rootzol|>|||<tuple|Keys|People|Rotzoll, Christian
      \P<with|font-family|<quote|tt>|language|<quote|verbatim>|rootzol>\Q|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x1C73060C7C176461>>>|<pageref|auto-11>>

      <tuple|<tuple|Software|Raspiblitz>|strong|c2 raspiblitz
      idx1|<tuple|Software|RaspiBlitz>|<pageref|auto-12>>
    </associate>
    <\associate|toc>
      13<space|2spc><with|font-shape|<quote|small-caps>|RaspiBlitz>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1>

      <with|par-left|<quote|1tab>|13.1<space|2spc>Background
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2>>

      <with|par-left|<quote|1tab>|13.2<space|2spc>History
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-7>>

      <with|par-left|<quote|1tab>|13.3<space|2spc>Public Key Details
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-9>>

      <with|par-left|<quote|2tab>|13.3.1<space|2spc><with|font-shape|<quote|small-caps>|Christian
      \Prootzol\Q Rotzoll> (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|1C73
      060C 7C17 6461>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-10>>
    </associate>
  </collection>
</auxiliary>