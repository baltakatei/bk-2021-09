<TeXmacs|2.1.1>

<project|../book.tm>

<style|<tuple|book|style-bk>>

<\body>
  <\hide-preamble>
    \;

    \;
  </hide-preamble>

  <new-page*><section|<name|GitHub>><label|c2 github>

  Last updated on 2023-06-15 by <name|Steven Baltakatei Sandoval>.

  <subsection|Background><label|c2 github-bg>

  <index-complex|<tuple|Software|GitHub>|strong|c2 github
  idx1|<tuple|Software|GitHub>><name|GitHub><\footnote>
    Main website: <hlinkv|https://github.com/>.
  </footnote> is a commercial <name|Git> repository hosting service company
  founded in 2008. It was purchased by <index|Microsoft><name|Microsoft> in
  2016.<cite|nyt_20180604_ms-buys-github>

  <subsection|History><label|c2 github-hist>

  <\description-compact>
    <item*|2008><name|GitHub> founded in San
    Francisco.<cite|nyt_20180604_ms-buys-github>

    <item*|2008-03-10><name|GitHub> parent company <index|Logical Awesome,
    LLC><name|Logical Awesome, LLC> registered in San Francisco by
    <subindex|People|Wanstrath, Chris>Chris Wanstrath.<\footnote>
      See <hlinkv|https://businesssearch.sos.ca.gov/Document/RetrievePDF?Id=200807010145-721605>
      and <hlinkv|https://businesssearch.sos.ca.gov/Document/RetrievePDF?Id=200807010145-2544282>from
      <hlinkv|https://opencorporates.com/companies/us_ca/200807010145>.
    </footnote>

    <item*|2008-05-14>First snapshot of the <hlinkv|https://github.com>
    website on the <name|Internet Archive>.<\footnote>
      See <hlinkv|https://web.archive.org/web/20080514210148/http://github.com/>.
    </footnote>

    <item*|2017-08-16>Creation date of the <long-id-spc|4AEE 18F8 3AFD EB23>
    public key according to itself.

    <item*|2017-11-14>Date of <name|Internet Archive> snapshot containing an
    early link to <hlinkv|https://github.com/web-flow.gpg> from a page on the
    <hlink|<verbatim|help.github.com>|https://help.github.com>
    domain.<\footnote>
      See <hlinkv|https://web.archive.org/web/20171114055613/https://help.github.com/articles/about-gpg/>.
    </footnote> Also the date of a post by <name|GitHub> user
    <subindex|People|Cross, Jonathan><verbatim|jonathancross><\footnote>
      Key fingerprint <long-id-spc|C0C0 7613 2FFA 7695>. Key at
      <hlinkv|https://github.com/jonathancross.gpg>.
    </footnote> observing that the <long-id-spc|4AEE 18F8 3AFD EB23> key
    appears to be a new feature<\footnote>
      <hlinkv|https://github.com/keepassxreboot/keepassxc/issues/1183#issuecomment-344386172>.
    </footnote>:

    <\quote-env>
      Yeah, just experimented and saw the same thing. Strange new \Pfeature\Q
      of GitHub it seems.
    </quote-env>

    <item*|2018-06-04>First snapshot of the <long-id-spc|4AEE 18F8 3AFD EB23>
    public key <hlinkv|https://github.com/web-flow.gpg> on the <name|Internet
    Archive>.<\footnote>
      <hlinkv|https://web.archive.org/web/20180604123146/https://github.com/web-flow.gpg>.
    </footnote>

    <item*|2021-05-25>Public key <long-id-spc|4AEE 18F8 3AFD EB23>
    fingerprint explicitly published at <name|GitHub> documentation
    website.<\footnote>
      See See <hlinkv|https://github.com/github/docs/commit/c4e1cb7a97704f0d90c0d6ed7e52d72b1e4946c1>.
    </footnote>
  </description-compact>

  <subsection|Public Key Details><label|c2 github-pk>

  <subsubsection|Web-flow commit signing (<long-id-spc|4AEE 18F8 3AFD EB23>)>

  <index-complex|<tuple|Keys|Organizations|Github|0x4AEE18F83AFDEB23>|||<tuple|Keys|Organizations|GitHub|<verb-sm|0x4AEE18F83AFDEB23>>>As
  of 2023-06-15, when a user logs into <hlink|<verbatim|github.com>|https://github.com>
  and creates a <name|git> commit through a web browser, <name|GitHub> will
  automatically sign the commit against a <name|gpg> key<\footnote>
    See <hlinkv|https://reboil.com/res/2021/txt/20210719_4AEE18F83AFDEB23..github.asc>
    or <hlinkv|><hlinkv|https://github.com/web-flow.gpg>.
  </footnote> with the fingerprint:

  <\vpk>
    pub \ \ rsa2048/0x4AEE18F83AFDEB23 2017-08-16 [SC]

    \ \ \ \ \ \ Key fingerprint = 5DE3 E050 9C47 EA3C F04A \ 42D3 4AEE 18F8
    3AFD EB23

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] GitHub (web-flow
    commit signing) \<less\>3ca48f8d\<gtr\>
  </vpk>

  This key is available for download at <name|GitHub>'s documentation website
  at <hlinkv|https://github.com/web-flow.gpg>.<\footnote>
    See <hlinkv|https://docs.github.com/en/github/authenticating-to-github/managing-commit-signature-verification/about-commit-signature-verification>.
  </footnote> This particular link as well as the full key fingerprint was
  added to the <name|GitHub> documentation repository in a commit dated
  2021-05-25<\footnote>
    See <hlinkv|https://github.com/github/docs/commit/c4e1cb7a97704f0d90c0d6ed7e52d72b1e4946c1>.
  </footnote>.

  <index-complex|<tuple|Software|GitHub>|strong|c2 github
  idx1|<tuple|Software|GitHub>>

  \;
</body>

<\initial>
  <\collection>
    <associate|chapter-nr|2>
    <associate|font|Linux Libertine>
    <associate|page-first|28>
    <associate|page-medium|paper>
    <associate|preamble|false>
    <associate|section-nr|7>
    <associate|subsection-nr|3>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|8|28>>
    <associate|auto-10|<tuple|8.3.1|28>>
    <associate|auto-11|<tuple|<tuple|Keys|Organizations|Github|0x4AEE18F83AFDEB23>|28>>
    <associate|auto-12|<tuple|<tuple|Software|GitHub>|28>>
    <associate|auto-2|<tuple|8.1|28>>
    <associate|auto-3|<tuple|<tuple|Software|GitHub>|28>>
    <associate|auto-4|<tuple|Microsoft|28>>
    <associate|auto-5|<tuple|8.2|28>>
    <associate|auto-6|<tuple|Logical Awesome, LLC|28>>
    <associate|auto-7|<tuple|People|28>>
    <associate|auto-8|<tuple|People|28>>
    <associate|auto-9|<tuple|8.3|28>>
    <associate|c2 github|<tuple|8|28>>
    <associate|c2 github-bg|<tuple|8.1|28>>
    <associate|c2 github-hist|<tuple|8.2|28>>
    <associate|c2 github-pk|<tuple|8.3|28>>
    <associate|footnote-8.1|<tuple|8.1|28>>
    <associate|footnote-8.10|<tuple|8.10|28>>
    <associate|footnote-8.11|<tuple|8.11|28>>
    <associate|footnote-8.2|<tuple|8.2|28>>
    <associate|footnote-8.3|<tuple|8.3|28>>
    <associate|footnote-8.4|<tuple|8.4|28>>
    <associate|footnote-8.5|<tuple|8.5|28>>
    <associate|footnote-8.6|<tuple|8.6|28>>
    <associate|footnote-8.7|<tuple|8.7|28>>
    <associate|footnote-8.8|<tuple|8.8|28>>
    <associate|footnote-8.9|<tuple|8.9|28>>
    <associate|footnr-8.1|<tuple|8.1|28>>
    <associate|footnr-8.10|<tuple|8.10|28>>
    <associate|footnr-8.11|<tuple|8.11|28>>
    <associate|footnr-8.2|<tuple|8.2|28>>
    <associate|footnr-8.3|<tuple|8.3|28>>
    <associate|footnr-8.4|<tuple|8.4|28>>
    <associate|footnr-8.5|<tuple|8.5|28>>
    <associate|footnr-8.6|<tuple|8.6|28>>
    <associate|footnr-8.7|<tuple|8.7|28>>
    <associate|footnr-8.8|<tuple|8.8|28>>
    <associate|footnr-8.9|<tuple|8.9|28>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|bib>
      nyt_20180604_ms-buys-github

      nyt_20180604_ms-buys-github
    </associate>
    <\associate|idx>
      <tuple|<tuple|Software|GitHub>|strong|c2 github
      idx1|<tuple|Software|GitHub>|<pageref|auto-3>>

      <tuple|<tuple|Microsoft>|<pageref|auto-4>>

      <tuple|<tuple|Logical Awesome, LLC>|<pageref|auto-6>>

      <tuple|<tuple|People|Wanstrath, Chris>|<pageref|auto-7>>

      <tuple|<tuple|People|Cross, Jonathan>|<pageref|auto-8>>

      <tuple|<tuple|Keys|Organizations|Github|0x4AEE18F83AFDEB23>|||<tuple|Keys|Organizations|GitHub|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x4AEE18F83AFDEB23>>>|<pageref|auto-11>>

      <tuple|<tuple|Software|GitHub>|strong|c2 github
      idx1|<tuple|Software|GitHub>|<pageref|auto-12>>
    </associate>
    <\associate|toc>
      8<space|2spc><with|font-shape|<quote|small-caps>|GitHub>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1>

      <with|par-left|<quote|1tab>|8.1<space|2spc>Background
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2>>

      <with|par-left|<quote|1tab>|8.2<space|2spc>History
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-5>>

      <with|par-left|<quote|1tab>|8.3<space|2spc>Public Key Details
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-9>>

      <with|par-left|<quote|2tab>|8.3.1<space|2spc>Web-flow commit signing
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|4AEE
      18F8 3AFD EB23>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-10>>
    </associate>
  </collection>
</auxiliary>