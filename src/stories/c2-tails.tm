<TeXmacs|2.1.1>

<project|../book.tm>

<style|<tuple|book|style-bk>>

<\body>
  <new-page*><section|<name|Tails>><label|c2 tails>

  Last updated on 2023-06-15 by <name|Steven Baltakatei Sandoval>.

  <subsection|Background><label|c2 tails-bg>

  <index-complex|<tuple|Software|Tails>|strong|c2 tails
  idx1|<tuple|Software|Tails>><name|Tails<\footnote>
    Main website: <hlinkv|https://tails.boum.org/>.
  </footnote>> is a <subindex|Software|Debian><name|Debian>-based
  <name|Gnu/Linux> operating system designed to preserve user anonymity
  through default use of <subindex|Software|Tor><name|Tor> for all network
  traffic.

  <name|Tails> is an acronym for <name|<strong|T>he (<strong|A>mnesic)
  <strong|I>ncognito <strong|L>ive <strong|S>ystem>. The name reflects the
  fact that the project is the result of a 2010 merger between two
  <name|Tor>-related projects known as <subindex|Software|Amnesia><name|Amnesia><\footnote>
    See <hlinkv|https://web.archive.org/web/20100716170307/http://amnesia.boum.org/>.
  </footnote> and <subindex|Software|Incognito><name|Incognito><\footnote>
    See \PIncognito and The Tor Project sign a licensing agreement\Q.
    Website: <hlinkv|blog.torproject.org>. Date: <hgroup|2008-06-27>. Archive
    URL: <hlinkv|https://web.archive.org/web/20081120073057/http://blog.torproject.org/blog/incognito-and-tor-project-sign-licensing-agreement>.
    Archive date: <hgroup|2008-11-20>.
  </footnote>. In addition to differing sets of developers, the two projects
  were based on two different <name|GNU/Linux> operating systems:
  <name|Amnesia> used <name|Debian> and <name|Incognito> used
  <subindex|Software|Gentoo><name|Gentoo>.

  As of 2023-06-15, the latest version of <name|Tails> is
  <em|5.14>.<\footnote>
    See <hlinkv|https://tails.boum.org/news/version_5.14/index.en.html>.
  </footnote>

  <subsection|History><label|c2 tails-hist>

  <\description-compact>
    <item*|2009-06-20>Date of first commit in <name|Amnesia>
    repository.<\footnote>
      See <hlinkv|https://gitlab.tails.boum.org/tails/tails/-/blob/345a927fbd6aa18a2bcd13331cbc2e22ef2e0639/config/chroot_local-includes/usr/share/doc/amnesia/Changelog>.
    </footnote>

    <item*|2009-08-14>Creation date of the mailing list key <long-id-spc|1D29
    75ED F93E 735F>.

    <item*|2010-04-07><name|Incognito><\footnote>
      See <hlinkv|https://web.archive.org/web/20100108093152/http://anonymityanywhere.com/>.
    </footnote> and <name|Amnesia> merge.<\footnote>
      See: anonym. \PIncognito + Amnesia = The (Amnesic) Incognito Live
      System\Q. Date: <hgroup|2010-04-07>. Archive date: <hgroup|2010-07-28>.
      Archive URL: <hlinkv|https://web.archive.org/web/20100728224716/http://www.anonymityanywhere.com:80/incognito>.
    </footnote> Renamed \P<name|The (Amnesic) Incognito Live
    System>\Q.<\footnote>
      See \Pnew project name\Q. Website: <hlinkv|amnesia.boum.org>. Date:
      <hgroup|2010-04-07>. Archive date: <hgroup|2010-08-17>. Archive URL:
      <hlinkv|https://web.archive.org/web/20100817180857/http://amnesia.boum.org/news/new_project_name/>.
    </footnote>

    <item*|2010-07-16>First <name|I.A.> snapshot of
    <hlinkv|http://amnesia.boum.org>.<\footnote>
      See <hlinkv|https://web.archive.org/web/20100716170307/http://amnesia.boum.org/>.
    </footnote>

    <item*|2010-10-07>Creation date of the signing key <long-id-spc|1202 821C
    BE2C D9C1>.

    <item*|2011-03-17>First <name|I.A.> snapshot of
    <hlinkv|https://tails.boum.org>.<\footnote>
      See <hlinkv|https://web.archive.org/web/20110317013911/https://tails.boum.org/>.
    </footnote>

    <item*|2015-01-18>Creation date of the signing key <long-id-spc|DBB8 02B2
    58AC D84F>.

    <item*|2015-03-16>Signing key <long-id-spc|1202 821C BE2C D9C1>
    officially retired and replaced by <long-id-spc|DBB8 02B2 58AC
    D84F>.<\footnote>
      See <hlinkv|https://web.archive.org/web/20150316172733/https://tails.boum.org/news/signing_key_transition/index.en.html>.
    </footnote>
  </description-compact>

  <subsection|Public Key Details><label|c2 tails-pk>

  As of 2023, several public keys are associated with the <name|Tails>
  project.<\footnote>
    See \POpenPGP keys\Q. Date accessed: 2022-03-12.
    <hlinkv|https://tails.boum.org/doc/about/openpgp_keys/index.en.html>.
  </footnote> This section describes only the current signing key (See
  <reference|c2 tails-pk-sign-2015>), mailing list encryption key (See
  <reference|c2 tails-pk-mailinglist-2009>), and the 2010\U2015 signing key
  (See <reference|c2 tails-pk-sign-2010>).

  <subsubsection|Signing key (2015\U<space|1em>) (<long-id-spc|DBB8 02B2 58AC
  D84F>)><label|c2 tails-pk-sign-2015>

  <index-complex|<tuple|Keys|Organizations|Tails|Signing|0xDBB802B258ACD84F
  (2015\U )>|||<tuple|Keys|Organizations|Tails|Signing|<verb-sm|0xDBB802B258ACD84F>
  (2015\U )>>Key used by the <name|Tails> developers to sign released
  <verbatim|.iso> installation images since <name|Tails> version <em|v1.3.1>
  which was released in 2015.<\footnote>
    See \PTails 1.3.1 is out\Q. Website: <hlinkv|tails.boum.org>. Date:
    2015-03-23. Archive URL: <hlinkv|https://web.archive.org/web/20150402101752/https://tails.boum.org/news/version_1.3.1/index.en.html>.
    Archive date: 2015-04-02.
  </footnote> The public key may be downloaded here<\footnote>
    See <hlinkv|https://tails.boum.org/tails-signing.key>.
  </footnote>.

  <\vpk>
    pub \ \ rsa4096/0xDBB802B258ACD84F 2015-01-18 [C] [expires: 2024-01-04]

    \ \ \ \ \ \ Key fingerprint = A490 D0F4 D311 A415 3E2B \ B7CA DBB8 02B2
    58AC D84F

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] Tails developers
    \<less\>16f58847\<gtr\>

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] Tails developers
    (offline long-term identity key) \<less\>16f58847\<gtr\>

    sub \ \ rsa4096/0xD21DAD38AF281C0B 2017-08-28 [S] [expires: 2024-01-04]

    \ \ \ \ \ \ Key fingerprint = 0546 9FB8 5EAD 6589 B43D \ 41D3 D21D AD38
    AF28 1C0B

    sub \ \ ed25519/0x90B2B4BD7AED235F 2017-08-28 [S] [expires: 2024-01-04]

    \ \ \ \ \ \ Key fingerprint = CD4D 4351 AFA6 933F 574A \ 9AFB 90B2 B4BD
    7AED 235F

    sub \ \ rsa4096/0x7BFBD2B902EE13D0 2021-10-14 [S] [expires: 2024-01-04]

    \ \ \ \ \ \ Key fingerprint = 753F 9013 77A3 09F2 731F \ A33F 7BFB D2B9
    02EE 13D0
  </vpk>

  <subsubsection|Mailing list key (2009\U<space|1em>) (<long-id-spc|1D29 75ED
  F93E 735F>)><label|c2 tails-pk-mailinglist-2009>

  <index-complex|<tuple|Keys|Organizations|Tails|Mailing
  list|0x1D2975EDF93E735F (2009\U )>|||<tuple|Keys|Organizations|Tails|Mailing
  list|<verb-sm|0x1D2975EDF93E735F> (2009\U )>>Key recommended by the
  <name|Tails> developers to be used to encrypt emails sent to their
  encrypted mailing list since at least 2011<\footnote>
    See <hlinkv|https://web.archive.org/web/20110318070814/http://tails.boum.org/GnuPG_key/index.en.html>.
  </footnote>, possibly 2009<\footnote>
    See <hlinkv|https://gitlab.tails.boum.org/tails/tails/-/blob/195b39cbf409fa7a8763cc6a6c5f91386db6735b/debian/changelog>.
  </footnote>, assuming the main repository timetamps are trustworthy. Until
  <name|Tails> <em|v0.5> and <em|v0.6~rc3>, released images were signed using
  this key.<\footnote>
    See <hlinkv|https://web.archive.org/web/20111205083704/https://tails.boum.org/doc/about/openpgp_keys/index.en.html>.
  </footnote> The public key may be downloaded here<\footnote>
    See <hlinkv|https://tails.boum.org/tails-email.key>.
  </footnote>.

  <\vpk>
    pub \ \ rsa4096/0x1D2975EDF93E735F 2009-08-14 [SC] [expires: 2025-01-02]

    \ \ \ \ \ \ Key fingerprint = 09F6 BC8F EEC9 D8EE 005D \ BAA4 1D29 75ED
    F93E 735F

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] Tails developers
    (Schleuder mailing-list) \<less\>16f58847\<gtr\>

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] Tails list (schleuder
    list) \<less\>a6d8622b\<gtr\>

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] Tails list (schleuder
    list) \<less\>87eb09d2\<gtr\>

    sub \ \ rsa4096/0xD843C2F5E89382EB 2009-08-14 [E] [expires: 2025-01-02]

    \ \ \ \ \ \ Key fingerprint = C394 8FE7 B604 C611 4E29 \ 4DDF D843 C2F5
    E893 82EB
  </vpk>

  <subsubsection|Signing key (2010\U2015) (<long-id-spc|1202 821C BE2C
  D9C1>)><label|c2 tails-pk-sign-2010>

  <index-complex|<tuple|Keys|Organizations|Tails|Signing|0x1202821CBE2CD9C1
  (2010\U2015)>|||<tuple|Keys|Organizations|Tails|Signing|<verb-sm|0x1202821CBE2CD9C1>
  (2010\U2015)>>Key used by the <name|Tails> developers to sign released
  images starting with <name|Tails> <em|v0.6><\footnote>
    See <hlinkv|https://web.archive.org/web/20111205083704/https://tails.boum.org/doc/about/openpgp_keys/index.en.html>.
  </footnote> until and including <em|v1.3>. The public key may be downloaded
  here<\footnote>
    See <hlinkv|https://web.archive.org/web/20141006010041/https://tails.boum.org/tails-signing.key>.
  </footnote>. This key was retired from use in 2015 and replaced with key
  <long-id-spc|DBB8 02B2 58AC D84F>.<\footnote>
    See \PTransition to a new OpenPGP signing key\Q. Website:
    <hlinkv|tails.boum.org>. Date: 2015-03-16. Archive URL:
    <hlinkv|https://web.archive.org/web/20150316172733/https://tails.boum.org/news/signing_key_transition/index.en.html>.
    Archive date: 2015-03-16.
  </footnote>

  <\vpk>
    pub \ \ rsa4096/0x1202821CBE2CD9C1 2010-10-07 [SC] [expired: 2015-04-30]

    \ \ \ \ \ \ Key fingerprint = 0D24 B36A A9A2 A651 7878 \ 7645 1202 821C
    BE2C D9C1

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ expired] Tails developers
    (signing key) \<less\>16f58847\<gtr\>
  </vpk>

  \;

  <index-complex|<tuple|Software|Tails>|strong|c2 tails
  idx1|<tuple|Software|Tails>>
</body>

<\initial>
  <\collection>
    <associate|chapter-nr|2>
    <associate|page-first|39>
    <associate|page-medium|papyrus>
    <associate|section-nr|14>
    <associate|subsection-nr|3>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|15|9>>
    <associate|auto-10|<tuple|15.3|9>>
    <associate|auto-11|<tuple|15.3.1|9>>
    <associate|auto-12|<tuple|<tuple|Keys|Organizations|Tails|Signing|0xDBB802B258ACD84F
    (2015\U )>|9>>
    <associate|auto-13|<tuple|15.3.2|10>>
    <associate|auto-14|<tuple|<tuple|Keys|Organizations|Tails|Mailing
    list|0x1D2975EDF93E735F (2009\U )>|?>>
    <associate|auto-15|<tuple|15.3.3|?>>
    <associate|auto-16|<tuple|<tuple|Keys|Organizations|Tails|Signing|0x1202821CBE2CD9C1
    (2010\U2015)>|?>>
    <associate|auto-17|<tuple|<tuple|Software|Tails>|?>>
    <associate|auto-2|<tuple|15.1|9>>
    <associate|auto-3|<tuple|<tuple|Software|Tails>|9>>
    <associate|auto-4|<tuple|Software|9>>
    <associate|auto-5|<tuple|Software|9>>
    <associate|auto-6|<tuple|Software|9>>
    <associate|auto-7|<tuple|Software|9>>
    <associate|auto-8|<tuple|Software|9>>
    <associate|auto-9|<tuple|15.2|9>>
    <associate|c2 tails|<tuple|15|9>>
    <associate|c2 tails-bg|<tuple|15.1|9>>
    <associate|c2 tails-hist|<tuple|15.2|9>>
    <associate|c2 tails-pk|<tuple|15.3|9>>
    <associate|c2 tails-pk-mailinglist-2009|<tuple|15.3.2|10>>
    <associate|c2 tails-pk-sign-2010|<tuple|15.3.3|10>>
    <associate|c2 tails-pk-sign-2015|<tuple|15.3.1|9>>
    <associate|footnote-1|<tuple|1|?>>
    <associate|footnote-15.1|<tuple|15.1|?>>
    <associate|footnote-15.10|<tuple|15.10|?>>
    <associate|footnote-15.11|<tuple|15.11|?>>
    <associate|footnote-15.12|<tuple|15.12|?>>
    <associate|footnote-15.13|<tuple|15.13|?>>
    <associate|footnote-15.14|<tuple|15.14|?>>
    <associate|footnote-15.15|<tuple|15.15|?>>
    <associate|footnote-15.16|<tuple|15.16|?>>
    <associate|footnote-15.17|<tuple|15.17|?>>
    <associate|footnote-15.18|<tuple|15.18|?>>
    <associate|footnote-15.19|<tuple|15.19|?>>
    <associate|footnote-15.2|<tuple|15.2|?>>
    <associate|footnote-15.20|<tuple|15.20|?>>
    <associate|footnote-15.21|<tuple|15.21|?>>
    <associate|footnote-15.3|<tuple|15.3|?>>
    <associate|footnote-15.4|<tuple|15.4|?>>
    <associate|footnote-15.5|<tuple|15.5|?>>
    <associate|footnote-15.6|<tuple|15.6|?>>
    <associate|footnote-15.7|<tuple|15.7|?>>
    <associate|footnote-15.8|<tuple|15.8|?>>
    <associate|footnote-15.9|<tuple|15.9|?>>
    <associate|footnote-2|<tuple|2|?>>
    <associate|footnote-3|<tuple|3|?>>
    <associate|footnr-15.1|<tuple|15.1|?>>
    <associate|footnr-15.10|<tuple|15.10|?>>
    <associate|footnr-15.11|<tuple|15.11|?>>
    <associate|footnr-15.12|<tuple|15.12|?>>
    <associate|footnr-15.13|<tuple|15.13|?>>
    <associate|footnr-15.14|<tuple|15.14|?>>
    <associate|footnr-15.15|<tuple|15.15|?>>
    <associate|footnr-15.16|<tuple|15.16|?>>
    <associate|footnr-15.17|<tuple|15.17|?>>
    <associate|footnr-15.18|<tuple|15.18|?>>
    <associate|footnr-15.19|<tuple|15.19|?>>
    <associate|footnr-15.2|<tuple|15.2|?>>
    <associate|footnr-15.20|<tuple|15.20|?>>
    <associate|footnr-15.21|<tuple|15.21|?>>
    <associate|footnr-15.3|<tuple|15.3|?>>
    <associate|footnr-15.4|<tuple|15.4|?>>
    <associate|footnr-15.5|<tuple|15.5|?>>
    <associate|footnr-15.6|<tuple|15.6|?>>
    <associate|footnr-15.7|<tuple|15.7|?>>
    <associate|footnr-15.8|<tuple|15.8|?>>
    <associate|footnr-15.9|<tuple|15.9|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|idx>
      <tuple|<tuple|Software|Tails>|strong|c2 tails
      idx1|<tuple|Software|Tails>|<pageref|auto-3>>

      <tuple|<tuple|Software|Debian>|<pageref|auto-4>>

      <tuple|<tuple|Software|Tor>|<pageref|auto-5>>

      <tuple|<tuple|Software|Amnesia>|<pageref|auto-6>>

      <tuple|<tuple|Software|Incognito>|<pageref|auto-7>>

      <tuple|<tuple|Software|Gentoo>|<pageref|auto-8>>

      <tuple|<tuple|Keys|Organizations|Tails|Signing|0xDBB802B258ACD84F
      (2015\U )>|||<tuple|Keys|Organizations|Tails|Signing|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0xDBB802B258ACD84F>>
      (2015\U )>|<pageref|auto-12>>

      <tuple|<tuple|Keys|Organizations|Tails|Signing|0x1202821CBE2CD9C1
      (2010\U2015)>|||<tuple|Keys|Organizations|Tails|Signing|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x1202821CBE2CD9C1>>
      (2010\U2015)>|<pageref|auto-14>>

      <tuple|<tuple|Keys|Organizations|Tails|Mailing list|0x1D2975EDF93E735F
      (2009\U )>|||<tuple|Keys|Organizations|Tails|Mailing
      list|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x1D2975EDF93E735F>>
      (2009\U )>|<pageref|auto-16>>

      <tuple|<tuple|Software|Tails>|strong|c2 tails
      idx1|<tuple|Software|Tails>|<pageref|auto-17>>
    </associate>
    <\associate|toc>
      15<space|2spc><with|font-shape|<quote|small-caps>|Tails>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1>

      <with|par-left|<quote|1tab>|15.1<space|2spc>Background
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2>>

      <with|par-left|<quote|1tab>|15.2<space|2spc>History
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-9>>

      <with|par-left|<quote|1tab>|15.3<space|2spc>Public Key Details
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-10>>

      <with|par-left|<quote|2tab>|15.3.1<space|2spc>Signing key
      (2015\U<space|1em>) (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|DBB8
      02B2 58AC D84F>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-11>>

      <with|par-left|<quote|2tab>|15.3.2<space|2spc>Signing key (2010\U2015)
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|1202
      821C BE2C D9C1>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-13>>

      <with|par-left|<quote|2tab>|15.3.3<space|2spc>Mailing list key
      (2009\U<space|1em>) (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|1D29
      75ED F93E 735F>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-15>>
    </associate>
  </collection>
</auxiliary>