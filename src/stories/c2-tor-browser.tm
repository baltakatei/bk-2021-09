<TeXmacs|2.1.1>

<project|../book.tm>

<style|<tuple|book|style-bk>>

<\body>
  <new-page*><section|<name|Tor Browser>><label|c2 tor-browser>

  Last updated 2023-06-15 by <person|Steven Baltakatei Sandoval>.

  <subsection|Background><label|c2 tor-browser-bg>

  <index-complex|<tuple|Software|Tor Browser>|strong|c2 tor-browser
  idx1|<tuple|Software|Tor Browser>><subindex|Software|Tor Browser><name|Tor
  Browser><\footnote>
    Main website: <hlinkv|https://www.torproject.org>.
  </footnote> is a browser software package that permits visiting websites
  with anonymity effected by onion routing. Although various<\footnote>
    See <hlinkv|https://web.archive.org/web/20210713130216/https://2019.www.torproject.org/docs/signing-keys.html.en>.
  </footnote> PGP keys have been used to sign various releases and archives,
  the <long-id-spc|4E2C 6E87 9329 8290> key has been used for the main
  <name|Tor Browser> installer since at least 2015.

  <subsection|History><label|c2 tor-browser-hist>

  <\description>
    <item*|2008-01-30><subindex|People|Murdoch, Steven J.><person|Steven J.
    Murdoch> announces development of <name|Tor Browser>.<\footnote>
      See <hlinkv|https://lists.torproject.org/pipermail/tor-talk/2008-January/007837.html>.
    </footnote>

    <item*|2014-12-15>Creation date of the <long-id-spc|4E2C 6E87 9329 8290>
    binary signing key.

    <item*|2019-06-29>Copies of the main release signing key
    <long-id-spc|4E2C 6E87 9329 8290> maintained by various keyservers
    suffered a certificate spamming attack.<\footnote>
      See <hlinkv|https://nvd.nist.gov/vuln/detail/CVE-2019-13050>.
    </footnote> Other high-profile <name|PGP> keys were also affected at this
    time.<\footnote>
      See <hlinkv|https://gist.github.com/rjhansen/67ab921ffb4084c865b3618d6955275f#gistcomment-2959168>.
    </footnote>
  </description>

  <subsection|Public Key Details><label|c2 tor-browser-pk>

  <subsubsection|Release Signing Key (2015\U ) (<long-id-spc|4E2C 6E87 9329
  8290>)>

  <index-complex|<tuple|Keys|Organizations|Tor
  Browser|Signing|0x4E2C6E8793298290 (2015\U
  )>|||<tuple|Keys|Organizations|Tor Browser|Signing|<verb-sm|0x4E2C6E8793298290>
  (2015\U )>>Public key used for signing <name|Tor Browser> releases since at
  least 2015-03-15<\footnote>
    See <hlinkv|https://web.archive.org/web/20150315013830/https://www.torproject.org/docs/verifying-signatures.html.en>.
  </footnote> until 2023-05-24<\footnote>
    See <hlinkv|https://web.archive.org/web/20230524172829/https://support.torproject.org/tbb/how-to-verify-signature/>.
  </footnote>.

  <\vpk>
    pub \ \ rsa4096/0x4E2C6E8793298290 2014-12-15 [C] [expires: 2025-07-21]

    \ \ \ \ \ \ Key fingerprint = EF6E 286D DA85 EA2A 4BA7 \ DE68 4E2C 6E87
    9329 8290

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] Tor Browser Developers
    (signing key) \<less\>85e84cd9\<gtr\>

    sub \ \ rsa4096/0xE53D989A9E2D47BF 2021-09-17 [S] [expires: 2023-09-17]

    \ \ \ \ \ \ Key fingerprint = 6131 88FC 5BE2 176E 3ED5 \ 4901 E53D 989A
    9E2D 47BF
  </vpk>

  <index-complex|<tuple|Software|Tor Browser>|strong|c2 tor-browser
  idx1|<tuple|Software|Tor Browser>>
</body>

<\initial>
  <\collection>
    <associate|chapter-nr|2>
    <associate|page-first|41>
    <associate|page-medium|paper>
    <associate|section-nr|15>
    <associate|subsection-nr|3>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|16|41>>
    <associate|auto-10|<tuple|<tuple|Software|Tor Browser>|41>>
    <associate|auto-2|<tuple|16.1|41>>
    <associate|auto-3|<tuple|<tuple|Software|Tor Browser>|41>>
    <associate|auto-4|<tuple|Software|41>>
    <associate|auto-5|<tuple|16.2|41>>
    <associate|auto-6|<tuple|People|41>>
    <associate|auto-7|<tuple|16.3|41>>
    <associate|auto-8|<tuple|16.3.1|41>>
    <associate|auto-9|<tuple|<tuple|Keys|Organizations|Tor
    Browser|Signing|0x4E2C6E8793298290 (2015\U )>|41>>
    <associate|c2 tor-browser|<tuple|16|41>>
    <associate|c2 tor-browser-bg|<tuple|16.1|41>>
    <associate|c2 tor-browser-hist|<tuple|16.2|41>>
    <associate|c2 tor-browser-pk|<tuple|16.3|41>>
    <associate|footnote-16.1|<tuple|16.1|41>>
    <associate|footnote-16.2|<tuple|16.2|41>>
    <associate|footnote-16.3|<tuple|16.3|41>>
    <associate|footnote-16.4|<tuple|16.4|41>>
    <associate|footnote-16.5|<tuple|16.5|41>>
    <associate|footnote-16.6|<tuple|16.6|41>>
    <associate|footnote-16.7|<tuple|16.7|41>>
    <associate|footnr-16.1|<tuple|16.1|41>>
    <associate|footnr-16.2|<tuple|16.2|41>>
    <associate|footnr-16.3|<tuple|16.3|41>>
    <associate|footnr-16.4|<tuple|16.4|41>>
    <associate|footnr-16.5|<tuple|16.5|41>>
    <associate|footnr-16.6|<tuple|16.6|41>>
    <associate|footnr-16.7|<tuple|16.7|41>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|idx>
      <tuple|<tuple|Software|Tor Browser>|strong|c2 tor-browser
      idx1|<tuple|Software|Tor Browser>|<pageref|auto-3>>

      <tuple|<tuple|Software|Tor Browser>|<pageref|auto-4>>

      <tuple|<tuple|People|Murdoch, Steven J.>|<pageref|auto-6>>

      <tuple|<tuple|Keys|Organizations|Tor Browser|Signing|0x4E2C6E8793298290
      (2015\U )>|||<tuple|Keys|Organizations|Tor
      Browser|Signing|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x4E2C6E8793298290>>
      (2015\U )>|<pageref|auto-9>>

      <tuple|<tuple|Software|Tor Browser>|strong|c2 tor-browser
      idx1|<tuple|Software|Tor Browser>|<pageref|auto-10>>
    </associate>
    <\associate|toc>
      16<space|2spc><with|font-shape|<quote|small-caps>|Tor Browser>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1>

      <with|par-left|<quote|1tab>|16.1<space|2spc>Background
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2>>

      <with|par-left|<quote|1tab>|16.2<space|2spc>History
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-5>>

      <with|par-left|<quote|1tab>|16.3<space|2spc>Public Key Details
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-7>>

      <with|par-left|<quote|2tab>|16.3.1<space|2spc>Release Signing Key
      (2015\U ) (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|4E2C
      6E87 9329 8290>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-8>>
    </associate>
  </collection>
</auxiliary>