<TeXmacs|2.1.1>

<project|../book.tm>

<style|<tuple|book|style-bk>>

<\body>
  <new-page*><section|<name|KeePassXC>><label|c2 keepassxc>

  Last updated on 2023-06-12 by <name|Steven Baltakatei Sandoval>.

  <subsection|Background><label|c2 keepassxc-bg>

  <index-complex|<tuple|Software|KeePassXC>|strong|c2 keepassxc
  idx1|<tuple|Software|KeePassXC>><name|KeePassXC<\footnote>
    Main website: <hlinkv|https://example.com>.
  </footnote>> is a cross-platform password manager maintained by the
  <subindex|Organizations|KeePassXC Team><name|KeePassXC Team>
  (<long-id-spc|105D 8D57 BB97 46BD>)<\footnote>
    See <hlinkv|https://keepassxc.org/team/>.
  </footnote>. The software is a fork of <name|KeePassX><\footnote>
    See <hlinkv|http://keepassx.org/>.
  </footnote> which is a fork of <name|KeePass><\footnote>
    See <hlinkv|https://keepass.info/>.
  </footnote>.

  As of 2023-06-12, the latest version is <em|2.7.5>.

  <subsection|History><label|c2 keepassxc-hist>

  <\description>
    <item*|2017-02-02>First snapshot of <hlinkv|https://keepassxc.org> on
    <acronym|IA>.<\footnote>
      See <hlinkv|https://web.archive.org/web/20170202034551/https://keepassxc.org/>.
    </footnote>
  </description>

  <subsection|Public Key Details><label|c2 keepassxc-pk>

  <subsubsection|Signing key (2017\U ) (<long-id-spc|CFB4 C216 6397 D0D2>)>

  <index-complex|<tuple|Keys|Organizations|KeePassXC|0xCFB4C2166397D0D2>|||<tuple|Keys|Organizations|<name|KeePassXC>|<verb-sm|0xCFB4C2166397D0D2>>>Key
  used by the <name|KeePassXC Team> to sign releases of <name|KeePassXC>
  since 2017<\footnote>
    See <hlinkv|https://web.archive.org/web/20170302132738/https://keepassxc.org/verifying-signatures>.
  </footnote>. A copy of the public key is available on the main
  website.<\footnote>
    See <hlinkv|https://keepassxc.org/keepassxc_master_signing_key.asc>.
  </footnote>

  <\vpk>
    pub \ \ rsa4096/0xCFB4C2166397D0D2 2017-01-03 [SC]

    \ \ \ \ \ \ Key fingerprint = BF5A 669F 2272 CF43 24C1 \ FDA8 CFB4 C216
    6397 D0D2

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] KeePassXC Release
    \<less\>27f4ea96\<gtr\>

    sub \ \ rsa2048/0xD8538E98A26FD9C4 2017-01-03 [S] [expires: 2024-12-04]

    \ \ \ \ \ \ Key fingerprint = 71D4 673D 73C7 F83C 17DA \ E6A2 D853 8E98
    A26F D9C4

    sub \ \ rsa2048/0xB7A66F03B59076A8 2017-01-03 [S] [expires: 2024-12-04]

    \ \ \ \ \ \ Key fingerprint = C1E4 CBA3 AD78 D3AF D894 \ F9E0 B7A6 6F03
    B590 76A8
  </vpk>

  <subsubsection|Developer key (2021) (<long-id-spc|CFB4 C216 6397 D0D2>)>

  <index-complex|<tuple|Keys|Organizations|KeePassXC|0x105D8D57BB9746BD>|||<tuple|Keys|Organizations|<name|KeePassXC>|<verb-sm|0x105D8D57BB9746BD>>>Key
  posted by the <name|KeePassXC Team> to authenticate correspondance since at
  least 2018.<\footnote>
    See <hlinkv|https://web.archive.org/web/20180624105451/https://keepassxc.org/team/>.
  </footnote>

  <\vpk>
    pub \ \ rsa4096/0x105D8D57BB9746BD 2017-11-14 [SCA] [expires: 2028-03-19]

    \ \ \ \ \ \ Key fingerprint = AA2A 7E1F F743 9CE6 2FB8 \ CA9C 105D 8D57
    BB97 46BD

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] KeePassXC Team
    \<less\>ad0196bf\<gtr\>

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] KeePassXC Abuse
    \<less\>3ff0432c\<gtr\>

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] KeePassXC Legal Issues
    \<less\>db562627\<gtr\>

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] KeePassXC Postmaster
    \<less\>3a37f8c9\<gtr\>

    uid \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ [ unknown] KeePassXC Security
    \<less\>a0c67402\<gtr\>

    sub \ \ rsa4096/0xEB7DB7042675F803 2017-11-14 [E] [expires: 2028-03-19]

    \ \ \ \ \ \ Key fingerprint = 6C6D 4645 D758 C9F3 3955 \ 627C EB7D B704
    2675 F803
  </vpk>

  <index-complex|<tuple|Software|KeePassXC>|strong|c2 keepassxc
  idx1|<tuple|Software|KeePassXC>>
</body>

<\initial>
  <\collection>
    <associate|chapter-nr|2>
    <associate|page-first|32>
    <associate|page-medium|paper>
    <associate|section-nr|9>
    <associate|subsection-nr|3>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|10|32>>
    <associate|auto-10|<tuple|<tuple|Keys|Organizations|KeePassXC|0x105D8D57BB9746BD>|32>>
    <associate|auto-11|<tuple|<tuple|Software|KeePassXC>|32>>
    <associate|auto-2|<tuple|10.1|32>>
    <associate|auto-3|<tuple|<tuple|Software|KeePassXC>|32>>
    <associate|auto-4|<tuple|Organizations|32>>
    <associate|auto-5|<tuple|10.2|32>>
    <associate|auto-6|<tuple|10.3|32>>
    <associate|auto-7|<tuple|10.3.1|32>>
    <associate|auto-8|<tuple|<tuple|Keys|Organizations|KeePassXC|0xCFB4C2166397D0D2>|32>>
    <associate|auto-9|<tuple|10.3.2|32>>
    <associate|c2 keepassxc|<tuple|10|32>>
    <associate|c2 keepassxc-bg|<tuple|10.1|32>>
    <associate|c2 keepassxc-hist|<tuple|10.2|32>>
    <associate|c2 keepassxc-pk|<tuple|10.3|32>>
    <associate|footnote-10.1|<tuple|10.1|32>>
    <associate|footnote-10.2|<tuple|10.2|32>>
    <associate|footnote-10.3|<tuple|10.3|32>>
    <associate|footnote-10.4|<tuple|10.4|32>>
    <associate|footnote-10.5|<tuple|10.5|32>>
    <associate|footnote-10.6|<tuple|10.6|32>>
    <associate|footnote-10.7|<tuple|10.7|32>>
    <associate|footnote-10.8|<tuple|10.8|32>>
    <associate|footnr-10.1|<tuple|10.1|32>>
    <associate|footnr-10.2|<tuple|10.2|32>>
    <associate|footnr-10.3|<tuple|10.3|32>>
    <associate|footnr-10.4|<tuple|10.4|32>>
    <associate|footnr-10.5|<tuple|10.5|32>>
    <associate|footnr-10.6|<tuple|10.6|32>>
    <associate|footnr-10.7|<tuple|10.7|32>>
    <associate|footnr-10.8|<tuple|10.8|32>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|idx>
      <tuple|<tuple|Software|KeePassXC>|strong|c2 keepassxc
      idx1|<tuple|Software|KeePassXC>|<pageref|auto-3>>

      <tuple|<tuple|Organizations|KeePassXC Team>|<pageref|auto-4>>

      <tuple|<tuple|Keys|Organizations|KeePassXC|0xCFB4C2166397D0D2>|||<tuple|Keys|Organizations|<with|font-shape|<quote|small-caps>|KeePassXC>|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0xCFB4C2166397D0D2>>>|<pageref|auto-8>>

      <tuple|<tuple|Keys|Organizations|KeePassXC|0x105D8D57BB9746BD>|||<tuple|Keys|Organizations|<with|font-shape|<quote|small-caps>|KeePassXC>|<with|font-base-size|<quote|8>|<with|font-family|<quote|tt>|language|<quote|verbatim>|0x105D8D57BB9746BD>>>|<pageref|auto-10>>

      <tuple|<tuple|Software|KeePassXC>|strong|c2 keepassxc
      idx1|<tuple|Software|KeePassXC>|<pageref|auto-11>>
    </associate>
    <\associate|toc>
      10<space|2spc><with|font-shape|<quote|small-caps>|KeePassXC>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1>

      <with|par-left|<quote|1tab>|10.1<space|2spc>Background
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2>>

      <with|par-left|<quote|1tab>|10.2<space|2spc>History
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-5>>

      <with|par-left|<quote|1tab>|10.3<space|2spc>Public Key Details
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-6>>

      <with|par-left|<quote|2tab>|10.3.1<space|2spc>Signing key (2017\U )
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|CFB4
      C216 6397 D0D2>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-7>>

      <with|par-left|<quote|2tab>|10.3.2<space|2spc>Developer key (2021)
      (<with|hmagnified-factor|<quote|0.75>|color|<quote|brown>|<hgroup|<with|font-effects|<quote|hmagnify=0.75>|<with|font-family|<quote|tt>|language|<quote|verbatim>|CFB4
      C216 6397 D0D2>>>>) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-9>>
    </associate>
  </collection>
</auxiliary>