<TeXmacs|2.1>

<style|<tuple|book|style-bk>>

<\body>
  <new-page*><section|<name|Umbrel>><label|c2 umbrel>

  <subsection|Background><label|c2 umbrel-bg>

  <name|Umbrel> is a software suite designed to facilitate operation of a
  <name|Lightning Network> node.

  <subsection|History>

  <\itemize>
    <item>On 2020-07-26, the first snapshot of the
    <hlink|<verbatim|getumbrel.com>|https://getumbrel.com/> website appeared
    on the Internet Archive.<\footnote>
      See <hlink|<verbatim|https://web.archive.org/web/20200726114748/https://getumbrel.com/>|https://web.archive.org/web/20200726114748/https://getumbrel.com/>
    </footnote>

    <item>On 2021-02-07, Andreas Antonopoulos<index-complex|<tuple|antonopoulos,
    andreas|lightning network|umbrel>|||<tuple|Antonopoulos,
    Andreas|Lightning Network|Umbrel>> posted a YouTube video identifying
    Umbrel as a popular Bitcoin full node software package.<\footnote>
      See <hlink|<verbatim|https://www.youtube.com/watch?v=AXUfwvhr3lg&t=25m53s>|https://www.youtube.com/watch?v=AXUfwvhr3lg&t=25m53s>
    </footnote> He highlighted Umbrel's blockchain explorer feature which
    permits a user to trace the provenance of bitcoins thorugh history.
  </itemize>

  \;

  \;

  <subsection|Public Key Details><label|c2 umbrel-pk>

  \;
</body>

<\initial>
  <\collection>
    <associate|chapter-nr|1>
    <associate|page-first|9>
    <associate|page-medium|paper>
    <associate|section-nr|0>
    <associate|subsection-nr|0>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|9>>
    <associate|auto-2|<tuple|1.1|9>>
    <associate|auto-3|<tuple|1.2|9>>
    <associate|auto-4|<tuple|<tuple|antonopoulos, andreas|lightning
    network|umbrel>|9>>
    <associate|auto-5|<tuple|1.3|9>>
    <associate|c2 umbrel|<tuple|1|9>>
    <associate|c2 umbrel-bg|<tuple|1.1|9>>
    <associate|c2 umbrel-pk|<tuple|1.3|9>>
    <associate|footnote-1.1|<tuple|1.1|9>>
    <associate|footnote-1.2|<tuple|1.2|9>>
    <associate|footnr-1.1|<tuple|1.1|9>>
    <associate|footnr-1.2|<tuple|1.2|9>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|idx>
      <tuple|<tuple|antonopoulos, andreas|lightning
      network|umbrel>|||<tuple|Antonopoulos, Andreas|Lightning
      Network|Umbrel>|<pageref|auto-4>>
    </associate>
    <\associate|toc>
      1<space|2spc><with|font-shape|<quote|small-caps>|Umbrel>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1>

      <with|par-left|<quote|1tab>|1.1<space|2spc>Background
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2>>

      <with|par-left|<quote|1tab>|1.2<space|2spc>History
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-3>>

      <with|par-left|<quote|1tab>|1.3<space|2spc>Public Key Details
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-5>>
    </associate>
  </collection>
</auxiliary>