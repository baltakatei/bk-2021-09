<TeXmacs|2.1.1>

<project|book.tm>

<style|<tuple|book|style-bk>>

<\body>
  <section*|Front Matter>

  This book is copyright \<copyright\>2023 by <name|Steven Baltakatei
  Sandoval>. This book is licensed under Creative Commons
  Attribution-ShareAlike 4.0 International License (CC BY-SA 4.0):

  <\padded-center>
    \ <hlink|<verbatim|https://creativecommons.org/licenses/by-sa/4.0/>|https://creativecommons.org/licenses/by-sa/4.0/>
  </padded-center>

  The book is available as source code in a
  <hlink|<verbatim|git>|https://git-scm.com/> repository available at:

  <\padded-center>
    \ <hlink|<verbatim|https://gitlab.com/baltakatei/npk.git>|https://gitlab.com/baltakatei/npk.git>
  </padded-center>

  This book was typeset using <strong|<TeXmacs>> version
  <verbatim|<TeXmacs-version>>.

  Fonts used include <strong|Linux Libertine>.

  This book was rendered on <verbatim|<date|%Y-%m-%dT%H:%M:%S%z>>.
</body>

<\initial>
  <\collection>
    <associate|chapter-nr|0>
    <associate|page-first|3>
    <associate|page-medium|paper>
    <associate|preamble|false>
    <associate|section-nr|0>
    <associate|subsection-nr|0>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|?|3>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|toc>
      Front Matter <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1>
    </associate>
  </collection>
</auxiliary>