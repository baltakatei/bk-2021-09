<TeXmacs|2.1>

<project|book.tm>

<style|<tuple|book|style-bk>>

<\body>
  <appendix|How Public Key Cryptography Works><label|a pubkey-how>

  This appendix describes in more detail how public key cryptography works.

  \;

  \;
</body>

<\initial>
  <\collection>
    <associate|chapter-nr|0>
    <associate|page-first|5>
    <associate|page-medium|paper>
    <associate|section-nr|0>
    <associate|subsection-nr|0>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|a pubkey-how|<tuple|A|5|c1.tm>>
    <associate|auto-1|<tuple|A|5|c1.tm>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|Appendix
      A<space|2spc>How Public Key Cryptography Works>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>
    </associate>
  </collection>
</auxiliary>