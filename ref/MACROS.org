* TeXmacs Macro definitions
** ~bktable3~
Desc: A simple table with horizontal borders highlighting the header
row and top and bottom of table.

** ~verbatim-8pt~
Desc: A modified ~<verbatim>~ tag that specifies a custom font size of
~8pt~. Intended for use with ~a4~ sized pages so at least 80 monospace
text characters don't bleed off of a page.

** ~vpk~
Desc: Short for "verbatim public key". Is basically the ~verbatim-8pt~
macro but with extra vertical space before and after the block of
monospace text. Intended to prominently display commandline output
containing information about a public key (e.g. ~gpg --list-keys~
output).

** ~hlinkv~
Desc: A modified version of an ~<hlink>~ tag. Accepts one argument
which should be a url. Macro displays url in monospace and also turns
the monospace url text into a hyperlink.

** ~verb-sm~
Desc: A ~<verbatim>~ tag with font reduced to ~8pt~. Intended to
contain public key fingerprint strings for in-line use in paragraphs
(since often fingerprints can be long).
